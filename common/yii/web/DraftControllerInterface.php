<?php

namespace common\models;

use Yii;
use common\yii\db\db;

Interface DraftControllerInterface
{
	/**
	 * create data stock agreement
	 * redirect ke halaman draft dengan data stock agreement yang baru dibuat
	 * @return response
	 */
	public function actionCreate();

	/**
	 * halaman draft
	 * @param  string $id stock agreement ID
	 * @return view
	 */
	public function actionDraft($id = null);

	/**
	 * delete master
	 * @param  string $id master ID
	 * @return response
	 */
	public function actionSave($id);
}
