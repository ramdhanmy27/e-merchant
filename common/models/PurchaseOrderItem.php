<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "purchase_order_item".
 *
 * @property integer $id_purchase_order_item
 * @property integer $qty
 * @property string $number_po
 * @property string $number_product
 * @property string $code_size
 * @property string $number_product_color
 *
 * @property Product $product
 * @property ProductColor $productColor
 * @property ProductSize $productSize
 * @property PurchaseOrder $purchaseOrder
 */
class PurchaseOrderItem extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase_order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            // [['id_purchase_order_item'], 'required'],

            // unique
            // [['id_purchase_order_item'], 'unique'],

            // safe

            // integer
            [['id_purchase_order_item', 'qty'], 'integer'],

            // string
            [['code_size'], 'string', 'max' => 10],
            [['number_po'], 'string', 'max' => 32],
            [['code_merchant', 'number_product'], 'string', 'max' => 10],
            [['number_product_color'], 'string', 'max' => 20],

            // exist
            [['number_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['number_product' => 'number_product']],
            [['number_product_color'], 'exist', 'skipOnError' => true, 'targetClass' => ProductColor::className(), 'targetAttribute' => ['number_product_color' => 'number_product_color']],
            [['code_size'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSize::className(), 'targetAttribute' => ['code_size' => 'code_size']],
            [['number_po'], 'exist', 'skipOnError' => true, 'targetClass' => PurchaseOrder::className(), 'targetAttribute' => ['number_po' => 'number_po']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_purchase_order_item' => 'Id Purchase Order Item',
            'qty' => 'Qty',
            'number_po' => 'Purchase Order Number',
            'number_product' => 'Id Product',
            'code_size' => 'Id Product Size',
            'number_product_color' => 'Id Product Color',
            'product' => 'Product',
            'productSize.name' => 'Product Size',
            'productColor.color' => 'Product Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductColor()
    {
        return $this->hasOne(ProductColor::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSize()
    {
        return $this->hasOne(ProductSize::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrder()
    {
        return $this->hasOne(PurchaseOrder::className(), ['number_po' => 'number_po']);
    }
}
