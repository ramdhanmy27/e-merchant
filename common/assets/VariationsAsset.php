<?php namespace common\assets;

class VariationsAsset extends \yii\web\AssetBundle
{
    public $baseUrl = '@web';

    public $js = [
        "js/variations.js",
    ];
 
    public $css = [
        "css/variations.css",
    ];

	public $depends = [
		"common\assets\AngularAsset"
	];
}
