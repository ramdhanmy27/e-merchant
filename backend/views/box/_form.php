<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
register(["@js/box/box.js"], [
	"depends" => [
		"backend\assets\AceAsset"
	]
]);

?>
<div class="category-form">

	<?php $form = ActiveForm::begin(); ?>
	
	<div class='row'>
		<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
			<!-- Category Information -->
			<div class='widget-box'>
				<div class="widget-header">
					<h5 class="widget-title">Box Information</h5>

					<div class="widget-toolbar">
						<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
					</div>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						<?= $form->field($model, 'number_rack')->widget(Select2::classname(), [
							'data' => \common\models\Rack::option("number_rack"),
							'options' => ['placeholder' => 'Select quick group'],
							'pluginOptions' => [
								'allowClear' => true
							],
						]) ?>
						<?= $form->field($model, 'number_box')->textInput(['maxlength' => true, "readonly" => "true"]) ?>
						<?= $form->field($model, 'rack_level')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'capacity')->textInput(['maxlength' => true, "value" => "20"]) ?>
					</div>
				</div>
			</div>
            
		</div>
	</div>
	
	<div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
	<hr>
</div>