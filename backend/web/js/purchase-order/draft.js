app.controller("purchaseOrderController", function($scope, $http, $func, Variations) {
	// stock agreement ID
	$scope.id = "";
	
	/*Contract Information*/
	$scope.contract = {
		"merchant": "",
		"date": "",
		"warehouse": "",
		"description": "",
	};

	var timeout;
	var setInformation = function() {
		if (timeout !== undefined)
			clearTimeout(timeout);

		timeout = setTimeout(function() {
			// update contract information
			$func.post(url.to("purchase-order/update-contract", {id: $scope.id}), $scope.contract)
				.error(function() { fn.alertError() });
		}, 1000);
	};

	$scope.$watch(function() {
		$("#purchaseorder-code_merchant").off("select2:select").on("select2:select", function() {
			$scope.contract.merchant = $(this).val();
			setInformation();
		});
		$("#purchaseorder-code_warehouse").off("select2:select").on("select2:select", function() {
			$scope.contract.warehouse = $(this).val();
			setInformation();
		});
		$("#purchaseorder-date").off("apply.daterangepicker").on("apply.daterangepicker", function() {
			$scope.contract.date = $(this).val();
			setInformation();
		});
		$("#purchaseorder-description").keyup(function() {
			$scope.contract.description = $(this).val();
			setInformation();
		});
	});

	/*Variations*/
	$scope.product = [];

	// product index
	$scope.index = "";

	var init = {
		product: $.extend(true, {}, Variations.init, {
			id: "",
			price: 0,
		}),
	};

	var initRow = function(index) {
		$(".product-details select[data-krajee-select2]:eq("+ index +")").off("select2:select").on("select2:select", function(e) {
			var $this = $(this);
			var value = $this.val();

			var resetProduct = function() {
				$this.val($scope.product[index].id).trigger("change");
			};

			// product already exists
			if ($.grep($scope.product, function(val, i) {return val.id==value}).length > 0) {
				fn.alert("Product is already exists");
				resetProduct();
				return;
			}
			else {
				delete_item(index, function() {
					$http.get(url.to("purchase-order/get-product-data", {id: value}))
						.success(function(data) {
							$scope.product[index] = $.extend(true, {}, init.product, data);
						})
						.error(function() {
							$scope.product[index].id = value;
							fn.alertError();
						})
				}, resetProduct);
			}
		});
	}

	var delete_item = function(index, yes, no) {
		if ($scope.f.isProductSelected(index)) {
			fn.confirm({
				body: "It will erase your data quantity. Are you sure ?",
				yes: function() {
					$func.post(url.to("purchase-order/delete-item", {id: $scope.id}), {product: $scope.product[index].id})
						.success(yes)
						.error(function() { fn.alertError() });
				},
				no: no
			})
		}
		else yes();
	}

	$scope.f = {
		init: function(id) {
			$scope.id = id;

			$http.get(url.to("purchase-order/get-draft-data", {id: id})).success(function(data) {
				if (fn.isEmpty(data))
					$scope.f.add();
				else {
					$scope.product = $.map(data, function(value, index) {
						return [$.extend(true, {}, init.product, value)];
					});

					$scope.$watch(function() {
						for (var index in $scope.product) {
							// add event to each row
							initRow(index);
						}
					});
				}
			})
		},
		add: function() {
			$scope.product.push($.extend(true, {}, init.product));
			$scope.$watch(function() {
				initRow($scope.product.length-1);
			});
		},
		delete: function(index) {
			delete_item(index, function() {
				$scope.product.splice(index, 1);
			});
		},
		priceTotal: function(index) {
			var qty = $scope.f.getQtyTotal(index);

			return qty * fn.int($scope.product[index].price);
		},
		isProductSelected: function(index) {
			return !fn.isEmpty($scope.product[index].id);
		},
		getQtyTotal: function(index) {
			var data = $scope.product[index].quantity;
			var total = 0;
			
			for (var row in data) {
				for (var value in data[row])
					total += fn.int(data[row][value]);
			}
					
			return total;
		},
		loadVariation: function(index) {
			$scope.$$childHead.variations = $scope.product[index];
			$scope.index = index;
		},
		formValidation: function(e) {
			for (var i in $scope.product) {
				// prevent submit & show variations modal
				if (fn.isset($scope.product[i], "error", "quantity") && !fn.isEmpty($scope.product[i].error.quantity)) {
					e.stopPropagation();
					e.preventDefault();
					$scope.f.loadVariation(i);
					$("#modal-variations").modal("show")

					return false;
				}
			}

			return true;
		},
	}

	$("#modal-variations").on("hidden.bs.modal", function() {
		$func.post(url.to("purchase-order/save-qty", {id: $scope.id}), {
			id: $scope.product[$scope.index].id,
			qty: $scope.product[$scope.index].quantity,
		})
		.error(function() { fn.alertError() });
	})
});