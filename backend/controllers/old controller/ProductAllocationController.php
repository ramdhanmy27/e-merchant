<?php

namespace backend\controllers;

use Yii;
use common\models\ProductAllocation;
use common\models\ProductAllocationSearch;
use common\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\yii\db\db;
use yii\data\Pagination;

/**
 * ProductAllocationController implements the CRUD actions for ProductAllocation model.
 */
class ProductAllocationController extends Controller
{
	public static $permissions = [];
	
	public function behaviors()
	{
		return [];
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			"access" => $this->access([
				[['index', 'view'], "view"],
				[['update'], "update"],
				[['delete'], "delete"],
			]),
		];
	}

	/**
	 * Lists all ProductAllocation models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		// collecting product items
		$qproduct = db::query("product p", "p.id, pi.id as itemid, pi.sku, pi.color, pi.size, p.name, pi.quantity, pi.base_price")
						->innerjoin("product_item pi", "pi.id_product=p.id");

		return $this->render('index', [
			'dataProvider' => new \yii\data\ActiveDataProvider([
				"query" => $qproduct,
			]),
		]);
	}

	/**
	 * get allocation data 
	 * @return json
	 * 
	 * POST
	 * 	@param  array pid product item id
	 */
	public function actionGetAllocation() {
		$qalloc = db::query("product_item pi", [
						"pi.id as pid", "c.id", "c.name", "coalesce(pa.quantity, 0) as quantity",
						"(case when pa.price is null then pi.base_price else pa.price end) as price", 
						"pa.sale_price", "pa.sale_start_date", "pa.sale_end_date",
					])
					->innerJoin("channel c", "true")
					->leftJoin("product_allocation pa", "c.id=pa.id_channel and pa.id_product_item=pi.id")
					->where(["in", "pi.id", ($pid=$this->request->post("pid")) ? $pid : []]);
		$allocation = [];

		foreach ($qalloc->each() as $channel) {
			if ($channel['sale_start_date'] !== null)
				$channel['sale_start_date']	= date("Y-m-d", $channel['sale_start_date']);

			if ($channel['sale_end_date'] !== null)
				$channel['sale_end_date']	= date("Y-m-d", $channel['sale_end_date']);

			$allocation[$channel['pid']][$channel["id"]] = $channel;
		}

		return json([
			"allocation" => $allocation, 
			"channel" => db::query("channel", "id, name")->all(),
		]);
	}

	/**
	 * save new allocation data
	 * @return json
	 * 
	 * POST
	 * 	@param  array pid product item id
	 */
	public function actionAlloc() {
		foreach ($this->request->post("data") as $pid => $alloc) {
			foreach ($alloc as $cid => $item) {
				$data[] = [
					$pid,	// id_product_item
					$cid,	// id_channel
					$item['quantity'],	// quantity
					$item['price'],	// price
					empty($item['sale_price']) ? null : $item['sale_price'],	// sale_price
					empty($item['sale_start_date']) ? null : strtotime($item['sale_start_date']),	// sale_start_date
					empty($item['sale_end_date']) ? null : strtotime($item['sale_end_date']),	// sale_end_date
					time(),	// created_at
					time(),	// updated_at
				];
			}
		}

		$updated = db::batchUpdate("product_allocation pa", [
						["id_product_item", 'int'], ["id_channel", 'int'], ["quantity", 'int'], ["price", 'int'], ["sale_price", 'int'], 
						["sale_start_date", 'int'], ["sale_end_date", 'int'], ["created_at", 'int'], ["updated_at", 'int'],
					], $data
					, "where pa.id_product_item=t.id_product_item::int and pa.id_channel=t.id_channel::int 
					returning t.id_product_item||'-'||t.id_channel"
				)->queryColumn();

		// remove updated data
		foreach ($updated as $key)
			unset($data[$key]);

		db::batchInsert("product_allocation", [
			"id_product_item", "id_channel", "quantity", "price", "sale_price", 
			"sale_start_date", "sale_end_date", "created_at", "updated_at",
		], $data)->execute();
	}
}
