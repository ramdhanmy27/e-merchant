<?php
return [
    'adminEmail' => 'irmailtestero@gmail.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'permissions' => [
    	'enable' => true,
    ],

	// 0 - show error page
	// 1 - set flash alert
    "error" => 1,
    "format" => [
        "date" => [
            "php" => "d M Y",
            "pgsql" => "dd mon YYYY",
        ],
    	"datetime" => [
    		"php" => "H:i - d M Y",
    		"pgsql" => "HH24:MI - dd mon YYYY",
    	],
    ],
    "error-msg" => [
        403 => "You are forbidden from accessing this page.",
        404 => "Page not found.",
    ]
];
