<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "storing_instruction".
 *
 * @property string $number_instruction
 * @property string $storing_time
 *
 * @property StoringItem[] $storingItems
 */
class StoringInstruction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storing_instruction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number_instruction'], 'required'],
            [['storing_time'], 'safe'],
            [['number_instruction'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number_instruction' => 'Number Instruction',
            'storing_time' => 'Storing Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoringItems()
    {
        return $this->hasMany(StoringItem::className(), ['number_instruction' => 'number_instruction']);
    }
}
