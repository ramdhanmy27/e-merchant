app.controller("rackController", function($scope, $http, $func) {
	/*initialize data*/
	$scope.rack = $func.init("rack");

	if ($scope.rack==undefined || $scope.rack.length==0) {
		$scope.rack = {
			error: {},
			data: [["","","",""],["","","",""],["","","",""],["","","",""],]
		};
	}

	$scope.f = {
		total: function() {
			var total = 0;

			for (var y in $scope.rack.data) {
				for (var x in $scope.rack.data[y]) {
					if (typeof $scope.rack.data[y][x]=="string" && $scope.rack.data[y][x].trim()!="")
						total++;
				}
			}

			return total;
		},
		addCol: function() {
			for (var i in $scope.rack.data) {
				$scope.rack.data[i].push("");
			}
		},
		addRow: function() {
			var rowIndex = $scope.f.getRowTotal();

			$scope.rack.data.push([]);

			for (var i=$scope.f.getColTotal()-1; i>=0; i--) {
				$scope.rack.data[rowIndex].push("")
			};
		},
		rmCol: function(index) {
			fn.confirm({
				yes: function() {
					$scope.$apply(function() {
						for (var i in $scope.rack.data) {
							$scope.rack.data[i].splice(index, 1);
						}
					})
				}
			});
		},
		rmRow: function(index) {
			fn.confirm({
				yes: function() {
					$scope.$apply(function() {
						$scope.rack.data.splice(index, 1);
					})
				}
			});
		},
		getRowTotal: function() {
			return $scope.rack.data.length;
		},
		getColTotal: function() {
			return $scope.rack.data[0].length;
		},
		validate: function(posSize, posAttr) {
			var isDuplicate = function(arr, index) {
				var coll = angular.copy(arr);
				coll.splice(index, 1);

				if (typeof arr[index]=="string" && arr[index].trim()!="")
					return coll.indexOf(arr[index]) !== -1;
			}

			// attribute
			if (posAttr!==undefined && posSize===undefined) {
				var index = posAttr;
				var type = "attribute";
			}
			// size
			else if (posSize!==undefined && posAttr===undefined) {
				var index = posSize;
				var type = "size";
			}

			// check if the error was valid after changes
			for (var i in $scope.rack.error[type]) {
				if ($scope.rack[type][index]!=$scope.rack[type][i] && !isDuplicate($scope.rack[type], i))
					delete $scope.rack.error[type][i];
			}

			// find duplicates
			var error = isDuplicate($scope.rack[type], index);

			if (error) 
				$scope.rack.error[type][index] = "Terdapat duplikasi untuk "+ type +" '"+ $scope.rack[type][index] +"'";
			else
				delete $scope.rack.error[type][index];
		}
	};
});