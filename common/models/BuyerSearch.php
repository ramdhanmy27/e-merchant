<?php namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Buyer;

/**
 * BuyerSearch represents the model behind the search form about `common\models\Buyer`.
 */
class BuyerSearch extends Buyer
{
    public function rules()
    {
        return [
            [[
                'code_buyer',
                'name',
                'delivery_address',
                'delivery_city',
                'post_code',
                'phone',
                'email',
            ], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param mixed $status
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Buyer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'code_buyer' => $this->code_buyer,
            'name' => $this->name,
            'delivery_address' => $this->delivery_address,
            'delivery_city' => $this->delivery_city,
            'post_code' => $this->post_code,
            'phone' => $this->phone,
            'email' => $this->email,
        ]);

        $query->andFilterWhere(['ilike', 'code_buyer', $this->code_buyer])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'delivery_address', $this->delivery_address])
            ->andFilterWhere(['ilike', 'delivery_city', $this->delivery_city])
            ->andFilterWhere(['ilike', 'post_code', $this->post_code])
            ->andFilterWhere(['ilike', 'phone', $this->phone])
            ->andFilterWhere(['ilike', 'email', $this->email]);

        return $dataProvider;
    }
}
