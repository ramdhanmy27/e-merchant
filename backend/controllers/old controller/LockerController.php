<?php

namespace backend\controllers;

use Yii;
use common\models\Locker;
use common\models\LockerSearch;
use common\models\Warehouse;
use common\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LockerController implements the CRUD actions for Locker model.
 */
class LockerController extends Controller
{
    public static $permissions = ["create", "update", "view"];

    public function behaviors()
    {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
            ]),
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['index', 'view', 'create'], "create"],
                [['index', 'view', 'update'], "update"],
            ]),
        ];
    }

    /**
     * Lists all Locker models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LockerSearch();
        $dataProvider = $searchModel->search(null,Yii::$app->request->queryParams);
        $model = new Locker();
        $warehouse = Warehouse::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->session->setFlash("success","Successfully add locker !");
            return $this->redirect(['index']);
        } else {
            return $this->render('index', [
                'model' => $model,
                'warehouse' => $warehouse,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Locker model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Locker model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Locker();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Locker model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nama_lama = $model->locker;
        $warehouse = Warehouse::find()->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->session->setFlash("success", "Successfully update locker $nama_lama");
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'warehouse' => $warehouse,
            ]);
        }
    }

    /**
     * Deletes an existing Locker model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionDeletebaru($id)
    // {
    //     $this->findModel($id)->delete();
    //     $this->session->setFlash("success", "Successfully delete locker");
    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Locker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Locker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Locker::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
