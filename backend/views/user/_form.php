<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
            <!-- Basic Info -->
            <div class='widget-box'>
                <div class="widget-header">
                    <h5 class="widget-title">User Information</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
                        
                        <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

                        <?php if($model->isNewRecord) { ?>
                        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'repassword')->passwordInput(['maxlength' => 255]) ?>
                        <?php } ?>

                        <div class="form-group field-roles-assignments">
                            <label class="control-label col-sm-4">Roles</label>
                            <div class="col-sm-8">

                                <?php foreach ($roles as $name => $role) :
                                    if(isset($assignments['merchant'])){
                                        $disabled =
                                            $name == 'merchant' ?
                                            true:true;
                                    } else {
                                        $disabled =
                                            $name == 'merchant' ?
                                            true:false;
                                    }
                                ?>

                                <p> <?= Html::checkbox(
                                    "assignments[]",
                                    isset($assignments[$name]),
                                    [
                                        "value" => $name,
                                        "disabled" => $disabled,
                                    ]
                                ) ?>
                                <?= $name ?>
                                </p>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-12" ?> text-right'>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <hr>
</div>