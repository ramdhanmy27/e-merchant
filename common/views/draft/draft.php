<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use yii\helpers\BaseInflector;

$controller = $this->controller->id;
$class = BaseInflector::camelize($controller);

register(["@js/$controller/draft.js"], [
    "depends" => [
        "common\assets\AngularAsset",
        "backend\assets\AceAsset",
    ]
]);
?>

<div class="<?= $controller ?>-draft" ng-controller="<?= lcfirst($class) ?>Controller">
    <div class="<?= $controller ?>-form" ng-init="f.init('<?= $id ?>')">
        <?php $form = ActiveForm::begin(["options" => ["onkeypress" => "return event.keyCode != 13;"]]); ?>

        <?= $this->render("@app/views/{$controller}/draft", [
            "form" => $form,
            "model" => $model,
        ]) ?>

        <?php ActiveForm::end(); ?>

        <hr>
        <div class="text-right">
            <?php if (access("delete")): ?>
                <?= Html::a("<i class='glyphicon glyphicon-remove'></i> Cancel", ["delete", "id" => $id], [
                    "class" => "btn btn-danger",
                    "data" => [
                        "method" => "POST",
                        "confirm" => "This action will remove your draft. Are you sure ?",
                    ],
                ]); ?>
            <?php endif; ?>

            <?= Html::a("<i class='glyphicon glyphicon-ok'></i> Save", ["save", "id" => $id], [
                'class' => 'btn btn-success',
                "ng-click" => 'f.formValidation($event)',
                "data" => [
                    "method" => "POST",
                    "confirm" => "You cannot edit this draft afterward. Are you sure ?",
                ],
            ]) ?>
        </div>
    </div>
</div>