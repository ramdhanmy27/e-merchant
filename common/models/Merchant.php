<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "merchant".
 *
 * @property string $code_merchant
 * @property string $name
 * @property string $address
 * @property string $contact_name
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property string $description
 * @property string $bank
 * @property string $bank_acc_no
 * @property string $bank_acc_name
 *
 * @property Product[] $products
 * @property PurchaseOrder[] $purchaseOrders
 * @property StockAgreement[] $stockAgreements
 */
class Merchant extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['code_merchant'], 'required'],

            // unique
            [['code_merchant'], 'unique'],

            // safe

            // integer

            // string
            [['code_merchant'], 'string', 'max' => 10],
            [['bank'], 'string', 'max' => 12],
            [['phone'], 'string', 'max' => 13],
            [['bank_acc_no'], 'string', 'max' => 18],
            [['name', 'contact_name', 'website'], 'string', 'max' => 20],
            [['email'], "string", "max" => 255],
            [['bank_acc_name'], 'string', 'max' => 24],
            [['address'], 'string', 'max' => 30],
            [['description'], 'string', 'max' => 64],
            [['email'], 'string', 'max' => 255],

            // exist
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_merchant' => 'Merchant Code',
            'name' => 'Name',
            'address' => 'Address',
            'contact_name' => 'Contact Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'website' => 'Website',
            'description' => 'Description',
            'bank' => 'Bank',
            'bank_acc_no' => 'Bank Acc No',
            'bank_acc_name' => 'Bank Acc Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['code_merchant' => 'code_merchant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::className(), ['code_merchant' => 'code_merchant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockAgreements()
    {
        return $this->hasMany(StockAgreement::className(), ['code_merchant' => 'code_merchant']);
    }
}
