<?php namespace backend\assets\Product;

class CreateAsset extends \yii\web\AssetBundle {
    public $baseUrl = '@web';
    
    public $js = [
    	"js/product/form-update.js",
    ];

    public $css = [
    ];

    public $depends = [
        "common\assets\AngularMessagesAsset",
        "common\assets\AngularAsset",
        "backend\assets\AceAsset",
        "common\assets\DropzoneAsset",
    ];
}