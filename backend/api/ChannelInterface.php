<?php namespace backend\api;

interface ChannelInterface {
	public function action($get);

	/*Getter - Setter*/
	public function setParam($key, $value);
}