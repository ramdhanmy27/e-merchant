<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Receiving;

/**
 * ReceivingSearch represents the model behind the search form about `common\models\Receiving`.
 */
class ReceivingSearch extends Receiving
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number_merch_do', 'receive_time', 'description', 'number_po', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param mixed $status
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Receiving::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'receive_time' => $this->receive_time,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['ilike', 'number_merch_do', $this->number_merch_do])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'number_po', $this->number_po]);

        return $dataProvider;
    }
}
