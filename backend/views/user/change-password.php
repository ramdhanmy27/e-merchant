<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Change Password ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profile'), 'url' => ['view-profile']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                // 'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]
    ); ?>
    <h4>Change Password</h4><br/>

		<?= $form->field($model, 'oldPassword')->passwordInput(['maxlength' => 255]) ?>

	    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>

	    <?= $form->field($model, 'repassword')->passwordInput(['maxlength' => 255]) ?>
    

    <?php //echo $form->field($model, 'status')->dropDownList(User::getArrayStatus()) ?>
    <div class='row'>
        <div class='col-sm-12'>
            <div class="pull-right">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
