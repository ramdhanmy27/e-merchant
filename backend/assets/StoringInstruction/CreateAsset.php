<?php namespace backend\assets\StoringInstruction;

class CreateAsset extends \yii\web\AssetBundle {
    public $baseUrl = '@web';
    
    public $js = [
    	"js/storing-instruction/form-create.js",
    ];

    public $css = [
    ];

    public $depends = [
        "common\assets\AngularMessagesAsset",
        "common\assets\AngularAsset",
        "backend\assets\AceAsset",
    ];
}