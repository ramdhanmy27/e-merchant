<?php namespace backend\assets\Receiving;

class CreateAsset extends \yii\web\AssetBundle {
    public $baseUrl = '@web';
    
    public $js = [
    	"js/receiving/form-create.js",
    ];

    public $css = [
    ];

    public $depends = [
        "common\assets\AngularMessagesAsset",
        "common\assets\AngularAsset",
        "backend\assets\AceAsset",
    ];
}