<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ucwords(str_replace("-", " ", $this->controller->id));
$model = $draft["filterModel"];

// closed
$open["columns"] = $draft["columns"] = $close["columns"] = [
    ['class' => 'yii\grid\SerialColumn'],
    
    "number_po",
    [
        "attribute" => 'date',
        "format" => 'datetime',
        "filter" => Html::activeTextInput($model, "date", ["class" => "input-date"]),
    ],
    [
        "label" => "Merchant",
        "attribute" => "code_merchant",
        "value" => "merchant.name",
    ],
    [
        "label" => "Warehouse",
        "attribute" => "code_warehouse",
        "value" => "warehouse.name",
    ],

    [
        'class' => 'common\yii\grid\ActionColumn',
        "template" => "{view}",
    ]
];

$key = count($open["columns"])-1;

// open
$open["columns"][$key] = [
    "class" => "common\yii\grid\ActionColumn",
    "template" => "{view} {close}",
    "buttons" => [
        "close" => function($url, $model, $id) {
            return Html::a(
                "<i class='glyphicon glyphicon-flag text-success' title='Close'></i>", 
                ["close", "id" => $id],
                ["data-confirm" => ""]
            );
        }
    ]
];

// draft
if (access("draft"))
    $draft["columns"][$key] = [
        "class" => "common\yii\grid\ActionColumn",
        "template" => "{view} {draft} {delete}",
        "buttons" => [
            "draft" => function($url, $model, $id) {
                return Html::a(
                    "<i class='glyphicon glyphicon-pencil' title='Update'></i>", ["draft", "id" => $id]
                );
            }
        ]
    ];
?>

<div class="purchase-order-index">
    <?php if (access("create")) : ?>
        <p> <?= Html::a("Create $this->title", ['create'], ['class' => 'btn btn-success']) ?> </p>
    <?php endif; ?>

    <div class="widget-box transparent">
        <div class="widget-header" style="border-bottom: none">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#open">
                        Open <?= ($count=$open["dataProvider"]->getTotalCount()) > 0 ? "($count)" : "" ?>
                    </a> 
                </li>
                <?php if (access("draft")) : ?>
                    <li> 
                        <a data-toggle="tab" href="#draft">
                            Draft <?= ($count=$draft["dataProvider"]->getTotalCount()) > 0 ? "($count)" : "" ?>
                        </a> 
                    </li>
                <?php endif; ?>
                <li> <a data-toggle="tab" href="#closed"> Closed </a> </li>
            </ul>
        </div>

        <div class="no-border widget-main tab-content">
            <div id="open" class="tab-pane active forscrollhor">
                <?= GridView::widget($open); ?>
            </div>

            <?php if (access("draft")) : ?>
                <div id="draft" class="tab-pane forscrollhor">
                    <?= GridView::widget($draft); ?>
                </div>
            <?php endif; ?>

            <div id="closed" class="tab-pane forscrollhor">
                <?= GridView::widget($close); ?>
            </div>
        </div>
    </div>
</div>