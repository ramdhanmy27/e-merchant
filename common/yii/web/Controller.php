<?php namespace common\yii\web;

use Yii;
use common\models\Permission;
use common\yii\web\View;
use common\yii\db\db;
use yii\base\Event;
use yii\helpers\Inflector;

class Controller extends \yii\web\Controller {
	
	public function beforeAction($action) {
		// initialize permissions
		if (isset(static::$permissions))
			Permission::initController($this->id, static::$permissions);
		else if (method_exists($this->controller, "permissions"))
			Permission::initController($this->id, static::permissions());

		if (!parent::beforeAction($action))
			return false;

		// action failed to execute
		return true;
	}

	/**
	 * return controller name id
	 * @return string
	 */
	public function name() {
		return ucwords(str_replace("-", " ", $this->id));
	}

	/**
	 * return controller name id
	 * @return string
	 */
	public static function id() {
		$class = get_called_class();
	    $mark = "controllers";
	    $classname = preg_replace("/Controller$/", "", substr($class, strpos($class, $mark)+strlen($mark)+1));
	    $pos = strrpos($classname, "\\");

	    return ($pos>0 ? substr($classname, 0, $pos)."\\" : '')
	    		.Inflector::camel2id(substr($classname, $pos!==false ? $pos+1 : 0));
	}

	/*View*/

	public function render($view=null, $params=[]) {
		$view = $view===null ? $this->action->id : $view;

		if ($this->request->isAjax)
			return $this->renderAjax($view, $params);
		else
			return parent::render($view, $params, $this);
	}

	public function renderAjax($view=null, $params=[]) {
		return $this->view->renderAjax($view===null ? $this->action->id : $view, $params, $this);
	}

	/*Request & Response*/

	/**
	 * abort request
	 * @param  int $code error code
	 * @param  string $msg error message
	 */
	public function abort($code=404, $msg=null) {
		if ($msg!=null)
			throw new \yii\web\HttpException($code, $msg);

		throw new \yii\web\HttpException($code, param("error-msg.$code"));
	}

	/*PHP Magic Methods*/

	/**
	 * invoke app method 
	 * @param  string 	$method
	 * @param  array 	$args
	 * @return
	 */
	public function __call($method, $args) {
		return call_user_func_array([Yii::$app, $method], $args);
	}

	/**
	 * accessing app component
	 * @param  string $var 
	 * @return
	 */
	public function __get($var) {
		return Yii::$app->{$var};
	}

	/*Role & Permisisons*/

	/**
	 * template accessControl on yii behaviors
	 * e.g. [['action1', 'action2'], ["permission1", "permission2"]] --> permission1 && permission2
	 * e.g. [["action"], true, ['post', 'get'], ['role1'], function() {return 'denycallback'}]
	 * e.g. [["action"], "permission1" | true && true | function() { return true; }]
	 * e.g. [["action"], null] --> return true
	 * e.g. ["*", null] --> will affect on all action
	 * @see  rule method
	 * 
	 * @param  array  	$permissions
	 * @param  Closure  $denyCallback
	 * @return array
	 */
	public function access(array $permissions, $denyCallback = null) {
		$rules = $only = array();

		try {
			foreach ($permissions as $perm) {
				if (!is_array($perm))
					throw new \Exception(
						"item '$perm' harus array. 
						e.g. [['action'], true, ['post', 'get'], ['role1'], function() {return 'denycallback'}]"
					);

				$rules[] = call_user_func_array([$this, "rule"], $perm);

				if (isset($perm[0])) {
					if (is_array($perm[0]))
						$only = array_merge($only, $perm[0]);
					else
						$only[] = $perm[0];
				}
			}
		}
		catch(Exception $e) {}

		return [
			'class' => \yii\filters\AccessControl::className(),
			'only' => $only,
			'denyCallback' => is_callable($denyCallback) ? $denyCallback() : function($rule, $action) { 
				$this->abort();
			},
			'rules' => $rules,
		];
	}

	/**
	 * template verbs on yii behaviors
	 * @param  array $action, e.g. 'delete' => ['post', 'put']
	 * @return array
	 */
	public function verbs(array $action) {
		return [
			'class' => \yii\filters\VerbFilter::className(),
			'actions' => $action,
		];
	}

	/**
	 * template rules for yii access behavior
	 * @param  array $action
	 * @param  mixed 		$perm
	 * @param  array  		$verbs 	"request method"   
	 * @param  array  		$roles
	 * @param  Closure 		$denyCallback
	 * @return array
	 */
	public function rule($action='*', $perm=true, $verbs = array('POST', 'GET'), $roles=null, $denyCallback = null) {
		return [
			'actions' => $action=='*' ? null : $action,
			'allow' => $this->can($perm),
			'verbs' => $verbs,
			'roles' => $roles,
			'denyCallback' => is_callable($denyCallback) ? $denyCallback : function($rule, $action) { 
				$this->abort(403); 
			},
		];
	}

	/**
	 * check authorization permission
	 * @param  mixed $perm
	 * @return boolean
	 */
	public function can($perms = null) {
		if (!is_array($perms))
			$perms = [$perms];

		$can = 1;

		foreach ($perms as $perm)
			$can *= can($this->id.":".$perm);

		return (bool) $can;
	}

	/**
	 * check existence of permission
	 * @param  string $perm
	 * @return boolean
	 */
	public function permExists($perm) {
		return db::query("auth_item")
				->where(["type" => 2, "name" => $this->id.":".$perm])
				->count() > 0;
	}
}