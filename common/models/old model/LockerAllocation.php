<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "locker_allocation".
 *
 * @property integer $id
 * @property integer $id_product
 * @property integer $quantity
 * @property string $sku
 * @property string $color
 * @property string $size
 * @property integer $base_price
 * @property string $created_by
 * @property string $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property OrderItem[] $orderItems
 * @property OrderItemHistory[] $orderItemHistories
 * @property ProductAllocation[] $productAllocations
 * @property Product $idProduct
 */
class LockerAllocation extends \yii\db\ActiveRecord
{
    /**
     * these are flags that are used by the form to dictate how the loop will handle each item
     */
    const UPDATE_TYPE_CREATE = 'create';
    const UPDATE_TYPE_UPDATE = 'update';
    const UPDATE_TYPE_DELETE = 'delete';
 
    const SCENARIO_BATCH_UPDATE = 'batchUpdate';
 
    private $_updateType;
 
    public function getUpdateType()
    {
        if (empty($this->_updateType)) {
            if ($this->isNewRecord) {
                $this->_updateType = self::UPDATE_TYPE_CREATE;
            } else {
                $this->_updateType = self::UPDATE_TYPE_UPDATE;
            }
        }
 
        return $this->_updateType;
    }
 
    public function setUpdateType($value)
    {
        $this->_updateType = $value;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locker_allocation';
    }

    /*public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }*/

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity', 'id_locker'], 'required'],
            [['id_product', 'id_locker', 'quantity'], 'integer'],
            ['updateType', 'required', 'on' => self::SCENARIO_BATCH_UPDATE],
            ['updateType',
                'in',
                'range' => [self::UPDATE_TYPE_CREATE, self::UPDATE_TYPE_UPDATE, self::UPDATE_TYPE_DELETE],
                'on' => self::SCENARIO_BATCH_UPDATE
            ],
            //allowing it to be empty because it will be filled by the ReceiptController
            ['id_product', 'required', 'except' => self::SCENARIO_BATCH_UPDATE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Product',
            'id_locker' => 'Lokasi',
            'quantity' => 'Quantity',
            /*'created_at' => 'Created At',
            'updated_at' => 'Updated At',*/
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLocker()
    {
        return $this->hasMany(Locker::className(), ['id' => 'id_locker']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product']);
    }
}
