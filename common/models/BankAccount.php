<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bank_account".
 *
 * @property string $code_bank
 * @property string $bank_name
 * @property string $account_number
 * @property string $account_name
 * @property string $current_balance
 *
 * @property BankTransaction[] $bankTransactions
 */
class BankAccount extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_bank'], 'required'],
            [['code_bank'], 'integer'],
            [['current_balance'], 'number'],

            // string
            //[['bank_code'], 'string', 'max' => 4],
            [['account_name'],'string','max'=>24],
            [['account_number'], 'string', 'max' => 16],
            [['bank_name', 'account_name'], 'string', 'max' => 24],
        ];	
    }

    public function attributeLabels()
    {
        return [
            'code_bank' => 'Bank Code',
            'bank_name' => 'Bank Name',
            'account_number' => 'Account Number',
            'account_name' => 'Account Name',
            'current_balance' => 'Current Balance',
            'bankTransactions.number_transaction' => 'Transaction Numbers',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankTransactions()
    {
        return $this->hasMany(BankTransaction::className(), ['code_bank' => 'code_bank']);
    }
}
