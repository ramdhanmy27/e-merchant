<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = 'Update Product: ' . $model->name;
?>

<div class="product-update">
	<?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>
</div>