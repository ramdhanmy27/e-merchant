<?php

namespace common\models;

use Yii;
use common\yii\db\db;

/**
 * This is the model class for table "box".
 *
 * @property integer $number_box
 * @property string $number_box
 * @property integer $rack_level
 * @property integer $capacity
 * @property integer $content_qty
 * @property string $number_rack
 *
 * @property Rack $rack
 * @property BoxContent[] $boxContents
 * @property StoringItem[] $storingItems
 * @property PickItem[] $pickItems
 */
class Box extends \common\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'box';
    }

    public function rules()
    {
        return [
            // required
            [['number_box',"rack_level", 'capacity'], 'required'],

            // unique
            [['number_box'], 'unique'],

            // integer
            [['rack_level', 'capacity', 'content_qty'], 'integer'],

            // string
            [['number_box', 'number_rack'], 'string', 'max' => 12],

            // exist
            [['number_rack'], 'exist', 'skipOnError' => true, 'targetClass' => Rack::className(), 'targetAttribute' => ['number_rack' => 'number_rack']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'number_box' => 'Box Number',
            'rack_level' => 'Rack Level',
            'capacity' => 'Capacity',
            'content_qty' => 'Content Qty',
            'number_rack' => 'Id Rack',
        ];
    }

    /**
     * update content qty with actual qty in box content
     * @return int affected rows
     */
    public static function updateContentQty() {
        return db::run(
            "UPDATE box b set content_qty=t.qty from (
                SELECT sum(qty) as qty, number_box FROM box_content group by number_box
            ) as t
            where b.number_box=t.number_box and b.content_qty!=t.qty"
        )->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRack()
    {
        return $this->hasOne(Rack::className(), ['number_rack' => 'number_rack']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoxContents()
    {
        return $this->hasMany(BoxContent::className(), ['number_box' => 'number_box']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoringItems()
    {
        return $this->hasMany(StoringItem::className(), ['number_box' => 'number_box']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPickItems()
    {
        return $this->hasMany(PickItem::className(), ['number_box' => 'number_box']);
    }
}
