<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;

register([
    "@web/js/user/user.js",
    "@web/css/user/user.css",
], [
    "depends" => ["backend\assets\AceAsset"]
]);

?>

<div class="user-index">
    <?php if (access("create")) : ?>
        <p> <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?> </p>
    <?php endif; ?>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'email:email',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                        if ($model->status === $model::STATUS_ACTIVE) {
                            $class = 'label-success';
                        } 
                        elseif ($model->status === $model::STATUS_INACTIVE) {
                            $class = 'label-warning';
                        }
                        return '<span class="label ' . $class . '" iduser="'.$model->id.'">' . $model->statusLabel . '</span>';
                    },
                'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        $arrayStatus,
                        ['class' => 'form-control', 'prompt' => Yii::t('app', 'Please Filter')]
                    )
            ],
            [
                'attribute' => 'created_at',
                'value' => 'created_at',
                'filter' => \yii\jui\DatePicker::widget([
                    "model" => $searchModel,
                    'language' => 'en',
                    'dateFormat' => 'dd-MM-yyyy',
                    'attribute'=>'created_at',
                ]),
                'format' => ['date', 'dd-MM-yyyy H:m:s'],
            ],
            [
                'attribute' => 'updated_at',
                'value' => 'updated_at',
                'filter' => \yii\jui\DatePicker::widget([
                    "model" => $searchModel,
                    'language' => 'en',
                    'dateFormat' => 'dd-MM-yyyy',
                    'attribute'=>'updated_at',
                ]),
                'format' => ['date', 'dd-MM-yyyy H:m:s'],
            ],
            [
				'class' => 'yii\grid\ActionColumn',
                'template' => $this->showAction([
                    "stat" => ["stat"],
                    "view" => ["view", "update", "stat"],
                    "update" => ["update"],
                ]),
                'buttons' => [
                    "update" => function($url, $model, $id){
                        return "<a href='".$url."' modal><i class='fa fa-pencil'></i></a>";
                    },
                    "stat" => function($url, $model, $id) {
                        if ($model->status == 1){
                            return '<div class="onoffswitch" style="float: left;">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox sw" checked id="myonoffswitch'.$id.'" id_user="'.$id.'" aktifkan="0">
                                        <label class="onoffswitch-label" for="myonoffswitch'.$id.'"></label>
                                    </div>';
                        }
                        else if($model->status == 0){
                            return '<div class="onoffswitch" style="float: left;">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox sw" id="myonoffswitch'.$id.'" id_user="'.$id.'" aktifkan="1">
                                        <label class="onoffswitch-label" for="myonoffswitch'.$id.'"></label>
                                    </div>';
                        }
                    },
                ],
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
