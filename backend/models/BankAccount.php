<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bank_account".
 *
 * @property string $code_bank
 * @property string $bank_name
 * @property string $account_number
 * @property string $account_name
 * @property string $current_balance
 *
 * @property BankTransaction[] $bankTransactions
 * @property SalesOrder[] $salesOrders
 */
class BankAccount extends \common\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bank_account';
    }

    public function rules()
    {
        return [
            [['code_bank'], 'required'],
            [['current_balance'], 'number'],
            [['code_bank'], 'string', 'max' => 4],
            [['bank_name', 'account_name'], 'string', 'max' => 24],
            [['account_number'], 'string', 'max' => 16],
        ];
    }

    public function attributeLabels()
    {
        return [
            'code_bank' => 'Code Bank',
            'bank_name' => 'Bank Name',
            'account_number' => 'Account Number',
            'account_name' => 'Account Name',
            'current_balance' => 'Current Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankTransactions()
    {
        return $this->hasMany(BankTransaction::className(), ['code_bank' => 'code_bank']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrders()
    {
        return $this->hasMany(SalesOrder::className(), ['code_bank' => 'code_bank']);
    }
}
