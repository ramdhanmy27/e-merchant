<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoringInstruction */

$this->title = 'Update Storing Instruction: ' . ' ' . $model->number_instruction;
$this->params['breadcrumbs'][] = ['label' => 'Storing Instructions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->number_instruction, 'url' => ['view', 'id' => $model->number_instruction]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="storing-instruction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
