<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PurchaseOrder;

/**
 * PurchaseOrderSearch represents the model behind the search form about `common\models\PurchaseOrder`.
 */
class PurchaseOrderSearch extends PurchaseOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['number_po', 'date', 'description', 'code_merchant', 'code_warehouse'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PurchaseOrder::find();
        $query->joinWith(['merchant', 'warehouse']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // buyer
        $dataProvider->sort->attributes['code_merchant'] = [
            'asc' => ['merchant.name' => SORT_ASC],
            'desc' => ['merchant.name' => SORT_DESC],
        ];

        // sales channel
        $dataProvider->sort->attributes['code_warehouse'] = [
            'asc' => ['warehouse.name' => SORT_ASC],
            'desc' => ['warehouse.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['ilike', 'number_po', $this->number_po])
            ->andFilterWhere(['ilike', "to_char(date, '".param("format.date.pgsql")."')", $this->date])
            ->andFilterWhere(['ilike', 'merchant.name', $this->code_merchant])
            ->andFilterWhere(['ilike', 'warehouse.name', $this->code_warehouse])
            ->andFilterWhere(['ilike', 'description', $this->description]);

        return $dataProvider;
    }
}
