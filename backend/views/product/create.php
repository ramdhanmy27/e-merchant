<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = 'Create Product';
?>

<div class="product-create">
	<?= $this->render('_form-create', [
        'model' => $model,
    ]) ?>
</div>
