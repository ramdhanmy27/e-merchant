<?php namespace backend\assets\Category;

class CategoryAsset extends \yii\web\AssetBundle
{
    public $baseUrl = '@web';
    
    public $js = [
        'ace/dist/js/fuelux/fuelux.tree.min.js',
        'js/category.js',
    ];

    public $depends = [
        "backend\assets\AceAsset",
    ];
}
