<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Warehouse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="warehouse-form">
    <?php ob_start(); ?>
    <?php $form = ActiveForm::begin(); ?>
	
    <?= $form->field($model, 'code_warehouse')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'address')->textArea(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textArea(['maxlength' => true]) ?>
    <?= $form->field($model, 'room_width')->textInput() ?>
    <?= $form->field($model, 'room_length')->textInput() ?>
    <?= $form->field($model, 'room_height')->textInput() ?>

    <hr />
    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?= $this->render("@common/views/widget", [
        "attr" => ["class" => $this->request->isAjax ? "col-md-12" : "col-md-8"],
        "title" => "Warehouse Information",
        "body" => ob_get_clean(),
    ]) ?>
    <div class="clearfix"></div>
</div>