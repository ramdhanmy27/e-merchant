<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Warehouse */

$this->title = $model->name;
?>

<div class="warehouse-view">
    <p>
        <?php if (access("update")) : ?>
            <?= Html::a('Update', ['update', 'id' => $model->code_warehouse], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php if (access("delete")) : ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->code_warehouse], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code_warehouse',
            'name',
            'city',
            'address',
            'description',
            'rack_count:integer',
            'room_width:integer',
            'room_length:integer',
            'room_height:integer',
        ],
    ]) ?>
</div>