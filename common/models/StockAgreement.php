<?php

namespace common\models;

use Yii;
use common\yii\db\db;

/**
 * This is the model class for table "stock_agreement".
 *
 * @property string $number_contract
 * @property string $contract_date
 * @property string $until_date
 * @property string $description
 * @property string $code_merchant
 *
 * @property AgreementItem[] $agreementItems
 * @property Merchant $merchant
 */
class StockAgreement extends \common\yii\db\ActiveRecord implements \common\yii\db\DraftModelInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_agreement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['number_contract'], 'required'],
            [['number_contract'], 'unique'],
            [['contract_date', 'until_date', "status"], 'safe'],

            [['number_contract'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 64],

            [['code_merchant'], 'exist', 'skipOnError' => true, 'targetClass' => Merchant::className(), 'targetAttribute' => ['code_merchant' => 'code_merchant']],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number_contract' => 'Contract Number',
            'contract_date' => 'From',
            'until_date' => 'To',
            'description' => 'Description',
            'code_merchant' => 'Merchant',
            'status' => 'Status',
            'merchant.name' => 'Merchant',
        ];
    }

    public function isDraft() {
        return $this->status == self::STATUS_DRAFT;
    }

    public static function getCode() {
        return unic();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementItems()
    {
        return $this->hasMany(AgreementItem::className(), ['number_contract' => 'number_contract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(Merchant::className(), ['code_merchant' => 'code_merchant']);
    }
}
