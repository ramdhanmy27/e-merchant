app.controller('barcodeController', function($scope, $http, $func) {
	$scope.product = {
		'number_product': '',
		'brand_name': '',
        'product_color' : null,
        'group_size' : null,
        'inventory' : null
	};

	$("#select2").off("select2:select").on("select2:select", function(e) {
		$scope.product.number_product = $(this).val();

		$scope.$apply(function() {
			if($scope.product.number_product != ''){
				$func
					.post(url.to('barcode/get-product'), { number_product : $scope.product.number_product })
					.success(function(response){
						console.log(response);
						$scope.product.number_product = response.product.number_product;
						$scope.product.brand_name = response.product.brand_name;
						$scope.product.product_color = response.product_color;
						$scope.product.group_size = response.group_size;
						$scope.product.inventory = response.inventory;
					});
			}
		});
	});
});