<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->name;
?>

<div class="category-view">
    <p>
    <?php if(access("update")){ ?>
        <?= Html::a('Update', ['update', 'id' => $model->code_category], ['class' => 'btn btn-primary', "modal"=>""]) ?> 
    <?php } ?>
    </p>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code_category',
            'name',
            'description',
        ],
    ]) ?>
</div>