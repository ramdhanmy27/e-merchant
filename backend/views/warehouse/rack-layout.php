<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\yii\db\db;

register(["@js/warehouse/rack-layout.js", "@css/warehouse/rack-layout.css"], [
	"depends" => [
		"common\assets\AngularAsset",
		"backend\assets\AceAsset",
	]
]);

$this->title = ucwords($model->name);
?>

<div class='warehouse-rack-layout'>
	<?= Html::beginForm("", "post", ["class"=>"row"]) ?>
		<div id='rack' ng-controller='rackController'>
			<!-- alert -->
			<div class="alert alert-danger" ng-if="!(rack.error | isEmpty)">
				<li ng-repeat="msg in rack.error track by $index">{{msg}}</li>
			</div>
			
			<div class='col-xs-12'>
				<!-- submit button -->
				<button type="submit" class="btn btn-success">
					<i class="glyphicon glyphicon-ok"></i> Simpan
				</button>

				<!-- size button -->
				<button type='button' class='btn btn-info' ng-click="f.addRow()">
					<i class='glyphicon glyphicon-plus'></i> <b>Baris</b>
				</button>

				<!-- attribute button -->
				<button type='button' class='btn btn-info' ng-click="f.addCol()">
					<i class='glyphicon glyphicon-plus'></i> <b>Kolom</b>
				</button>
			</div>

			<br> <hr>

			<div class='col-xs-12'>
				<p>Total Rack : {{f.total()}}</p>

				<div class='table-wrapper'>
					<div class='table-container'>
						<!-- remove per column -->
						<div class='data-row'>
							<button type="button" class='cell btn-remove' ng-if="rack.data[0].length > 1" 
								ng-repeat='col in rack.data[0] track by $index' ng-click="f.rmCol($index)">
								<i class='glyphicon glyphicon-remove text-danger'></i>
							</button>
						</div>

						<div ng-repeat='(y, row) in rack.data track by y' class='data-row'>
							<!-- remove per row -->
							<button type="button" class='btn-remove btn-rm-col' ng-click="f.rmRow(y)" 
								ng-if="rack.data.length > 1">
								<i class='glyphicon glyphicon-remove text-danger'></i>
							</button>

							<!-- data -->
							<div ng-repeat='(x, col) in row track by x' class='cell'>
								<input type='text' ng-model='rack.data[y][x]' class='form-control' 
									name="rack[{{y}}-{{x}}]" id="_{{y}}-{{x}}"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?= Html::endForm() ?>

	<?php
		// initialize rack layout data
		$query = db::query("rack", [db::raw("max(column_number) as col"), db::raw("max(row_number) as row")])
					->where("code_warehouse=:code", ["code" => $this->request->get("id")]);

		// set multidimension empty array
		$max = $query->one();
		$data = array_fill(0, $max['row']+1, array_fill(0, $max["col"]+1, ""));

		// fill empty array with existing value from database
		$query->select(["number_rack", "row_number", "column_number"]);

		foreach ($query->each() as $item) {
			$data[$item["row_number"]][$item["column_number"]] = $item["number_rack"];
		}

		$data = [
			"error" => [],
			"data" => $data,
		];
	?>

	<script type="text/javascript">
		window.data = {
			rack: <?= json_encode($data) ?>
		};
	</script>
</div>