<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BankAccount */

$this->title = 'Update Bank Account: ' . ' ' . $model->code_bank;
$this->params['breadcrumbs'][] = ['label' => 'Bank Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code_bank, 'url' => ['view', 'id' => $model->code_bank]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bank-account-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
