<?php

use yii\db\Schema;
use yii\db\Migration;
use common\yii\db\db;

class m130524_201442_init extends Migration
{
    // private $menucsv = "@console/backup";

    public function safeUp()
    {
        /* User */
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'name' => $this->string(100)->notNull(),
            'auth_key' => $this->string(32),
            'access_token' => $this->string(40)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        /*Yii::$app->runAction('migrate', [
            'migrationPath' => '@yii/rbac/migrations',
            'interactive' => false,
        ]);*/

        /* Menu */
        /*$this->createTable("{{%menu}}", [
            "id" => $this->primaryKey(),
            "parent" => $this->integer(),
            "url" => $this->text()->notNull(),
            "title" => $this->string(100)->notNull()->defaultValue(""),
            "enable" => $this->boolean()->notNull()->defaultValue(true),
            "order" => $this->integer()->notNull()->defaultValue(0),
            "icon" => $this->string(),
        ]);*/

        // $this->execute("ALTER TABLE {%menu} ADD COLUMN permission varchar[]");

        $this->seed();
    }

    public function safeDown()
    {
        $auth = Yii::$app->getAuthManager();
        $auth->removeAllAssignments();
        $auth->removeAllPermissions();
        $auth->removeAllRoles();

/*        Yii::$app->runAction('migrate/down', [
            'migrationPath' => '@yii/rbac/migrations',
            //'interactive' => false,
        ]);
*/
        /* User */
        $this->dropTable('{{%user}}');

        /* Menu */
        // backup menu to csv
        /*db::run("COPY (SELECT * FROM foo) TO '".Yii::getAlias($this->menucsv)."' WITH CSV;");
        $this->dropTable('{{%menu}}');*/
    }

    public function seed() {
        $this->insert('user', [
            'username' => 'admin',
            'name' => 'admin',
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'access_token' => Yii::$app->getSecurity()->generateRandomString(40),
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('webmaster'),
            'email' => 'admin@site.com',
            'status' => '1',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $auth = Yii::$app->getAuthManager();

        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator Saqina';
        $auth->add($admin);

        $staff = $auth->createRole('staff');
        $staff->description = 'Staff Saqina';
        $auth->add($staff);

        $merchant = $auth->createRole('merchant');
        $merchant->description = 'Merchant';
        $auth->add($merchant);

        $perm = $auth->createPermission('user:permissions');
        $auth->add($perm);

        $auth->addChild($auth->getRole('admin'), $perm);
        $auth->assign($auth->getRole('admin'), 1);
    }
}