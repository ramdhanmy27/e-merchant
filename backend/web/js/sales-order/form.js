app.controller("salesOrderController", function($scope, $http, $func) {
	/*Ordered Product*/
	$scope.order = [];
	$scope.shipping_cost = 0;

	var init = {
		order: {
			id: "",
			name: "",
			availableQty: 0,
			price: 0,
			qty: null,
			variation: "",
			size: "",
			color: "",
			variations: [""],
		},
	};

	$scope.f = {
		// table row
		initRow: function(index) {
			// select2 onchange event
			$("#order select[data-krajee-select2]:eq("+ index +")").off("select2:select").on("select2:select", function(e) {
				var value = $(this).val();
				$scope.order[index].id = value;

				$http.get(url.to("sales-order/get-product-detail", {id: value}), {cache: true})
					.success(function(data) {
						$scope.order[index].name = data.name;
						$scope.order[index].price = data.price;
						$scope.order[index].variations = data.variations;
					})
					.error(function() { fn.alertError() })
			})
		},
		add: function() {
			$scope.order.push($.extend({}, init.order));

			$scope.$watch(function() {
				$scope.f.initRow($scope.order.length-1);
			});
		},
		delete: function(index) {
			var delete_item = function() {
				$scope.order.splice(index, 1);
			};

			if (!fn.isEmpty($scope.order[index].id)) {
				fn.confirm({
					body: "Are you sure want to delete this item ?",
					yes: function() {
						$scope.$apply(function() {
							delete_item();
						})
					}
				})
			}
			else delete_item();
		},
		
		// buyer
		findBuyer: function(phone) {
			$scope.loading = true;

			$func.post(url.to("sales-order/find-buyer"), {phone: phone})
				.success(function(data) {
					$scope.loading = false;

					if (fn.isEmpty(data)) {
						delete $scope.code_buyer;
						return;
					}

					for (var index in data) 
						$scope[index] = data[index];
				})
				.error(function() { 
					$scope.loading = false;
					fn.alertError() 
				})
		},
		isBuyerExists: function() {
			return fn.isset($scope.code_buyer) && !fn.isEmpty($scope.code_buyer);
		},

		// product
		isProductSelected: function(index) {
			return !fn.isEmpty($scope.order[index].id);
		},
		getAvailableQty: function(index) {
			if (fn.isEmpty($scope.order[index].variation))
				return;

			// set size & color
			var tmp = $scope.order[index].variation.split(':');
			// console.log(tmp, $scope.order[index].variation)
			$scope.order[index].color = tmp[0];
			$scope.order[index].size = tmp[1];

			var isDuplicate = $.grep($scope.order, function(val, i) {
					if (i==index)
						return false;

					return $scope.order[index].id==val.id && 
						$scope.order[index].size==val.size && 
						$scope.order[index].color==val.color;
				}).length > 0;

			if (isDuplicate) {
				fn.alert("This product variation is already exists. Please choose another one");
				$scope.order[index].color = "";
				return;
			}

			$scope.order[index].availableQty = "Please wait...."
			$scope.order[index].qty = null;

			$http.get(url.to("sales-order/get-available-qty", {
				id: $scope.order[index].id,
				size: $scope.order[index].size,
				color: $scope.order[index].color,
			}))
			.success(function(data) {
				$scope.order[index].availableQty = fn.int(data.qty);
			})
			.error(function() { fn.alertError() })
		},

		// summary
		totalQty: function() {
			var total = 0;

			for (var i in $scope.order)
				total += fn.int($scope.order[i].qty);

			return total;
		},
		totalPrice: function() {
			var total = 0;

			for (var i in $scope.order)
				total += fn.int($scope.order[i].qty * $scope.order[i].price);

			return total;
		},

		// validation
		formValidation: function() {
			
		},
	}

	$scope.f.add();
});