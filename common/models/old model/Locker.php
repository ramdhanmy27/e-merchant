<?php

namespace common\models;

use Yii;
use common\models\Warehouse;

/**
 * This is the model class for table "locker".
 *
 * @property integer $id
 * @property integer $id_warehouse
 * @property string $locker
 */
class Locker extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_warehouse', 'locker'], 'required'],
            [['id_warehouse'], 'integer'],
            [['locker'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_warehouse' => 'Warehouse',
            'locker' => 'Locker',
            'idWarehouse.name' => 'Warehouse',
        ];
    }

    public function getIdWarehouse()
    {
        return $this->hasOne(Warehouse::className(), ['id' => 'id_warehouse']);
    }
}
