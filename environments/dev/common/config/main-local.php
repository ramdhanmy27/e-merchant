<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yii2advanced',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        // local - origin
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=127.0.0.1;port=5432;dbname=yiiproject',
            'username' => 'postgres',
            'password' => 'admin',
        ],

        // local - test
        /* 'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=127.0.0.1;port=5432;dbname=yiiadvanced',
            'username' => 'postgres',
            'password' => 'admin',
        ],*/
        
        // server 123 - test
        /* 'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=192.168.1.123;port=5432;dbname=yii2-application',
            'username' => 'postgres',
            'password' => 'admindev',
        ],*/
    ],
];
