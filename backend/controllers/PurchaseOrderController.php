<?php

namespace backend\controllers;

use Yii;
use common\yii\db\db;
use common\models\PurchaseOrder;
use common\models\PurchaseOrderSearch;
use common\models\Product;

class PurchaseOrderController extends \common\yii\web\Controller
{
    use \common\yii\web\DraftControllerTrait;

    public static $permissions = ["create", "draft", "delete", "view", "close"];

    public function behaviors() {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
                'save' => ['post'],
            ]),
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['create'], "create"],
                [['draft'], "draft"],
                [['delete'], "delete"],
                [['close'], "close"],
            ]),
        ];
    }

    /**
     * Lists all data.
     * @return mixed
     */
    public function actionIndex() {
        $search_model = new PurchaseOrderSearch();
        $param = [];
        $search_param = $this->request->queryParams;

        foreach (PurchaseOrder::$status as $status => $var) {
            $search_param["PurchaseOrderSearch"]["status"] = (string) $status;

            $param[$var] = [
                "filterModel" => $search_model,
                "dataProvider" => $search_model->search($search_param),
            ];
        }

        return view($param);
    }

    /**
     * set order status to close
     * @param  string $id contract number
     * @return response
     */
    public function actionClose($id) {
        $model = $this->findModel($id);

        if ($model->status!==PurchaseOrder::STATUS_SAVE)
            return $this->abort();

        $model->setCloseStatus()->save();

        return redirect(["index"]);
    }

    /*Service Provider*/

    /**
     * POST - update contract information
     * @param  string $id purchase order ID
     */
    public function actionUpdateContract($id) {
        $post = $this->request->post();
        $format = "m/d/Y";

        db::update("purchase_order", [
            "code_merchant" => trim($post["merchant"])=="" ? null : $post["merchant"],
            "code_warehouse" => trim($post["warehouse"])=="" ? null : $post["warehouse"],
            "description" => trim($post["description"])=="" ? null : $post["description"],
            "date" => ($datetime = \DateTime::createFromFormat($format, $post['date'])) ? $datetime->format("Y-m-d") : null,
        ], "number_po=:id", ["id" => $id])->execute();
    }

    /**
     * POST - delete product item 
     * @param  string $id purchase order ID
     */
    public function actionDeleteItem($id) {
        db::delete("purchase_order_item", "number_po=:id and number_product=:product", [
            "id" => $id, "product" => $this->request->post("product")
        ])->execute();
    }

    /**
     * POST - save quantity di setiap product
     * @param  string $id   purchase order ID 
     */
    public function actionSaveQty($id) {
        $post = $this->request->post();

        // ambil data variasi qty untuk dibandingkan nanti
        $query = db::query("purchase_order_item", ["code_size", "number_product_color", "qty"])
                    ->where(["number_po" => $id, "number_product" => $post['id']]);
        $data = [];

        foreach ($query->each() as $val) 
            $data[$val["code_size"]][$val["number_product_color"]] = $val["qty"];

        // membandingkan data yang dikirim dan yang ada pada database
        // dan menentukan aksi yang akan dilakukan pada data lama
        $insert = $update = $delete = [];

        foreach ($post["qty"] as $size => $item) {
            foreach ($item as $color => $newval) {
                $oldval_exists = isset($data[$size][$color]);
                $oldval = $oldval_exists ? $data[$size][$color] : null;
                $newval_exists = trim($newval)!="";

                // data yang dimasukkan bukan angka
                // atau tidak ada perbedaan data dengan yang ada pada database
                if (($newval_exists && !is_numeric($newval)) || ($oldval_exists && $newval==$oldval))
                    continue;

                if ($oldval_exists) {
                    // delete data quantity
                    if (!$newval_exists) {
                        $delete[] = [$id, $post["id"], $size, $color];
                    }
                    // update quantity
                    else if ($newval!=$data[$size][$color])
                        $update[] = [$id, $post["id"], $size, $color, $newval];
                }
                else if ($newval_exists) {
                    // buat data baru
                    $insert[] = [$id, $post["id"], $size, $color, $newval];
                }
            }
        }

        $field = [
            "number_po", 
            "number_product", 
            "code_size", 
            "number_product_color", 
            "qty",
        ];

        if (count($insert) > 0)
            db::batchInsert("purchase_order_item", $field, $insert)->execute();
        
        if (count($update) > 0)
            db::batchUpdate(
                "purchase_order_item i", ["qty=t.qty::int"], $field, $update, 
                "where t.number_po=i.number_po 
                    and t.number_product=i.number_product
                    and t.code_size::char=i.code_size::char
                    and t.number_product_color=i.number_product_color"
            )->execute();

        if (count($delete) > 0) {
            $bind = $condition = [];

            foreach ($delete as $i => $val) {
                $bind["n$i"] = $val[0];
                $bind["p$i"] = $val[1];
                $bind["s$i"] = $val[2];
                $bind["c$i"] = $val[3];
                $condition[] = "(number_po=:n$i and number_product=:p$i and code_size=:s$i and number_product_color=:c$i)";
            }

            db::delete("purchase_order_item", implode(" or ", $condition), $bind)->execute();
        }
    }

    /*Data Provider*/

    /**
     * ambil data pertama kali saat halaman draft di load
     * @param  string $id purchase order ID
     * @param  string $product product ID
     * @return json
     */
    public function actionGetDraftData($id) {
        $query = db::run(
            "SELECT p.number_product, p.basic_price, c.number_product_color, c.color, s.code_size, s.name, i.qty from purchase_order po
            left join (
                select distinct i.number_po, p.number_product, p.group_size, p.basic_price from product p
                inner join purchase_order_item i on p.number_product=i.number_product
            ) as p on po.number_po=p.number_po
            right join product_color c on c.number_product=p.number_product
            right join product_size s on s.group_size=p.group_size
            left join purchase_order_item i on i.number_po=po.number_po
                and c.number_product_color=i.number_product_color
                and s.code_size=i.code_size
            where po.number_po=:id order by p.number_product", ["id" => $id]
        )->query();
        $data = [];

        foreach ($query as $val) {
            $data[$val["number_product"]]["id"] = $val["number_product"];
            $data[$val["number_product"]]["price"] = $val["basic_price"];
            $data[$val["number_product"]]["size"][$val["code_size"]] = $val["name"];
            $data[$val["number_product"]]["attribute"][$val["number_product_color"]] = $val["color"];
            $data[$val["number_product"]]["quantity"][$val["code_size"]][$val["number_product_color"]] = $val["qty"];
        }

        return $this->request->isAjax || $this->controller->action->id=="get-draft-data" ? json($data) : $data;
    }

    /**
     * load variasi product
     * @param  string $id product ID
     * @return json
     */
    public function actionGetProductData($id) {
        if (!($model = Product::findOne($id)))
            return $this->abort();

        $data = [
            "id" => $id,
            "price" => $model->basic_price,
        ];

        $query = Product::getVariation($id);

        foreach ($query as $val) {
            // $data["price"] = $val["basic_price"];
            $data["size"][$val["code_size"]] = $val["name"];
            $data["attribute"][$val["number_product_color"]] = $val["color"];
            $data["quantity"][$val["code_size"]][$val["number_product_color"]] = null;
        }

        // prin($data);
        return $this->request->isAjax || $this->controller->action->id=="get-product-data" ? json($data) : $data;
    }

    /**
     * delete master
     * @param  string $id master ID
     * @return response
     */
    public function actionDelete($id) {
        db::delete("purchase_order_item", "number_po=:id", ["id" => $id])->execute();
        db::delete("purchase_order", "number_po=:id", ["id" => $id])->execute();

        return redirect(["index"]);
    }
}
