<?php namespace backend\controllers;

use common\models\DeliveryInstruction;
use common\models\SalesOrderSearch;
use common\models\SalesOrder;
use common\yii\db\db;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class DeliveryInstructionController extends \common\yii\web\Controller {
	public $breadcrumb;
	public static $permissions = ["view", "deliver"];

	public function behaviors() {
		$this->breadcrumb = [
			"index" => [
				"label" => $this->name(),
				"child" => [
					"deliver" => [
						"label" => "Delivery : [[get:id]]",
						"url" => ["deliver", "id"=>"[[get:id]]"],
						"child" => [
							"order-item" => ["label" => "Detail Order"],
						]
					],
				]
			]
		];

		return [
			"verbs" => $this->verbs([
				"cancel" => ["post"]
			]),
			"access" => $this->access([
				[["index"], "view"],
				[["deliver"], "deliver"],
			]),
		];
	}

	public function actionIndex() {
		$filterModel = new SalesOrderSearch();
		
		$param = $this->request->queryParams;
		$param["SalesOrderSearch"]["status"] = SalesOrder::STATUS_CLOSED;

		return view([
			"filterModel" => $filterModel,
			"dataProvider" => $filterModel->search($param),
		]);
	}

	public function actionDeliver($id) {
		$model = SalesOrder::findOne(["number_order" => $id]);

		if ($model == null)
			return $this->abort();

		if ($warehouse = $this->request->post("warehouse")) {
			try {
				$transaction = db::transaction();

				// set sales order status to deliver
				$model->status = SalesOrder::STATUS_DELIVER;

                if (!$model->save())
                    throw new \Exception(current($model->getFirstErrors()));

                // create delivery instruction
				db::insert("delivery_instruction", [
					"instruction_time" => db::raw("now()"),
					"code_warehouse" => $warehouse,
					"number_order" => $id,
					"delivery_status" => DeliveryInstruction::STATUS_OPEN,
				])->execute();

				$transaction->commit();
				return redirect(["index"]);
			}
			catch(\Exception $e) {
                $transaction->rollBack();
                handle($e);
			}
		}

		$order = db::run(
			"SELECT i.id_order_item as id, p.name, s.name as size, c.color, i.qty, ii.qty as available_qty, 
				ii.code_warehouse, w.name as warehouse FROM order_item i
			inner join product p on p.number_product=i.number_product
			inner join product_size s on s.code_size=i.code_size
			inner join product_color c on c.number_product_color=i.number_product_color
			inner join inventory_item ii on 
				ii.number_product_color=i.number_product_color and
				ii.code_size=i.code_size and
				ii.number_product=i.number_product
			inner join warehouse w on w.code_warehouse=ii.code_warehouse
			where number_order=:id
			order by p.name", ["id" => $id]
		)->queryAll();

		$data = [];
		$warehouse = [];

		foreach ($order as $val) {
			if (!isset($data[$val["id"]])) {
				$data[$val["id"]] = $val;
				$data[$val["id"]]["warehouse"] = [];				
			}

			if (!isset($warehouse[$val["code_warehouse"]]))
				$warehouse[$val["code_warehouse"]] = $val["warehouse"];

			$data[$val["id"]]["warehouse"][$val["code_warehouse"]] = $val["available_qty"];
			unset($data[$val["id"]]["code_warehouse"], $data[$val["id"]]["available_qty"], $data[$val["id"]]["id"]);
		}

		return view([
			"model" => $model, 
			"order" => $data, 
			"warehouse" => $warehouse,
		]);
	}
}