<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use common\models\Menu;
use common\yii\db\db;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */

// default value
if ($model->isNewRecord) {
    $model->enable = true;

    if (isset($parent))
        $model->parent = $parent;
}

?>

<div class='menu-form'>
    <?php ob_start(); ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'url')->textInput() ?>
    <?= $form->field($model, 'parent')->widget(Select2::classname(), [
        'data' => Menu::option("title"),
        'options' => ['placeholder' => 'Root Menu'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>
    <?= $form->field($model, 'icon')->textInput() ?>
    <?= $form->field($model, 'enable')->checkbox(['value' => true]) ?>
    <?= $form->field($model, '_permission')
            ->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    db::query("auth_item", "name")->where("type=2")->orderby("name")->all(), 
                    "name", "name"
                ),
                'options' => [
                    'placeholder' => 'Select items ..',
                    'multiple' => true,
                    "name" => "Menu[permission]",
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

    <hr>
    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?= $this->render("@common/views/widget", [
        "attr" => ["class" => $this->request->isAjax ? "col-md-12" : "col-md-8"],
        "title" => "Menu Information",
        "body" => ob_get_clean(),
    ]) ?>
    <div class="clearfix"></div>
</div>