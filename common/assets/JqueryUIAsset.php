<?php namespace common\assets;

use yii\web\AssetBundle;

class JqueryUIAsset extends AssetBundle
{
    public $sourcePath = '@bower'; 
    
    public $css = [
        "jquery-ui/themes/eggplant/jquery-ui.min.css",
    ];

    public $js = [
        "jquery-ui/jquery-ui.min.js",
    ];

	public $depends = ["yii\web\JqueryAsset"];
}
