<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoringInstruction */

$this->title = 'Create Storing Instruction';
$this->params['breadcrumbs'][] = ['label' => 'Storing Instructions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storing-instruction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
