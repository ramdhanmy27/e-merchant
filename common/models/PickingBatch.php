<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "picking_batch".
 *
 * @property integer $number_batch
 * @property string $number_batch
 * @property string $batch_time
 *
 * @property PickItem[] $pickItems
 */
class PickingBatch extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'picking_batch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['number_batch'], 'required'],

            // unique
            [['number_batch'], 'unique'],

            // safe
            [['batch_time'], 'safe'],

            // integer
            [['qty'], 'integer'],
            [['unit_price'], 'number'],

            // string
            [['number_batch'], 'string', 'max' => 10],

            // exist
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number_batch' => 'Batch Number',
            'batch_time' => 'Batch Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPickItems()
    {
        return $this->hasMany(PickItem::className(), ['number_batch' => 'number_batch']);
    }
}
