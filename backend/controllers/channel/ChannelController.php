<?php

namespace backend\controllers\channel;

use Yii;
use common\models\Channel;
use common\models\ChannelSearch;
use common\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\httpclient\Client;
use yii\helpers\Inflector;

/**
 * ChannelController implements the CRUD actions for Channel model.
 */
class ChannelController extends Controller
{
    use \common\yii\web\CrudControllerTrait;

    private $construct;

    public function __construct($id, $app, $config = []) {
        $this->construct = get_defined_vars();
        parent::__construct($id, $app, $config);
    }

    public function behaviors() {
        return [];
    }

    public function init() {
        $this->param = [
            'model' => "common\\models\\Channel",
        ];
        return parent::init();
    }    

    public function actionApi($id, $action="index") {
        $get = $this->request->get();
        unset($get['r'], $get['id']);

        // initiate controller class
        if ($model = $this->findModel($id)) {
            $controller = "backend\\controllers\\channel\\api\\".Inflector::camelize($model->name."Controller");
        }
        else $controller = null;
        prin($this->action);
        
        if (!class_exists($controller))
            $this->abort(404);

        $api = new $controller($this->construct['id'], $this->construct['app'], $this->construct['config'], $model);
        $actions = preg_split("/\/\\\\/", $action);

        return call_user_func_array([$api, "action".Inflector::camelize(array_pop($actions))], $actions);
    }

    /**
     * Lists all Channel models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ChannelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
