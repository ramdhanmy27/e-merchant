<?php namespace backend\api;

use yii\httpclient\Client;
use yii\helpers\Url;

class Channel extends \yii\base\Object implements ChannelInterface {
	protected $url, $userid, $apikey, $param;

	public function action($param) {
		$this->setParam($param);
	}

	/**
	 * set key-value parameter
	 * @param string|array 	$data 
	 * @param mixed 		$value
	 */
	public function setParam($data, $value = null) {
		if (is_array($data)) {
			foreach ($data as $key => $val)
				$this->param[$key] = $val;
		}
		else $this->param[$data] = $value;
	}

	/**
	 * make http client request
	 * @param  string|array  $urlParam
	 * @return yii\httpclient\Client
	 */
	public function client($urlParam = []) {
		return (new Client())->createRequest()->setUrl(is_array($urlParam) ? $this->url($urlParam) : $urlParam);
	}

	/**
	 * build url
	 * @param  array  $param http query param
	 * @return string
	 */
	public function url($param = []) {
		return $this->url.(is_array($param) && count($param)>0 ? "?".http_build_query($param, '', '&', PHP_QUERY_RFC3986) : null);
	}
}