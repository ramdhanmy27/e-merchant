<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SalesChannel */

$this->title = 'Update Sales Channel: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sales Channels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->code_sales_channel]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sales-channel-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
