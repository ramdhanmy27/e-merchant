# Controller
Untuk membuat sebuah controller, pastikan parent class mengarah pada `common\yii\web\Controller`. Jika sudah, anda bisa mengakses property-property pada instance `Yii::$app` seperti component yang didaftarkan di config main/main-local. Property-property tersebut bisa anda akses dengan menuliskan `$this->namaProperty` atau `$this->namaMethod()`.

# Permissions

## Registrasi Permission
Berikut contoh untuk meregistrasikan permissions pada sebuah controller

```php
class ContohController extends \common\yii\web\Controller {
	// registrasi menggunakan variable
	public static $permissions = [
		["create", "Tambah Data"],
        ["update"],
        "delete",
	];

	// registrasi menggunakan fungsi
	public static function permissions() {
		return ["create", "update", "delete", "view"];
	}
}
```

Anda dapat memilih metode manapun yang anda suka. Pada index pertama isikan nama permision lalu isikan juga deskripsi pada index selanjutnya. Atau cukup isikan nama permisionnya saja dan controller yang akan mengurus sisanya.

## Cek Permission User
Permission yang sudah diregistrasikan akan tesimpan dalam database dengan format `contoh:create`. Jika anda ingin mengecek apakah user memiliki hak akses `create` pada `ContohController`. Anda bisa tuliskan sebagai berikut :

```php
if (Yii::$app->user->can('contoh:create')) {
	// eksekusi create
}
```

Namun apabila itu terlalu panjang untuk anda, bisa juga bisa dituliskan seperti ini:

```php
if ($this->can('create')) {
	// eksekusi create
}
```

## Permissions pada Action
```php
class ContohController extends \common\yii\web\Controller {

	public function behaviors()
	{
		return [
			"access" => $this->access([
				[['index', 'view'], "view"],
				[['create'], "create"],
			]),
		];
	}

	public function actionIndex() {
		// Halaman Awal Controller
	}

	public function actionView($id) {
		// Tampilkan Data
	}

	public function actionCreate() {
		// Eksekusi action create
	}
}
```

Seperti yang anda lihat. Action `index` dan `view` akan dieksekusi apabila user memiliki permission `view`. Begitu pula dengan action create, yang akan dieksekusi jika user memiliki permission `create`.

# Error
Untuk menampilkan halaman error anda bisa lakukan seperti ini

```php
$this->abort(404, 'Halaman Tidak ditemukan');
```

parameter pertama diisikan kode status requestnya. Lalu isikan pesan error pada parameter selanjutnya. Apabila anda terlalu sibuk untuk menuliskan pesannya, bisa juga anda cukup isikan kode statusnya saja `$this->abort(403)` atau kosongkan parameternya `$this->abort()` untuk kode status `404`.

# CRUD
Untuk membuat sebuah CRUD sederhana bisa dipercepat dengan bantuan `common\yii\web\CrudControllerTrait`. Untuk membuatnya pertama siapkan sebuah model dan file kosong untuk controllernya dan isikan sebagai berikut.

```php
class ContohController extends \common\yii\web\Controller
{
	use \common\yii\web\CrudControllerTrait;

	public $param = [
		'id' => 'nama_kolom_id', // optional
		'model' => 'common\models\NamaModel',
	];
}
```

berikut nama-nama method yang telah disiapkan:

`public function actionIndex()`

`public function actionView($id)`

`public function actionCreate()`

`public function actionUpdate($id)`

`public function actionDelete($id)`

Jika ada proses yang tidak sesuai dengan keinginan, bisa anda tuliskan kembali method tersebut pada `ContohController` seperti ini

```php
class ContohController extends \common\yii\web\Controller
{
	use \common\yii\web\CrudControllerTrait;

	public $param = [
		'id' => 'nama_kolom_id', // optional
		'model' => 'common\models\NamaModel',
	];

	public function actionIndex() {
		// Tampilkan halaman index versi saya
	}
}
```

Namun apabila yang tidak sesuai adalah tampilan\view-nya anda cukup membuat file `@app\views\contoh\index.php`

# Master - Detail
Cara penggunaan trait ini hampir sama seperti `CrudControllerDetail`, perbedaannya terletak pada model "Detail" nya saja.

```php
class ContohController extends \common\yii\web\Controller
{
	use \common\yii\web\MasterDetailControllerTrait;

	public function init() {
		parent::init();

		$this->param = [
			// master
			'id' => 'nama_kolom_id', // optional
			'model' => 'common\models\NamaModel',

	
			// detail
			'detail_id' => 'nama_kolom_id_detail', // optional
			'detail_model' => 'common\models\NamaModelDetail',
			'detail_parent_id' => 'nama_kolom_id', // optional
		];
	}
}
```

berikut nama-nama method yang telah disiapkan:

`public function actionIndex()`

`public function actionView($id)`

`public function actionCreate()`

`public function actionUpdate($id)`

`public function actionDelete($id)`

`public function actionUpdateDetail($parent, $id)`

`public function actionViewDetail($parent, $id)`

`public function actionDeleteDetail($parent, $id)`

`public function actionCreateDetail($parent)`

Untuk view pada action yang terdapat nama "Detail" seperti `actionUpdateDetail` anda bisa buat dengan nama `@app\views\contoh\update-detail.php` atau bisa juga `@app\views\contoh\detail\update.php`

# View
Untuk merender sebuah view anda bisa tuliskan seperti ini

```php
return $this->render('@app\views\contoh\nama-view', [
	'param1' => 'value param1'
]);
```

atau seperti ini. View yang digunakan adalah yang pertama kali di temukan, pencarian akan di lakukan dari `nama-view1` ke `nama-view2`.

```php
return $this->render([
	'@app\views\contoh\nama-view1',
	'@app\views\contoh\nama-view2',
	], [
	'param1' => 'value param1'
]);
```

atau jika anda terlalu sibuk, bisa juga anda tuliskan seperti ini. view yang akan diambil adalah nama Action ID yang sedang diakses (diambil dari nama action method). 

```php
return $this->render([
	'param1' => 'value param1'
]);
```

Pada template view, anda dapat mengakses property dari instance `Yii::$app` dengan instance `$this` sama  seperti controller. Anda juga bisa mengakses instance controller dengan `$this->context`.

## Breadcrumb
Sebelumnya, untuk membuat breadcrumb anda harus tuliskan pada template view seperti ini.

```php
$this->params['breadcrumbs'][] = ['label' => 'Contoh', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
```

Akan tetapi bagi anda yang tidak ingin menuliskan breadcrumb pada semua view yang anda buat, anda bisa serahkan tugas itu pada controller.
Daftarkan breadcrumb sebelum action dieksekusi. Anda bisa daftarkan pada method `init` atau `beforeAction` sebagai salah satu contoh.

```php
class ContohController extends \common\yii\web\Controller
{
	public function beforeAction($action) {
		$this->breadcrumb = [
			"index" => [
				"label" => $this->name(),
				"child" => [
					"nama-action" => [
						"label" => "[[id]]",
						"url" => ["view", "id"=>"[[get:id]]"],
					],
					"create" => ["label" => "Tambah"],
				]
			]
		];

		return parent::beforeAction($action);
	}
}
```

index array berisikan nama action-id dan valuenya adalah array yang terdiri dari `label` dan `url`. Jika url tidak diisi, maka otomatis akan mengarah pada halaman action. 

#### Template Expression
Apabila saat membuat breadcrumb anda ingin menyisipkan sebuah variable, anda bisa tuliskan seperti ini `[[nama_parameter]]` dan jika anda ingin sesuatu yang lebih, bisa juga anda tuliskan seperti ini `{{empty($nama_parameter) ? $nama_parameter : 'other'}}`.

## Helpers

### gridview
Menampilkan gridview beserta menu exportnya

```php
gridview($columns, $dataProvider, $grid=[], $export=[])
```

berikut contoh penggunaannya

```php
$searchModel = new MerchantSearch();

gridview([
	[['class' => 'yii\grid\SerialColumn']],
	"column1",
	"column2",
	[['class' => 'yii\grid\ActionColumn']]
]
, $searchModel->search($this->request->queryParams)
, [
	"filterModel" => $searchModel,
]
, [
	"target" => kartik\export\ExportMenu::TARGET_SELF,
])
```

jika anda tidak ingin ada menu exportnya berikan nilai `null` di parameter terakhir seperti ini.

```php
gridview($columns, $dataProvider, [], null);
```

### register
register js atau css file

```php
register("@web/js/script.js", ["depends" => ["backend\assets\AppAsset"]);
register(["@web/js/script.js", "@web/js/style.css"], ["depends" => ["backend\assets\AppAsset"]);
```

### xml_encode

```php
echo htmlentities(xml_encode(["data" => ["item" => [1, 2, 3]]]));
// will print '<data><item>1</item><item>2</item><item>3</item></data>'
```

### json response

```php
public function actionJson() {
	return json([1,2,3]);
}
```

# Debug

berikut fungsi-fungsi yang telah disiapkan

`prins` : menampilkan isi variable dari `$var_1` sampai `$var_n`

```php
prins($var_1, $var_2, ..., $var_n)
```

`prin` : menampilkan isi variable dari `$var_1` sampai `$var_n` dan `exit`

```php
prin($var_1, $var_2, ..., $var_n)
```

`traces` : backtrace process dan menampilkan isi variable dari `$var_1` sampai `$var_n`

```php
traces($var_1, $var_2, ..., $var_n)
```

`trace` : backtrace process dan menampilkan isi variable dari `$var_1` sampai `$var_n` dan `exit`

```php
trace($var_1, $var_2, ..., $var_n)
```

# Database
untuk hal yang berhubungan dengan database anda bisa gunakan class `common\yii\db\db`.
Anda bisa mengakses property `yii\db\Command` seperti ini

```php
db::insert();
db::update();
db::delete();
```

## Connection
berikut cara untuk mengganti koneksi database

```php
db::connection('nama_component');
```

atau 

```php
db::$connection = 'nama_component';
```

## Query
Untuk eksekusi query biasa anda bisa gunakan 

```php
db::run('SELECT * FROM table');
```

atau jika anda ingin menggunakan query builder yii

```php
db::query('table', '*')->where('id', 10);
```

### Query Builder
Pada query builder anda dapat mengambil query yang sedang di proses.

```php
echo db::query('table', '*')->where('id', 10)->sql;
```

atau apabila anda ingin langsung mendebugnya dengan helper `prin()` bisa anda tuliskan seperti ini.

```php
db::query('table', '*')->where('id', 10)->debug();
```

# Asset Bundle

## Register Asset
Pada framework yii untuk meregistrasikan sebuah asset bundle anda bisa tuliskan dalam view seperti ini.

```php
\backend\assets\ContohAsset::register($this);
```

Tapi hal tersebut akan terasa menyebalkan apabila kita ingin punya asset yang berbeda-beda pada setiap action. Jadi untuk menyiasati hal tersebut anda cukup membuat nama class assetnya sama seperti nama action yang dituju.

Misalkan pada class `ContohController` kita punya sebuah method action bernama `actionIndex()`. Jika anda menginginkan sebuah asset bundle yang langsung mengarah pada `actionIndex`. Pertama-tama buatlah folder `contoh` didalam folder `assets`. Lalu buat class baru didalam folder `contoh` yang bernama `IndexAsset`.

Apabila anda ingin asset bundle langsung mengarah pada semua action pada sebuah controller. Caranya hampir sama seperti diatas hanya saja, class yang dibuat dinamakan dengan nama controller. Seperti ini `ContohAsset`.

# Javascript

## Modal

### Membuat modal
Untuk membuat javascript modal anda perlu menambahkaan htmlnya sendiri ke dalam view. Bentuk html itu sendiri sama seperti html untuk membuat modal dengan menggunakan bootstrap. Karena memang bootstrap yang digunakan :3.
Jika menuliskan htmlnya terlalu panjang bagi anda, anda bisa tuliskan seperti ini.

```php
$this->render("@common/views/modal", [
    "attr" => ["id" => "winconfirm"],
    "title" => "Apakah Anda Yakin ?",
    "body" => "<div>Data akan terhapus secara permanen.</div>",
    "footer" => 
    	'<button type="button" data-dismiss="modal" class="btn btn-default">Tidak</button>
        <button type="button" class="btn btn-primary modal-accept">Ya</button>',
])
```

Script diatas merupakan contoh pembuatan modal untuk konfirmasi aksi. Berikut penjelasan dari setiap indexnya.

> **attr** - attribute untuk modal

> **title** - Judul Modal

> **body** - Konten isi modal

> **footer** - Konten footer modal

Jika anda tidak memerlukan salah satu dari index diatas, anda tidak harus mengisinya dengan `null` atau string kosong `""` cukup hiraukan saja. Karena tidak ada satupun yang bersifat required.

### Pengunaan Modal
Aplikasi ini sudah menyediakan sebuah modal untuk menampilkan halaman yang direquest secara asynchronus dengan javascript.

> **#modal-basic** - modal untuk menampilkan konten dari halaman yang di request secara asynchronus (ajax).

Untuk menggunakan modal diatas ataupun modal yang sudah anda buat, anda cukup menambahkan attribut `modal` dan atur element modal yang akan dieksekusi. Berikut contoh penggunaannya.

```php
<a href='contoh/delete/12' modal='#modal-basic'>Hapus Data</a>

\yii\helpers\Html::a(
	'<i class="glyphicon glyphicon-remove"></i>', $url, [
	'data' => [
		'confirm' => 'Are you sure you want to delete this item ?',
		'method' => 'post',
	]
]);
```

Value untuk attribut modal tidak harus anda isi. Anda bisa cukup tambahkan attribut modalnya saja, jika anda ingin menggunakan `#modal-basic`.

