$(document).ready(function(){
	$(".reallocate").click(function(){
		$(".totalinput").val(0);
		var inter=setInterval(function(){
			if($("#ndatas").length!=0 || $("#nodatas").length != 0){
				clearInterval(inter);						
				var cs = $(".stockinput");
				cs.each(function(){
					var sales=$(this).attr("alt");
					var channel=$('.'+sales+'');
					var total=0;
					var totalunal=0;
					channel.each(function(){
						total=parseInt(total)+parseInt($(this).val());
						$("#total"+sales).val(total);
						$("#totalbef"+sales).val(total);
					});				
				});
				var tqc=$(".totalqtycount");
				var ttqc=0;
				tqc.each(function(){
					ttqc=parseInt(ttqc)+parseInt($(this).val());
					$("#totalqty").val(ttqc);
				});
				var tuc=$(".totalunalcount");
				var ttuc=0;
				tuc.each(function(){
					ttuc=parseInt(ttuc)+parseInt($(this).val());
					$("#totalunallo").val(ttuc);
					$("#unallobef").val(ttuc);
				});

			}
		},500);
	});
	$("#submitbtn").click(function(e){
		e.preventDefault();
		save($(this).attr("url"));
	});
});

function totalsales(sales){
	$(document).ready(function(){
		$("#toggleunallocated").remove();
		var channel=$('.'+sales+'');
		var total=0;
		channel.each(function(){
			total=parseInt(total)+parseInt($(this).val());
			$("#total"+sales).val(total);
		});
		//change
		var change=parseInt($("#total"+sales).val())-parseInt($("#totalbef"+sales).val());
		$("#change"+sales).val(change);
		// end change
		var cs=$(".stockinput");
		var totalunal=0;
		var jumlahbaris=$(".totalinput").length;
		var loop=0;
		cs.each(function(){
			if(loop<jumlahbaris){
				var selchan=$("#total"+$(this).attr("alt"));
				totalunal=parseInt(totalunal)+parseInt(selchan.val());
			loop++;
			}
		});
		totalunal=parseInt($("#totalqty").val())-parseInt(totalunal);
		$("#totalunallo").val(totalunal);

		//change
		var changeun=parseInt($("#totalunallo").val())-parseInt($("#unallobef").val());
		$("#changeunallo").val(changeun);
		//endchange
		if($(".thistrerror").length!=0){
			$("#submitbtn").attr("disabled","disabled");
		}else{
			$("#submitbtn").removeAttr("disabled");
		}
	});
}

function gogo(url){
	var cat="&c="+$("select[name='Category']").val();
	if(cat==="&c=allcategory"){
		cat="";
	}
	window.location.href=url+cat;
}
function save(url){
	$.ajax({
		url : url,
		method : 'POST',
		data : $("#forminfo").serialize(),
		success : function(){
			//ntar penggantiin ya jadi modal
			fn.alert("Product Allocation data updated");
		}
	});
}