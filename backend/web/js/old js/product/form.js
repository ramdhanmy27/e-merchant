$(document).ready(function() {
	/*Locker Allocation Product Quantity JS Validation*/
	var availableQuantity, usedUpQuantity = 0;
	var tempQuantityPerUnit = [];
	var sum = 0;

    $(".lockerallocation-quantity").each(function(){
        sum += +$(this).val();
    });

    var unallocatedQuantity = $("#product-quantity").val() - sum;

    $(".available-quantity").html(unallocatedQuantity);

    if(unallocatedQuantity <= 0)
        $('button[name="addRow"]').attr('disabled', 'disabled');

	/*Images*/
	var fileInput = $("input.file-input");

	if (fileInput.length>0 && typeof fileInput.fileinput === "function") {
		fileInput.fileinput({
			'showUpload':false, 
			'previewFileType':'any',
			'maxFileCount': fileInput.attr("sesa"),
			'mainClass': "input-group-md"
		});

		$(".file-input .btn").addClass("btn-sm");
	}

	var containerFileInput = $("div.file-input");

	if (fileInput.attr("sesa") == '0')
		containerFileInput.css("display","none");

	var colorbox = $('.colorbox');

	if (colorbox.length>0 && typeof colorbox.colorbox === "function") {
		var $overflow = '';

		colorbox.colorbox({
			rel: 'colorbox',
			reposition: true,
			scalePhotos: true,
			scrolling: false,
			previous: '<i class="ace-icon fa fa-arrow-left"></i>',
			next: '<i class="ace-icon fa fa-arrow-right"></i>',
			close: '&times;',
			current: '{current} of {total}',
			maxWidth: '100%',
			maxHeight: '100%',

			onOpen: function() {
				$overflow = document.body.style.overflow;
				document.body.style.overflow = 'hidden';
			},
			onClosed: function() {
				document.body.style.overflow = $overflow;
			},
			onComplete: function() {
				$.colorbox.resize();
			}
		});
	}

	// delete images
	$(".image-delete").click(function(e) {
		var $this = $(this);

		fn.confirm({
			body: "Apakah anda yakin ingin menghapus gambar ini ?",
			yes: function() {
				$.ajax({
					url: decodeURIComponent($this.attr("href")),
					type: "POST",
					success: function() {
						$this.parents(".image-item").fadeOut("slow", function() {
							$(this).remove();
						});
						
						fileInput.attr("sesa", parseInt(fileInput.attr("sesa")) + 1);
						
						if(fileInput.attr("sesa")==0){
							containerFileInput.css("display","none");
						}
						else {
							containerFileInput.css("display","block");	
						}
						if (fileInput.length>0 && typeof fileInput.fileinput === "function") {
							fileInput.fileinput('refresh', {
								'showUpload':false, 
								'previewFileType':'any',
								'maxFileCount': parseInt(fileInput.attr("sesa")),
								'mainClass': "input-group-sm"
							});
							
							$(".file-input .btn").addClass("btn-sm");
						}
					},
					error: function() {
						fn.alertError();
					}
				})
			}
		});

		e.preventDefault();
	});
	
	var onChangeQty = function(){
		sum = 0;

	    $(".lockerallocation-quantity").each(function(){
	        sum += +$(this).val();
	    });
	    
	    if($(this).val() < sum){
	    	if (isAngular) 
		    	angular.element(this).scope().f.onChangeQty(sum);
	    		
	    	$(this).val(sum);
	    	$(".quantity_message").html('<br/><font style="color: red">'+sum+' quantity is allocated in warehouse, to reduce this quantity, please edit locations first</font>');
	    	$(".quantity_message").show(0);
	    	
	    	setTimeout(function(){
	    		$(".quantity_message").hide("slow");
	    		setTimeout(function(){
	    			$(".quantity_message").html("");
	    		},1000);
	    	}, 5000);

	    	$('button[name="addRow"]').attr('disabled', 'disabled');
	    }
	    else if ($(this).val() == sum) $('button[name="addRow"]').attr('disabled', 'disabled');
	    else $('button[name="addRow"]').removeAttr('disabled');

        unallocatedQuantity = $("#product-quantity").val() - sum;
        $(".available-quantity").html(unallocatedQuantity);
	};

	// angular
	if (isAngular) {
	    var scope = angular.element($("[ng-controller]")).scope();

	    if (fn.isset(scope))
		    $("#_"+scope.variations.pos.x+'-'+scope.variations.pos.y).change(onChangeQty);
	}
	$(document).on('change', '#product-quantity', onChangeQty);

	$(document).on('change', '.lockerallocation-quantity', function(){
		sum = 0;
	    $(".lockerallocation-quantity").each(function(){
	        sum += +$(this).val();
	    });
	    unallocatedQuantity = $("#product-quantity").val() - sum;
	    $(".available-quantity").html(unallocatedQuantity);
	    if(unallocatedQuantity <= 0){
	    	$('button[name="addRow"]').attr("disabled", "disabled");
	    	$(this).val(unallocatedQuantity + parseInt($(this).val()));

	    	sum = 0;
	    	$(".lockerallocation-quantity").each(function(){
		        sum += +$(this).val();
		    });
		    unallocatedQuantity = $("#product-quantity").val() - sum;

	    	$(".available-quantity").html(unallocatedQuantity);
	    } else {
	    	$('button[name="addRow"]').removeAttr("disabled");
	    }

		/*if(!isNaN($(this).val() && $(this).val() !== "")){
			if((availableQuantity - $(this).val()) < 0){
				$(this).val() = "";
			}
			else{
				// console.log("yang ini : "+$(this).attr('id'));
				if(tempQuantityPerUnit[$(this).attr("id").split("-")[1]] != undefined){
					if(tempQuantityPerUnit[$(this).attr("id").split("-")[1]] - $(this).val() > 0){
						var beda = parseInt(tempQuantityPerUnit) - parseInt($(this).val());
						var valBefore 	= tempQuantityPerUnit[$(this).attr("id").split("-")[1]];
						var baru = tempQuantityPerUnit[$(this).attr("id").split("-")[1]] - beda;
						var sisa = availableQuantity - (usedUpQuantity - valBefore + baru);
						console.log("masuk lebih ada sisa", availableQuantity , usedUpQuantity , valBefore,  baru, sisa);
						if(sisa < 0){
							$(this).val(0);
						}
					}
					else if(tempQuantityPerUnit[$(this).attr("id").split("-")[1]] - $(this).val() < 0){
						var beda = parseInt(tempQuantityPerUnit) - parseInt($(this).val());
						var valBefore 	= tempQuantityPerUnit[$(this).attr("id").split("-")[1]];
						var baru = tempQuantityPerUnit[$(this).attr("id").split("-")[1]] + beda;
						if(availableQuantity - (usedUpQuantity - valBefore) + baru < 0){
							$(this).val(0);
							$("tr.locker_allocation").each(function(){
								var m = $(this).attr('class').match(/\-(\d)$/)[1];
								usedUpQuantity = parseInt(usedUpQuantity) + parseInt($('#lockerallocation-'+m+'-quantity').val());
								tempQuantityPerUnit[m] = parseInt($('#lockerallocation-'+m+'-quantity').val());
							});
							console.log(usedUpQuantity);
						}
					}
				}
				
					usedUpQuantity = 0;
					$("tr.locker_allocation").each(function(){
						var m = $(this).attr('class').match(/\-(\d)$/)[1];
						usedUpQuantity = parseInt(usedUpQuantity) + parseInt($('#lockerallocation-'+m+'-quantity').val());
						tempQuantityPerUnit[m] = parseInt($('#lockerallocation-'+m+'-quantity').val());
					});
					
					// $(".available-quantity").html();
				
			}
		}*/
	});

	$('button[name="addRow"]').click(function(e){
		var content = $('.locker_allocation-0').clone();
		var lastTableRow = $( "tr.locker_allocation:last" );
		var m = lastTableRow.attr('class').match(/\-(\d)$/);
		var counter = parseInt(m[1])+1;

		content = $('<div>').append(content).html().replace(/-0/g, "-" + counter).replace(/\[0\]/g, "[" + counter + "]");
		lastTableRow.after(content);

		$('#lockerallocation-'+counter+'-id').val('');
		$('#lockerallocation-'+counter+'-updatetype').val('create');
		$('#lockerallocation-'+counter+'-id_locker').val('');
		$('#lockerallocation-'+counter+'-quantity').val('');
		$('tr.locker_allocation:last td:last').html('<button type="button" class="delete-button btn btn-danger"><span class="fa fa-trash"></span></button>');

		var $el = $('#lockerallocation-' + counter + '-id_locker'); // your input id for the HTML select input
		var $el0 = $('#lockerallocation-0-id_locker');
	    
		if (settings = window[$el0.attr('data-krajee-select2')]);
			$el.select2(settings);

		$el.next().next().css("display","none");

		var validation = new Array();

		validation["id_locker"] = jQuery.extend(true, {}, $('#product').yiiActiveForm('find', 'lockerallocation-0-id_locker'));
		validation["id_locker"].id="lockerallocation-"+counter+"-id_locker";
		validation["id_locker"].name="["+counter+"][id_locker]";
		validation["id_locker"].container = ".field-lockerallocation-"+counter+"-id_locker";
		validation["id_locker"].input = "#lockerallocation-"+counter+"-id_locker";
		validation["id_locker"].value = "";
		validation["id_locker"].status = 0;

		validation["quantity"] = jQuery.extend(true, {}, $('#product').yiiActiveForm('find', 'lockerallocation-0-quantity'));
		validation["quantity"].id="lockerallocation-"+counter+"-quantity";
		validation["quantity"].name="["+counter+"][quantity]";
		validation["quantity"].container = ".field-lockerallocation-"+counter+"-quantity";
		validation["quantity"].input = "#lockerallocation-"+counter+"-quantity";
		validation["quantity"].value = "";
		validation["quantity"].status = 0;

		$('#product').yiiActiveForm('add', validation["id_locker"]);
		$('#product').yiiActiveForm('add', validation["quantity"]);

		e.preventDefault();
	});
	
	

	var submit = true;
	var id_product = window.location.href.split("&")[1];
	if(id_product == null){
		id_product = -1;
	}
	else{
		id_product = id_product.split("=")[1];
	}

	$('#product')
	.on("afterValidate", function(e) {
		// location
		if ($(".locker_allocation .form-group.has-error").length > 0){
			$("#modal-location").modal("show");
			submit = false;
		}
		// variation
		else if ($('#variations input.danger').length > 0) {
			$("#modal-variations").modal("show");
			submit = false;
		}
		else if($("#product .form-group.has-error").length > 0){
			submit = false;
		}
		
		// submit = true;
		
		ceksku($("#product-sku").val(), id_product);
	})
	.on("beforeSubmit", function(e) {
		return $("#product .form-group.has-error").length == 0 && submit;
	});

	function ceksku(valna, id){
		$.ajax({
			url : "index.php?r=product/skucheck",
			type : "POST",
			data : {sku : valna, id : id},
			success:function(data){
				if(data == 1){
					$(".field-product-sku").addClass("has-error");
					$(".field-product-sku .help-block").html("SKU has already been taken");
					submit = false;
				}
				else if(data == 0){
					$(".field-product-sku").removeClass("has-error");
					$(".field-product-sku .help-block").html("");
					submit = true;
				}
			}
		});
	}

	var skuValidation = function(){
		var valna = $(this).val();
		ceksku(valna, id_product);
	}

	$("#product-sku").blur(skuValidation).focus(skuValidation)
});

app.controller("productController", function($scope, $http, $func) {
	/*initialize data*/
	var product = $func.init("product");

	/*Pricing*/
	$scope.pricing = {
		regular_price: "0",
		sale_price : "0",
		sale_disc : "10",
		employee_price : "0",
		employee_disc : "10",
		wholesale_price : "0",
		wholesale_disc : "10",
		custom_price : "0",
		custom_disc : "10",
		msrp : "0",
		cost : "0",
		margin : "0",
		markup : "0",
	};

	$scope.g = {
		regular_price: function() {
			$scope.g.sale_disc();
			$scope.g.employee_disc();
			$scope.g.wholesale_disc();
			$scope.g.custom_disc();
			$scope.g.cost();
		},
		sale_price: function() {
			$scope.pricing.sale_disc = Math.round(fn.int(100-$scope.pricing.sale_price/$scope.pricing.regular_price * 100));
		},
		sale_disc: function() {
			$scope.pricing.sale_price = Math.round(fn.int($scope.pricing.regular_price - ($scope.pricing.regular_price * $scope.pricing.sale_disc/100)));
		},
		employee_price: function() {
			$scope.pricing.employee_disc = Math.round(fn.int(100-$scope.pricing.employee_price/$scope.pricing.regular_price * 100));
		},
		employee_disc: function() {
			$scope.pricing.employee_price = Math.round(fn.int($scope.pricing.regular_price - ($scope.pricing.regular_price * $scope.pricing.employee_disc/100)));
		},
		wholesale_price: function() {
			$scope.pricing.wholesale_disc = Math.round(fn.int(100-$scope.pricing.wholesale_price/$scope.pricing.regular_price * 100));
		},
		wholesale_disc: function() {
			$scope.pricing.wholesale_price = Math.round(fn.int($scope.pricing.regular_price - ($scope.pricing.regular_price * $scope.pricing.wholesale_disc/100)));
		},
		custom_price: function() {
			$scope.pricing.custom_disc = Math.round(fn.int(100-$scope.pricing.custom_price/$scope.pricing.regular_price * 100));
		},
		custom_disc: function() {
			$scope.pricing.custom_price = Math.round(fn.int($scope.pricing.regular_price - ($scope.pricing.regular_price * $scope.pricing.custom_disc/100)));
		},
		cost: function() {
			$scope.pricing.margin = Math.round(fn.int(100 * ($scope.pricing.regular_price - $scope.pricing.cost) / $scope.pricing.regular_price));
			$scope.pricing.markup = Math.round(fn.int(100 * ($scope.pricing.regular_price - $scope.pricing.cost) / $scope.pricing.cost));
		},
		margin: function() {
			$scope.pricing.regular_price = Math.round(fn.int($scope.pricing.cost / (1 - $scope.pricing.margin/100)));
			
			$scope.g.sale_disc();
			$scope.g.employee_disc();
			$scope.g.wholesale_disc();
			$scope.g.custom_disc();
			$scope.pricing.markup = Math.round(fn.int(100 * ($scope.pricing.regular_price - $scope.pricing.cost) / $scope.pricing.cost));
		},
		markup: function() {
			$scope.pricing.regular_price = Math.round(fn.int($scope.pricing.cost * ($scope.pricing.markup/100 + 1)));
			
			$scope.g.sale_disc();
			$scope.g.employee_disc();
			$scope.g.wholesale_disc();
			$scope.g.custom_disc();
			$scope.pricing.margin = Math.round(fn.int(100 * ($scope.pricing.regular_price - $scope.pricing.cost) / $scope.pricing.regular_price));
		},
	};

	/*Variations*/
	$scope.variations = {
		error: {
			size: {}, 
			attribute: {},
			quantity: {},
		},
		size: [""],
		attribute: [""],
		quantity: {},
		pos: {x: 0, y: 0},
	};

	$scope.f = {
		total: function(posSize, posAttr) {
			var total = 0;
			var quantity = $scope.variations.quantity;

			// attribute
			if (posAttr!==undefined && posSize===undefined) {
				for (var i in quantity) 
					total += fn.int(quantity[i][posAttr]);
			}
			// size
			else if (posSize!==undefined && posAttr===undefined) {
				for (var i in quantity[posSize]) 
					total += fn.int(quantity[posSize][i]);
			}
			// all
			else {
				for (var i in quantity) {
					for (var j in quantity[i]) 
						total += fn.int(quantity[i][j]);
				}
			}

			return fn.int(total);
		},
		addAttribute: function() {
			$scope.variations.attribute.push("");
		},
		addSize: function() {
			$scope.variations.size.push("");
		},
		validate: function(posSize, posAttr) {
			var isDuplicate = function(arr, index) {
				var coll = angular.copy(arr);
				coll.splice(index, 1);

				if (typeof arr[index]=="string" && arr[index].trim()!="")
					return coll.indexOf(arr[index]) !== -1;
			}

			// attribute
			if (posAttr!==undefined && posSize===undefined) {
				var index = posAttr;
				var type = "attribute";
			}
			// size
			else if (posSize!==undefined && posAttr===undefined) {
				var index = posSize;
				var type = "size";
			}

			// check if the error was valid after changes
			for (var i in $scope.variations.error[type]) {
				if ($scope.variations[type][index]!=$scope.variations[type][i] && !isDuplicate($scope.variations[type], i))
					delete $scope.variations.error[type][i];
			}

			// find duplicates
			var error = isDuplicate($scope.variations[type], index);

			if (error) 
				$scope.variations.error[type][index] = "Terdapat duplikasi untuk "+ type +" '"+ $scope.variations[type][index] +"'";
			else
				delete $scope.variations.error[type][index];
		},
		onChangeQty: function(value) {
			$scope.$apply(function() {
		        $scope.variations.quantity[$scope.variations.pos.x][$scope.variations.pos.y] = value;
		    });
		},
		validateQuantity: function(x, y) {
			var key = x+'-'+y;

			// check empty size
			if (x!==undefined && y===undefined) {
				var removeError = function() {
					delete $scope.variations.error.size[key];
					return false;
				}

				if (fn.isEmpty($scope.variations.size[x])) {
					if (typeof $scope.variations.quantity[x] == "undefined")
						return removeError();

					var exists = false;

					for (var i in $scope.variations.attribute) {
						// value exists
						if (fn.isset($scope.variations.quantity, x, i) && $scope.variations.quantity[x][i].trim()!="") {
							exists = true;
							break;
						}
					}

					if (!exists)
						return removeError();

					$scope.variations.error.size[key] = "Baris Size wajib diisi!";
					return true;
				}
				else return removeError();
			}
			// check empty attribute
			else if (y!==undefined && x===undefined) {
				var removeError = function() {
					delete $scope.variations.error.size[key];
					return false;
				}

				if (fn.isEmpty($scope.variations.attribute[y])) {
					var exists = false;

					for (var i in $scope.variations.size) {
						// value exists
						if (fn.isset($scope.variations.quantity, i, y) && $scope.variations.quantity[i][y].trim()!="") {
							exists = true;
							break;
						}
					}

					if (!exists)
						return removeError();
					
					$scope.variations.error.attribute[key] = "Kolom Attribute wajib diisi!";
					return true;
				}
				else return removeError();
			}
			else {
				// invalid quantity
				if (fn.isset($scope.variations.quantity, x, y) 
						&& new String($scope.variations.quantity[x][y]).trim()!="" 
						&& !fn.is_numeric($scope.variations.quantity[x][y])) {
					$scope.variations.error.quantity[key] = "Nilai tidak valid! (Baris: "+ (x+1) +", Kolom: "+ (y+1) +")";
					return true;
				}
				else delete $scope.variations.error.quantity[key];
			}

			return false;
		}
	};

	// update (data exists)
	if (product.id != null) {		
		// Pricing
		for (var field in $scope.pricing) {
			if (product.hasOwnProperty(field))
				$scope.pricing[field] = product[field];
		}

		// Variations
		$scope.variations = product.variations;
		$scope.oldVariations = angular.copy(product.variations);
	}
});