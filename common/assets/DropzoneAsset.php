<?php namespace common\assets;

use yii\web\AssetBundle;

class DropzoneAsset extends AssetBundle
{
    public $sourcePath = '@webroot/ace/dist';

    public $js = [
        "js/dropzone.min.js",
    ];

    public $css = [
        "css/dropzone.min.css",
    ];
}
