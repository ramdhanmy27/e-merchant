<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SalesChannel;

/**
 * SalesChannelSearch represents the model behind the search form about `common\models\SalesChannel`.
 */
class SalesChannelSearch extends SalesChannel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_sales_channel', 'name', 'description', 'front_link', 'backend_link', 'script1', 'script2', 'script3'], 'safe'],
            [['is_internal'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SalesChannel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'is_internal' => $this->is_internal,
        ]);

        $query->andFilterWhere(['ilike', 'code_sales_channel', $this->code_sales_channel])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'front_link', $this->front_link])
            ->andFilterWhere(['ilike', 'backend_link', $this->backend_link])
            ->andFilterWhere(['ilike', 'script1', $this->script1])
            ->andFilterWhere(['ilike', 'script2', $this->script2])
            ->andFilterWhere(['ilike', 'script3', $this->script3]);

        return $dataProvider;
    }
}
