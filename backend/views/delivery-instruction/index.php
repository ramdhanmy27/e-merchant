<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\SalesChannel;

$this->title = 'Delivery Instruction';
?>

<div class="confirmation-order-index">
	<?= GridView::widget([
		"dataProvider" => $dataProvider,
		"filterModel" => $filterModel,
		"columns" => [
			['class' => 'yii\grid\SerialColumn'],

			'number_order',
			[
				'label' => 'Buyer',
				'attribute' => 'code_buyer',
				'value' => 'buyer.name',
			],
			[
				'label' => 'Channel',
				'attribute' => 'code_sales_channel',
                "filter" => SalesChannel::option("name", "code_sales_channel"),
				'value' => 'salesChannel.name',
			],
			[
				'attribute' => 'order_time',
				'format' => 'datetime',
				"filter" => Html::activeTextInput($filterModel, "order_time", ["class" => "input-date"]),
			],
			[
				'attribute' => 'total_price',
				"options" => ['width' => "130px"],
				'content' => function($model) {
					return currency($model->total_price);
				}
			],
			[
				"class" => "common\yii\grid\ActionColumn",
				"template" => "{deliver}",
				"buttons" => [
					"deliver" => function($url, $model, $id) {
						return Html::a(
							"<i class='glyphicon glyphicon-arrow-right'></i> Deliver", 
							["deliver", "id" => $model->number_order], 
							["modal-lg" => "", "class" => "btn btn-primary"]
						);
					}
				]
			],
		]
	]) ?>
</div>