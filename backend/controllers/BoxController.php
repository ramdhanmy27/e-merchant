<?php namespace backend\controllers;

use common\models\Box;
use common\yii\web\Controller;
use common\yii\db\db;
use Yii;


class BoxController extends \common\yii\web\Controller
{
    use \common\yii\web\CrudControllerTrait;

    public function actionCreate()
    {
        $model = new Box();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->number_box]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id){
    	$model = $this->findModel($id);

    	if ($model->load(Yii::$app->request->post()) && $model->save()) {
    	    return $this->redirect(['view', 'id' => $model->number_box]);
    	} else {
    	    return $this->render('update', [
    	        'model' => $model,
    	    ]);
    	}
    }

    public function actionNumber_rack_check(){
    	$data 	= $this->request->post("data");
    	// $query = db::query('box', '*')->where("number_rack",)->all();
    	$query = db::run("SELECT max(number_box) FROM box WHERE number_rack=:nr", ["nr" => $data])->queryScalar();
    	$rack  = db::run("SELECT level_count FROM rack WHERE number_rack=:nr", ["nr" => $data])->queryScalar();

    	return json(array("data" => $data, "result" => $query, "rack" => $rack));
    }
}