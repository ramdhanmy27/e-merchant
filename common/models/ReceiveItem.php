<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "receive_item".
 *
 * @property integer $id_receive_item
 * @property integer $qty
 * @property string $id_receiving
 * @property string $number_product
 * @property string $code_size
 * @property string $number_product_color
 *
 * @property Product $product
 * @property ProductColor $productColor
 * @property ProductSize $productSize
 * @property Receiving $receiving
 */
class ReceiveItem extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'receive_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            // [['id_receive_item'], 'required'],

            // unique
            // [['id_receive_item'], 'unique'],

            // safe

            // integer
            [['qty'], 'integer'],
            [['id_receiving'], 'integer'],

            // string
            [['code_size'], 'string', 'max' => 10],
            [['number_product'], 'string', 'max' => 10],
            [['number_product_color'], 'string', 'max' => 20],

            // exist
            [['number_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['number_product' => 'number_product']],
            [['number_product_color'], 'exist', 'skipOnError' => true, 'targetClass' => ProductColor::className(), 'targetAttribute' => ['number_product_color' => 'number_product_color']],
            [['code_size'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSize::className(), 'targetAttribute' => ['code_size' => 'code_size']],
            [['id_receiving'], 'exist', 'skipOnError' => true, 'targetClass' => Receiving::className(), 'targetAttribute' => ['id_receiving' => 'id_receiving']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_receive_item' => 'Id Receive Item',
            'qty' => 'Qty',
            'id_receiving' => 'Receiving Number',
            'number_product' => 'Id Product',
            'code_size' => 'Id Product Size',
            'number_product_color' => 'Id Product Color',
            'product.name' => 'Product',
            'productSize.name' => 'Product Size',
            'productColor.color' => 'Product Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductColor()
    {
        return $this->hasOne(ProductColor::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSize()
    {
        return $this->hasOne(ProductSize::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiving()
    {
        return $this->hasOne(Receiving::className(), ['id_receiving' => 'id_receiving']);
    }
}
