<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Merchant */

$this->title = 'Update Box: ' . ' ' . $model->number_box;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="merchant-update">
    <?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>
</div>
