<?php namespace common\yii\db;

Interface DraftModelInterface
{
    const STATUS_DRAFT = "0";
    const STATUS_SAVE = "1";

    /**
     * check data status
     * @return boolean
     */
    public function isDraft();

    /**
     * get new unique master code
     * @return string
     */
    public static function getCode();
}
