<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="product-form" ng-controller="productController">
    <?php $form = ActiveForm::begin(["id" => "product", 'options' => ['enctype'=>'multipart/form-data', 'name' => 'product_form']]); ?>

    <div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
            <!-- Basic Information -->
            <div class='widget-box'>
                <div class="widget-header">
                    <h5 class="widget-title">Basic Information</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <?= $form->field($model, 'number_product')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'code_category')->widget(Select2::classname(), [
                            'data' => common\models\Category::option(),
                            'options' => ['placeholder' => 'Select Category'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]) ?>

                        <?= $form->field($model, 'brand_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'description')->textArea() ?>

                        <?= $form->field($model, 'group_size')->widget(Select2::classname(), [
                            'data' => common\models\Product::getGroupSize(),
                            'options' => ['placeholder' => 'Select Group Size'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]) ?>

                        <?= $form->field($model, 'basic_price')->textInput() ?>
                    </div>
                </div>
            </div>
            
        </div>

        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
            <!-- Product Color -->
            <div class='widget-box'>
                <div class="widget-header">
                    <h5 class="widget-title">Product Color</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <table class="table table-responsive table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Color</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="(x, y) in f.data track by x" class="color">
                                    <td>
                                        <?= Html::input('hidden', 'color[{{x}}][number_product_color]', '{{y.number_product_color}}') ?>

                                        <?= Html::input('text', 'color[{{x}}][color]', '',
                                            ['ng-model'=>'f.data[x].color', 'required' => '']) ?>
                                        <div ng-messages='product_form["color["+x+"][color]"].$error' 
                                             ng-if='product_form["color["+x+"][color]"].$dirty'>
                                            <div ng-message="required">This field is required</div>
                                        </div>
                                    </td>
                                    
                                    <td>
                                        <?= Html::button('<span class="fa fa-trash"></span>', ['class' => 'delete-color btn btn-danger', "ng-click" => 'f.deleteRow(x)']) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                    <div class="col-md-12">
                                    <div class="form-group">
                                        <?= ""/*Html::button('Add more product', ['class' => 'btn btn-default'])*/ ?>
                                        <?= Html::button('Add row', ['name' => 'addRow', 'value' => 'true', 'class' => 'btn btn-info', "ng-click" => "f.addRow()"]) ?>
                                    </div>
                                    </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <hr>
</div>

<?php
    $productColor = [];
    foreach($model->productColors as $key => $value){
        $productColor[$key] = $value->attributes;
        $productColor[$key]['number_product_color'] = $value->number_product_color;
    }
?>
<script>
    window.data = {
        productColor: <?= json_encode($productColor) ?>
    }
</script>