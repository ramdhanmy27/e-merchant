<?php namespace common\yii\web;

/**
 * TODO
 * permissions
 * handling param
 */

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * CRUD trait
 * @property array $param
 *   @param string model
 *   @param string id (optional)
 *   @param string detail_model
 *   @param string detail_id (optional)
 *   @param string detail_parent_id (optional)
 */
trait MasterDetailControllerTrait {
    use \common\yii\web\CrudControllerTrait;

    /**
     * mengambil field primary dari table master
     * @return string
     */
    public function getParentPrimary() {
        return isset($this->param['detail_parent_id']) ? $this->param['detail_parent_id'] : $this->param["id"];
    }


    public function beforeAction($action) {
        $this->breadcrumb = [
            "index" => [
                "label" => $this->name(),

                "child" => [
                    "view" => ["label" => "[[id]]"],
                    "create" => ["label" => "Tambah"],
                    "update" => [
                        "label" => '{{$detail_model::name() or $model->name()}}',
                        "url" => ['update', "id" => "[[parent]]"],

                        "child" => [
                            "view-detail" => [
                                "label" => "[[id]]",
                                "url" => ["view-detail", "id" => "[[id]]", "parent" => "[[parent]]"],

                                "child" => [
                                    "update-detail" => ["label" => "Edit"],
                                ],
                            ],
                            "create-detail" => ["label" => "Tambah"],
                        ]
                    ],
                ]
            ]
        ];

        return parent::beforeAction($action);
    }

    /*Action*/

    /**
     * Updates an existing master data.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param mixed $id
     * @return mixed
     */
    public function actionUpdate($id) {
        // master
        $model = $this->findModel($id);
        $parent_primary = $this->getParentPrimary();

        // detail
        $model_class_child = $this->param["detail_model"];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->{$parent_primary}]);
        } 
        else {
            return $this->render(
                [
                    "@app/views/$this->id/update",
                    "@common/views/master/detail/update-master",
                ],
                [
                    'parent' => $id,
                    'model' => $model,
                    'detail_model' => $model_class_child,
                    'child_list' => \yii\grid\GridView::widget([
                        'dataProvider' => new ActiveDataProvider([
                            'query' => $model_class_child::find()->where([$parent_primary => $id])
                        ]),
                        'columns' => array_merge(
                            [['class' => 'yii\grid\SerialColumn']],
                            array_keys((new $model_class_child)->attributeLabels()),
                            [[
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'update' => function ($url, $model) {
                                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ['modal' => ""]);
                                    }
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) use ($id) {
                                    return Url::to(["$action-detail", "parent" => $id, "id" => $key]);
                                }
                            ]]
                        ),
                        
                    ])
                ]
            );
        }
    }

    /**
     * Updates an existing detail data.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param mixed $parent
     * @param mixed $id
     * @return mixed
     */
    public function actionUpdateDetail($parent, $id) {
        $model = $this->findModelDetail($id, $parent);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-detail', 'id' => $model->{$this->param["detail_id"]}, "parent" => $parent]);
        }
        else {
            return $this->render(
                [
                    "@app/views/$this->id/detail/update",
                    "@app/views/$this->id/update-detail",
                    "@common/views/master/detail/update",
                ],
                [
                    'model' => $model,
                    'parent' => $parent,
                    'id' => $id,
                ]
            );
        }
    }

    /**
     * Displays a single data.
     * @param integer $id
     * @return mixed
     */
    public function actionViewDetail($parent, $id) {
        $model = $this->findModelDetail($id);

        return $this->render(
            [
                "@app/views/$this->id/detail/view",
                "@app/views/$this->id/view-detail",
                "@common/views/master/detail/view",
            ],
            [
                'model' => $model, 
                'parent' => $parent,
                'id' => $id,
            ]
        );
    }

    /**
     * Deletes an existing detail data.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteDetail($parent, $id) {
        $this->findModelDetail($id)->delete();

        return $this->redirect(['update', "id" => $parent]);
    }

    /**
     * Creates a new data.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateDetail($parent) {
        $model_class = $this->param["detail_model"];

        $model = new $model_class();
        $model->{$this->getParentPrimary()} = $parent;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-detail', 'id' => $model->{$this->param["detail_id"]}, "parent" => $parent]);
        }
        else {
            return $this->render(
                [
                    "@app/views/$this->id/detail/create",
                    "@app/views/$this->id/create-detail",
                    "@common/views/master/detail/create",
                ],
                [
                    'model' => $model,
                    'parent' => $parent,
                ]
            );
        }
    }

    /**
     * Finds the data based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelDetail($id, $parent = null) {
        $model = $this->param["detail_model"];
        $model = $model::findOne($id);

        if ($model!==null && ($parent===null || $model->{$this->getParentPrimary()}==$parent))
            return $model;
        
        $this->abort(404);
    }
}