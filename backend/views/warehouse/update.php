<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Warehouse */

$this->title = 'Update Warehouse: ' . $model->name;
?>

<div class="warehouse-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
