<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SalesChannel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sales Channels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-channel-view">
    <?php if(!$this->request->isAjax) { ?>
        <p>
        <?php if(access("update")){ ?>
            <?= Html::a('Update', ['update', 'id' => $model->code_sales_channel], ['class' => 'btn btn-primary', "modal"=>""]) ?>
        <?php } ?>

        <?php if(access("delete")){ ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->code_sales_channel], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?> 
        <?php } ?>
        </p>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code_sales_channel',
            'code',
            'name',
            'description',
            'is_internal:boolean',
            'front_link',
            'backend_link',
            'script1',
            'script2',
            'script3',
        ],
    ]) ?>

</div>
