<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Warehouse */

?>

<div class="warehouse-index">
    <?php if (access("create")) : ?>
        <p> <?= Html::a('Create Warehouse', ['create'], ['class' => 'btn btn-success']) ?> </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'code_warehouse',
            'name',
            'city',
            'address',
            'rack_count:integer',
            
            [
                'class' => 'common\yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'nowrap'],
                'template' => "{view} {update} {rack-layout} {rack-config} {delete}",
                'buttons' => [
                    "update" => function($url, $model, $id) {
                        return Html::a(
                            "<i class='glyphicon glyphicon-pencil'></i>", $url, 
                            ["title" => "Update"]
                        );
                    },
                    "rack-layout" => function($url, $model, $id) {
                        return Html::a(
                            "<i class='glyphicon glyphicon-blackboard text-default'></i>", $url, 
                            ["title" => "Rack Layout"]
                        );
                    },
                    "rack-config" => function($url, $model, $id) {
                        return Html::a(
                            "<i class='glyphicon glyphicon-cog text-warning'></i>", $url, 
                            ["title" => "Rack Configuration"]
                        );
                    },
                ]
            ],
        ],
    ]); ?>
</div>
