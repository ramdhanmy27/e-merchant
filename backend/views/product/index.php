<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
?>

<div class="product-index">
    <?php if(!$this->request->isAjax) { ?>
        <p>
        <?php if($this->can("create")){ ?>
            <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
        <?php } ?>
        </p>
    <?php } ?>
    
    <div class="forscrollhor">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                
                'number_product',
                'brand_name',
                'category.name',
                'merchant.name',
                'name',
                'description',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>