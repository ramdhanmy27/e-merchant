<?php namespace common\yii\web;

use Yii;
use common\yii\db\db;
use common\yii\db\DraftModelInterface;
use yii\base\ErrorException;
use yii\helpers\Html;
use yii\helpers\BaseInflector;

trait DraftControllerTrait
{
	use CrudControllerTrait {
		actionUpdate as private;
		beforeAction as beforeActionTrait;
	}

	public static function permissions() {
		return ["create", "draft", "delete", "view"];
	}

	public function behaviors() {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
                'save' => ['post'],
            ]),
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['create'], "create"],
                [['draft'], "draft"],
                [['delete'], "delete"],
            ]),
        ];
    }

    public function beforeAction($action) {
    	if (!isset($this->breadcrumb))
			$this->breadcrumb = [
				"index" => [
					"label" => $this->name(),
					"child" => [
						"view" => [
							"label" => "[[get:id]]",
							"url" => ["view", "id"=>"[[get:id]]"],
							"child" => [
								"draft" => ["label" => "Draft"],
							]
						],
						"create" => ["label" => "Create"],
					]
				]
			];

    	$return = $this->beforeActionTrait($action);
    	Yii::setAlias("draft", "@common/views/draft");

		try {
			$interfaces = class_implements($this->param("model"));

			if (!isset($interfaces[DraftModelInterface::class]))
				throw new ErrorException($this->param("model")." class must implement interface ".DraftModelInterface::class);
		}
		catch(ErrorException $e) {
			Yii::error($e->getMessage());
		}

		return $return;
    }

	/* Action Pages */

	/**
	 * Lists all data.
	 * @return mixed
	 */
    public function actionIndex() {
    	$class = $this->param("model");
    	$model = $this->param("model-search");
        $search_model = new $model();
        $model_name = BaseInflector::camelize($this->id)."Search";

    	return view([
			"@app/views/$this->id/index",
			"@common/views/draft/index",
		],
		[
    		'column' =>  array_merge(
				[['class' => 'yii\grid\SerialColumn']],
				array_keys((new $class)->attributeLabels()),
				[[
					'class' => 'yii\grid\ActionColumn',
					"template" => "{view}",
				]]
			),
    		"list" => [
    			"dataProvider" => $search_model->search(
    				$this->request->queryParams + [$model_name => ["status" => $model::STATUS_SAVE]]
    			),
    			"filterModel" => $search_model,
    		],
    		"draft" => [
    			"dataProvider" => $search_model->search(
    				$this->request->queryParams + [$model_name => ["status" => $model::STATUS_DRAFT]]
    			),
    			"filterModel" => $search_model,
    		],
    	]);
    }
    
	/**
	 * create data master
	 * redirect ke halaman draft dengan data master yang baru dibuat
	 * @return response
	 */
	public function actionCreate() {
		$model = $this->param("model");
		$code = $model::getCode();
		db::insert($model::tableName(), [$this->param("id") => $code, "status" => $model::STATUS_DRAFT])->execute();

		return $this->redirect(["draft", "id" => $code]);
	}

	/**
	 * halaman draft
	 * @param  string $id master ID
	 * @return view
	 */
	public function actionDraft($id) {
		$model = $this->findModel($id);

		if (!$model->isDraft()) {
			return $this->abort();
		}

		return $this->render("@draft/draft", [
			"id" => $model->{$this->param("id")},
			"model" => $model,
		]);
	}

	/**
	 * delete master
	 * @param  string $id master ID
	 * @return response
	 */
	public function actionSave($id) {
		$model = $this->param("model");
		db::update($model::tableName(), ["status" => $model::STATUS_SAVE], $this->param("id")."=:id", ["id" => $id])->execute();

		return redirect(["index"]);
	}

	/**
     * ambil data pertama kali saat halaman draft di load
     * @param  string $id master ID
     * @param  string $itemID item ID
     * @return json
     */
	abstract public function actionGetDraftData($id, $itemID=null);

	/**
     * POST - delete product item
     * @param  string $id master ID
     */
    abstract public function actionDeleteItem($id);
}
