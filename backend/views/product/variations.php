<?php \common\assets\VariationsAsset::register($this); ?>

<div id='variations' class='row' ng-controller="variationsController">
	<!-- alert -->
	<div class="alert alert-danger" ng-repeat="(key, messages) in variations.error track by key" ng-if="!(messages | isEmpty)">
		<li ng-repeat="msg in messages track by $index">{{msg}}</li>
	</div>

	<div class='col-xs-12'>
		<div class='table-wrapper pull-left'>
			<!-- header (Attributes) -->
			<div class='data-row'>
				<div class='cell text-center label'>
					<b>Size \ Attribute</b>
				</div>

				<!-- attributes -->
				<div ng-repeat='(y, attribute) in variations.attribute track by y' class='cell text-center label label-info'>
					<b title="{{variations.attribute[y]}}">{{variations.attribute[y]}}</b>
				</div>

				<!-- total col -->
				<div class='cell label'> <b>Total (Size)</b> </div>
			</div>

			<div ng-repeat='(x, size) in variations.size track by x'>
				<!-- content -->
				<div class='data-row'>
					<!-- label size -->
					<div class='cell text-center label label-info'> 
						<b title="{{variations.size[x]}}" data-placement="left">{{variations.size[x]}}</b> 
					</div>

					<!-- data -->
					<div ng-repeat='(y, attribute) in variations.attribute track by y' class='cell'>
						<input type='text' ng-model='variations.quantity[x][y]' class='form-control' 
							name="variations[qty][{{x}}][]" ng-class="{danger:f.validateQuantity(x, y)}" id="_{{x}}-{{y}}"/>
					</div>

					<!-- total -->
					<div class='cell'>
						<input type='text' ng-value='f.total(x)' class='form-control' disabled />
					</div>
				</div>

				<!-- last row (total) -->
				<div class='data-row' ng-if="f.isLastRow(x)">
					<div class='cell label'> <b>Total (Attribute)</b> </div>

					<!-- data -->
					<div ng-repeat='(y, attribute) in variations.attribute track by y' class='cell'>
						<input type='text' class='form-control' ng-value='f.total(undefined, y)' disabled />
					</div>

					<!-- all total -->
					<div class='cell'>
						<input type='text' class='form-control' ng-value='f.total()' disabled />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if (isset($data) && is_array($data)) { ?>
	<script type="text/javascript">
		if (typeof window.data == 'undefined')
			window.data = {};

		window.data.variations = <?= json_encode($data) ?>
	</script>
<?php } ?>