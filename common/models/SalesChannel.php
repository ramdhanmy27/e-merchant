<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sales_channel".
 *
 * @property string $code_sales_channel
 * @property string $name
 * @property string $description
 * @property boolean $is_internal
 * @property string $front_link
 * @property string $backend_link
 * @property string $script1
 * @property string $script2
 * @property string $script3
 *
 * @property Allocmutation[] $allocmutations
 * @property SalesOrder[] $salesOrders
 * @property StockAllocation[] $stockAllocations
 */
class SalesChannel extends \common\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'sales_channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['code_sales_channel'], 'required'],

            // unique
            [['code_sales_channel'], 'unique'],

            // safe

            // integer
            [['is_internal'], 'boolean'],

            // string
            [['code_sales_channel'], 'string', 'max' => 4],
            [['name'], 'string', 'max' => 16],
            [['front_link', 'backend_link'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 32],
            [['script1', 'script2', 'script3'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_sales_channel' => 'Code',
            'name' => 'Name',
            'description' => 'Description',
            'is_internal' => 'Is Internal',
            'front_link' => 'Front Link',
            'backend_link' => 'Backend Link',
            'script1' => 'Script1',
            'script2' => 'Script2',
            'script3' => 'Script3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocmutations()
    {
        return $this->hasMany(Allocmutation::className(), ['code_sales_channel' => 'code_sales_channel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrders()
    {
        return $this->hasMany(SalesOrder::className(), ['code_sales_channel' => 'code_sales_channel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockAllocations()
    {
        return $this->hasMany(StockAllocation::className(), ['code_sales_channel' => 'code_sales_channel']);
    }
}
