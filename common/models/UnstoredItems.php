<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unstored_items".
 *
 * @property integer $id_unstored_items
 * @property integer $qty
 * @property string $sku
 *
 * @property InventoryItem $inventoryItem
 */
class UnstoredItems extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unstored_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            // [['id_unstored_items'], 'required'],

            // unique
            // [['id_unstored_items'], 'unique'],

            // safe

            // integer
            [['qty'], 'integer'],

            // string
            [['sku'], 'string', 'max' => 255],

            // exist
            [['sku'], 'exist', 'skipOnError' => true, 'targetClass' => InventoryItem::className(), 'targetAttribute' => ['sku' => 'sku']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_unstored_items' => 'Id Unstored Items',
            'qty' => 'Qty',
            'sku' => 'Inventory Item',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItem()
    {
        return $this->hasOne(InventoryItem::className(), ['sku' => 'sku']);
    }
}
