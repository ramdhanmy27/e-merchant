<?php

//use barcode\barcode\BarcodeGenerator;
use common\barcode\GeneratedCodebar;
use common\models\Product;
use common\yii\db\db;

$product  = db::query('product', '*')->where(["number_product" => $number_product])->one();
$productColor = db::query("product_color")->where(["number_product" => $number_product])->all();
$groupsize  = db::query("product_size", "*")->where(["group_size" => $product["group_size"]])->all();
$inventory = [];
$datana = db::query("inventory_item", "*")->where([
    "number_product" => $number_product,
    "code_warehouse" => Yii::$app->user->identity->code_warehouse,
])->all();

foreach($datana as $key=>$data){
    $inventory[0][$datana[$key]['code_size']][$datana[$key]['number_product_color']] = $data['qty'];
    $inventory[1][$datana[$key]['code_size']][$datana[$key]['number_product_color']] = $data['sku'];
}

$product       = $product; 
$product_color = $productColor;
$group_size    = $groupsize;
$inventory     = $inventory;

?>

<div class='kertas_print'>
    <?php
        // prins($product, $product_color, $inventory, $group_size);
        $c = 0;
        foreach($product_color as $pc){ 
            $b = 0;
            foreach($inventory[0] as $key => $i){
                $check = [];
                foreach($i as $key2 => $pcn){
                    $check[] = $key2;
                }
                if(in_array($pc['number_product_color'], $check)){
                    $jml = $i[$pc['number_product_color']];
                    for($a=1;$a<=$jml;$a++){
                        /*${'barcode'.$c.'_'.$b.'_'.$a} = array(
                            'elementId'=> 'showBarcode'.$c.'_'.$b.'_'.$a, // div or canvas id
                            'value'=> $inventory[1][$key][$pc['number_product_color']], // value for EAN 13 be careful to set right values for each barcode type 
                            'type'=>'code93', // supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix
                        );*/ ?>
                            <div class='labels' style="margin-right: 2px !important;">
                                <div class="container-inside">
                                    <div style='text-align: left; ' class="text"><?=$product['name']?></div>
                                    <div style='text-align: left; ' class="text"><?=$product['brand_name']?></div>
                                    <div class="container-float" class="text">
                                        <div style='float: left; width: 28%'>size: <?=$key?></div>
                                        <div style='float: left; width: 70%'>color: <?=$pc['color']?></div>
                                    </div>
                                    <?php
                                    ob_start();
                                    $barcode = new GeneratedCodebar();
                                    $barcode->message = $inventory[1][$key][$pc['number_product_color']];
                                    $barcode->run();
                                    echo "<img class='barcode-container' src='data:image/png;base64,".base64_encode(ob_get_clean())."'>";
                                    ?>
                                    <!-- <div style='text-align: left; font: 10pt arial; text-align: center;' class="barcode-container" id="showBarcode<?=$c?>_<?=$b?>_<?=$a?>"></div> -->
                                </div>
                            </div>
                        <?php
                        //echo BarcodeGenerator::widget(${'barcode'.$c.'_'.$b.'_'.$a});
                    }
                }
            $b++;
            }
        $c++;
        }
    ?>
</div>