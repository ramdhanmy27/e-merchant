<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use common\yii\db\db;

/**
 * This is the model class for table "bank_transaction".
 *
 * @property string $number_transaction
 * @property string $transaction_time
 * @property string $description
 * @property boolean $is_debet
 * @property string $transaction_value
 * @property string $code_bank
 *
 * @property BankAccount $bankAccount
 * @property SalesOrder[] $salesOrders
 */
class BankTransaction extends \common\yii\db\ActiveRecord
{
	public $path = "@webroot/images/confirmation-order", 
		 $url = "@web/images/confirmation-order",
		 $ext = [".gif", ".png", ".jpg", ".jpeg"];

	public function __construct() {
		$this->path = Yii::getAlias($this->path);
		$this->url = Yii::getAlias($this->url);
	}
		
	public static function tableName()
	{
		return 'bank_transaction';
	}

	public function rules()
	{
		return [
			// required
			[['number_transaction'/*, 'code_bank'*/], 'required'],

			// unique
			[['number_transaction'], 'unique'],

			// safe
			[['transaction_time'], 'safe'],

			// integer
			[['transaction_value'], 'number'],
			[['is_debet'], 'boolean'],

			// string
			[['code_bank'], 'string', 'max' => 4],
			[['number_transaction'], 'string', 'max' => 12],

			// exist
			[['code_bank'], 'exist', 'skipOnError' => true, 'targetClass' => BankAccount::className(), 'targetAttribute' => ['code_bank' => 'code_bank']],
		];
	}

	public function attributeLabels()
	{
		return [
			'number_transaction' => 'Transaction Number',
			'transaction_time' => 'Transaction Time',
			'description' => 'Description',
			'is_debet' => 'Is Debet',
			'transaction_value' => 'Transaction Value',
			'code_bank' => 'Bank Account ID',
		];
	}

	/**
	 * upload transfer receipt image 
	 * @param  string $file filepath
	 * @return boolean
	 */
	public function uploadReceipt($file) {
		// model is new record
		// invalid uploaded file
		// path is not readable and writeable
		if ($this->isNewRecord || !is_string($file) || !file_exists($file) || !FileHelper::createDirectory($this->path) 
				|| !in_array(image_type_to_extension(exif_imagetype($file)), $this->ext))
			return false;

		move_uploaded_file($file, $this->path."/".$this->number_transaction.".jpg");
		return true;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBankAccount()
	{
		return $this->hasOne(BankAccount::className(), ['code_bank' => 'code_bank']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSalesOrders()
	{
		return $this->hasMany(SalesOrder::className(), ['number_transaction' => 'number_transaction']);
	}
}
