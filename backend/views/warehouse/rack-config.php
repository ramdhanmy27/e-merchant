<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;

register(["@js/warehouse/rack-config.js", "@css/warehouse/rack-config.css"], [
	"depends" => [
		"common\assets\AngularAsset",
		"backend\assets\AceAsset",
	]
]);

$this->title = ucwords($model->name);
?>

<div class='warehouse-rack-config' ng-controller="rackConfigController">
	<div class="row col-md-8 text-right">
		<?php $form = ActiveForm::begin(["options" => ["form-validation" => ""]]); ?>
			<table class="table table-bordered table-striped table-responsive">
				<thead>
					<tr> <th>Rack</th> <th>Level</th> <th>Rack Capacity</th> <th>Qty Box</th> </tr>
				</thead>

				<tbody>
					<?php 
						$tpl = ["template" => "<div class='col-md-12'>{input}\n{hint}\n{error}</div>"];

						foreach($model->getRacks()->orderBy("number_rack")->each() as $i => $rack) : 
							$box_exists = $rack->box_count!=null;
					?>
						<tr ng-init="
							box.capacity[<?=$i?>]='<?=$rack->box_capacity!=null ? $rack->box_capacity : "" ?>'; 
							box.qty[<?=$i?>]='<?=$rack->box_count!=null ? $rack->box_count : ""?>';
						">
							<th class="text-center">
								<?= $rack->number_rack ?>
								<?= Html::input("hidden", "Rack[$i][number_rack]", $rack->number_rack) ?>
							</th>
							<td><?= $form->field($rack, "[$i]level_count", $tpl)->textInput() ?></td>
							<td> <?= $form->field($rack, "[$i]box_capacity", $tpl)->textInput(["ng-model" => "box.capacity[$i]"]) ?> </td>
							<td>
								<?= $form->field($rack, "[$i]box_count", $tpl)->textInput([
									"ng-model" => "box.qty[$i]",
									"disabled" => $box_exists ? "disabled" : null,
								]) ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>

				<thead>
					<tr class="table-summary">
						<th colspan="2" class="text-right"> Default Box Capacity </th>
						<th> 
							<input number name="default_capacity" class="form-control" ng-model="default_capacity" 
								title="Default Box Capacity will only affect to the new box" min="1" /> 
						</th>
						<th>
							<label class="col-md-6"><b>Total :</b></label> 
							<div class="col-md-6 text-right">{{f.total()}}</div>
						</th>
					</tr>
				</thead>
			</table>

			<button type="submit" class="btn btn-success">
				<i class="glyphicon glyphicon-ok"></i> Save
			</button>
		<?php ActiveForm::end(); ?>
	</div>
</div>