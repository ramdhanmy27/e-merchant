<?php

namespace common\yii\grid;

use Yii;
use Closure;
use yii\helpers\Html;
use yii\helpers\Url;

class ActionColumn extends \yii\grid\ActionColumn
{
    public function init() {
        parent::init();

        foreach ($this->buttons as $perm => $view) {
            // permission not granted -> hide the button
            if (perm_exists($perm) && !access($perm))
                unset($this->buttons[$perm]);
        }
    }

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons() {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key) {
                return Html::a(
                    '<span class="glyphicon glyphicon-eye-open text-muted"></span>', $url, 
                    array_merge([
                        'title' => Yii::t('yii', 'View'),
                        'aria-label' => Yii::t('yii', 'View'),
                        'data-pjax' => '0',
                        'target' => '_blank',
                    ], $this->buttonOptions)
                );
            };
        }

        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                return Html::a(
                    '<span class="glyphicon glyphicon-pencil"></span>', $url,
                    array_merge([
                        'title' => Yii::t('yii', 'Update'),
                        'aria-label' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ], $this->buttonOptions)
                );
            };
        }

        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                return Html::a(
                    '<span class="glyphicon glyphicon-trash text-danger"></span>', $url, 
                    array_merge([
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item ?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ], $this->buttonOptions)
                );
            };
        }
    }
}
