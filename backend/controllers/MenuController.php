<?php

namespace backend\controllers;

use Yii;
use common\models\Menu;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\db\Query;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends \common\yii\web\Controller
{
	use \common\yii\web\CrudControllerTrait {
		actionView as private;
	}

    public function behaviors() {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
                'position' => ['post'],
                'status' => ['post'],
            ]),
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['create'], "create"],
                [['update', 'status', "position"], "update"],
                [['delete'], "delete"],
            ]),
        ];
    }
		
	/**
	 * Lists all data.
	 * @return mixed
	 */
	public function actionIndex() {	
		Menu::reorder();

		return view([
			'query' => Menu::select('order by r.order_path'),
		]);
	}

	/**
	 * Creates a new data.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($parent = null) {
		$model = new Menu();

		if ($model->load($this->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		} 
		else {
			return view([
				'model' => $model,
				'parent' => $parent,
			]);
		}
	}

	/**
     * Updates an existing data.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

		unset($this->breadcrumb["index"]["child"]["view"]);
		$this->breadcrumb["index"]["child"]["update"] = [
			"label" => "Update : $model->title"
		];

        if ($this->request->isPost) {
        	$post = $this->request->post();

        	// if parent updated, put order to last
	        if ($model->parent!=$post["Menu"]["parent"])
	        	$model->order = (new Query)->from("menu")
				    				->select('max("order")+1')
				    				->where(["parent" => !empty($post["Menu"]["parent"]) ? $post["Menu"]["parent"] : null])
				    				->scalar();

        	$model->load($post);
        	$model->save();

            return $this->redirect(['index']);
        } 
        else return view(['model' => $model]);
    }

	/**
	 * change menu status (enable : 1|disable : 0)
	 * @param integer $id
	 * @return mixed
	 */
	public function actionStatus($id) {
		$model = $this->findModel($id);
		$model->enable = !$model->enable;
		$model->save();

		return $this->redirect(['index']);
	}

	/**
	 * update menu position
	 * ajax request only
	 */
	public function actionPosition() {
		if (!$this->request->isAjax)
			return $this->redirect(['index']);
		
		$post = $this->request->post();
		$this->findModel($post['id'])->updatePosition($post['order'], $post['parent']);
		exit;
	}
}
