<?php

namespace backend\controllers;

use Yii;
use common\models\Product;
use common\models\ProductSearch;
use common\models\ProductImages;
use common\models\LockerAllocation;
use common\yii\db\db;
use common\yii\web\Controller;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    use \common\yii\web\CrudControllerTrait;

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteImage($id, $product) {
        $delete = false;

        if ($model = ProductImages::findOne($id)) 
            $delete = $model->delete();
        
        if (!$this->request->isAjax) {
            if ($delete === true)
                $this->session->setFlash("success", "Successfully delete image");
            
            return $this->redirect(["update", "id" => $product]);
        }
    }

    public function actionCreate() {
        $model  = new Product();
        $post = $this->request->post();

        $lockerAllocations = [];
        $formLockerAllocations = Yii::$app->request->post('LockerAllocation',[]);

        foreach ($formLockerAllocations as $formLockerAllocation) {
            $lockerAllocation = new LockerAllocation(['scenario' => LockerAllocation::SCENARIO_BATCH_UPDATE]);
            $lockerAllocation->setAttributes($formLockerAllocation);
            $lockerAllocations[] = $lockerAllocation;
        }

        if ($model->load($post) && $model->validate() && Model::validateMultiple($lockerAllocations) && $model->save(false)) {
            // save locker
            foreach ($lockerAllocations as $lockerAllocation) {
                $lockerAllocation->id_product = $model->id;
                $lockerAllocation->save(false);
            }

            $model->uploadImages("images"); 

            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        $lockerAllocations[] = new LockerAllocation(['scenario' => LockerAllocation::SCENARIO_BATCH_UPDATE]);
        
        return $this->render('create', [
            'model' => $model,
            'lockerAllocations' => $lockerAllocations,
        ]);
    }

    public function actionSkucheck(){
        $post = $this->request->post();
        
        if (count($post) > 0)
            return Product::find()->where(["sku" => $post["sku"]])->andWhere(["!=", "id", $post["id"]])->count();
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $post = $this->request->post();

        $lockerAllocations = $model->lockerAllocations;
        $formLockerAllocations = Yii::$app->request->post('LockerAllocation', []);

        foreach ($formLockerAllocations as $i => $formLockerAllocation) {
            // loading models if they are not new
            if (isset($formLockerAllocation['id']) && isset($formLockerAllocation['updateType']) 
                    && $formLockerAllocation['updateType']!=LockerAllocation::UPDATE_TYPE_CREATE) {
                $lockerAllocation = LockerAllocation::findOne([
                    'id' => $formLockerAllocation['id'], 
                    'id_product' => $model['id'],
                ]);
                $lockerAllocation->setScenario(LockerAllocation::SCENARIO_BATCH_UPDATE);
                $lockerAllocation->setAttributes($formLockerAllocation);
                $lockerAllocations[$i] = $lockerAllocation;
            } 
            else {
                $lockerAllocation = new LockerAllocation(['scenario' => LockerAllocation::SCENARIO_BATCH_UPDATE]);
                $lockerAllocation->setAttributes($formLockerAllocation);
                $lockerAllocations[] = $lockerAllocation;
            }
        }

        if ($model->load($post) && Model::validateMultiple($lockerAllocations) && $model->save()) {
            foreach($lockerAllocations as $lockerAllocation) {
                if ($lockerAllocation->updateType == LockerAllocation::UPDATE_TYPE_DELETE) {
                    $lockerAllocation->delete();
                } 
                else {
                    $lockerAllocation->id_product = $model->id;
                    $lockerAllocation->save();
                }
            }

            // Images
            $model->uploadImages("images");

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'lockerAllocations' => $lockerAllocations,
        ]);
    }
}
