<?php namespace backend\controllers;

use common\models\DeliveryInstruction;
use common\models\PickingBatch;
use common\models\SalesOrder;
use common\models\Box;
use common\yii\db\db;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class BatchDeliveryController extends \common\yii\web\Controller {
	public $breadcrumb;
	public static $permissions = ["view", "deliver"];

	public function behaviors() {
		$this->breadcrumb = [
			"index" => [
				"label" => $this->name(),
				"child" => [
					"stock-picking" => [
						"label" => "Stock Picking Instruction : [[get:id]]",
						"url" => ["stock-picking", "id"=>"[[get:id]]"],
					],
				]
			]
		];

		return [
			"verbs" => $this->verbs([
				"list-product" => ["post"]
			]),
			"access" => $this->access([
				[["index"], "view"],
				[["deliver"], "deliver"],
			]),
		];
	}

	public function actionIndex() {
		$post = $this->request->post();

		if (isset($post["delivery"]) && count($post["delivery"])) {
			try {
				$transaction = db::transaction();

				# Delivery Instruction
				// update delivery status to process
				db::update("delivery_instruction", ["delivery_status" => DeliveryInstruction::STATUS_PROCESS], [
					"id_delivery_instruction" => $post["delivery"],
				])->execute();

				# Batch Item
				// create picking batch
				$number_batch = unic();
				db::insert("picking_batch", ["number_batch" => $number_batch, "batch_time" => db::raw("now()")])->execute();

				// insert order item into pick item
				$ids = "'".implode("','", $post["delivery"])."'";
				$c_item = db::run(
							"INSERT into pick_item (sku, number_box, qty, number_batch) 
								with recursive r as (
									SELECT null::varchar as number_box, ii.sku, array[]::varchar[] as history, 
										0 as level, sum(oi.qty) as qty, array[]::bigint[] as taken
									from delivery_instruction di
									inner join order_item oi on oi.number_order=di.number_order
									inner join product p on p.number_product=oi.number_product
									inner join product_color c on c.number_product_color=oi.number_product_color
									inner join product_size s on s.code_size=oi.code_size
									inner join inventory_item ii on ii.code_size=s.code_size 
										and ii.number_product_color=c.number_product_color
										and ii.number_product=p.number_product
										and ii.code_warehouse=:warehouse
									where di.id_delivery_instruction in ($ids)
									GROUP BY ii.sku
									union all
									select c.number_box, c.sku, r.history||c.number_box, level+1, r.qty-c.qty,
										case when c.qty<r.qty then r.taken||c.qty::bigint else r.taken||r.qty end
									from box_content c 
									inner join r on c.sku=r.sku and c.qty>0 and r.qty>0 
										and (array[c.number_box] <@ r.history)=false
								)
								select t.sku, unnest(t.history) as number_box, unnest(t.taken) as taken, :id from (
									select distinct on (sku) sku as id, * from r
									where number_box is not null and qty<=0 
									order by sku, level, history
								) as t
								order by sku, taken desc", 
							["id" => $number_batch, "warehouse" => $this->user->identity->code_warehouse]
						)->execute();

				// decrease qty phisycally
				if ($c_item > 0) {
					# Box Content
					// decrease box content quantity
					db::run(
						"UPDATE box_content c set qty=c.qty-t.qty from pick_item t
						where t.sku=c.sku and t.number_box=c.number_box and t.number_batch=:id",
						["id" => $number_batch]
					)->execute();

					// update content Qty
					Box::updateContentQty();
					
					# Inventory Item
					db::run(
						"UPDATE inventory_item i set qty=i.qty-t.qty from (
							select sum(qty) as qty, r.code_warehouse, pi.sku from pick_item pi
							inner join box b on b.number_box=pi.number_box
							inner join rack r on r.number_rack=b.number_rack
							where pi.number_batch=:id
							group by r.code_warehouse, pi.sku
						) as t
						where i.sku=t.sku and t.code_warehouse=i.code_warehouse",
						["id" => $number_batch]
					)->execute();
				}

				$transaction->commit();
				return redirect(["stock-picking", "id" => $number_batch]);
			}
			catch (\Exception $e) {
                $transaction->rollBack();
				handle($e);
			}
		}

		$query = db::query("delivery_instruction di", [
					db::raw("sum(oi.qty) as qty"), 
					"di.id_delivery_instruction as id", 
					"so.number_order", 
					"so.delivery_city", 
					"di.instruction_time", 
					"b.name",
				])
				->innerJoin("sales_order so", "so.number_order=di.number_order")
				->innerJoin("order_item oi", "oi.number_order=so.number_order")
				->innerJoin("buyer b", "b.code_buyer=so.code_buyer")
				->where(["di.delivery_status" => DeliveryInstruction::STATUS_OPEN])
				->groupBy("di.id_delivery_instruction, so.number_order, di.instruction_time, b.name, so.delivery_city")
				->orderBy("di.instruction_time");

		return view([
			"dataProvider" => new ActiveDataProvider([
				"query" => $query,
			]),
		]);
	}

	/**
	 * view inventory stock picking instruction
	 * @param  string $id   number_batch
	 * @param  string $view format view null|pdf
	 * @return view|pdf
	 */
	public function actionStockPicking($id, $view=null) {
		$model = PickingBatch::findOne(["number_batch" => $id]);

		if ($model == null)
			return $this->abort();

		$param = [
			"view" => $view,
			"model" => $model,
			"query" => db::run(
				"SELECT p.name, s.name as size, c.color, b.number_rack, pi.number_box, pi.qty from pick_item pi
				inner join inventory_item ii on ii.sku=pi.sku
				inner join product p on p.number_product=ii.number_product
				inner join product_color c on c.number_product_color=ii.number_product_color
				inner join product_size s on s.code_size=ii.code_size
				inner join box b on b.number_box=pi.number_box
				where pi.number_batch=:id
				order by p.name, s.name, c.color, pi.number_box",
				["id" => $id]
			),
		];

		if ($view === "pdf") {
	        $this->pdf->content = $this->renderPartial("stock-picking", $param);
            return $this->pdf->render();
		}

		return view($param);
	}

	/**
	 * POST - menampilkan data product bedasarkan order yang di pilih
	 * @return view
	 */
	public function actionListProduct() {
		$id = $this->request->post("delivery");

		if (count($id) == 0)
			return $this->abort();

		$query = db::query("delivery_instruction di", ["p.name", "s.name as size", "c.color", db::raw("sum(oi.qty) as qty")])
					->innerJoin("order_item oi", "oi.number_order=di.number_order")
					->innerJoin("product p", "p.number_product=oi.number_product")
					->innerJoin("product_color c", "c.number_product_color=oi.number_product_color")
					->innerJoin("product_size s", "s.code_size=oi.code_size")
					->where(["di.id_delivery_instruction" => $id])
					->groupBy("p.name, s.name, c.color")
					->orderBy("p.name, s.name, c.color");

		return $this->renderAjax("list-product", [
			"dataProvider" => new ActiveDataProvider([
				"query" => $query,
				"pagination" => false,
			])
		]);
	}
}