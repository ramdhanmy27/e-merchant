<?php namespace common\yii\web;

use Yii;
use yii\helpers\BaseInflector;

// include view helpers
include_once Yii::getAlias("@common/components/view.php");

class View extends \yii\web\View {

	/**
	 * render view
	 * @param  string|array $view
	 * @param  array $params  
	 * @param  object $context
	 * @return 
	 */
	public function render($view=null, $params=[], $context=null) {
		$controller = $context!=null ? $context : Yii::$app->controller;

		/*defined parameters*/
		$this->title = ucwords(str_replace("-", " ", $controller->id));

		// if $view was an array then set view file to action name id
		if (is_array($view) || $view===null) {
			if ($params==null) {
				$params = $view;
				$view = $controller->action->id;
			}
			else $view = self::getValidView($view);
		}
		
		// render as usual
		$this->params = array_merge($params, $this->params);
		$this->generateBreadcrumb($controller);

		// register asset if exists
		$this->registerAsset($controller);

		return parent::render($view, $params, $context);
	}

	/**
	 * render partial view
	 * @param  string|array $view
	 * @param  array $params  
	 * @param  object $context
	 * @return 
	 */
	public function renderAjax($view=null, $params=[], $context=null) {
		$controller = $context!=null ? $context : Yii::$app->controller;

		if (is_array($view) || $view===null) {
			// view set to default (Action ID) | param set to $view value
			if ($params==null) {
				$params = $view;
				$view = $controller->action->id;
			}
			else $view = self::getValidView($view);
		}

		// render ajax
		return parent::renderAjax($view, $params, $context);
	}

	/**
	 * register asset if any
	 * @param  Controller $controller
	 */
	public function registerAsset($context) {
		// format: app\assets\[path\to\ControllerName]\\
		$path = explode("/", $context->id);
		$controller = BaseInflector::camelize(array_pop($path));
		$base = "\\".preg_replace("/^app\-/", "", Yii::$app->id)."\\assets\\".(count($path)>0 ? implode("\\", $path)."\\" : null).$controller."\\";

		// register main controller asset if exists
		$class = $base.$controller."Asset";

		if (class_exists($class))
			$class::register($this);

		// register action controller asset if exists
		$class = $base.BaseInflector::camelize($context->action->id)."Asset";

		if (class_exists($class))
			$class::register($this);
	}

	/**
	 * breadcrumb generator
	 * @param  Controller $controller
	 * @param  array  $params
	 */
	public function generateBreadcrumb($controller) {
		if (!isset($controller->breadcrumb) || count($controller->breadcrumb)==0
			|| (isset($this->params['breadcrumbs']) && count($this->params['breadcrumbs']) > 0))
			return;

		// flatten breadcrumbs
		$flatten = function($data, $parent = null) use (&$flatten) {
			$result = [];

			foreach ($data as $url => $item) {
				$result[$url] = [
					"label" => $item['label'], 
					"url" => isset($item['url']) ? $item['url'] : [$url],
					"parent" => $parent
				];

				if (isset($item['child'])) 
					$result = array_merge($result, $flatten($item['child'], $url));
			}

			return $result;
		};

		$breadcrumb = $flatten($controller->breadcrumb);

		// generate breadcrumbs
		$generateBreadcrumb = function($url, $link = true) use (&$generateBreadcrumb, $breadcrumb) {
			if (!isset($breadcrumb[$url]))
				return [];

			// render expression
			$breadcrumb[$url]['label'] = $this->renderExpr($breadcrumb[$url]['label']);
			$breadcrumb[$url]['url'] = $this->renderExpr($breadcrumb[$url]['url']);

			// generate data breadcrumb
			$parent = $generateBreadcrumb($breadcrumb[$url]['parent']);
			unset($breadcrumb[$url]['parent']);

			if ($link == false)
				unset($breadcrumb[$url]['url']);

			return array_merge([$breadcrumb[$url]], $parent);
		};

		$this->params['breadcrumbs'] = array_reverse($generateBreadcrumb($controller->action->id, false));
	}

	/**
	 * render data expressions
	 * @param  string $str
	 * [[paramName]] will return $this->params[paramName]
	 * {{expressions}} execute expressions
	 * @return string
	 */
	public function renderExpr($str) {
		return preg_replace_callback("/(\[\[((\w+)\:)?(\w+)\]\]|\{\{(.+)\}\})/", 
			function($match) {
				// handle variable parsing
				if ($var = $match[4]) {
					switch ($match[3]) {
						default:
							if (isset($this->params[$var]))
								return $this->params[$var];

						case 'get':
							if (isset($_GET[$var]))
								return $this->request->get($var);
							break;
					}
				}
				// hanlde expression parsing
				elseif ($exprressions = $match[5]) {
					extract($this->params);
					
					foreach (preg_split("/ ?or ?/i", $exprressions) as $expr) {
						try {
							return eval("return $expr;");
						}
						catch (\yii\base\ErrorException $e) {}
					}
				}

				return $match[0];
			}
			, $str
		);
	}

	/**
	 * get valid view file
	 * @param  array  $list
	 * @return string
	 */
	public static function getValidView(array $list) {
		foreach ($list as $file) {
			if (is_file(Yii::getAlias($file).".php"))
				return $file;
		}

		return $file;
	}

	/**
	 * accessing app component
	 * @param  string $var
	 * @return
	 */
	public function __get($var) {
		return Yii::$app->{$var};
	}

	public function can($perm) {
		return $this->controller->can($perm);
	}

    public function showAction($mappings){
        $temp   = [];
        foreach($mappings as $button => $permissions){
            foreach($permissions as $permission){
	            if($this->can($permission)){
	                $temp[] = '{'.$button.'}';
	            }
	            break;
	        }
        }
        
        return implode(" | ", $temp);
    }
}