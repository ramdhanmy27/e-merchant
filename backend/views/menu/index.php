<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (access("update"))
    $this->registerJsFile('@web/js/menu.js', ['depends' => ["backend\assets\AppAsset"]]);

$this->registerCssFile('@web/css/menu.css');

function menu_tree($list, $parent = null) {
    $html = null;

    foreach ($list as $menu) {
        // parent has child
        if ($menu["parent"] == $parent) {
            if (!isset($menu['enable']))
                continue;
            
            $html .= "<li ".($menu['enable'] ? null : "class='text-muted'")." data-id='$menu[id]'>
                        <i class='glyphicon glyphicon-th sortable-handle'></i> 
                        <span>$menu[title]</span>

                        <div class='pull-right'>"
                            .(access("update") ? 
                                Html::a( // status
                                    $menu['enable'] ? "Aktif" : "Non-Aktif", 
                                    ["status", "id"=>$menu["id"]],
                                    [
                                        "class"=>[$menu['enable'] ? "label label-success" : "label label-danger"],
                                        "data-method" => "POST",
                                    ]
                                ) : 
                                "<span class='label ".($menu['enable'] ? "label-success" : "label-danger")."'>"
                                    .($menu['enable'] ? "Aktif" : "Non-Aktif")
                                ."</span>"
                            )
                        ."</div>
                        <div class='pull-right parent-hover'>"
                            .(access("create") ? Html::a( // create
                                "<i class='glyphicon glyphicon-plus'></i> Tambah", 
                                ["create", "parent"=>$menu["id"]],
                                ["modal"=>""]
                            ) : "")
                            .(access("update") ? Html::a( // update
                                "<i class='glyphicon glyphicon-edit'></i> Edit", 
                                ["update", "id"=>$menu["id"]],
                                ["modal"=>""]
                            ) : "")
                            .(access("delete") ? Html::a( // delete
                                "<i class='glyphicon glyphicon-remove'></i> Hapus", 
                                ["delete", "id"=>$menu["id"]],
                                [
                                    "data"=> [
                                        "confirm"=>"",
                                        "method"=>"post",
                                    ]
                                ]
                            ) : "")
                        ."</div> <div class='clearfix'></div>"
                        .menu_tree($list, $menu['id'])
                    ."</li>";
        }
    }

    return "<ul>$html</ul>";
}

?>

<div class="menu-index">
    <?php if (access("create")) : ?>
        <p> <?= Html::a('Tambah Menu', ['create'], ['class' => 'btn btn-success', "modal"=>""]) ?> </p>
    <?php endif; ?>

    <div id="sortable"> 
        <?= menu_tree($query->queryAll()) ?> 
        <div class='loading'><?= Html::img("@web/images/loading.gif") ?></div>
    </div>
</div>