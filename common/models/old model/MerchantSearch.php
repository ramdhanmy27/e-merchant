<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Merchant;

/**
 * MerchantSearch represents the model behind the search form about `common\models\Merchant`.
 */
class MerchantSearch extends Merchant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'phone', 'status', 'created_at', 'updated_at'], 'integer'],
            [['shop_name', 'company_name', 'address_1', 'address_2', 'city', 'country', 'postal_code', 'PIC', 'bank', 'bank_code', 'bank_swift', 'bank_account_name', 'bank_account_number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Merchant::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'phone' => $this->phone,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'shop_name', $this->shop_name])
            ->andFilterWhere(['ilike', 'company_name', $this->company_name])
            ->andFilterWhere(['ilike', 'address_1', $this->address_1])
            ->andFilterWhere(['ilike', 'address_2', $this->address_2])
            ->andFilterWhere(['ilike', 'city', $this->city])
            ->andFilterWhere(['ilike', 'country', $this->country])
            ->andFilterWhere(['ilike', 'postal_code', $this->postal_code])
            ->andFilterWhere(['ilike', 'PIC', $this->PIC])
            ->andFilterWhere(['ilike', 'bank', $this->bank])
            ->andFilterWhere(['ilike', 'bank_code', $this->bank_code])
            ->andFilterWhere(['ilike', 'bank_swift', $this->bank_swift])
            ->andFilterWhere(['ilike', 'bank_account_name', $this->bank_account_name])
            ->andFilterWhere(['ilike', 'bank_account_number', $this->bank_account_number]);

        return $dataProvider;
    }
}
