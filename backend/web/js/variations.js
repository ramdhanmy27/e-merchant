app.factory("Variations", function($rootScope) {
	return {
		init: {
			error: {
				quantity: {},
			},
			size: [""],
			attribute: [""],
			quantity: {},
		},
	}
});

app.controller("variationsController", function($scope, $http, $func, Variations) {
	/*initialize data*/
	if (!fn.isset($scope.variations)) {
		$scope.variations = Variations.init;

		var variations;

		if (variations = $func.init("variations"))
			$scope.variations = variations;
	}
	
	$scope.f = {
		total: function(posSize, posAttr) {
			var total = 0;
			var quantity = $scope.variations.quantity;

			// attribute
			if (posAttr!==undefined && posSize===undefined) {
				for (var i in quantity) 
					total += fn.int(quantity[i][posAttr]);
			}
			// size
			else if (posSize!==undefined && posAttr===undefined) {
				for (var i in quantity[posSize]) 
					total += fn.int(quantity[posSize][i]);
			}
			// all
			else {
				for (var i in quantity) {
					for (var j in quantity[i]) 
						total += fn.int(quantity[i][j]);
				}
			}

			return fn.int(total);
		},
		validateQuantity: function(x, y) {
			var key = x+'-'+y;
			$scope.$broadcast("variations-validate", x, y);

			// invalid quantity
			if (fn.isset($scope.variations.quantity, x, y) 
					&& !fn.isEmpty($scope.variations.quantity[x][y])
					&& !fn.is_numeric($scope.variations.quantity[x][y])) {
				$scope.variations.error.quantity[key] = "Invalid Value. ["+ $scope.variations.size[x] 
															+" - "+ $scope.variations.attribute[y] 
														+"]";
				return true;
			}
			else if (fn.isset($scope.variations.error, "quantity", key)) {
				delete $scope.variations.error.quantity[key];
			}

			return false;
		},
		isLastRow: function(x) {
			if ($scope.variations.size instanceof Array)
				return x==$scope.variations.size.length-1;
			else 
				return x==Object.keys($scope.variations.size).pop();
		}
	};
});