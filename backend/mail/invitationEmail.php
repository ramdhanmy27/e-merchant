<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
	<style type='text/css'>
			/*
		*************************************
		          Copyright 2014
		             benague
		*************************************
		*/

		/* Google Fonts */
		@import url(http://fonts.googleapis.com/css?family=Open+Sans);

		/* set global font to Open Sans */
		body {
		  font-family: 'Open Sans', 'sans-serif';
		  background-image: url(http://benague.ca/files/pw_pattern.png);
		}

		/* header */
		h1 {
		  color: #55acee;
		  text-align: center;
		}

		/* header/copyright link */
		.link {
		  text-decoration: none;
		  color: #55acee;
		  border-bottom: 2px dotted #55acee;
		  transition: .3s;
		  -webkit-transition: .3s;
		  -moz-transition: .3s;
		  -o-transition: .3s;
		  cursor: url(http://cur.cursors-4u.net/symbols/sym-1/sym46.cur), auto;
		}

		.link:hover {
		  color: #2ecc71;
		  border-bottom: 2px dotted #2ecc71;
		}

		/* button div */
		#buttons {
		  padding-top: 50px;
		  text-align: center;
		}

		a:active {
		  transform: translate(0px, 5px);
		  -webkit-transform: translate(0px, 5px);
		  box-shadow: 0px 1px 0px 0px;
		}

		a {
		  background-color: #55acee;
		  box-shadow: 0px 5px 0px 0px #3C93D5;
		}

		a:hover {
		  background-color: #6FC6FF;
		}
				/* start da css for da buttons */
		a {
		  border-radius: 5px;
		  padding: 15px 25px;
		  font-size: 22px;
		  text-decoration: none;
		  margin: 20px;
		  color: white;
		  position: relative;
		  display: inline-block;
		}
		p {
		  text-align: center;
		  padding-top: 20px;
		}
	</style>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
	<center>
		<img src="http://www.saqina.com/skin/frontend/ultimo/default/images/new-saqina.gif">
		<p>
			Klik tombol dibawah untuk melanjutkan
		</p>
		<?= Html::a("Lanjutkan",Url::toRoute(["/merchant/confirm", "verif_code"=>$verif_code], true)); ?>
	</center>
	<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>