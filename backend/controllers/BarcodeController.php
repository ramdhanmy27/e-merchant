<?php namespace backend\controllers;

use common\models\Product;
use common\yii\web\Controller;
use common\yii\db\db;
use Yii;


class BarcodeController extends \common\yii\web\Controller
{
    use \common\yii\web\CrudControllerTrait;

    public function actionIndex()
    {
        if($this->request->post()){

            $this->pdf->content = $this->renderPartial("barcode-print", [               
                'number_product' => $this->request->post("product")
            ]);

            $this->pdf->cssInline = '
                body{
                    margin: 0px;
                    font-size: 10pt;
                    font-family : calibri;
                }
                .kertas_print{
                    height: 812.598425197px;
                    width: 623.622047244px;
                    margin: auto;
                }
                .labels{
                    width: 188.976377953px;
                    height: 71.811024px;
                    border-radius: 5px;
                    border: 1px solid black;
                    float:left;
                    margin-right: 5.559055118px;
                    margin-bottom: 20.559055118px;
                }
                .container-inside{
                    width:180px; 
                    height:71.811024px; 
                    position: absolute; 
                    margin-left:4px; 
                    margin-top:0px; 
                }
                .text{
                    height: 13px;
                }
                .barcode-container{
                    height: 20px;
                    margin: auto;
                }
            ';

            $this->pdf->api->SetFooter('|Page {PAGENO}|');
            $this->pdf->api->SetHeader('|Page {PAGENO}|');
            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');
            return $this->pdf->render();
		}
		else{
			$model  = db::run('SELECT * FROM product');
			return $this->render('index', [
				'model' => $model,
			]);
		}
	}

	public function actionGetProduct(){
		$number_product = $this->request->post("number_product");
		$product  = db::query('product', '*')->where(["number_product" => $number_product])->one();
		$product_color = db::query("product_color")->where(["number_product" => $number_product])->all();
		$group_size  = db::query("product_size", "*")->where(["group_size" => $product["group_size"]])->all();
		
		$inventory = [];
		$datana = db::query("inventory_item", "*")->where([
			"number_product" => $number_product,
			"code_warehouse" => Yii::$app->user->identity->code_warehouse,
		])->all();
		foreach($datana as $key=>$data){
			$inventory[$datana[$key]['code_size']][$datana[$key]['number_product_color']] = $data['qty'];
		}
		
		return json([
			"product"        => $product, 
			"product_color"  => $product_color,
			"group_size"     => $group_size,
			"inventory"      => $inventory
		]);
	}
}