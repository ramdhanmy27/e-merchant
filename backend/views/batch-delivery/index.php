<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\yii\widgets\ActiveForm;

register(["@js/batch-delivery/wizard.js"], [
	"depends" => ["common\assets\FormWizardAsset"],
]);

$this->title = 'Batch Delivery';
?>

<div class="confirmation-order-index">
    <?php ActiveForm::begin(["options" => ["id" => "wizard"]]); ?>
		<h3></h3>
		<section>
			<?= GridView::widget([
				"dataProvider" => $dataProvider,
				"columns" => [
					['class' => 'yii\grid\SerialColumn'],
					[
						"headerOptions" => ['class' => "text-center"],
						'header' => Html::checkbox(null, false, [
							"class" => "mark-checkbox-all",
							"style" => "cursor:pointer",
						]),
						"contentOptions" => ['class' => "text-center"],
						'content' => function($data) {
							return Html::checkbox("delivery[]", false, [
								"value" => $data["id"], 
								"class" => "mark-checkbox",
								"style" => "cursor:pointer",
							]);
						}
					],
					'number_order',
					[
						'attribute' => 'instruction_time',
						"options" => ['width' => "130px"],
						'content' => function($data) {
							return date("d/m/Y H:i", strtotime($data["instruction_time"]));
						}
					],
					[
						'header' => 'Cust. Name',
						'attribute' => 'name',
					],
					'delivery_city',
					[
						'header' => 'Total Qty',
						'attribute' => 'qty',
						'content' => function($data) {
							return number_format($data["qty"]);
						}
					],
				]
			]) ?>
		</section>

		<h3></h3>
		<section class="picking-list">
			<div class="text-center"><?= Html::img("@web/images/loading.gif") ?></div>
		</section>
    <?php ActiveForm::end(); ?>
</div>