<?php namespace backend\controllers;

use common\models\SalesOrder;
use common\models\SalesOrderSearch;
use common\models\BankTransaction;
use common\models\OrderItem;
use common\yii\db\db;
use yii\data\ActiveDataProvider;

class ConfirmationOrderController extends \common\yii\web\Controller {
	public $breadcrumb;
	public static $permissions = ["view", "confirm", "cancel"];

	public function behaviors() {
		$this->breadcrumb = [
			"index" => [
				"label" => $this->name(),
				"child" => [
					"confirm" => [
						"label" => "Confirm : [[get:id]]",
						"url" => ["confirm", "id"=>"[[get:id]]"],
						"child" => [
							"order-item" => ["label" => "Detail Order"],
						]
					],
				]
			]
	];

		return [
			"verbs" => $this->verbs([
				"cancel" => ["post"]
			]),
			"access" => $this->access([
				[["index"], "view"],
				[["confirm"], "confirm"],
				[["cancel"], "cancel"],
			]),
		];
	}

	public function actionIndex() {
		$filterModel = new SalesOrderSearch();
		$param = $this->request->queryParams;
		$param["SalesOrderSearch"]["status"] = SalesOrder::STATUS_OPEN;

		return view([
			"filterModel" => $filterModel,
			"dataProvider" => $filterModel->search($param),
		]);
	}

	public function actionOrderItem($id) {
		if (!($model = SalesOrder::findOne(["number_order" => $id])))
			return $this->abort();

		return view([
			'id' => $id, 
			"model" => $model,
			"dataProvider" => new ActiveDataProvider([
		        "query" => OrderItem::find()->where(["number_order" => $id]),
		    ]),
		]);
	}

	public function actionConfirm($id) {
		$order = SalesOrder::findOne(["status" => SalesOrder::STATUS_OPEN, "number_order" => $id]);
 
		if ($order == null)
			return $this->abort();

		$post = $this->request->post();
		$model = new BankTransaction();
		$model->code_bank = $order->code_bank;

		if ($model->load($post)) {
			try {
				$transaction = db::transaction();

				// create bank transaction
				$model->is_debet = true;
				$model->description = "pembayaran dari {$order->buyer->name} untuk order nomor $order->number_order";
				$model->transaction_time = db::raw("now()");
				$model->transaction_value = $order->total_bill;

                if (!$model->save())
                    throw new \Exception(current($model->getFirstErrors()));

				// upload transfer receipt
				if (!file_exists($_FILES["receipt"]["tmp_name"]))
                    throw new \Exception("Transfer Receipt is required");
				else if (!$model->uploadReceipt($_FILES["receipt"]["tmp_name"]))
                    throw new \Exception("Cannot upload transfer receipt");

				// set order to CLOSE
				$order->scenario = SalesOrder::SC_CONFIRM;
				$order->status = SalesOrder::STATUS_CLOSED;
				$order->number_transaction = $model->number_transaction;
				$order->code_bank = $model->code_bank;

                if (!$order->save())
                    throw new \Exception(current($order->getFirstErrors()));

				$transaction->commit();
				$this->session->setFlash("success", "Order has successfully confirmed");

				return redirect(["index"]);
			}
			catch(\Exception $e) {
                $transaction->rollBack();
                handle($e);
			}
		}

		return view(["order" => $order, "model" => $model]);
	}

	public function actionCancel($id) {
		if (!($model = SalesOrder::findOne(["number_order" => $id])))
			return $this->abort();
		
		try {
			$transaction = db::transaction();

			// update order status to cancel
			$model->status = SalesOrder::STATUS_CANCEL;

            if (!$model->save())
                throw new \Exception(current($model->getFirstErrors()));

            // increase stock allocation
            $query = OrderItem::find()->where(["number_order" => $id]);
            $data = [];

            foreach ($query->each() as $item)
            	$data[] = [
            		$item["number_product"], 
            		$item["number_product_color"], 
            		$item["code_size"], 
            		$item["qty"],
            	];

            if (count($data))
	            db::batchUpdate(
	                "stock_allocation sa", ["allocation_qty=allocation_qty+t.qty::int"], 
	                ["number_product", "number_product_color", "code_size", "qty"], $data,
	                "where sa.number_product_color=t.number_product_color and sa.code_size=t.code_size 
	                    and sa.number_product=t.number_product and sa.code_sales_channel=:channel",
	                ["channel" => SalesOrder::CHANNEL_MOBILE]
	            )->execute();

			$transaction->commit();
			return redirect(["index"]);
		}
		catch(\Exception $e) {
            $transaction->rollBack();
            handle($e);
		}
	}
}