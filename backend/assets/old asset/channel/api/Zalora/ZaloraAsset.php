<?php namespace backend\assets\channel\api\Zalora;

class ZaloraAsset extends \yii\web\AssetBundle
{
    public $baseUrl = '@web/js/channel/api';
    
    public $js = [
    	"zalora.js"
    ];

    public $depends = [
        "backend\assets\channel\Channel\ChannelAsset",
    ];
}
