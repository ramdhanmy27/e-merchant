<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\SalesChannel;
\kartik\file\FileInputAsset::register($this);

$this->title = 'Confirmation Order';
?>

<div class="confirmation-order-index">
	<?= GridView::widget([
		"dataProvider" => $dataProvider,
		"filterModel" => $filterModel,
		"columns" => [
			['class' => 'yii\grid\SerialColumn'],

			'number_order',
			[
				'label' => 'Buyer',
				'attribute' => 'code_buyer',
				'value' => 'buyer.name',
			],
			[
				'label' => 'Channel',
				'attribute' => 'code_sales_channel',
                "filter" => SalesChannel::option("name", "code_sales_channel"),
				'value' => 'salesChannel.name',
			],
			[
				'format' => 'datetime',
				'attribute' => 'order_time',
				"options" => ['width' => "130px"],
				"filter" => Html::activeTextInput($filterModel, "order_time", ["class" => "input-date"]),
			],
			[
				'attribute' => 'total_bill',
				"options" => ['width' => "130px"],
				'content' => function($model) {
					return currency($model->total_bill);
				}
			],
			[
				'attribute' => 'payment_deadline',
				"filter" => Html::activeTextInput($filterModel, "payment_deadline", ["class" => "input-date"]),
				"options" => ['width' => "130px"],
				'content' => function($model) {
					$time = new DateTime();
					$deadline = new DateTime($model->payment_deadline);
					$diff = $time->diff($deadline);

					if ($deadline <= $time)
						$class = "label label-danger";
					else if ($diff->format('%h')=='0')
						$class = "label label-warning";
					else
						$class = "label label-success";

					return "<span class='$class'>".$deadline->format(param("format.datetime.php"))."</span>";
				}
			],
			[
				"class" => "yii\grid\ActionColumn",
				"contentOptions" => ['class' => "nowrap"],
				"template" => "{confirm} {cancel}",
				"buttons" => [
					"confirm" => function($url, $model, $id) {
						return Html::a(
							"<i class='glyphicon glyphicon-ok text-success'></i>", 
							["confirm", "id" => $model->number_order], 
							["modal" => "", "title" => "Confirm"]
						);
					},
					"cancel" => function($url, $model, $id) {
						return Html::a(
							"<i class='glyphicon glyphicon-remove text-danger'></i>", 
							["cancel", "id" => $model->number_order], 
							["data" => ["confirm" => "", "method" => "POST"], "title" => "Cancel"]
						);
					}
				]
			],
		]
	]) ?>
</div>