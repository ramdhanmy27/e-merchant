<?php

namespace backend\controllers;

use Yii;
use common\models\Menu;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\db\Query;

/**
 * MenuController implements the CRUD actions for Menu model.
 * @propery     string  $model  "model classname"
 */
class MenuController extends \common\yii\web\Controller
{
	use \common\yii\web\CrudControllerTrait;

	public function init() {
		parent::init();
		$this->param['model'] = Menu::className();
	}
	
	/**
	 * Lists all data.
	 * @return mixed
	 */
	public function actionIndex() {	
		Menu::reorder();

		return $this->render("@app/views/menu/index", [
			'query' => Menu::select('order by r.order_path'),
		]);
	}

	/**
	 * Creates a new data.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($parent = null) {
		$model = new Menu();

		if ($model->load($this->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} 
		else {
			return $this->render("@app/views/menu/create", [
				'model' => $model,
				'parent' => $parent,
			]);
		}
	}

	/**
     * Updates an existing data.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost) {
        	$post = $this->request->post();

        	// if parent updated, put order to last
	        if ($model->parent!=$post["Menu"]["parent"])
	        	$model->order = (new Query)->from("menu")
				    				->select('max("order")+1')
				    				->where(["parent" => !empty($post["Menu"]["parent"]) ? $post["Menu"]["parent"] : null])
				    				->scalar();

        	$model->load($post);
        	$model->save();

            return $this->redirect(['view', 'id' => $model->{$this->param("id")}]);
        } 
        else {
            return $this->render("@app/views/menu/update", [
                'model' => $model,
            ]);
        }
    }

	/**
	 * change menu status (enable : 1|disable : 0)
	 * @param integer $id
	 * @return mixed
	 */
	public function actionStatus($id) {
		$model = $this->findModel($id);
		$model->enable = !$model->enable;
		$model->save();

		return $this->redirect(['index']);
	}

	/**
	 * update menu position
	 * ajax request only
	 */
	public function actionPosition() {
		if (!$this->request->isAjax)
			return $this->redirect(['index']);
		
		$post = $this->request->post();
		$this->findModel($post['id'])->updatePosition($post['order'], $post['parent']);
		exit;
	}
}
