<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sales_order".
 *
 * @property string $number_order
 * @property string $order_time
 * @property string $total_price
 * @property string $shipping_cost
 * @property string $total_bill
 * @property string $status
 * @property string $delivery_address
 * @property string $delivery_city
 * @property string $post_code
 * @property string $payment_deadline
 * @property string $code_sales_channel
 * @property string $code_buyer
 * @property string $number_transaction
 * @property string $code_service
 * @property string $code_bank
 *
 * @property DeliveryInstruction[] $deliveryInstructions
 * @property OrderItem[] $orderItems
 * @property BankTransaction $bankTransaction
 * @property Buyer $buyer
 * @property SalesChannel $salesChannel
 * @property LogisticService $logisticService
 * @property BankAccount $bankAccount
 */
class SalesOrder extends \common\yii\db\ActiveRecord
{
	public static $deadline_opt = [
		"1" => "1 Jam",
		"2" => "2 Jam",
		"3" => "3 Jam",
		"4" => "4 Jam",
		"5" => "5 Jam",
	];

	// Status
	const STATUS_OPEN = '0';
	const STATUS_CLOSED = '1';
	const STATUS_CANCEL = '2';
	const STATUS_DELIVER = '3';

	// Code
    const CHANNEL_MOBILE = "SC02";

    // Scenario
    const SC_CREATE = "create";
    const SC_CONFIRM = "confirm";

    public static $status = [
        self::STATUS_OPEN => "open",
        self::STATUS_CLOSED => "close",
        self::STATUS_CANCEL => "cancel",
        self::STATUS_DELIVER => "deliver",
    ];

	public static function tableName()
	{
		return 'sales_order';
	}

	public function rules()
	{
		return [
			// required
			[["number_order", "delivery_address", "post_code", "delivery_city", 'payment_deadline'], 'required', "on" => self::SC_CREATE],
			[["code_bank", 'number_transaction'], 'required', "on" => self::SC_CONFIRM],

			// unique
			[['number_order'], 'unique'],

			// safe
			[['order_time'], 'safe'],
			[['payment_deadline'], 'safe'],

			// integer
			[['total_price', 'shipping_cost', 'total_bill'], 'number'],

			// string
			[['status'], 'string', 'max' => 1],
			[['code_sales_channel'], 'string', 'max' => 4],
			[['code_service'], 'string', 'max' => 4],
			[['code_bank'], 'string', 'max' => 4],
			[['number_order', 'code_buyer', 'number_transaction'], 'string', 'max' => 12],
			[['post_code'], 'string', 'max' => 6],
			[['delivery_city'], 'string', 'max' => 100],
			[['delivery_address'], 'string'],
			
			// exist
			[['number_transaction'], 'exist', 'skipOnError' => true, 'targetClass' => BankTransaction::className(), 'targetAttribute' => ['number_transaction' => 'number_transaction']],
			[['code_buyer'], 'exist', 'skipOnError' => true, 'targetClass' => Buyer::className(), 'targetAttribute' => ['code_buyer' => 'code_buyer']],
			[['code_sales_channel'], 'exist', 'skipOnError' => true, 'targetClass' => SalesChannel::className(), 'targetAttribute' => ['code_sales_channel' => 'code_sales_channel']],
			[['code_service'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticService::className(), 'targetAttribute' => ['code_service' => 'code_service']],
			[['code_bank'], 'exist', 'skipOnError' => true, 'targetClass' => BankAccount::className(), 'targetAttribute' => ['code_bank' => 'code_bank']],
		
		];
	}

	public function attributeLabels()
	{
		return [
			'number_order' => 'No Order',
			'order_time' => 'Time',
			'total_price' => 'Total Price',
			'status' => 'Status',
			'shipping_cost' => 'Shipping Cost',
			'total_bill' => 'Total Bill',
			'code_sales_channel' => 'Id Sales Channel',
			'code_buyer' => 'Kode Buyer',
			'number_transaction' => 'Transaction Number',
		];
	}

    public function getLabelStatus($textOnly=false) {
        if (!isset(self::$status[$this->status]))
            return null;

        $label = ucwords(self::$status[$this->status]);

        if ($textOnly)
	        return $label;

	    $class = [
	        self::STATUS_OPEN => "label label-default",
	        self::STATUS_CLOSED => "label label-warning",
	        self::STATUS_CANCEL => "label label-danger",
	        self::STATUS_DELIVER => "label label-primary",
	    ];

	    return "<span class='{$class[$this->status]}'>$label</span>";
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDeliveryInstructions()
	{
		return $this->hasMany(DeliveryInstruction::className(), ['number_order' => 'number_order']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrderItems()
	{
		return $this->hasMany(OrderItem::className(), ['number_order' => 'number_order']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBankTransaction()
	{
		return $this->hasOne(BankTransaction::className(), ['number_transaction' => 'number_transaction']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBuyer()
	{
		return $this->hasOne(Buyer::className(), ['code_buyer' => 'code_buyer']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSalesChannel()
	{
		return $this->hasOne(SalesChannel::className(), ['code_sales_channel' => 'code_sales_channel']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLogisticService()
	{
		return $this->hasOne(LogisticService::className(), ['code_service' => 'code_service']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBankAccount()
	{
		return $this->hasOne(BankAccount::className(), ['code_bank' => 'code_bank']);
	}
}
