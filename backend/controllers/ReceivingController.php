<?php

namespace backend\controllers;

use Yii;
use common\models\Receiving;
use common\models\ReceivingSearch;
use common\models\PurchaseOrder;
use common\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\yii\db\db;

/**
 * ReceivingController implements the CRUD actions for Receiving model.
 */
class ReceivingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Receiving models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ReceivingSearch();

        return view([
            "list" => [
                "dataProvider" => $model->search(
                    $this->request->queryParams + ["ReceivingSearch" => ["status" => 1]]
                ),
                "filterModel" => $model,
            ],
            "draft" => [
                "dataProvider" => $model->search(
                    $this->request->queryParams + ["ReceivingSearch" => ["status" => 0]]
                ),
                "filterModel" => $model,
            ],
        ]);
    }

    /**
     * Displays a single Receiving model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Receiving model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $raw = db::insert("receiving", ["status" => 0])->rawSql ;
        $lastInsertId = db::run($raw." returning id_receiving")->queryScalar();

        return $this->redirect(["draft", "id" => $lastInsertId]);
    }

    /**
     * halaman draft
     * @param  string $id receiving ID
     * @return view
     */
    public function actionDraft($id = null) {
        $model = $this->findModel($id);

        if (!$model->isDraft()) {
            $this->abort();
            return;
        }

        return $this->render([
            "model" => $model
        ]);
    }

    /**
     * dipanggil ketika ganti PO di select2, ambil data banyak product & keterangan product & quantitynya
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGetPurchaseOrderItem($number_po, $product=null)
    {
/*        $modelPurchaseOrder = PurchaseOrder::findOne($number_po);
        if(!isset($modelPurchaseOrder)){
            return json([]);
        }

        $purchaseOrderItems = [];
        foreach($modelPurchaseOrder->purchaseOrderItems as $key => $value){
            $purchaseOrderItems[$key] = $value->attributes;
            $purchaseOrderItems[$key]['id_purchase_order_item'] = $value->id_purchase_order_item;
        }

        return json($purchaseOrderItems);*/

        $bind = ["number_po" => $number_po];

        if ($product!=null)
            $bind["product"] = $product;

        $query = db::run(
            "SELECT p.number_product, c.number_product_color, c.color, s.code_size, s.name, i.qty
            FROM
            (
                    SELECT DISTINCT i.number_po, p.number_product, p.group_size
                    FROM product p
                    INNER JOIN purchase_order_item i ON p.number_product=i.number_product
                    WHERE i.number_po=:number_po
            ) AS p
            INNER JOIN product_color c ON c.number_product=p.number_product
            INNER JOIN product_size s ON s.group_size=p.group_size
            LEFT JOIN purchase_order_item i ON i.number_po=p.number_po
                    AND c.number_product_color=i.number_product_color
                    AND s.code_size=i.code_size"
            .($product==null ? "" : " WHERE p.number_product=:product")
            ." ORDER BY p.number_product", $bind
        )->query();
        $data = [];

        foreach ($query as $val) {
            $data[$val["number_product"]]["number_product"] = $val["number_product"];
            $data[$val["number_product"]]["size"][$val["code_size"]] = $val["name"];
            $data[$val["number_product"]]["attribute"][$val["number_product_color"]] = $val["color"];
            $data[$val["number_product"]]["qty"]["total"][$val["code_size"]][$val["number_product_color"]] = $val["qty"];
        }

        return $this->request->isAjax || $this->controller->action->id=="get-purchase-order-item" ? json($data) : $data;
    }

    /**
     * POST - update receiving information
     * @param  string $id_receiving id_receiving
     */
    public function actionUpdateReceiving($id_receiving) {
        $post = $this->request->post();
        $receive_time = date('Y-m-d H:i:s');
        $format = "m/d/Y H:i:s";

        db::update("receiving", [
            "number_po" => trim($post["number_po"])=="" ? null : $post["number_po"],
            "number_merch_do" => trim($post["number_merch_do"])=="" ? null : $post["number_merch_do"],
            "description" => trim($post["description"])=="" ? null : $post["description"],
            "receive_time" => $receive_time,
        ], "id_receiving=:id_receiving", ["id_receiving" => $id_receiving])->execute();
    }
    //time

    /**
     * save receiving
     * @param  string $id receiving ID
     * @return response
     */
    public function actionSave($id_receiving) {
        
        $bind = ["id_receiving" => $id_receiving];
        $bind2 = ["id_receiving" => $id_receiving];

        if ($this->user->identity->code_warehouse)
            $bind2["code_warehouse"] = $this->user->identity->code_warehouse;
        else
            $bind2["code_warehouse"] = null;
        
        db::update("receiving", ["status" => "1"], "id_receiving=:id_receiving", $bind)->execute();

        /*upsert sku inventory item*/

        db::run(
            "UPDATE inventory_item
            SET qty = t.qty
            FROM (
                SELECT inventory_item.sku, COALESCE(inventory_item.qty,0)+COALESCE(receive_item.qty,0) AS qty
                FROM receive_item
                INNER JOIN inventory_item
                    ON inventory_item.number_product = receive_item.number_product
                    AND inventory_item.number_product_color = receive_item.number_product_color
                    AND inventory_item.code_size = receive_item.code_size
                WHERE receive_item.id_receiving = :id_receiving
            ) AS t
            WHERE t.sku = inventory_item.sku", $bind
        )->execute();

        db::run(
            "INSERT INTO inventory_item (sku, qty, number_product, code_warehouse, number_product_color, code_size)
                SELECT receive_item.number_product || '-' || :code_warehouse || '-' || receive_item.code_size || '-' || product_color.color AS sku,
                receive_item.qty, receive_item.number_product, :code_warehouse AS code_warehouse, receive_item.number_product_color, receive_item.code_size
                FROM receive_item
                INNER JOIN product_color
                    ON product_color.number_product_color = receive_item.number_product_color
                LEFT JOIN inventory_item
                    ON inventory_item.number_product = receive_item.number_product
                    AND inventory_item.number_product_color = receive_item.number_product_color
                    AND inventory_item.code_size = receive_item.code_size
                WHERE receive_item.id_receiving = :id_receiving
                    AND inventory_item.sku IS NULL", $bind2
        )->execute();

        /*upsert unstored item*/

        db::run(
            "UPDATE unstored_items
            SET qty = t.qty
            FROM (
                SELECT inventory_item.sku, COALESCE(unstored_items.qty,0)+COALESCE(receive_item.qty,0) AS qty
                FROM receive_item
                INNER JOIN inventory_item
                    ON inventory_item.number_product = receive_item.number_product
                    AND inventory_item.number_product_color = receive_item.number_product_color
                    AND inventory_item.code_size = receive_item.code_size
                INNER JOIN unstored_items
                    ON unstored_items.sku = inventory_item.sku
                WHERE receive_item.id_receiving = :id_receiving
            ) AS t
            WHERE t.sku = unstored_items.sku", $bind
        )->execute();

        db::run(
            "INSERT INTO unstored_items (sku, qty)
                SELECT inventory_item.sku, receive_item.qty
                FROM receive_item
                INNER JOIN inventory_item
                    ON inventory_item.number_product = receive_item.number_product
                    AND inventory_item.number_product_color = receive_item.number_product_color
                    AND inventory_item.code_size = receive_item.code_size
                INNER JOIN product_color
                    ON product_color.number_product_color = receive_item.number_product_color
                LEFT JOIN unstored_items
                    ON unstored_items.sku = inventory_item.sku
                WHERE receive_item.id_receiving = :id_receiving
                    AND unstored_items.sku IS NULL", $bind
        )->execute();

        /*upsert unallocated*/

        db::run(
            "UPDATE unallocated
            SET qty = t.qty
            FROM (
                SELECT COALESCE(receive_item.qty,0)+COALESCE(unallocated.qty,0) as qty, unallocated.code_size, unallocated.number_product, unallocated.number_product_color
                FROM receive_item
                INNER JOIN unallocated
                    ON unallocated.number_product = receive_item.number_product
                    AND unallocated.number_product_color = receive_item.number_product_color
                    AND unallocated.code_size = receive_item.code_size
                WHERE receive_item.id_receiving = :id_receiving
            ) AS t
            WHERE t.number_product = unallocated.number_product
                AND t.number_product_color = unallocated.number_product_color
                AND t.code_size = unallocated.code_size", $bind
        )->execute();

        db::run(
            "INSERT INTO unallocated (qty, code_size, number_product, number_product_color)
                SELECT receive_item.qty, receive_item.code_size, receive_item.number_product, receive_item.number_product_color
                FROM receive_item
                LEFT JOIN unallocated
                    ON unallocated.number_product = receive_item.number_product
                    AND unallocated.number_product_color = receive_item.number_product_color
                    AND unallocated.code_size = receive_item.code_size
                WHERE receive_item.id_receiving = :id_receiving
                    AND unallocated.id_unallocated IS NULL", $bind
        )->execute();

        //muiz.ali@gmail.com
        return redirect(["index"]);
    }

    /**
     * delete receiving
     * @param  string $id receiving ID
     * @return response
     */
    public function actionDelete($id_receiving) {
        db::delete("receive_item", "id_receiving=:id_receiving", [
            "id_receiving" => $id_receiving
        ])->execute();
        db::delete("receiving", "id_receiving=:id_receiving", ["id_receiving" => $id_receiving])->execute();
        return redirect(["index"]);
    }

    /**
     * POST - delete product item 
     * @param  string $id receiving ID
     */
    public function actionDeleteItem($id) {
        if(null!==$this->request->post("product"))
            db::delete("receive_item", "id_receiving=:id and number_product=:product", [
                "id" => $id, "product" => $this->request->post("product")
            ])->execute();
        else
            db::delete("receive_item", "id_receiving=:id", [
                "id" => $id
            ])->execute();
    }

    /**
     * POST - dipanggil ketika close modal qty product, save quantity product yang diisi
     * @param  string $id   stock agreement ID 
     * @param  string $type commit|ready
     */
    public function actionSaveQty($id_receiving, $type) {
        $post = $this->request->post();
        $field_type = [
            "total" => "qty",
        ];

        // invalid type
        if (!isset($field_type[$type])) {
            $this->abort(400);
            return;
        }

        // ambil data variasi qty untuk dibandingkan nanti
        $query = db::query("receive_item", [
                    "code_size", 
                    "number_product_color", 
                    "qty as qty", 
                ])
                ->where(["id_receiving" => $id_receiving, "number_product" => $post['number_product']]);
        $data = [];

        foreach ($query->each() as $val) 
            $data[$val["code_size"]][$val["number_product_color"]] = [
                "total" => $val["qty"],
            ];

        // prin($data, $post);

        // membandingkan data yang dikirim dan yang ada pada database
        // dan menentukan aksi yang akan dilakukan pada data lama
        $insert = $update = $delete = [];

        foreach ($post["qty"] as $size => $item) {
            foreach ($item as $color => $newval) {
                $oldval_exists = isset($data[$size][$color]);
                $oldval = $oldval_exists ? $data[$size][$color][$type] : null;
                $newval_exists = trim($newval)!="";

                // data yang dimasukkan bukan angka
                // atau tidak ada perbedaan data dengan yang ada pada database
                if (($newval_exists && !is_numeric($newval)) || ($oldval_exists && $newval==$oldval))
                    continue;

                if ($oldval_exists) {
                    // delete data quantity
                    if (!$newval_exists) {
                        $will_delete = true;

                        foreach ($data[$size][$color] as $itype => $val) {
                            if ($type == $itype)
                                continue;

                            // hanya update null jika masih ada qty di salah satu kolom (total|)
                            if ($val != null) {
                                $update[] = [$id_receiving, $post["number_product"], $size, $color, null];
                                $will_delete = false;
                                break;
                            }
                        }

                        // delete jika qty di semua kolom null
                        if ($will_delete)
                            $delete[] = [$id_receiving, $post["number_product"], $size, $color];
                    }
                    // update quantity
                    else if ($newval!=$data[$size][$color])
                        $update[] = [$id_receiving, $post["number_product"], $size, $color, $newval];
                }
                else if ($newval_exists) {
                    // buat data baru
                    $insert[] = [$id_receiving, $post["number_product"], $size, $color, $newval];
                }
            }
        }

        $field = [
            "id_receiving", 
            "number_product", 
            "code_size", 
            "number_product_color", 
            $field_type[$type],
        ];

        if (count($insert) > 0)
            db::batchInsert("receive_item", $field, $insert)->execute();
        
        if (count($update) > 0)
            db::batchUpdate(
                "receive_item i", [$field_type[$type]."=t.".$field_type[$type]."::int"], $field, $update, 
                "where t.id_receiving::int=i.id_receiving 
                    and t.number_product=i.number_product
                    and t.code_size::char=i.code_size::char
                    and t.number_product_color=i.number_product_color"
            )->execute();

        if (count($delete) > 0) {
            $bind = $condition = [];

            foreach ($delete as $i => $val) {
                $bind["n$i"] = $val[0];
                $bind["p$i"] = $val[1];
                $bind["s$i"] = $val[2];
                $bind["c$i"] = $val[3];
                $condition[] = "(id_receiving=:n$i and number_product=:p$i and code_size=:s$i and number_product_color=:c$i)";
            }

            db::delete("receive_item", implode(" or ", $condition), $bind)->execute();
        }
    }

    /**
     * dipanggil pertama kali saat halaman draft di load, ambil data banyak product & keterangan product & quantitynya
     * @param  string $id receiving ID
     * @param  string $product product ID
     * @return json
     */
    public function actionGetDraftData($id, $product = null) {
        $bind = ["id" => $id];

        if ($product!=null)
            $bind["product"] = $product;

        $query = db::run(
            "SELECT p.number_product, c.number_product_color, c.color, s.code_size, s.name, 
                i.qty from receiving r 
            left join (
                select distinct i.id_receiving, p.number_product, p.group_size from product p
                inner join receive_item i on p.number_product=i.number_product
            ) as p on r.id_receiving=p.id_receiving
            INNER join product_color c on c.number_product=p.number_product
            INNER join product_size s on s.group_size=p.group_size
            left join receive_item i on i.id_receiving=r.id_receiving
                and c.number_product_color=i.number_product_color
                and s.code_size=i.code_size
            where r.id_receiving=:id ".($product==null ? "" : "and p.number_product=:product")
            ." order by p.number_product", $bind
        )->query();
        $data = [];

        foreach ($query as $val) {
            $data[$val["number_product"]]["number_product"] = $val["number_product"];
            $data[$val["number_product"]]["size"][$val["code_size"]] = $val["name"];
            $data[$val["number_product"]]["attribute"][$val["number_product_color"]] = $val["color"];
            $data[$val["number_product"]]["qty"]["total"][$val["code_size"]][$val["number_product_color"]] = $val["qty"];
        }

        return $this->request->isAjax || $this->controller->action->id=="get-draft-data" ? json($data) : $data;
    }

    /**
     * dipanggil ketika milih product di select2, ambil data keterangan product & init null untuk quantitynya
     * @param  string $id product ID
     * @return json
     */
    public function actionGetProductData($id) {
        $query = db::run(
            "SELECT c.number_product_color, c.color, s.code_size, s.name from product p
            left join product_size s on s.group_size=p.group_size
            left join product_color c on c.number_product=p.number_product
            where p.number_product=:id order by s.code_size, c.color",
            ["id" => $id]
        )->query();
        $data = [];

        foreach ($query as $val) {
            $data["number_product"] = $id;
            $data["size"][$val["code_size"]] = $val["name"];
            $data["attribute"][$val["number_product_color"]] = $val["color"];
            $data["qty"]["total"][$val["code_size"]][$val["number_product_color"]] = null;
        }

        return $this->request->isAjax || $this->controller->action->id=="get-product-data" ? json($data) : $data;
    }

    /**
     * Finds the Receiving model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Receiving the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Receiving::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
