<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\modelName */
?>

<div class="view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
            if($this->can("update")){
                echo Html::a('Update', ['update', 'id' => $model->{$this->context->param("id")}], ['class' => 'btn btn-primary', "modal"=>""]);
            }
        ?>
        <?php
            if($this->can("delete")){
                echo Html::a('Delete', ['delete', 'id' => $model->{$this->context->param("id")}], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]);
            }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => array_keys($model->attributeLabels()),
    ]) ?>
</div>
