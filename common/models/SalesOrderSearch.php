<?php namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SalesOrder;

/**
 * SalesOrderSearch represents the model behind the search form about `common\models\SalesOrder`.
 */
class SalesOrderSearch extends SalesOrder
{
    public function rules()
    {
        return [
            [[
                'number_order',
                'order_time',
                'total_price',
                'status',
                'shipping_cost',
                'total_bill',
                'code_sales_channel',
                'code_buyer',
                'payment_deadline',
                'number_transaction',
            ], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param mixed $status
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SalesOrder::find();
        $query->joinWith(['buyer', 'salesChannel']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // buyer
        $dataProvider->sort->attributes['code_buyer'] = [
            'asc' => ['buyer.name' => SORT_ASC],
            'desc' => ['buyer.name' => SORT_DESC],
        ];

        // sales channel
        $dataProvider->sort->attributes['code_sales_channel'] = [
            'asc' => ['sales_channel.name' => SORT_ASC],
            'desc' => ['sales_channel.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sales_order.status' => $this->status,
            'sales_order.code_sales_channel' => $this->code_sales_channel,
            "to_char(sales_order.order_time, 'DD/MM/YYYY')" => $this->order_time,
            "to_char(sales_order.payment_deadline, 'DD/MM/YYYY')" => $this->payment_deadline,
        ]);

        $query->andFilterWhere(['ilike', 'sales_order.number_order', $this->number_order])
            ->andFilterWhere(['ilike', 'sales_order.shipping_cost', $this->shipping_cost])
            ->andFilterWhere(['ilike', '"sales_order"."total_bill"::varchar', $this->total_bill])
            ->andFilterWhere(['ilike', '"sales_order"."total_price"::varchar', $this->total_price])
            ->andFilterWhere(['ilike', 'buyer.name', $this->code_buyer])
            ->andFilterWhere(['ilike', 'sales_order.number_transaction', $this->number_transaction]);

        // prin($query->createCommand()->rawsql);
        return $dataProvider;
    }
}
