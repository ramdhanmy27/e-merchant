<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BankAccount */

$this->title = $model->code_bank;
$this->params['breadcrumbs'][] = ['label' => 'Bank Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bank-account-view">
    <p>
        <?php if (access("update")) : ?>
            <?= Html::a('Update', ['update', 'id' => $model->code_bank], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php
            if ($transaction==0 && access("delete")) {
    	        echo Html::a('Delete', ['delete', 'id' => $model->code_bank], [
    	            'class' => 'btn btn-danger',
    	            'data' => [
    	                'confirm' => 'Are you sure you want to delete this item?',
    	                'method' => 'post',
    	            ],
    	        ]); 
        	}
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code_bank',
            'bank_name',
            'account_number',
            'account_name',
            'current_balance',
        ],
    ]) ?>
</div>