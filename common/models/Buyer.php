<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "buyer".
 *
 * @property string $code_buyer
 * @property string $name
 * @property string $delivery_address
 * @property string $delivery_city
 * @property string $post_code
 * @property string $phone
 * @property string $email
 *
 * @property SalesOrder[] $salesOrders
 */
class Buyer extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buyer';
    }

    public function rules()
    {
        return [
            // required
            [['code_buyer'], 'required'],

            // unique
            [['code_buyer', 'phone', 'email'], 'unique'],

            // string
            [['post_code'], 'string', 'max' => 6],
            [['code_buyer'], 'string', 'max' => 12],
            [['phone'], 'string', 'max' => 13],
            [['name', 'delivery_city', 'email'], 'string', 'max' => 100],
            [['delivery_address'], 'string'],
            [['email'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'code_buyer' => 'Buyer Code',
            'name' => 'Name',
            'delivery_address' => 'Delivery Address',
            'delivery_city' => 'Delivery City',
            'post_code' => 'Post Code',
            'phone' => 'Phone',
            'email' => 'Email',
        ];
    }

    public function save($validate=true, $attr=null) {
        if ($this->code_buyer == null)
            $this->code_buyer = date("ymdHis");

        return parent::save($validate, $attr);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrders()
    {
        return $this->hasMany(SalesOrder::className(), ['code_buyer' => 'code_buyer']);
    }
}
