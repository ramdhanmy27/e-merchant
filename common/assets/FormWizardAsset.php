<?php namespace common\assets;

class FormWizardAsset extends \yii\web\AssetBundle
{
    public $baseUrl = '@web';

    public $js = ["js/jquery.steps.min.js", "ace/dist/js/jquery.validate.min.js"];
    public $css = ["css/jquery.steps.min.css"];
    
    public $depends = [
    	"yii\web\JqueryAsset",
    ];
}
