<?php

use kartik\mpdf\Pdf;

include_once dirname(__DIR__)."/components/debug.php";
include_once dirname(__DIR__)."/components/controller.php";
date_default_timezone_set('Asia/Jakarta');

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'view' => [
            'class' => 'common\yii\web\View',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=192.168.1.123;port=5432;dbname=yii2-application',
            'username' => 'postgres',
            'password' => 'admindev',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,//set this property to false to send mails to real email addresses
            //comment the following array to send mail using php's mail function
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'irmailtestero@gmail.com',
                'password' => "admindev123",
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'pdf' => [
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'cssFile' => "@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css",
            'cssInline' => file_get_contents(Yii::getAlias("@backend/web/css/pdf-report.css")),
        ],
        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD', //GD or Imagick
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    "js" => ['jquery.min.js']
                ]
            ]
        ],
        "formatter" => [
            "dateFormat" => "php:d M Y",
            "datetimeFormat" => "php:H:i - d M Y",
        ],
    ],
    "modules" => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'redactor' => 'yii\redactor\RedactorModule',
    ]
];
