<?php namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Warehouse;

class WarehouseSearch extends Warehouse
{
    public function rules()
    {
        return [
            [['rack_count', 'room_width', 'room_length', 'room_height'], 'integer'],
            [['code_warehouse', 'name', 'address', 'city', 'description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Warehouse::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rack_count' => $this->rack_count,
            'room_width' => $this->room_width,
            'room_length' => $this->room_length,
            'room_height' => $this->room_height,
        ]);
        
        $query->andFilterWhere(['ilike', 'code_warehouse', $this->code_warehouse])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'address', $this->address])
            ->andFilterWhere(['ilike', 'city', $this->city])
            ->andFilterWhere(['ilike', 'description', $this->description]);

        return $dataProvider;
    }
}
