<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Inventory Picking List';
?>

<div class="batch-delivery-index">
	<?= GridView::widget([
		"dataProvider" => $dataProvider,
		"columns" => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'header' => 'Product Name',
				'attribute' => 'name',
			],
			'size',
			'color',
			[
				'header' => 'Qty',
				'attribute' => 'qty',
				'content' => function($data) {
					return number_format($data["qty"]);
				}
			],
		]
	]) ?>
</div>