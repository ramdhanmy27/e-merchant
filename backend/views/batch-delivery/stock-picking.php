<?php 

use yii\helpers\Url;

$this->title = 'Inventory Stock Picking Instruction'; 
?>

<?php if ($view == "pdf"): ?>
    <div class="page-header" style="padding: 0px 10px 10px">
    	<h3><?= $this->title ?></h3>

    	<table width="100%">
    		<tr> 
    			<td class="no-pad"> 
    				<h4 style="margin:0px; margin-top:5px" class="text-muted"><?= $model->number_batch ?></h4> 
    			</td>
    			<td class="text-right text-muted no-pad text-middle">
    				<?= date("H:i - d F Y", strtotime($model->batch_time)) ?>
    			</td> 
    		</tr>
    	</table>
    </div>
<?php endif; ?>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Product Name</th>
            <th>Size</th>
            <th>Color</th>
            <th>Rack</th>
            <th>Box</th>
            <th>Qty</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($query->query() as $val): ?>
        <tr>
            <td><?= $val["name"] ?></td>
            <td><?= $val["size"] ?></td>
            <td><?= $val["color"] ?></td>
            <td><?= $val["number_rack"] ?></td>
            <td><?= $val["number_box"] ?></td>
            <td><?= $val["qty"] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php if ($view !== "pdf"): ?>
	<div class="text-right">
		<a href="<?= Url::to(["stock-picking", "id" => $model->number_batch, "view" => "pdf"]) ?>" 
			class="btn btn-primary" target="_blank">
			Print Instruction
		</a>
	</div>
<?php endif; ?>