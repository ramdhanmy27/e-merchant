<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logistic_partner".
 *
 * @property string $code_tpl
 * @property string $name
 * @property string $member_acc_number
 * @property string $contact_person
 * @property string $phone
 * @property string $email
 * @property string $description
 *
 * @property LogisticService[] $logisticServices
 */
class LogisticPartner extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistic_partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['code_tpl'], 'required'],

            // unique
            [['code_tpl'], 'unique'],

            // safe

            // integer

            // string
            [['code_tpl'], 'string', 'max' => 4],
            [['name', 'member_acc_number'], 'string', 'max' => 12],
            [['phone'], 'string', 'max' => 13],
            [['contact_person'], 'string', 'max' => 16],
            [['description'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 255],

            // exist
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_tpl' => 'Tpl Code',
            'name' => 'Tpl Name',
            'member_acc_number' => 'Member Acc Number',
            'contact_person' => 'Contact Person',
            'phone' => 'Phone',
            'email' => 'Email',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticServices()
    {
        return $this->hasMany(LogisticService::className(), ['code_tpl' => 'code_tpl']);
    }
}
