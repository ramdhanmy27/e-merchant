<?php

namespace common\models;

use Yii;
use common\yii\db\db;
use common\models\ProductImages;

/**
 * This is the model class for table "product".
 * 
 * @property Category $idCategory
 * @property Merchant $idMerchant
 * @property LockerAllocation $lockerAllocations
 */
class Product extends \common\yii\db\ActiveRecord
{
    public static function tableName() {
        return 'product';
    }

    public function behaviors() {
        return [
            ['class' => \yii\behaviors\TimestampBehavior::className()],
        ];
    }

    public function rules() {
        return [
            [['parent', 'id_category', 'id_merchant', 'created_at', 'updated_at', 'group', 'measurement', 'regular_price', 'sale_price', 'sale_disc', 'employee_price', 'employee_disc', 'wholesale_price', 'wholesale_disc', 'custom_price', 'custom_disc', 'cost', 'avg_cost', 'order_cost', 'msrp', 'margin', 'markup', 'quantity'], 'integer'],
            [['id_category', 'id_merchant', 'name', 'regular_price', 'quantity'], 'required'],
            [['description', 'comment'], 'string'],
            [['print_tags', 'unorderable', 'use_serial', 'earn_commission', 'earn_rewards'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['sku'], 'string', 'max' => 20],
            [['sku'], 'unique'],
            [['size', 'attribute', 'upc', 'alu', 'reorder_point', 'manufacturer', 'shipping_weight', 'shipping_height', 'shipping_length', 'shipping_width'], 'string', 'max' => 100]
        ];
    }

    public function attributeLabels($attr = null) {
        $attributes = [
            'id' => 'ID',
            'parent' => 'Parent',
            'id_category' => 'Category',
            'id_merchant' => 'Merchant',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'name' => 'Name',
            'description' => 'Description',
            'size' => 'Size',
            'attribute' => 'Attribute',
            'group' => 'Group',
            'comment' => 'Comment',
            'sku' => 'SKU',
            'measurement' => 'Measurement',
            'upc' => 'UPC',
            'alu' => 'ALU',
            'reorder_point' => 'Reorder Point',
            'manufacturer' => 'Manufacturer',
            'print_tags' => 'Print Tags',
            'unorderable' => 'Unorderable',
            'use_serial' => 'Use Serial',
            'earn_commission' => 'Earn Commission',
            'earn_rewards' => 'Earn Rewards',
            'shipping_weight' => 'Shipping Weight',
            'shipping_height' => 'Shipping Height',
            'shipping_length' => 'Shipping Length',
            'shipping_width' => 'Shipping Width',
            'regular_price' => 'Regular Price',
            'sale_price' => 'Sale Price',
            'sale_disc' => 'Sale Disc',
            'employee_price' => 'Employee Price',
            'employee_disc' => 'Employee Disc',
            'wholesale_price' => 'Wholesale Price',
            'wholesale_disc' => 'Wholesale Disc',
            'custom_price' => 'Custom Price',
            'custom_disc' => 'Custom Disc',
            'cost' => 'Cost',
            'avg_cost' => 'Avg Cost',
            'order_cost' => 'Order Cost',
            'msrp' => 'Msrp',
            'margin' => 'Margin',
            'markup' => 'Markup',
            'quantity' => 'Quantity',
        ];

        return isset($attributes[$attr]) ? $attributes[$attr] : $attributes;
    }

    public function save($validate=true, $attributeNames=null) {
        $post = Yii::$app->request->post();
        $id_locker = isset($post['LockerAllocation'][0]['id_locker']) ? $post['LockerAllocation'][0]['id_locker'] : null;

        if (trim($this->sku) == "")
            $this->sku = null;

        // update
        if (!$this->isNewRecord) {
            if (isset($post['variations']) && isset($post['oldVariations']))
                $this->setVariations($post['variations'], $post['oldVariations'], $id_locker);

            return parent::save($validate, $attributeNames);
        }
        // create
        else {
            $save = parent::save($validate, $attributeNames);

            if (isset($post['variations']))
                $this->setVariations($post['variations'], isset($post['oldVariations']) ? $post['oldVariations'] : [], $id_locker);

            return $save;
        }
    }

    /**
     * get model attributes + product variations
     * @param  int  $id
     * @return array
     */
    public function getAllAttributes($id = null) {
        if ($id==null || ($model = $this->findOne($id))==null)
            $model = $this;

        $data = $model->getAttributes();
        $data["variations"] = [];

        // model is empty
        // return empty default data
        if ($model->isNewRecord)
            return $data;

        // generate data
        $data["variations"] = $model->getVariations();

        return $data;
    }

    /**
     * get parent group ID (yes it does)
     * @return int
     */
    public function getParentGroupId() {
        return $this->parent!=null ? $this->parent : $this->id;
    }

    /**
     * get product variations data
     * @return array
     */
    public function getVariations() {
        if ($this->isNewRecord)
            return false;

        return db::query("product", "size, attribute, quantity")
                ->where("(parent=:parent or id=:parent)", [
                    "parent" => $this->getParentGroupId()
                ])
                ->orderBy("id") // null last
                ->all();
    }

    /**
     * update product variations
     */
    protected function setVariations($variations, $oldVariations = [], $id_locker = null) {
        // model or data is empty
        if ($this->isNewRecord || $variations == null)
            return false;
 
        // find duplicates in attribute/size
        foreach (["attributes", "sizes"] as $type) {
            foreach(array_count_values($variations[$type]) as $count) {
                if ($count > 1)
                    return false;
            }
        }

        $parent = $this->getParentGroupId();

        /*Updates Attribute & size*/
        foreach (["sizes", "attributes"] as $type) {
            $data = ["parent" => $parent];
            $values = [];

            foreach ($variations[$type] as $i => $label) {
                // update attributes label
                if (isset($oldVariations[$type][$i]) && $oldVariations[$type][$i]!=$variations[$type][$i]) {
                    $data["old_$i"] = $oldVariations[$type][$i];
                    $data["new_$i"] = $variations[$type][$i];
                    $values[] = "(:".implode(",:", ["old_$i", "new_$i"]).")";
                }
            }

            if (count($values) == 0)
                continue;

            $field = substr($type, 0, strlen($type)-1);

            // update label size or attrbute on database
            db::run(
                "UPDATE product p set $field=t.newval
                from (
                    select array_agg(id) id, t.oldval, t.newval from product p
                    inner join (values ".implode(",", $values).") as t(oldval, newval) on t.oldval=p.$field
                    where (id=:parent or parent=:parent)
                    group by t.oldval, t.newval
                ) as t
                where p.id=any(t.id)", $data
            )->query();
        }

        /*Generates Variations*/
        $data = [
            "insert" => [],
            "update" => [],
        ];

        $attrModel = $this->getAttributes();
        unset($attrModel["id"], $attrModel["sku"]);

        foreach ($variations['sizes'] as $i => $size) {
            foreach ($variations['attributes'] as $j => $attribute) {
                // data was reference to current model
                // or quantity was not a number
                // or either size or attribute was empty
                if (($this->size==$size && $this->attribute==$attribute) 
                    || !isset($variations["qty"][$i][$j]) || !is_numeric($variations["qty"][$i][$j])
                    || trim($size)=="" || trim($attribute)=="")
                    continue;

                /*Batch Insert Data*/
                if (!isset($oldVariations["quantity"][$i][$j]) || !is_numeric($oldVariations["quantity"][$i][$j])) {
                    $data["insert"][] = array_merge($attrModel, [
                        "size" => $size,
                        "attribute" => $attribute,
                        "quantity" => $variations["qty"][$i][$j],
                        "parent" => $parent,
                    ]);
                }

                /*Batch Update Data*/
                if (isset($oldVariations["quantity"][$i][$j]) 
                    && is_numeric($oldVariations["quantity"][$i][$j])
                    && $variations["qty"][$i][$j]!=$oldVariations["quantity"][$i][$j]) {
                    $data["update"][] = [
                        "quantity" => $variations["qty"][$i][$j],
                        "size" => $size,
                        "attribute" => $attribute,
                    ];
                }
            }
        }

        // insert all data
        if (count($data["insert"]) > 0) {
            $id_locker = is_numeric($id_locker) ? $id_locker : null;

            $data_alloc = db::run(
                db::batchInsert("product", array_keys($attrModel), $data["insert"])->sql
                ." RETURNING id, quantity, :locker::int", ["locker" => $id_locker]
            )->queryAll();
            
            // insert data allocation if any
            if ($id_locker != null && count($data_alloc) > 0)
                db::batchInsert("locker_allocation", ["id_product", "quantity", "id_locker"], $data_alloc)->execute();
        }

        // update all quantity
        if (count($data["update"]) > 0) 
            db::batchUpdate(
                "product p", [["quantity", "int"], "size", "attribute"], $data["update"], 
                "where p.attribute=t.attribute and p.size=t.size and (p.parent=:parent or p.id=:parent)", 
                ["parent" => $parent]
            )
            ->execute();
    }

    /**
     * upload product images
     * @param  string $name "input name"
     * @return boolean
     */
    public function uploadImages($name) {
        if (!isset($_FILES[$name]))
            return false;

        $model = new ProductImages($this->id);
        return $model->upload($_FILES[$name]["tmp_name"]);
    }

    /*Relations*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMerchant()
    {
        return $this->hasOne(Merchant::className(), ['id' => 'id_merchant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLockerAllocations()
    {
        return $this->hasMany(LockerAllocation::className(), ['id_product' => 'id']);
    }

    public function getImages(){
        return $this->hasMany(ProductImages::classname(), ["id_product" => "id"]);
    }
}
