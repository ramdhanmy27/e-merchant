<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "channel".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Order[] $orders
 * @property ProductAllocation[] $productAllocations
 */
class Channel extends \yii\db\ActiveRecord
{
    public static $channel = ["zalora"];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['api_key'], 'safe'],
            [['user_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'api_key' => 'Api Key',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id_channel' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAllocations()
    {
        return $this->hasMany(ProductAllocation::className(), ['id_channel' => 'id']);
    }
}
