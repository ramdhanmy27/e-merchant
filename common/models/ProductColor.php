<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use common\yii\db\db;

/**
 * This is the model class for table "product_color".
 *
 * @property integer $number_product_color
 * @property string $color
 * @property string $photo1
 * @property string $photo2
 * @property string $photo3
 * @property string $photo4
 * @property string $photo5
 * @property string $number_product
 *
 * @property AgreementItem[] $agreementItems
 * @property InventoryItem[] $inventoryItems
 * @property OrderItem[] $orderItems
 * @property Product $product
 * @property PurchaseOrderItem[] $purchaseOrderItems
 * @property ReceiveItem[] $receiveItems
 * @property StockAllocation[] $stockAllocations
 * @property Unallocated[] $unallocateds
 */
class ProductColor extends \common\yii\db\ActiveRecord
{
    public 
        // product images path
        $path = "@webroot/images/product", 
        // product images path
        $url = "@web/images/product", 
        
        // allowed extensions
        $ext = [".gif", ".png", ".jpg", ".jpeg"],

        // custom size
        $size = [
            "small" => [100, 100], 
            "thumb" => [600, 600],
        ];
        
    public function __construct() {
        $this->path = Yii::getAlias($this->path);
        $this->url = Yii::getAlias($this->url);
    }

    public static function tableName() {
        return 'product_color';
    }

    public function rules() {
        return [
            // required
            [['number_product_color', 'number_product'], 'required'],

            // string
            [['color'], 'string', 'max' => 16],

            // safe
            [['photo'], 'safe'],

            // exist
            [['number_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 
                'targetAttribute' => ['number_product' => 'number_product']],
        ];
    }

    public function save($validate=true, $attributeNames=null) {
        if ($this->number_product_color === null)
            $this->number_product_color = date("Ymdhis").rand();

        return parent::save($validate, $attributeNames);
    }

    public function attributeLabels() {
        return [
            'number_product_color' => 'Product Color Number',
            'color' => 'Color',
            'photo' => 'Photo',
            'number_product' => 'Product Number',
            'product.name' => 'Product',
        ];
    }

    public function getDir() {
        return $this->path."/".$this->number_product."/".$this->number_product_color;
    }

    public function getBaseUrl() {
        return $this->url."/".$this->number_product."/".$this->number_product_color;
    }

    /**
     * get image url path
     * @param  string  $size  @see property $size
     * @return string
     */
    public function getUrl($number = null, $size = null) {
        $number_exists = $number!=null;

        if ($this->isNewRecord)
            return false;

        $path = function($photo) use($size) {
            return $this->getBaseUrl()."/".($size==null ? null : $size."-").$photo;
        };

        if ($number_exists && !($photo = $this->getPhoto($number))) {
            return $path($photo);
        }
        elseif (!$number_exists) {
            $data = [];

            foreach ($this->getPhoto() as $file) {
                if (is_file($this->getDir()."/".$file))
                    $data[] = $path($file);
            }

            return $data;
        }
    }

    public function getPhoto($index = null) {
        if ($this->photo !== null) {
            $data = preg_split("/\"?\,\"?/", preg_replace("/(^\{\"?|\"?\}$)/", "", $this->photo));

            if ($index === null)
                return $data;
            else if (isset($data[$index]))
                return $data[$index];
        }

        return [];
    }

    public function getPhotoInfo($index=null, $size="") {
        $data = [];
        $size = $size==null ? "" : $size."-";
        $dir = $this->getDir();
        $baseurl = $this->getBaseUrl();

        foreach ($this->getPhoto() as $img) {
            $origin = $dir."/".$img;
            $filename = $size.$img;

            if (!file_exists($origin))
                continue;

            $data[] = [
                "name" => $img,
                "url" => $baseurl."/".(file_exists($dir."/".$filename) ? $filename : $img),
                "size" => filesize($origin),
            ];
        }

        if ($index === null)
            return $data;
        else if (isset($data[$index]))
            return $data[$index];
        else 
            return false;
    }

    /**
     * delete data (also it's file)
     * @return boolean
     */
    public function delete() {
        if ($this->isNewRecord)
            return false;

        $dir = $this->getDir();

        if (file_exists($dir))
            FileHelper::removeDirectory($dir);
        
        return parent::delete();
    }

    /**
     * upload image files (group by product id)
     * @param  string|array $files
     * @return boolean
     */
    public function upload($files) {
        if ($this->isNewRecord)
            return false;

        $path = $this->getDir();

        // path is readable and writeable
        if (!FileHelper::createDirectory($path))
            return false;

        // filter files by checking its existence
        $files = is_array($files) ? array_values($files) : [$files];
        $images = [];

        foreach ($files as $i => $file) {
            if (!file_exists($file))
                continue;

            $ext = image_type_to_extension(exif_imagetype($file));
            
            if (!in_array($ext, $this->ext))
                continue;

            // Upload Images
            // original
            $image = Yii::$app->image->load($file);
            $images[] = $filename = date("Ymdhis").rand().$ext;
            $image->save($path."/".$filename);
            
            // custom size
            foreach ($this->size as $label => $size) {
                $image->resize($size[0], $size[1], \yii\image\drivers\Image::HEIGHT);
                $image->save($path."/".$label."-".$filename);
            }
        }

        if (count($images) == 0)
            return false;
        
        db::update(
            "product_color", ["photo" => db::raw("array_cat(photo, '{\"".implode('","', $images)."\"}'::varchar[])")],
            "number_product_color=:id", ["id" => $this->number_product_color]
        )->execute();

        return $images;
    }

    /**
     * remove images
     * @param  string|array $file filename
     */
    public function deleteImages($images) {
        $images = is_array($images) ? $images : [$images];

        foreach ($images as $img) {
            $size = array_keys($this->size);
            $size["origin"] = "";

            foreach ($size as $label) {
                $file = $this->getDir()."/".($label=="" ? "" : $label."-").$img;

                if (!is_file($file))
                    continue;

                @unlink($file);
            }
        }

        if (count($images)==0)
            return;

        db::update(
            "product_color", ["photo" => db::raw("(
                select array_agg(t.id) from (
                    select unnest(photo) id except select '".implode("' except select '", $images)."' ORDER BY id
                ) t
            )")],
            "number_product_color=:id", ["id" => $this->number_product_color]
        )->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementItems() {
        return $this->hasMany(AgreementItem::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItems() {
        return $this->hasMany(InventoryItem::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems() {
        return $this->hasMany(OrderItem::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderItems() {
        return $this->hasMany(PurchaseOrderItem::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiveItems() {
        return $this->hasMany(ReceiveItem::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockAllocations() {
        return $this->hasMany(StockAllocation::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnallocateds() {
        return $this->hasMany(Unallocated::className(), ['number_product_color' => 'number_product_color']);
    }
}
