<?php
/**
 * widget box
 * @param  string $title
 * @param  string $body
 */

$attribute = null;

if (isset($attr)) {
    if (is_array($attr)) {
        foreach ($attr as $key => $val) {
        	if ($key == "class")
        		continue;

            $attribute .= " $key='$val'";
        }
    }
    else $attribute = $attr;
}
?>

<div class="form-group <?= @$attr["class"] ?>" <?= $attribute ?>>
    <label class="control-label col-md-4 col-xs-12" <?= isset($for) ? "for='$for'" : "" ?>> <?= @$label ?> </label>
    <div class="col-md-8 col-xs-12">
        <?= @$content ?>
    </div>
</div>