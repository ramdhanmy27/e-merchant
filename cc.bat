@echo off

@setlocal

set ROOT_PATH=%~dp0

cd "%ROOT_PATH%tests/codeception/backend"
codecept %*

@endlocal