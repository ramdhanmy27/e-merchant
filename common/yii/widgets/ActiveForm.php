<?php namespace common\yii\widgets;

class ActiveForm extends \yii\widgets\ActiveForm {
    public $fieldClass = 'common\yii\widgets\ActiveField';

    public static function begin($config = []) {
    	if (!isset($config["options"]["class"]))
    		if (!isset($config["options"]))
	    		$config["options"] = ["class" => "form-horizontal"];
	    	else
	    		$config["options"] = array_merge(["class" => "form-horizontal"], $config["options"]);

    	return parent::begin($config);
    }
}