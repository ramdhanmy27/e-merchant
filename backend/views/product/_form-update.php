<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;

$data = [];

foreach ($model->productColors as $val)
    $data[] = [
        'color' => $val->color,
        'photo' => $val->getPhotoInfo(null, "small"),
    ];

?>

<div class="product-form" ng-controller="productController" 
    ng-init='f.init(<?= json_encode($data) ?>); id="<?= $model->number_product ?>"'>
    <?php $form = ActiveForm::begin([
        "id" => "product", 
        'options' => [
            'enctype' => 'multipart/form-data', 
            'name' => 'product_form',
        ]
    ]); ?>

    <!-- Basic Information -->
    <div class='widget-box col-md-8'>
        <div class="widget-header">
            <h5 class="widget-title">Basic Information</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>

        <div class="widget-body widget-main">
            <?= $form->field($model, 'number_product')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'code_category')->textInput() ?>
            <?= $form->field($model, 'brand_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textArea() ?>
            <?= $form->field($model, 'group_size')->option(common\models\Product::getGroupSize()) ?>
            <?= $form->field($model, 'basic_price')->textInput() ?>

            <hr>
            <div class='text-right'>
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div>

    <!-- Product Color -->
    <div class='widget-box'>
        <div class="widget-header">
            <h5 class="widget-title">Product Color</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>

        <div class="widget-body widget-main">
            <table class="table table-responsive table-hover table-striped table-bordered" form-validation>
                <thead>
                    <tr> <th>Color</th> <th width="75%">Images</th> <th>Action</th> </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="(i, val) in f.data track by i" class="color">
                        <td>
                            <?= Html::input('text', 'color[{{i}}][color]', '', [
                                'ng-model' => 'f.color[i]',
                                'ng-change' => 'f.setColor(i)',
                                'ng-model-options' => '{debounce: 1000}',
                                "class" => "form-control",
                                'required' => '',
                            ]) ?>

                            <div ng-messages='product_form["color["+i+"][color]"].$error' 
                                 ng-if='product_form["color["+i+"][color]"].$dirty'>
                                <div ng-message="required">This field is required</div>
                            </div>

                            <div ng-if="loading[i]" class="text-muted">Please Wait...</div>
                        </td>
                        <td>
                            <form class="dropzone" id="dropzone-{{i}}">
                                <div class="fallback">
                                    <input name="file" type="file" multiple="" />
                                </div>
                            </form>
                        </td>
                        <td class="nowrap">
                            <?= Html::button('<span class="fa fa-trash"></span>', [
                                "class" => "delete-color btn btn-danger", 
                                'ng-click' => 'f.deleteRow(i)',
                                "title" => "Delete",
                            ]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="form-group pull-right">
                                <?= Html::button('Add row', [
                                    'name' => 'addRow', 
                                    'value' => 'true', 
                                    'class' => 'btn btn-info', 
                                    "ng-click" => "f.addRow()",
                                ]) ?>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>