<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "box_content".
 *
 * @property integer $id_box_content
 * @property integer $qty
 * @property string $sku
 * @property string $number_box
 *
 * @property Box $box
 * @property InventoryItem $inventoryItem
 */
class BoxContent extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'box_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            // [['id_box_content'], 'required'],

            // unique
            // [['id_box_content'], 'unique'],

            // safe

            // integer
            [['qty'], 'integer'],

            // string
            [['number_box'], 'string', 'max' => 12],
            [['sku'], 'string', 'max' => 255],

            // exist
            [['number_box'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['number_box' => 'number_box']],
            [['sku'], 'exist', 'skipOnError' => true, 'targetClass' => InventoryItem::className(), 'targetAttribute' => ['sku' => 'sku']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_box_content' => 'Id Box Content',
            'qty' => 'Qty',
            'sku' => 'SKU',
            'number_box' => 'Box Number',
            'box.number_rack' => 'Rack Number',
            'inventoryItem.product.name' => 'Product Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBox()
    {
        return $this->hasOne(Box::className(), ['number_box' => 'number_box']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItem()
    {
        return $this->hasOne(InventoryItem::className(), ['sku' => 'sku']);
    }
}
