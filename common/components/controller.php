<?php

use yii\helpers\Url;
use Respect\Validation\Exceptions\ValidationException;

function can($perm) {
	if (!param("permissions.enable"))
		return true;
	
	switch (gettype($perm)) {
		case 'NULL':
			return true;

		case "boolean":
			return $perm;

		case "object":
			if (is_callable($perm))
				return $perm();
			break;

		case "array":
			$can = 1;

			foreach ($perm as $permName)
				$can *= \Yii::$app->user->can($permName);

			return (bool) $can;

		case "string": 
			return \Yii::$app->user->can($perm);

		default: return (bool) $perm;
	}
}

function numeric($value, $type = null) {
	$fmt = new NumberFormatter('de_DE', NumberFormatter::DECIMAL);

	if ($type != null)
		return $fmt->parse($value, $type);
	else
		return $fmt->parse($value);
}

function int($value) {
	return numeric($value, NumberFormatter::TYPE_INT64);
}

function json($data) {
	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    return $data;
}

function xml_encode($data) {
	$xml = '';

	foreach ($data as $key => $value) {
		// if array is sequential
		if (is_array($value) && array_keys($value)===range(0, count($value)-1)) {
			foreach ($value as $val)
				$xml .= "<$key>".(is_array($val) ? xml_encode($val) : $val)."</$key>";
		}
		else $xml .= "<$key>".(is_array($value) ? xml_encode($value) : $value)."</$key>";
	}

	return $xml;
}

function view($view=null, $params=[]) {
	return Yii::$app->controller->render($view, $params);
}

/**
 * Redirects the browser to the specified URL.
 * This method is a shortcut to [[Response::redirect()]].
 *
 * You can use it in an action by returning the [[Response]] directly:
 *
 * ```php
 * // stop executing this action and redirect to login page
 * return $this->redirect(['login']);
 * ```
 *
 * @param string|array $url the URL to be redirected to. This can be in one of the following formats:
 *
 * - a string representing a URL (e.g. "http://example.com")
 * - a string representing a URL alias (e.g. "@example.com")
 * - an array in the format of `[$route, ...name-value pairs...]` (e.g. `['site/index', 'ref' => 1]`)
 *   [[Url::to()]] will be used to convert the array into a URL.
 *
 * Any relative URL will be converted into an absolute one by prepending it with the host info
 * of the current request.
 *
 * @param integer $statusCode the HTTP status code. Defaults to 302.
 * See <http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html>
 * for details about HTTP status code
 * @return Response the current response object
 */
function redirect($url, $statusCode = 302) {
    return Yii::$app->getResponse()->redirect(Url::to($url), $statusCode);
}

/**
 * return 16 digits unique number
 * @return string number
 */
function unic() {
	return date("YmdHis").rand(0, 99);
}

/**
 * check existence of permission
 * @param  string $perm 
 * @return boolean
 */
function perm_exists($perm) {
	return Yii::$app->controller->permExists($perm);
}

/**
 * handle exception
 * @param  Exception $e exception instance
 * @param  int $mode error handle mode
 *   @see @common/config/params-local
 *   0 - throw exception
 *   1 - set flash alert 
 */
function handle($e, $mode=null) {
	switch ($mode===null ? param("error") : $mode) {
		// set flash alert
		case 1:
			if ($e instanceof ValidationException) {
				$msg = $e->getMessages();
			}
			else $msg = $e->getMessage();

			Yii::$app->session->setFlash("error", $msg);
			break;
		
		default: throw $e;
	}
}

/**
 * get params value
 * @param  string $path path.to.value
 * @return
 */
function param($path) {
	$tmp = Yii::$app->params;

	foreach (explode(".", $path) as $key) {
		if (!isset($tmp[$key]))
			return null;

		$tmp = $tmp[$key];
	}

	return $tmp;
}