<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->number_contract;

?>

<div class="stock-agreement-view">
    <?php if(!$this->request->isAjax) : ?>
        <div class="page-header">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    <?php endif; ?>

    <p>
    <?php 
        if ($model->isDraft() && $this->can("draft"))
            echo Html::a('Update', ['draft', 'id' => $model->number_contract], ['class' => 'btn btn-primary']);

        echo Html::a('Delete', ['delete', 'id' => $model->number_contract], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) 
    ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number_contract',
            'contract_date',
            'until_date',
            'description',
            'merchant.name',
        ],
    ]) ?>
</div>