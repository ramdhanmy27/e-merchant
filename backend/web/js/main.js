var csrfToken = $('meta[name="csrf-token"]').attr("content");

/* URL Library */
var url = function(url, params) {
	if (params===undefined || params.length==0)
		return url;

	var query = [];

	for (var key in params)
		query.push(key + '=' + encodeURIComponent(params[key]));

	return url + '?' + query.join("&");
};

url.base = window.location.origin + window.location.pathname;

/**
 * generate yii url
 * @param  array data
 * @return string
 */
url.to = function(path, params) {
	params = params || {};
	params['r'] = path;

	return url(this.base, params);
};

/*General Library*/
var fn = {
	is_numeric: function(value) {
		return !((typeof value=="string" && value.trim()=="") || isNaN(value) || !isFinite(value) || value===undefined || value===null)
	},
	int: function(value) {
		return fn.is_numeric(value) ? parseInt(value) : 0;
	},
	alertError: function(message, title) {
		var modal = $("#error-alert");

		modal.modal("show");

		if (title !== undefined)
			$(".modal-title", modal).html(title);

		if (message !== undefined)
			$(".modal-body", modal).html(message);
	},
	alert: function(message, title) {
		var modal = $("#modal-basic");

		modal.modal("show");

		$(".modal-title", modal).html(title || "Info");
		$(".modal-body", modal).html(message);
	},
	confirm: function(opt) {
		var modal = $(opt.selector || "#winconfirm");

		// show modal event
		if (!modal.hasClass("in")) {
			modal.find(".modal-title").html(opt.title || "Warning!");
			modal.find(".modal-body").html(opt.body || "Are you Sure ?");
			modal.modal("show");

			var accept = false;

			// yes
			modal.find('.modal-accept').unbind("click").bind("click", function(e) {
				accept = true;

				if (opt.hasOwnProperty("yes"))
					opt.yes(e);
			});
			
			// no
			modal.off("hidden.bs.modal").on("hidden.bs.modal", function(e) {
				if (accept===false && opt.hasOwnProperty("no"))
					opt.no(e)
			})
		}

		return modal;
	},
	isEmpty: function(object) {
		var type = typeof object;

		switch (type) {
			case "undefined":
				return true;

			case "object":
				if (object === null)
					return true;
				
				return object.hasOwnProperty('length') ? object.length==0 : Object.keys(object).length==0;
				
			case "string":
				return object.trim()==="";

			default: return false;
		}
	},
	isset: function(value) {
		if (value === undefined || value === null)
			return false;

		for (var i in arguments) {
			if (i == 0)
				continue;

			if (typeof value[arguments[i]] === "undefined")
				return false;

			value = value[arguments[i]];
		}

		return true;
	},
	validate: function(value, type) {
		switch (type) {
			case "required": 
				return !fn.isEmpty(value);

			default: 
				if (!fn.isset(fn.validate.validation, type, "pattern"))
					return false; 

				return !!value.match(fn.validate.validation[type].pattern);
		}
	}
};

fn.validate.validation = {
	integer : {
		element: "[integer]",
		message: "It must be an integer.",
		pattern: /^\s*[+-]?\d+\s*$/,
	},
	number : {
		element: "[type='number'],[number]",
		message: "It must be a number.",
		pattern: /^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?\s*$/,
	},
	required : {
		element: "[required]",
		message: "It cannot be blank.",
	},
	string : {
		element: "[string]",
		message: "It cannot be blank.",
	},
	email : {
		element: "[type='email'],[email]",
		message: "It is not a valid email address.",
		pattern: /^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/,
	},
};

/* Angular */
var isAngular = typeof angular !== 'undefined';

if (isAngular) {
	var moduleExists = function(name) {
       try{
          angular.module(name);
          return true;
       }
       catch(err){
          return false;
       }
    }
	var module = ["ngMessages"];

	for (var i in module) {
		if (!moduleExists(module[i]))
			module.splice(i, 1);
	}

	var app = angular.module("main-app", module);

	// Factories
	app.factory("$func", function($http) {
		return {
			post: function(url, data) {
				var data = data || {};
				data._csrf = csrfToken;

				return $http({
					method: "POST",
					url : url,
					data : $.param(data),
					headers : {'Content-Type': 'application/x-www-form-urlencoded'}
				})
			},
			init: function(data) {
				if (window.hasOwnProperty("data") && window.data.hasOwnProperty(data))
					return window.data[data];
			}
		}
	});

	// Filters
	app.filter("isEmpty", function() {
		return fn.isEmpty;
	})

	// Directives
	app.directive("ngRepeat", function () {
	    return function (scope, element, attrs) {
	        scope.$watch(function () {
	            initEvents(element);
	        });
	    };
	})
	.directive("ngDate", function ($filter) {
	    return {
	    	restrict: "A",
	    	require: "ngModel",
	    	link: function(scope, ele, attr, ngModel) {
				if (!ngModel) return;

	    		scope.$watch(function() {
	    			var value = $filter("date")(ngModel.$modelValue, 'dd/MM/yyyy');

					ngModel.$setViewValue(value);
					ngModel.$render();

					return value;
	    		})
	    	}
	    }
	});
}

/*JQuery Plugin*/
// reinit select2
$.fn.initSelect2 = function() {
	if (typeof this.select2 !== "function")
		return;

	var select2;

	if (settings = window[this.attr('data-krajee-select2')]);
		select2 = this.select2(settings);
	
	this.parent().parent().find(".kv-plugin-loading").remove();

	return select2;
};

$.ajaxSetup({
    headers: {'X-CSRF-Token': csrfToken}
});

/*JQuery Events*/
var initEvents = function(scope) {
	var $$ = function(ele) {
		return $(ele, scope);
	};

	/*Form Validation*/
	$$('form').on("beforeSubmit", function(e) {
		return $(".danger,.has-error,.ng-invalid", this).length==0;
	});
	
	for (var i in fn.validate.validation) {
		var ele = $$(fn.validate.validation[i].element);

		if (ele.length == 0)
			continue;

		var type = i;
		var validation = fn.validate.validation[type];
		var help = ele.next();

		if (!help.hasClass("help-block")) {
			ele.after("<div class='help-block col-md-12'></div>");
			help = ele.next();
		}

		ele.keyup(function() {
			var $this = $(this);

			// success
			if (fn.validate($this.val(), type)) {
				help.removeClass("danger").html("");
				$this.removeClass("danger").addClass("success");
			}
			// error
			else {
				help.addClass("danger").html(validation.message);
				$this.removeClass("success").addClass("danger");
			}
		});
	}

	/*Select2*/
	$$("[data-krajee-select2]").initSelect2();

	/*Fileinput*/
	if ($.fn.fileinput) {
		$$("input.fileinput").fileinput({
			'showUpload':false, 
			'previewFileType':'any',
			'maxFileCount': 1,
			'mainClass': "input-group-md"
		});

		$$(".file-input .btn").addClass("btn-sm");
	}
	
	/*Bootstrap JS*/
	// tooltip
	$$('[title]').tooltip();

	// tab
	var tmp = localStorage.getItem("hash-gridview-tab");

	$$('[data-toggle="tab"]')
		.each(function() {
			var hash = $(this).attr('href');

			if (location.hash == hash)
				this.click();
			else if (tmp == hash) {
				this.click();
				localStorage.removeItem("hash-gridview-tab");
			}
		})
		.click(function() {
			location.hash = $(this).attr("href")
		})

	$(document).on('change.yiiGridView keydown.yiiGridView', ".grid-view .filters input", function() {
		localStorage.setItem("hash-gridview-tab", location.hash);
	})
	
	// Bootstrap Modal
	$$("[modal],[modal-sm],[modal-md],[modal-lg]").click(function(e) {
		// must left button
		if (e.which != 1)
			return true;
		
		var button = $(this);
		var content = button.attr('modal-content') || '.page-content';
		var title = button.attr('modal-title') || $("title").text();
		var href = button.attr("href");

		// size
		var sizeSet = ["modal","modal-sm","modal-md","modal-lg"];

		for (var i in sizeSet) {
			// attribute is exists
			if (button.attr(sizeSet[i])!==undefined) {
				var target = $(button.attr(sizeSet[i]) || '#modal-basic');
				
				// set modal size
				if (sizeSet[i]!='modal')
					$(".modal-dialog", target).addClass(sizeSet[i]);
				break;
			}
		}

		target.find(".modal-title").html(title);
		target.modal("show");

		if (href != undefined) {
			var body = target.find(".modal-body");

			body.html(
				"<div class='text-center'>"
					+ "<img src='images/loading.gif'>"
				+ "</div>"
			);

			$.ajax({
				url: href,
				success: function(res) {
					var content = $(res).find(content);

					if (content.length == 0)
						content = $(res);

					// recompile angular if exists
					if (isAngular) {
						angular.element(document).injector().invoke(function($compile) {
							var scope = angular.element(body).scope();
							$compile(body)(scope);
						})
					}

					initEvents(content)
					body.html(content);
				}
			})
		}

		e.preventDefault();
	});

	/*Datepicker*/
	if ($.fn.daterangepicker) {
		// adding calendar icon
		var date_el = $$('.input-date-range, .input-date').each(function() {
			var $this = $(this);

			$("<div class='input-group input-icon input-icon-right'>")
				.append($this.clone())
				.prepend("<span class='input-group-addon'><i class='fa fa-calendar bigger-110'></i></span>")
				.insertBefore($this);

			$this.remove();
		});
		
		// date range picker
		var date_range = $$('.input-date-range');
		var range = date_range.val()==undefined ? undefined : date_range.val().split(/\s+-\s+/);
		var opt = {
		    applyClass : 'btn-sm btn-success',
		    cancelClass : 'btn-sm btn-default',
	        format: 'DD/MM/YYYY',
		    locale: {
		        applyLabel: 'Apply',
		        cancelLabel: 'Cancel',
		    },
		};

		if (typeof range=="object") {
			opt['startDate'] = fn.isEmpty(range[0]) ? new Date() : range[0];
			opt['endDate'] = fn.isEmpty(range[0]) ? new Date() : range[1];
		}

		date_range.daterangepicker(opt);

		// single date picker
		$$('.input-date').each(function() {
			var $this = $(this);
			var remove = $('<i class="ace-icon glyphicon glyphicon-remove" style="cursor:pointer"></i>');

			$this.daterangepicker({
				singleDatePicker: true, 
				showDropdowns: true,
		        format: 'DD/MM/YYYY',
			}, function() {
				remove.show();

				if (isAngular)
					angular.element(this).triggerHandler('input')
			});

			// clear button
			remove.click(function() {
				$this.val("");
				$this.trigger("apply.daterangepicker")
				$(this).hide();
			})
			.insertAfter($this);

			// hide remove icon if its empty
			if (fn.isEmpty($this.val()))
				remove.hide();
		})
	}
};

$(document).ready(function() {
	initEvents();

	/*Yii Overrides*/
	// action confirmation popup
	yii.confirm = function(msg, func) {
		var modal = fn.confirm({
			title: $(this).attr("confirm-title"),
			body: msg,
			yes: func
		});

		if (!modal.hasClass("in"))
			return false;
	}

	//resize column width
	var pressed = false;
    var start = undefined;
    var startX, startWidth;
    
    $(".forscrollhor th").hover(function(){
    	$(this).addClass("resizing");
    });

    $(".forscrollhor th").mousedown(function(e) {
        start = $(this);
        pressed = true;
        startX = e.pageX;
        startWidth = $(this).width();
        $(start).addClass("resizing");
    });
    
    $(document).mousemove(function(e) {
        if(pressed) {
        	var newsize = startWidth+(e.pageX-startX);
            $(start).css("min-width", newsize);
        }
    });
    
    $(document).mouseup(function() {
        if(pressed) {
            $(start).removeClass("resizing");
            pressed = false;
        }
    });
});