<?php namespace common\assets;

use yii\web\AssetBundle;

class ColorboxAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/ace/dist';

    public $js = [
        "js/jquery.colorbox.min.js",
    ];
 
    public $css = [
        "css/colorbox.min.css",
    ];

	public $depends = ["yii\web\JqueryAsset"];
}
