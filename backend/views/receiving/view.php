<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Receiving */

$this->title = $model->number_merch_do;
$this->params['breadcrumbs'][] = ['label' => 'Receivings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receiving-view">
    <p>
        <?php if (access("update")) : ?>
            <?= Html::a('Update', ['update', 'id' => $model->number_merch_do], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        
        <?php if (access("delete")) : ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->number_merch_do], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number_merch_do',
            'receive_time',
            'description',
            'number_po',
        ],
    ]) ?>

</div>
