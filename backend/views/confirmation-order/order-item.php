<?php 

use kartik\grid\GridView;
use yii\helpers\Html;

$total = 0;
$this->title = "Detail Order : $id";

ob_start();
?>

<?= GridView::widget([
    "dataProvider" => $dataProvider,
    "columns" => [
        [
            "header" => "Product ID",
            "attribute" => "number_product",
        ],
        "product.name",
        "productSize.name",
        "productColor.color",
        [
            "header" => "Unit Price",
            "content" => function($model) {
                return currency($model->unit_price);
            }
        ],
        [
            "header" => "Qty",
            "attribute" => "qty",
        ],
        [
            "header" => "Subtotal",
            "content" => function($model) use (&$total) {
                $subtotal = $model->unit_price * $model->qty;
                $total += $subtotal;
                return currency($subtotal);
            }
        ],
    ]
]) ?>

<div>
    <div class="col-md-offset-8 col-md-2 text-right"><b>Total Price</b></div> 
    <div class="col-md-2">: <?= currency($total) ?></div> 
</div>
<div>
    <div class="col-md-offset-8 col-md-2 text-right"><b>Shipping Cost</b></div> 
    <div class="col-md-2">: <?= currency($model->shipping_cost) ?></div> 
</div>
<div>
    <div class="col-md-offset-8 col-md-2 text-right"><b>Total Bill</b></div> 
    <div class="col-md-2">: <?= currency($model->total_bill) ?></div> 
</div>
<div class="clearfix"> </div>