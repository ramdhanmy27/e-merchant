$(document).ready(function() {
	var parent_id = parent_id || '';

	$('#tree1').ace_tree({
		dataSource: function(opt, callback) {
			$.ajax({
				url: url.to("category/categories", {"parent": opt.id || '', "id": parent_id}),
				dataType: "json",
				success: function(res) {
					if (res.length == 0)
						$("#category-tree").hide();

					for (var i=res.length-1; i>=0; i--) {
						var id = "input-"+res[i]['id'];

						res[i]['text'] = "<label for='"+id+"'>"
											+"<input type='radio' name='Category[parent]' id='"+id+"' "
												+"value='"+res[i]['id']+"' autocomplete='off' "
												+(res[i]['checked'] || res[i]['parent']==parent_id ? "checked='checked'" : '')+"/> "
											+res[i]['text']
										+"</label>";
						res[i]['type'] = res[i]['type']==0 ? "item" : "folder";
					}

					callback({data : res});
				}
			})
		},
		loadingHTML : '<div class="tree-loading"><i class="ace-icon fa fa-refresh fa-spin blue"></i></div>',
		cacheItems: true,
		'open-icon' : 'ace-icon tree-minus',
		'close-icon' : 'ace-icon tree-plus',
		'selectable' : false,
		'selected-icon' : '',
		'unselected-icon' : '',
	});
});