<?php
/**
 * widget box
 * @param  string $title
 * @param  string $body
 */

$attribute = null;

if (isset($attr)) {
    if (is_array($attr)) {
        foreach ($attr as $key => $val) {
        	if ($key == "class")
        		continue;

            $attribute .= " $key='$val'";
        }
    }
    else $attribute = $attr;
}
?>

<div class='widget-box <?= @$attr['class'] ?>' <?= $attribute ?>>
    <div class="widget-header">
        <h5 class="widget-title"><?= @$title ?></h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main"><?= @$body ?></div>
    </div>
</div>