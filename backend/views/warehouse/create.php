<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Warehouse */

$this->title = 'Create Warehouse';
?>

<div class="warehouse-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
