<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "delivery_order".
 *
 * @property string $number_delivery_order
 * @property string $mass
 * @property string $volume
 * @property string $charge
 * @property string $destination_name
 * @property string $destination_address
 * @property string $destination_phone
 * @property string $destination_city
 * @property string $post_code
 * @property string $code_service
 *
 * @property LogisticService $logisticService
 */
class DeliveryOrder extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delivery_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['number_delivery_order'], 'required'],

            // unique
            [['number_delivery_order'], 'unique'],

            // safe
            [['instruction_time'], 'safe'],

            // integer
            [['mass', 'volume', 'charge'], 'number'],

            // string
            [['code_service'], 'string', 'max' => 4],
            [['post_code'], 'string', 'max' => 6],
            [['destination_phone'], 'string', 'max' => 13],
            [['number_delivery_order', 'destination_name', 'destination_city'], 'string', 'max' => 16],
            [['destination_address'], 'string', 'max' => 32],

            // exist
            [['code_service'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticService::className(), 'targetAttribute' => ['code_service' => 'code_service']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number_delivery_order' => 'Delivery Order Number',
            'mass' => 'Mass',
            'volume' => 'Volume',
            'charge' => 'Charge',
            'destination_name' => 'Destination Name',
            'destination_address' => 'Destination Address',
            'destination_phone' => 'Destination Phone',
            'destination_city' => 'Destination City',
            'post_code' => 'Post Code',
            'code_service' => 'Id Logistic Service',
            'logisticService.code_tpl.name' => 'Logistic Name',
            'logisticService.name' => 'Logistic Service',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticService()
    {
        return $this->hasOne(LogisticService::className(), ['code_service' => 'code_service']);
    }
}
