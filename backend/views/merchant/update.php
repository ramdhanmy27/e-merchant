<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Merchant */

$this->title = 'Update Merchant: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Merchants', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->code_merchant]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="merchant-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
