<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
?>
<div class="product-view">

    <?php if(!$this->request->isAjax) { ?>
        <?php if($this->can("update")){ ?>
            <p> <?= Html::a('Update', ['update', 'id' => $model->number_product], ['class' => 'btn btn-primary', "modal"=>""]) ?> </p>
        <?php } ?>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number_product',
            'brand_name',
            'name',
            'description',
            'group_size',
            'basic_price',
            'is_deprecated:boolean',
            'category.name',
            'merchant.name',
        ],
    ]) ?>

</div>