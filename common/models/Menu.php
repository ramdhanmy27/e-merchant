<?php

namespace common\models;

use Yii;
use common\yii\db\db;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $parent
 * @property string $url
 * @property string $title
 * @property boolean $enable
 * @property integer $order
 * 
 * @property array $param
 *   @param string id           "primary key"
 *   @param string labelField   "nama field untuk label row. eg: combo box"
 */
class Menu extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'order'], 'integer'],
            [['title'], 'required'],
            [['url', 'icon'], 'string'],
            [['enable'], 'boolean'],
            [['title'], 'string', 'max' => 100],
            [["permission"], "safe"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'url' => 'Url',
            'title' => 'Title',
            'enable' => 'Enable',
            'order' => 'Order',
            'icon' => 'Icon',
            'permission' => 'Permission',
        ];
    }

    /**
     * querying menu
     * @param  string $sql    
     * @param  string $select
     * @param  array  $bind
     * @return yii\db\Command
     */
    public static function select($sql=null, $select="*", $bind=[]) 
    {
        return db::run(
            "with recursive r as (
                select *, array[id] as path, array[\"order\"] as order_path, 0 as level from menu where parent is null
                union all
                select m.*, path || m.id, order_path || m.\"order\", level+1 from menu m
                inner join r on r.id=m.parent
            )
            select $select from r $sql", $bind
        );
    }

    public function save($validate = true, $attributes = null) {
        $this->permission = is_array($this->permission) ? '{"'.addcslashes(implode('","', $this->permission), "\\").'"}' : null;

        return parent::save($validate, $attributes);
    }

    public function get_permission() {
        if (is_string($this->permission))
            return str_getcsv(preg_replace(["/^\{(.+)\}$/", "/\\\\\\\\/"], ["$1", "\\\\"], $this->permission));
        else
            return $this->permission;
    }

    /**
     * returns data menu navigation in array
     * @param  string $sql  [description]
     * @param  array  $bind [description]
     * @return array
     */
    public static function nav($sql='where enable=true order by r.order_path', $bind=[]) 
    {
        $bind["userid"] = (string) Yii::$app->user->identity->id;

        return self::tree(
                    self::select($sql, "id, parent, url, title as label, icon, coalesce((permission && (
                        SELECT array_agg(distinct p.child) FROM auth_assignment r 
                        inner join auth_item_child p on p.parent=r.item_name
                        where r.user_id=:userid
                    ))::int, 1) as visible", 
                    $bind
                )->queryAll());
    }

    public static function tree(array $list, $parent = null) {
        $branch = [];

        foreach ($list as $menu) {
            if ($menu['parent'] == $parent) {
                $menu['url'] = ["/".$menu['url']];

                if ($items = self::tree($list, $menu['id']))
                    $menu['items'] = $items;

                $branch[] = $menu;
            }
        }

        return $branch;
    }

    /**
     * update list order
     * @param  int $order
     * @param  int $parent 
     * @return boolean
     */
    public function updatePosition($order, $parent) {
        $parent = empty($parent) || $parent==false ? null : $parent;

        if ($this->parent==$parent && $this->order==$order)
            return false;

        // old & new parent was differ
        if ($this->parent != $parent) {
            # old parent group
            $param = ["order" => $this->order];

            // menu has parent
            if (!($is_root = $this->parent==null)) 
                $param["parent"] = $this->parent;

            db::update(
                "menu", ["order" => db::raw('"order" - 1')], 
                "parent ".($is_root ? "is null" : "=:parent").' and "order">:order', $param
            )
            ->execute();

            # new parent group
            $param = ["order" => $order];

            // menu has parent
            if (!($is_root = $parent==null)) 
                $param["parent"] = $parent;

            db::update(
                "menu", ["order" => db::raw('"order" + 1')], 
                "parent ".($is_root ? "is null" : "=:parent").' and "order">=:order', $param
            )
            ->execute();
        }
        // sorting on same parent group
        else {
            // the new position order is less than the old one
            if ($order < $this->order) {
                $expr = '"order" + 1';
                $condition = " and \"order\" between :new_order and :old_order-1";
            }
            else {
                $expr = '"order" - 1';
                $condition = " and \"order\" between :old_order and :new_order";
            }

            // execute update
            $param = [
                "old_order" => $this->order,
                "new_order" => $order,
            ];

            // menu has parent
            if (!($is_root = $parent==null)) 
                $param["parent"] = $parent;

            db::update(
                "menu", ["order" => db::raw($expr)],
                "parent ".($is_root ? "is null" : "=:parent").$condition, $param
            )
            ->execute();
        }

        $this->parent = $parent;
        $this->order = $order;

        return $this->save();
    }

    /**
     * reorder menu if it's needed
     */
    public static function reorder() {
        // don't need to reorder
        if (self::select("group by order_path having count(*)>1 order by order_path", "count(*)")->execute() == 0)
            return;

        db::run(
            'UPDATE menu m set "order"=num-1 from ( 
                with recursive r as (
                    select row_number() over(order by id) as num, id from menu where parent is null
                        union all
                    select row_number() over(partition BY m.parent order by m.parent, m.id), m.id from menu m
                    inner join r on r.id=m.parent
                )
                select * from r
            ) as r
            where r.id=m.id'
        )->query();
    }

    /**
     * delete menu child
     * @return integer|false the number of rows deleted, or false if the deletion is unsuccessful for some reason.
     * Note that it is possible the number of rows deleted is 0, even though the deletion execution is successful.
     */
    public function delete() {
        if ($this->isNewRecord)
            return false;

        db::run(
            "DELETE from menu where id in (
                with recursive r as (
                    select id, parent from menu where parent=:id
                    union all
                    select m.id, m.parent from menu m
                    inner join r on r.id=m.parent
                )
                select id from r
            )", ["id" => $this->id]
        )->execute();
        parent::delete();
    }
}
