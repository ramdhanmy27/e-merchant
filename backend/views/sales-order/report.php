<?php $total = 0; ?>

Kepada Yth <?= $order['name'] ?> <br>
<?= $order['phone'] ?> <br><br>

Kami telah menerima dan mencatat pesanan anda dengan kode pesanan <?= $id ?> berupa <br>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Kode</th>
            <th>Nama Produk</th>
            <th>Size</th>
            <th>Color</th>
            <th>Quantity</th>
            <th>Harga</th>
            <th>Sub Total</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($item as $val): ?>
        <tr>
            <td><?= $val["number_product"] ?></td>
            <td><?= $val["name"] ?></td>
            <td><?= $val["size"] ?></td>
            <td><?= $val["color"] ?></td>
            <td><?= $val["qty"] ?></td>
            <td>Rp <?= number_format($val["unit_price"], 2) ?></td>
            <td>Rp <?= number_format($val["qty"]*$val["unit_price"], 2) ?></td>
        </tr>
        <?php $total += $val["qty"]*$val["unit_price"]; ?>
    <?php endforeach; ?>
    </tbody>
</table>

<table>
    <tr> <th>Total Harga</th> <td>: <?= number_format($total, 2) ?></td> </tr>
    <tr> <th>Ongkos Kirim</th> <td>: <?= number_format($order["shipping_cost"], 2) ?></td> </tr>
    <tr> <th>Total Tagihan</th> <td>: <?= number_format($total+$order["shipping_cost"], 2) ?></td> </tr>
</table>

Mohon Melakukan pembayaran melalui salah satu rekening bank berikut :

<ul>
<?php foreach($account as $val): ?>
    <li><?= $val['bank_name'] ?> <?= $val['account_number'] ?> a.n. <?= $val['account_name'] ?></li>
<?php endforeach; ?>
</ul>

<?php $deadline = strtotime($order["payment_deadline"]); ?>
Selambat-lambatnya tanggal <?= date("d-m-Y", $deadline) ?> Jam <?= date("H:i", $deadline) ?> WIB
Jika pembayaran belum dilakukan hingga waktu tersebut, maka order batal.