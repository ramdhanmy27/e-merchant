<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use common\yii\db\db;
use common\models\BankAccount;

\kartik\file\FileInputAsset::register($this);
?>

<div class="bank-account-form">
    <?php $form = ActiveForm::begin(["options" => ["enctype" => "multipart/form-data"]]); ?>
    <?php ob_start(); ?>

    <?= $this->render("@common/views/form-group", [
        "label" => "Order No.",
        "content" => "<h5>$order->number_order</h5>"
    ]) ?>
    
    <?= $this->render("@common/views/form-group", [
        "label" => "Buyer",
        "content" => "<h5>{$order->buyer->name}</h5>"
    ]) ?>
    
    <?php 
        $attr = ["class" => "btn btn-sm"];

        if ($this->request->isAjax)
            $attr["target"] = "_blank";
        else
            $attr["modal-lg"] = "";
    ?>

    <?= $this->render("@common/views/form-group", [
        "label" => "Detail Order",
        "content" => Html::a(
            "<i class='glyphicon glyphicon-list-alt'></i> Detail", 
            ["confirmation-order/order-item", "id" => $order->number_order], 
            $attr
        ) 
    ]) ?>
    
    <?= $this->render("@common/views/form-group", [
        "label" => "Total Bill",
        "content" => "<h5>".currency($order->total_bill)."</h5>",
    ]) ?>

    <?= $form->field($model, 'code_bank')->option(BankAccount::option(db::raw("bank_name||' '||account_number"), "code_bank")) ?>
    
    <?= $this->render("@common/views/form-group", [
        "label" => "Transfer Receipt",
        "content" => Html::fileInput("receipt", null, ["class" => "fileinput", "required" => ''])
    ]) ?>

    <?= $form->field($model, 'number_transaction')->textInput(['maxlength' => true]) ?>

    <hr>
    <div class="text-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <!-- box widget -->
    <?= $this->render("@common/views/widget", [
        "title" => "Confirmation Order",
        "attr" => ["class" => $this->request->isAjax ? "col-xs-12" : "col-md-8"],
        "body" => ob_get_clean(),
    ]) ?>

    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div>
</div>