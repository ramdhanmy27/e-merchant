$(document).ready(function() {
	var form = $("#wizard");

	form.steps({
	    headerTag: "h3",
	    bodyTag: "section",
	    transitionEffect: "slideLeft",
	    onStepChanging: function(event, currentIndex, newIndex) {
	    	if (newIndex == 1) {
    			var data = {};
    			var name = '';

    			$('.mark-checkbox').each(function() {
    				var $this = $(this);

    				if (!$this.prop("checked"))
    					return false;

    				if (fn.isEmpty(name)) {
	    				name = $this.attr('name').replace(/\W+/, '');
	    				data[name] = [];
    				}

    				data[name].push($this.val())
    			})

    			if (fn.isEmpty(data)) {
    				fn.alert("You must check at least one Delivery Instruction.");
    				return false;
    			}

    			$.ajax({
    				url: url.to("batch-delivery/list-product"),
    				method: "POST",
    				data: data,
    				success: function(res) {
    					$("#wizard .picking-list").html(res);
    				}
    			})
	    	}

	    	form.validate().settings.ignore = ":disabled,:hidden";
	        return form.valid();
	    },
	    onFinishing: function(event, currentIndex) {
	    	form.submit()
	    }
	})
	.validate({
		errorPlacement: function(error, element) { 
			element.after(error); 
		},
	})

	$('#wizard ul[role="tablist"]').hide();
	
	$('.mark-checkbox-all').change(function() {
		var $this = $(this);

		$this.parentsUntil("table").parent().find(".mark-checkbox").each(function() {
			$(this).prop("checked", $this.prop("checked")).trigger("change");
		})
	});

	$('.mark-checkbox').change(function() {
		var $this = $(this);
		var className = "bg-warning";

		$this.parentsUntil("tr").parent().find("td").each(function() {
			if ($this.prop("checked"))
				$(this).addClass(className);
			else
				$(this).removeClass(className);
		})
	})
})