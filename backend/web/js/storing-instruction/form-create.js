app.controller("storingInstructionController", function($scope, $http, $func) {
	// receiving ID
	$scope.number_instruction = "";
	
	/*receiving Information*/
	$scope.storing_instruction = {
		"number_instruction": "",
		"storing_time": "",
	};

	/*var timeout;
	var setInformation = function() {
		if (timeout !== undefined)
			clearTimeout(timeout);

		timeout = setTimeout(function() {
			// update receiving information
			$func.post(url.to("storing-instruction/update-storing-instruction", {number_instruction: $scope.number_instruction}), $scope.storing_instruction)
				.error(fn.alertError);
		}, 1000);
	};

	$scope.$watch(function() {
		$("#storing_instruction-number_instruction").keyup(function() {
			// $scope.storing_instruction["number_instruction"] = $(this).val();
			setInformation();
		});
		$("#storing_instruction-storing_time").keyup(function() {
			// $scope.storing_instruction["storing_time"] = $(this).val();
			setInformation();
		});
	});*/

	/*Variations*/

	// product items
	$scope.product = [];

	$scope.storing_item = [];

	// product selected index
	$scope.index = "";
	// menentukan qty type: (total) yang dipilih
	$scope.type = "";

	// object product yang ditambah
	var init = {
		product: {
			number_product: "",
			size: [""],
			attribute: [""],
			qty: {
				total: {},
			},
		},
		storing_item: {
			number_rack: "",
			number_box: "",
			number_product: "",
			code_size: "",
			number_product_color: "",
			qty: ""
		}
	};



	// POST - dipanggil ketika close modal qty product, save quantity product yang diisi
	/*$("#modal-variations").on("hidden.bs.modal", function() {
		$func.post(
			url.to("storing-instruction/save-qty", {type: $scope.type, number_instruction: $scope.number_instruction}),
			{
				number_product: $scope.product[$scope.index].number_product,
				qty: $scope.product[$scope.index].qty[$scope.type]
			}
		)
		.error(fn.alertError);
	});*/

	/*initialize data*/

	// dipanggil ketika milih product di select2, ambil data keterangan product & init null untuk quantitynya
	var initRow = function(index) {
		$(".product-details select[data-krajee-select2]:eq("+ index +")").off("select2:select").on("select2:select", function(e) {
			var $this = $(this);
			var value = $this.val();

			var unsetProduct = function() {
				$this.val($scope.product[index].number_product).trigger("change");
			};
			var setProduct = function() {
				// product already exists
				if ($.grep($scope.product, function(val, i) {return val.number_product==value}).length > 0) {
					fn.alert("Product is already exists");
					unsetProduct();
					return;
				}

				$scope.$apply(function() {
					$http.get(url.to("storing-instruction/get-product-data", {id: value}))
						.success(function(data) {
							$scope.product[index] = data;
						})
						.error(function() {
							$scope.product[index].id = value;
							fn.alertError();
						})
				});
			};

			// on update product
			if (!fn.isEmpty($scope.product[index].number_product)) {
				fn.confirm({
					body: "It will erase your data quantity. Are you sure ?",
					yes: setProduct,
					no: unsetProduct
				});
			}
			// first try
			else setProduct();

			return false;
		});
	}

	$scope.f = {
		// dipanggil ketika klik button load, ambil data banyak product & keterangan product & quantitynya
		loadItem : function(){
			var delete_item = function() {
				$scope.product.splice(0);
			};

			var get_item = function() {
				$func
					.post(
						url.to("storing-instruction/get-unstored-item")
					)
					.success(function(response) {
						$scope.product = $.map(response, function(value, index) {
							return [value];
						});

						$scope.$watch(function() {
							for (var index in $scope.product) {
								// add event to each row
								initRow(index);
							};
						});

					})
					.error(fn.alertError);
			}

			if (!fn.isEmpty($scope.product)) {
				fn.confirm({
					body: "It will erase your existing products. Are you sure ?",
					yes: function() {
						delete_item();
						get_item(); //get and save new item
						$scope.storing_item.splice(0);
					},
					no: function() {
					}
				});
			}
			else {
				delete_item(); // delete old item di angular
				get_item(); //get new item
				$scope.storing_item.splice(0);
			}
		},
		generate: function() {
			var empty_box;
			var unstored_item;

			var delete_storing_item = function() {
				$scope.storing_item.splice(0);
			};

			var a = function(){
			$func
				.post(
					url.to("storing-instruction/query-empty-box")
				)
				.success(function(response) {
					empty_box = $.map(response, function(value, index) {
						return [value];
					});
					b();
				})
				.error(fn.alertError);
			};

			var b = function(){
				$func
					.post(
						url.to("storing-instruction/query-unstored-item")
					)
					.success(function(response) {
						unstored_item = $.map(response, function(value, index) {
							return [value];
						});
						c();
						console.log(empty_box);
						console.log(unstored_item);
						console.log($scope.storing_item);
						console.log(empty_box);
						console.log(unstored_item);

					})
					.error(fn.alertError);
			};

			var c = function(){
				for (var i in empty_box) {
					for (var j in unstored_item) {
						// jika kapasitas box melebihi total produk yang ingin disimpan
						if(empty_box[i].sisa >= unstored_item[j].qty && unstored_item[j].qty != 0 && empty_box[i].sisa != 0){
							
							console.log("empty " + empty_box[i].number_box + " box:" + empty_box[i].sisa + " mjd:" + (empty_box[i].sisa-unstored_item[j].qty));
							empty_box[i].sisa = empty_box[i].sisa-unstored_item[j].qty;
							
							$scope.storing_item.push($.extend({}, {
								"number_rack" : empty_box[i].number_rack,
								"rack_level" : empty_box[i].rack_level,
								"number_box" : empty_box[i].number_box,
								"number_product" : unstored_item[j].number_product,
								"code_size" : unstored_item[j].code_size,
								"number_product_color" : unstored_item[j].number_product_color,
								"qty" : unstored_item[j].qty,
								"sku" : unstored_item[j].sku,
							}));

							console.log("unstored item:" + unstored_item[j].qty + " mjd:" + 0);
							unstored_item[j].qty = 0;
						}
						// jika total produk yang ingin disimpan melebihi kapasitas box
						else if (empty_box[i].sisa < unstored_item[j].qty && unstored_item[j].qty != 0 && empty_box[i].sisa != 0){
							
							console.log("unstored_item " + unstored_item[j].sku + " :" + unstored_item[j].qty + " mjd:" + (unstored_item[j].qty-empty_box[i].sisa));
							unstored_item[j].qty = unstored_item[j].qty-empty_box[i].sisa;
							console.log("empty " + empty_box[i].number_box + " box:" + empty_box[i].sisa + " mjd:" + 0);
							
							$scope.storing_item.push($.extend({}, {
								"number_rack" : empty_box[i].number_rack,
								"rack_level" : empty_box[i].rack_level,
								"number_box" : empty_box[i].number_box,
								"number_product" : unstored_item[j].number_product,
								"code_size" : unstored_item[j].code_size,
								"number_product_color" : unstored_item[j].number_product_color,
								"qty" : empty_box[i].sisa,
								"sku" : unstored_item[j].sku,
							}));
							empty_box[i].sisa = 0;
						}
					};
				};
			};

			if (!fn.isEmpty($scope.storing_item)) {
				fn.confirm({
					body: "It will erase your existing unstored_item. Are you sure ?",
					yes: function() {
						delete_storing_item();
						a(); //get and save new item
					},
					no: function() {
					}
				});
			}
			else {
				delete_storing_item(); // delete old item di angular
				a(); //get new item
			}
		},
		saveStoringItem: function() {
			$func
				.post(
					url.to("storing-instruction/save", {number_instruction: $scope.number_instruction}),
					{
						storing_instruction: $scope.storing_instruction,
						storing_item: $scope.storing_item,
					}
				)
				.success(function(response) {
						if(response){
							window.location.href=url.to("storing-instruction/download-pdf", {number_instruction : $scope.number_instruction});
						}
					})
				.error(fn.alertError);
		},
		add: function() {
			$scope.product.push($.extend({}, init.product));
			$scope.$watch(function() {
				initRow($scope.product.length-1);
			});
		},
		delete: function(index) {
			var delete_item = function() {
				$scope.product.splice(index, 1);
			};

			if (!fn.isEmpty($scope.product[index].number_product)) {
				fn.confirm({
					body: "It will erase your data quantity. Are you sure ?",
					yes: function() {
						$scope.$apply(function() {
							delete_item();
						})
					}
				})
			}
			else delete_item();
		},
		isProductSelected: function(index) {
			return !fn.isEmpty($scope.product[index].number_product);
		},
		qtyProduct: function(index) {
			return $scope.f.getQtyTotal($scope.product[index].qty.total);
		},
		getQtyTotal: function(data) {
			var total = 0;
			
			for (var row in data) {
				for (var value in data[row])
					total += fn.int(data[row][value]);
			}
					
			return total;
		},
		loadVariation: function(index, type) {
			$scope.$$childHead.variations.size = $scope.product[index].size;
			$scope.$$childHead.variations.attribute = $scope.product[index].attribute;
			$scope.$$childHead.variations.quantity = $scope.product[index].qty[type];

			$scope.index = index;
			$scope.type = type;
		},
		formValidation: function() {
			
		},
	}
});