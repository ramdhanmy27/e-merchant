<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "agreement_item".
 *
 * @property integer $id_agreement_item
 * @property integer $total_qty
 * @property integer $ready_qty
 * @property integer $commited_qty
 * @property string $number_product
 * @property string $number_product_color
 * @property string $code_size
 * @property string $number_contract
 *
 * @property Product $product
 * @property ProductColor $productColor
 * @property ProductSize $productSize
 * @property StockAgreement $stockAgreement
 */
class AgreementItem extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            //[['id_agreement_item'], 'required'],

            // unique
            //[['id_agreement_item'], 'unique'],

            // safe

            // integer
            [['total_qty', 'ready_qty', 'commited_qty'], 'integer'],

            // string
            [['number_product'], 'string', 'max' => 10],
            [['number_contract'], 'string', 'max' => 20],
            [['number_product_color'], 'string', 'max' => 20],
            [['code_size'], 'string', 'max' => 10],

            // exist
            [['number_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['number_product' => 'number_product']],
            [['number_product_color'], 'exist', 'skipOnError' => true, 'targetClass' => ProductColor::className(), 'targetAttribute' => ['number_product_color' => 'number_product_color']],
            [['code_size'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSize::className(), 'targetAttribute' => ['code_size' => 'code_size']],
            [['number_contract'], 'exist', 'skipOnError' => true, 'targetClass' => StockAgreement::className(), 'targetAttribute' => ['number_contract' => 'number_contract']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_agreement_item' => 'Id Agreement Item',
            'total_qty' => 'Total Qty',
            'ready_qty' => 'Ready Qty',
            'commited_qty' => 'Commited Qty',
            'stockAgreement' => 'Stock Agreement',
            'product.name' => 'Product',
            'productColor.color' => 'Color',
            'productSize.name' => 'Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductColor()
    {
        return $this->hasOne(ProductColor::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSize()
    {
        return $this->hasOne(ProductSize::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockAgreement()
    {
        return $this->hasOne(StockAgreement::className(), ['number_contract' => 'number_contract']);
    }
}
