<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SalesChannel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sales-channel-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
            <!-- Basic Info -->
            <div class='widget-box'>
                <div class="widget-header">
                    <h5 class="widget-title">Sales Information</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <?= $form->field($model, 'code_sales_channel')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'is_internal')->checkbox() ?>

                        <?= $form->field($model, 'front_link')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'backend_link')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'script1')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'script2')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'script3')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <hr>
    <?php ActiveForm::end(); ?>
</div>
