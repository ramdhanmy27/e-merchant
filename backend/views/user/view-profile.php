<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['view-profile']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <p>
        <?= Html::a(Yii::t('app', 'Edit Profile'), ['edit-profile'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Change Password'), ['change-password'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'name',
            'email:email',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

    <?php if($modelMerchant) { ?>
    <?= DetailView::widget([
        'model' => $modelMerchant,
        'attributes' => [
            'phone',
            'shop_name',
            'company_name',
            'address_1',
            'address_2',
            'city',
            'country',
            'postal_code',
            'PIC',
            'bank',
            'bank_code',
            'bank_swift',
            'bank_account_name',
            'bank_account_number',
            'status',
            'created_at:time',
            'updated_at:time',
        ],
    ]) ?>
    <?php } ?>

</div>
