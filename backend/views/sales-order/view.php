<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->number_order;

?>

<div class="stock-agreement-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number_order',
            'number_transaction',
            'order_time:datetime',
            [
                'label' => 'Buyer',
                'attribute' => 'buyer.name',
            ],
            [
                'label' => 'Channel',
                'attribute' => 'salesChannel.name',
            ],
            [
                'attribute' => 'total_price',
                'value' => currency($model->total_price),
            ],
            [
                'attribute' => 'shipping_cost',
                'value' => currency($model->shipping_cost),
            ],
            [
                'attribute' => 'total_bill',
                'value' => currency($model->total_bill),
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => $model->labelStatus,
            ],
        ],
    ]) ?>
</div>