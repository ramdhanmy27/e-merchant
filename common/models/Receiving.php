<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "receiving".
 *
 * @property string $id_receiving
 * @property string $number_merch_do
 * @property string $receive_time
 * @property string $description
 * @property string $status
 * @property string $number_po
 *
 * @property ReceiveItem[] $receiveItems
 * @property PurchaseOrder $purchaseOrder
 */
class Receiving extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'receiving';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            // [['id_receiving'], 'required'],

            // unique
            // [['id_receiving'], 'unique'],

            // safe
            [['receive_time'], 'safe'],

            // integer

            // string
            [['status'], 'string', 'max' => 1],
            [['number_po'], 'string', 'max' => 16],
            [['number_merch_do'], 'string', 'max' => 16],
            [['description'], 'string', 'max' => 32],

            // exist
            [['number_po'], 'exist', 'skipOnError' => true, 'targetClass' => PurchaseOrder::className(), 'targetAttribute' => ['number_po' => 'number_po']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number_merch_do' => 'Merch Do Number',
            'receive_time' => 'Receive Time',
            'description' => 'Description',
            'number_po' => 'Purchase Order Number',
        ];
    }

    public function isDraft() {
        return $this->status != "1";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiveItems()
    {
        return $this->hasMany(ReceiveItem::className(), ['id_receiving' => 'id_receiving']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrder()
    {
        return $this->hasOne(PurchaseOrder::className(), ['number_po' => 'number_po']);
    }
}
