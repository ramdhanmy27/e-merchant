CREATE TABLE merchant (
	code_merchant VARCHAR (10) PRIMARY KEY,
	NAME VARCHAR (20),
	address VARCHAR (30),
	contact_name VARCHAR (20),
	phone VARCHAR (13),
	email VARCHAR (255),
	website VARCHAR (20),
	description VARCHAR (64),
	bank VARCHAR (12),
	bank_acc_no VARCHAR (18),
	bank_acc_name VARCHAR (24),
	verif_code VARCHAR (255)
);

CREATE TABLE purchase_order (
	number_po VARCHAR (32) PRIMARY KEY,
	DATE DATE,
	status CHAR (1),
	description VARCHAR (32)
);

CREATE TABLE stock_agreement (
	number_contract VARCHAR (32) PRIMARY KEY,
	contract_date DATE,
	until_date DATE,
	description VARCHAR (64),
	status CHAR (1)
);

CREATE TABLE agreement_item (
	id_agreement_item serial PRIMARY KEY,
	ready_qty INT,
	commited_qty INT
);

CREATE TABLE purchase_order_item (
	id_purchase_order_item serial PRIMARY KEY,
	qty INT
);

CREATE TABLE product (
	number_product VARCHAR (10) PRIMARY KEY,
	brand_name VARCHAR (16),
	NAME VARCHAR (32),
	description VARCHAR (64),
	group_size VARCHAR (3),
	basic_price NUMERIC,
	is_deprecated BOOLEAN
);

CREATE TABLE category (
	code_category VARCHAR (10) PRIMARY KEY,
	NAME VARCHAR (16),
	description VARCHAR (64)
);

CREATE TABLE product_color (
	number_product_color VARCHAR (20) PRIMARY KEY,
	color VARCHAR (16),
	photo VARCHAR []
);

CREATE TABLE product_size (
	code_size VARCHAR (10) PRIMARY KEY,
	NAME VARCHAR (50),
	group_size VARCHAR (3)
);

CREATE TABLE warehouse (
	code_warehouse VARCHAR (4) PRIMARY KEY,
	NAME VARCHAR (16),
	address VARCHAR (255),
	city VARCHAR (16),
	description VARCHAR (255),
	rack_count INT,
	room_width INT,
	room_length INT,
	room_height INT
);

CREATE TABLE rack (
	number_rack VARCHAR (12) PRIMARY KEY,
	ROW_NUMBER VARCHAR (4),
	column_number VARCHAR (4),
	level_count INT,
	box_capacity INT,
	box_count INT
);

CREATE TABLE box(
	number_box VARCHAR (12) PRIMARY KEY,
	rack_level INT,
	capacity INT,
	content_qty INT
);

CREATE TABLE inventory_item (
	sku VARCHAR (255) PRIMARY KEY,
	qty INT
);

CREATE TABLE box_content (
	id_box_content serial PRIMARY KEY,
	qty INT
);

CREATE TABLE receiving (
	id_receiving serial PRIMARY KEY,
	number_merch_do VARCHAR (16),
	receive_time TIMESTAMP,
	description VARCHAR (32),
	status CHAR (1)
);

CREATE TABLE receive_item (
	id_receive_item serial PRIMARY KEY,
	qty INT
);

CREATE TABLE storing_instruction (
	number_instruction VARCHAR (20) PRIMARY KEY,
	storing_time TIMESTAMP
);

CREATE TABLE storing_item (
	id_storing_item serial PRIMARY KEY,
	qty INT
);

CREATE TABLE picking_batch (
	number_batch VARCHAR (20) PRIMARY KEY,
	batch_time TIMESTAMP
);

CREATE TABLE pick_item (
	id_pick_item serial PRIMARY KEY,
	qty INT
);

CREATE TABLE unstored_items (
	id_unstored_items serial PRIMARY KEY,
	qty INT
);

CREATE TABLE sales_channel (
	code_sales_channel VARCHAR (4) PRIMARY KEY,
	NAME VARCHAR (16),
	description VARCHAR (32),
	is_internal BOOLEAN,
	front_link VARCHAR (20),
	backend_link VARCHAR (20),
	script1 VARCHAR (128),
	script2 VARCHAR (128),
	script3 VARCHAR (128),
	status CHAR
);

CREATE TABLE stock_allocation (
	id_stock_allocation serial PRIMARY KEY,
	allocation_qty INT,
	published_qty INT
);

CREATE TABLE allocmutation (
	mutationdate DATE,
	isdebet BOOLEAN,
	qty INT,
	note VARCHAR (32)
);

CREATE TABLE unallocated (
	id_unallocated serial PRIMARY KEY,
	qty INT
);

CREATE TABLE sales_order (
	number_order VARCHAR (12) PRIMARY KEY,
	order_time TIMESTAMP,
	total_price NUMERIC,
	shipping_cost NUMERIC,
	total_bill NUMERIC,
	status CHAR (1),
	delivery_address VARCHAR (255),
	delivery_city VARCHAR (16),
	post_code VARCHAR (6),
	payment_deadline TIMESTAMP,
	number_receipt VARCHAR (32)
);

CREATE TABLE order_item (
	id_order_item serial PRIMARY KEY,
	qty INT,
	unit_price NUMERIC
);

CREATE TABLE buyer (
	code_buyer VARCHAR (12) PRIMARY KEY,
	NAME VARCHAR (16),
	delivery_address VARCHAR (255),
	delivery_city VARCHAR (16),
	post_code VARCHAR (6),
	phone VARCHAR (13),
	email VARCHAR (255)
);

CREATE TABLE logistic_partner (
	code_tpl VARCHAR (16) PRIMARY KEY,
	NAME VARCHAR (12),
	member_acc_number VARCHAR (12),
	contact_person VARCHAR (16),
	phone VARCHAR (13),
	email VARCHAR (255),
	description VARCHAR (32)
);

CREATE TABLE logistic_service (
	code_service VARCHAR (16) PRIMARY KEY,
	NAME VARCHAR (255),
	description VARCHAR (32)
);

CREATE TABLE delivery_order (
	number_delivery_order VARCHAR (16) PRIMARY KEY,
	mass NUMERIC,
	volume NUMERIC,
	charge NUMERIC,
	destination_name VARCHAR (16),
	destination_address VARCHAR (32),
	destination_phone VARCHAR (13),
	destination_city VARCHAR (16),
	post_code VARCHAR (6)
);

CREATE TABLE delivery_instruction (
	id_delivery_instruction serial PRIMARY KEY,
	instruction_time TIMESTAMP,
	notes TEXT,
	delivery_status CHAR
);

CREATE TABLE bank_account (
	code_bank VARCHAR (4) PRIMARY KEY,
	bank_name VARCHAR (24),
	account_number VARCHAR (16),
	account_name VARCHAR (24),
	current_balance NUMERIC
);

CREATE TABLE bank_transaction (
	number_transaction VARCHAR (32) PRIMARY KEY,
	transaction_time TIMESTAMP,
	description TEXT,
	is_debet BOOLEAN,
	transaction_value NUMERIC
);

CREATE TABLE staff (
	id_staff serial PRIMARY KEY,
	staff_name VARCHAR (24),
	is_male BOOLEAN,
	phone VARCHAR (13),
	email VARCHAR (20),
	photo VARCHAR (18)
);

CREATE TABLE "user" (
	ID serial PRIMARY KEY,
	username VARCHAR (255) NOT NULL UNIQUE,
	NAME VARCHAR (100) NOT NULL,
	auth_key VARCHAR (32),
	access_token VARCHAR (40) NOT NULL,
	password_hash VARCHAR (255) NOT NULL,
	password_reset_token VARCHAR (255) UNIQUE,
	email VARCHAR (255) NOT NULL UNIQUE,
	status INT NOT NULL,
	created_at INT NOT NULL,
	updated_at INT NOT NULL
);

ALTER TABLE purchase_order ADD COLUMN code_merchant VARCHAR (10);

ALTER TABLE purchase_order ADD FOREIGN KEY (code_merchant) REFERENCES merchant (code_merchant);

ALTER TABLE purchase_order ADD COLUMN code_warehouse VARCHAR (4);

ALTER TABLE purchase_order ADD FOREIGN KEY (code_warehouse) REFERENCES warehouse (code_warehouse);

ALTER TABLE stock_agreement ADD COLUMN code_merchant VARCHAR (10);

ALTER TABLE stock_agreement ADD FOREIGN KEY (code_merchant) REFERENCES merchant (code_merchant);

ALTER TABLE agreement_item ADD COLUMN number_contract VARCHAR (10);

ALTER TABLE agreement_item ADD FOREIGN KEY (number_contract) REFERENCES stock_agreement (number_contract);

ALTER TABLE agreement_item ADD COLUMN number_product VARCHAR (10);

ALTER TABLE agreement_item ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE agreement_item ADD COLUMN number_product_color VARCHAR (20);

ALTER TABLE agreement_item ADD FOREIGN KEY (number_product_color) REFERENCES product_color (number_product_color);

ALTER TABLE agreement_item ADD COLUMN code_size VARCHAR (10);

ALTER TABLE agreement_item ADD FOREIGN KEY (code_size) REFERENCES product_size (code_size);

ALTER TABLE purchase_order_item ADD COLUMN number_po VARCHAR (10);

ALTER TABLE purchase_order_item ADD FOREIGN KEY (number_po) REFERENCES purchase_order (number_po);

ALTER TABLE purchase_order_item ADD COLUMN number_product VARCHAR (10);

ALTER TABLE purchase_order_item ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE purchase_order_item ADD COLUMN code_size VARCHAR (10);

ALTER TABLE purchase_order_item ADD FOREIGN KEY (code_size) REFERENCES product_size (code_size);

ALTER TABLE purchase_order_item ADD COLUMN number_product_color VARCHAR (20);

ALTER TABLE purchase_order_item ADD FOREIGN KEY (number_product_color) REFERENCES product_color (number_product_color);

ALTER TABLE product ADD COLUMN code_merchant VARCHAR (10);

ALTER TABLE product ADD FOREIGN KEY (code_merchant) REFERENCES merchant (code_merchant);

ALTER TABLE product ADD COLUMN code_category VARCHAR (10);

ALTER TABLE product ADD FOREIGN KEY (code_category) REFERENCES category (code_category);

ALTER TABLE product_color ADD COLUMN number_product VARCHAR (10);

ALTER TABLE product_color ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE rack ADD COLUMN code_warehouse VARCHAR (4);

ALTER TABLE rack ADD FOREIGN KEY (code_warehouse) REFERENCES warehouse (code_warehouse);

ALTER TABLE box ADD COLUMN number_rack VARCHAR (12);

ALTER TABLE box ADD FOREIGN KEY (number_rack) REFERENCES rack (number_rack);

ALTER TABLE inventory_item ADD COLUMN number_product VARCHAR (10);

ALTER TABLE inventory_item ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE inventory_item ADD COLUMN code_warehouse VARCHAR (4);

ALTER TABLE inventory_item ADD FOREIGN KEY (code_warehouse) REFERENCES warehouse (code_warehouse);

ALTER TABLE inventory_item ADD COLUMN number_product_color VARCHAR (20);

ALTER TABLE inventory_item ADD FOREIGN KEY (number_product_color) REFERENCES product_color (number_product_color);

ALTER TABLE inventory_item ADD COLUMN code_size VARCHAR (10);

ALTER TABLE inventory_item ADD FOREIGN KEY (code_size) REFERENCES product_size (code_size);

ALTER TABLE box_content ADD COLUMN sku VARCHAR (255);

ALTER TABLE box_content ADD FOREIGN KEY (sku) REFERENCES inventory_item (sku);

ALTER TABLE box_content ADD COLUMN number_box VARCHAR (12);

ALTER TABLE box_content ADD FOREIGN KEY (number_box) REFERENCES box(number_box);

ALTER TABLE receiving ADD COLUMN number_po VARCHAR (10);

ALTER TABLE receiving ADD FOREIGN KEY (number_po) REFERENCES purchase_order (number_po);

ALTER TABLE receive_item ADD COLUMN id_receiving serial;

ALTER TABLE receive_item ADD FOREIGN KEY (id_receiving) REFERENCES receiving (id_receiving);

ALTER TABLE receive_item ADD COLUMN number_product VARCHAR (10);

ALTER TABLE receive_item ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE receive_item ADD COLUMN code_size VARCHAR (10);

ALTER TABLE receive_item ADD FOREIGN KEY (code_size) REFERENCES product_size (code_size);

ALTER TABLE receive_item ADD COLUMN number_product_color VARCHAR (20);

ALTER TABLE receive_item ADD FOREIGN KEY (number_product_color) REFERENCES product_color (number_product_color);

ALTER TABLE storing_item ADD COLUMN number_instruction VARCHAR (20);

ALTER TABLE storing_item ADD FOREIGN KEY (number_instruction) REFERENCES storing_instruction (number_instruction);

ALTER TABLE storing_item ADD COLUMN sku VARCHAR (255);

ALTER TABLE storing_item ADD FOREIGN KEY (sku) REFERENCES inventory_item (sku);

ALTER TABLE storing_item ADD COLUMN number_box VARCHAR (12);

ALTER TABLE storing_item ADD FOREIGN KEY (number_box) REFERENCES box(number_box);

ALTER TABLE pick_item ADD COLUMN number_batch VARCHAR (20);

ALTER TABLE pick_item ADD FOREIGN KEY (number_batch) REFERENCES picking_batch (number_batch);

ALTER TABLE pick_item ADD COLUMN sku VARCHAR (255);

ALTER TABLE pick_item ADD FOREIGN KEY (sku) REFERENCES inventory_item (sku);

ALTER TABLE pick_item ADD COLUMN number_box VARCHAR (12);

ALTER TABLE pick_item ADD FOREIGN KEY (number_box) REFERENCES box(number_box);

ALTER TABLE unstored_items ADD COLUMN sku VARCHAR (255);

ALTER TABLE unstored_items ADD FOREIGN KEY (sku) REFERENCES inventory_item (sku);

ALTER TABLE stock_allocation ADD COLUMN code_sales_channel VARCHAR (4);

ALTER TABLE stock_allocation ADD FOREIGN KEY (code_sales_channel) REFERENCES sales_channel (code_sales_channel);

ALTER TABLE stock_allocation ADD COLUMN number_product VARCHAR (10);

ALTER TABLE stock_allocation ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE stock_allocation ADD COLUMN code_size VARCHAR (10);

ALTER TABLE stock_allocation ADD FOREIGN KEY (code_size) REFERENCES product_size (code_size);

ALTER TABLE stock_allocation ADD COLUMN number_product_color VARCHAR (20);

ALTER TABLE stock_allocation ADD FOREIGN KEY (number_product_color) REFERENCES product_color (number_product_color);

ALTER TABLE allocmutation ADD COLUMN code_sales_channel VARCHAR (4);

ALTER TABLE allocmutation ADD FOREIGN KEY (code_sales_channel) REFERENCES sales_channel (code_sales_channel);

ALTER TABLE allocmutation ADD COLUMN number_product_color VARCHAR (20);

ALTER TABLE allocmutation ADD FOREIGN KEY (number_product_color) REFERENCES product_color (number_product_color);

ALTER TABLE allocmutation ADD COLUMN code_size VARCHAR (10);

ALTER TABLE allocmutation ADD FOREIGN KEY (code_size) REFERENCES product_size (code_size);

ALTER TABLE allocmutation ADD COLUMN number_product VARCHAR (10);

ALTER TABLE allocmutation ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE unallocated ADD COLUMN code_size VARCHAR (10);

ALTER TABLE unallocated ADD FOREIGN KEY (code_size) REFERENCES product_size (code_size);

ALTER TABLE unallocated ADD COLUMN number_product VARCHAR (10);

ALTER TABLE unallocated ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE unallocated ADD COLUMN number_product_color VARCHAR (20);

ALTER TABLE unallocated ADD FOREIGN KEY (number_product_color) REFERENCES product_color (number_product_color);

ALTER TABLE sales_order ADD COLUMN code_sales_channel VARCHAR (4);

ALTER TABLE sales_order ADD FOREIGN KEY (code_sales_channel) REFERENCES sales_channel (code_sales_channel);

ALTER TABLE sales_order ADD COLUMN code_buyer VARCHAR (12);

ALTER TABLE sales_order ADD FOREIGN KEY (code_buyer) REFERENCES buyer (code_buyer);

ALTER TABLE sales_order ADD COLUMN number_transaction VARCHAR (12);

ALTER TABLE sales_order ADD FOREIGN KEY (number_transaction) REFERENCES bank_transaction (number_transaction);

ALTER TABLE sales_order ADD COLUMN code_service VARCHAR (4);

ALTER TABLE sales_order ADD FOREIGN KEY (code_service) REFERENCES logistic_service (code_service);

ALTER TABLE sales_order ADD COLUMN code_bank VARCHAR (4);

ALTER TABLE sales_order ADD FOREIGN KEY (code_bank) REFERENCES bank_account (code_bank);

ALTER TABLE order_item ADD COLUMN number_order VARCHAR (12);

ALTER TABLE order_item ADD FOREIGN KEY (number_order) REFERENCES sales_order (number_order);

ALTER TABLE order_item ADD COLUMN number_product VARCHAR (10);

ALTER TABLE order_item ADD FOREIGN KEY (number_product) REFERENCES product (number_product);

ALTER TABLE order_item ADD COLUMN number_product_color VARCHAR (20);

ALTER TABLE order_item ADD FOREIGN KEY (number_product_color) REFERENCES product_color (number_product_color);

ALTER TABLE order_item ADD COLUMN code_size VARCHAR (10);

ALTER TABLE order_item ADD FOREIGN KEY (code_size) REFERENCES product_size (code_size);

ALTER TABLE logistic_service ADD COLUMN code_tpl VARCHAR (4);

ALTER TABLE logistic_service ADD FOREIGN KEY (code_tpl) REFERENCES logistic_partner (code_tpl);

ALTER TABLE delivery_order ADD COLUMN code_service VARCHAR (4);

ALTER TABLE delivery_order ADD FOREIGN KEY (code_service) REFERENCES logistic_service (code_service);

ALTER TABLE delivery_instruction ADD COLUMN number_order VARCHAR (12);

ALTER TABLE delivery_instruction ADD FOREIGN KEY (number_order) REFERENCES sales_order (number_order);

ALTER TABLE delivery_instruction ADD COLUMN code_warehouse VARCHAR (4);

ALTER TABLE delivery_instruction ADD FOREIGN KEY (code_warehouse) REFERENCES warehouse (code_warehouse);

ALTER TABLE bank_transaction ADD COLUMN code_bank VARCHAR (4);

ALTER TABLE bank_transaction ADD FOREIGN KEY (code_bank) REFERENCES bank_account (code_bank);

ALTER TABLE "user" ADD COLUMN code_warehouse VARCHAR (4);

ALTER TABLE "user" ADD FOREIGN KEY (code_warehouse) REFERENCES warehouse (code_warehouse);