<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoringInstruction */

$this->title = $model->number_instruction;
$this->params['breadcrumbs'][] = ['label' => 'Storing Instructions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storing-instruction-view">
    <p>
        <?php if (access("update")) : ?>
            <?= Html::a('Update', ['update', 'id' => $model->number_instruction], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        

        <?php if (access("delete")) : ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->number_instruction], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number_instruction',
            'storing_time',
        ],
    ]) ?>
</div>