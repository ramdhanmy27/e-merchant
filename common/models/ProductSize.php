<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_size".
 *
 * @property string $code_size
 * @property string $name
 * @property string $group_size
 *
 * @property AgreementItem[] $agreementItems
 * @property InventoryItem[] $inventoryItems
 * @property OrderItem[] $orderItems
 * @property PurchaseOrderItem[] $purchaseOrderItems
 * @property ReceiveItem[] $receiveItems
 * @property StockAllocation[] $stockAllocations
 * @property Unallocated[] $unallocateds
 */
class ProductSize extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_size';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['code_size'], 'required'],

            // unique
            [['code_size'], 'unique'],

            // safe

            // integer

            // string
            [['group_size'], 'string', 'max' => 2],
            [['code_size'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 50],

            // exist
        
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_size' => 'Code Size',
            'name' => 'Size Name',
            'group_size' => 'Group Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementItems()
    {
        return $this->hasMany(AgreementItem::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItems()
    {
        return $this->hasMany(InventoryItem::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderItems()
    {
        return $this->hasMany(PurchaseOrderItem::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiveItems()
    {
        return $this->hasMany(ReceiveItem::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockAllocations()
    {
        return $this->hasMany(StockAllocation::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnallocateds()
    {
        return $this->hasMany(Unallocated::className(), ['code_size' => 'code_size']);
    }
}
