<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_item".
 *
 * @property integer $id_order_item
 * @property integer $qty
 * @property string $unit_price
 * @property string $number_order
 * @property string $number_product
 * @property string $number_product_color
 * @property string $code_size
 *
 * @property Product $product
 * @property ProductColor $productColor
 * @property ProductSize $productSize
 * @property SalesOrder $salesOrder
 */
class OrderItem extends \common\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'order_item';
    }

    public function rules()
    {
        return [
            // required
            //[['id_order_item'], 'required'],

            // unique
            //[['id_order_item'], 'unique'],

            // safe

            // integer
            [['qty'], 'integer'],
            [['unit_price'], 'number'],

            // string
            [['code_size'], 'string', 'max' => 10],
            [['number_product'], 'string', 'max' => 10],
            [['number_order'], 'string', 'max' => 12],
            [['number_product_color'], 'string', 'max' => 20],

            // exist
            [['number_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['number_product' => 'number_product']],
            [['number_product_color'], 'exist', 'skipOnError' => true, 'targetClass' => ProductColor::className(), 'targetAttribute' => ['number_product_color' => 'number_product_color']],
            [['code_size'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSize::className(), 'targetAttribute' => ['code_size' => 'code_size']],
            [['number_order'], 'exist', 'skipOnError' => true, 'targetClass' => SalesOrder::className(), 'targetAttribute' => ['number_order' => 'number_order']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_order_item' => 'Id Order Item',
            'qty' => 'Qty',
            'unit_price' => 'Unit Price',
            'number_order' => 'Id Sales Order',
            'number_product' => 'Id Product',
            'number_product_color' => 'Id Product Color',
            'code_size' => 'Id Product Size',
            'salesOrder.number' => 'Sales Order Number',
            'product.name' => 'Product Name',
            'productColor.color' => 'Product Color',
            'productSize.name' => 'Product Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductColor()
    {
        return $this->hasOne(ProductColor::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSize()
    {
        return $this->hasOne(ProductSize::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrder()
    {
        return $this->hasOne(SalesOrder::className(), ['number_order' => 'number_order']);
    }
}
