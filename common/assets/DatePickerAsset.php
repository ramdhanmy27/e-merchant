<?php namespace common\assets;

class DatePickerAsset extends \yii\web\AssetBundle {
    public $sourcePath = '@app/web/ace/dist';

    public $js = [
        "js/date-time/moment.min.js",
        "js/date-time/daterangepicker.min.js",
    ];

    public $css = [
        "css/daterangepicker.min.css",
    ];
    
    // public $depends = [
    //     "backend\assets\AceAsset",
    // ];
}
