<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ucwords(str_replace("-", " ", $this->controller->id));

// closed
$list["columns"] = $draft["columns"] = [
    ['class' => 'yii\grid\SerialColumn'],
    
    'number_merch_do',
    'receive_time',
    'description',
    'number_po',

    [
        'class' => 'yii\grid\ActionColumn',
        "template" => "{view}",
    ]
];

$key = count($list["columns"])-1;

// draft
$draft["columns"][$key] = [
    "class" => "yii\grid\ActionColumn",
    "template" => "{view} {draft} {delete}",
    "buttons" => [
        "draft" => function($url, $model, $id) {
            return Html::a(
                "<i class='glyphicon glyphicon-pencil' title='Update'></i>", ["draft", "id" => $id]
            );
        },
        "delete" => function($url, $model, $id) {
            return Html::a(
                "<i class='glyphicon glyphicon-trash' title='Delete'></i>", $url, [
                    "data" => [
                        "method" => "POST",
                        "confirm" => "",
                    ]
                ]
            );
        }
    ]
];
?>

<div class="<?= $this->controller->id ?>-index">
    <?php if (!$this->request->isAjax): ?>
        <p> <?= Html::a("Create $this->title", ['create'], ['class' => 'btn btn-success']) ?> </p>
    <?php endif; ?>

    <div class="widget-box transparent">
        <div class="widget-header" style="border-bottom: none">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#list">
                        Receiving <?= ($count=$list["dataProvider"]->getTotalCount()) > 0 ? "($count)" : "" ?>
                    </a> 
                </li>
                <li> 
                    <a data-toggle="tab" href="#draft">
                        Draft <?= ($count=$draft["dataProvider"]->getTotalCount()) > 0 ? "($count)" : "" ?>
                    </a> 
                </li>
            </ul>
        </div>

        <div class="no-border widget-main tab-content">
            <div id="list" class="tab-pane active forscrollhor">
                <?= GridView::widget($list); ?>
            </div>

            <div id="draft" class="tab-pane forscrollhor">
                <?= GridView::widget($draft); ?>
            </div>
        </div>
    </div>
</div>