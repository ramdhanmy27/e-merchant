<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\modelName */

?>

<div class="create">
    <div class="page-header">
        <h1>
            <?= Html::encode($this->title) ?>
        </h1>
    </div>

    <?= $this->render([
	    	"@app/views/".$this->controller->id."/_form",
	    	'_form',
    	]
    	, ['model' => $model]
    ) ?>
</div>
