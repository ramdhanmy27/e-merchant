$(document).ready(function() {
	var mark = function(input, header) {
		var cls = "bg-warning";
		var ele = $("#order-item tbody tr");
		var index = header.index() + 4;

		if (input.prop("checked"))
			header.addClass(cls);
		else
			header.removeClass(cls);

		ele.each(function(i, item) {
			var item = $("td:eq("+ index +")", item);

			if (input.prop("checked"))
				item.addClass(cls);
			else
				item.removeClass(cls);
		})
	}

	var tmp;

	$("#order-item input[type='radio']").change(function() {
		var $this = $(this);
		
		mark($this, $this.parentsUntil("td").parent());

		if (tmp !== undefined)
			mark(tmp, tmp.parentsUntil("td").parent());

		tmp = $this;
	});

	$("#order-item input[type='radio']:eq(0)").prop("checked", true).trigger("change");
})