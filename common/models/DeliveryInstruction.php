<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "delivery_instruction".
 *
 * @property integer $id_delivery_instruction
 * @property string $instruction_time
 * @property string $notes
 * @property string $delivery_status
 * @property string $number_order
 * @property string $code_warehouse
 *
 * @property SalesOrder $salesOrder
 * @property Warehouse $warehouse
 */
class DeliveryInstruction extends \common\yii\db\ActiveRecord
{
    const STATUS_OPEN = '0';
    const STATUS_PROCESS = '1';
    const STATUS_DELIVER = '2';

    public static function tableName()
    {
        return 'delivery_instruction';
    }

    public function rules()
    {
        return [
            // unique
            [['id_delivery_instruction'], 'unique'],

            // safe
            [['instruction_time'], 'safe'],

            // string
            [['notes'], 'string'],
            [['delivery_status'], 'string', 'max' => 1],

            // exist
            [['number_order'], 'exist', 'skipOnError' => true, 'targetClass' => SalesOrder::className(), 'targetAttribute' => ['number_order' => 'number_order']],
            [['code_warehouse'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['code_warehouse' => 'code_warehouse']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_delivery_instruction' => 'Delivery Instruction ID',
            'instruction_time' => 'Instruction Time',
            'notes' => 'Notes',
            'delivery_status' => 'Delivery Status',
            'number_order' => 'Sales Order ID',
            'code_warehouse' => 'Warehouse ID',
            // 'salesOrder.number_order' => 'Order Number',
            // 'warehouse.name' => 'Warehouse Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrder()
    {
        return $this->hasOne(SalesOrder::className(), ['number_order' => 'number_order']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::className(), ['code_warehouse' => 'code_warehouse']);
    }
}
