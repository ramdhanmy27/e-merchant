<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\yii\widgets\ActiveForm;
use common\models\Merchant;
use common\models\Product;
use kartik\select2\Select2;

$this->title = 'Stock Agreement: '.$model->number_contract;

register(["@js/stock-agreement/form.js"], [
    "depends" => [
        "common\assets\AngularAsset",
        "backend\assets\AceAsset",
    ]
]);

$format = "d/m/Y";
$daterange = $model->contract_date!=null || $model->until_date!=null ? 
                date($format, strtotime($model->contract_date))." - ".date($format, strtotime($model->until_date)) : 
                null;

?>

<div class="stock-agreement-draft" ng-controller="stockAgreementController">
    <div class="stock-agreement-form" ng-init="f.init('<?= $model->number_contract ?>')">
        <?php $form = ActiveForm::begin(["options" => ["onkeypress" => "return event.keyCode != 13;"]]); ?>

        <!-- Contract Information -->
        <div class='widget-box row col-md-8'>
            <div class="widget-header">
                <h5 class="widget-title">Contract Information</h5>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                </div>
            </div>

            <div class="widget-body" ng-init="
                contract.description='<?= $model->description ?>'; 
                contract.merchant='<?= $model->code_merchant ?>'; 
                contract.date='<?= $daterange ?>';
            ">
                <div class="widget-main">
                    <?= $form->field($model, 'code_merchant')->option(Merchant::option("name", "code_merchant"), null, []) ?>

                    <div class="form-group">
                        <label class="control-label col-md-4 col-xs-12" for="date-range"> Allocation Commitment </label>
                        <div class="col-md-8 col-xs-12">
                            <?= Html::input("text", null, $daterange, ["id"=>"date-range", "class"=>"input-date-range", "required"=>""]) ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'description')->textArea(['maxlength' => true]) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <div class="clearfix"></div>

        <!-- Product Details -->
        <div class='widget-box'>
            <div class="widget-header">
                <h5 class="widget-title">Product Details</h5>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main product-details">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr> <th>Name</th> <th class="nowrap">Qty Commitment</th> <th class="nowrap">Qty Safety</th> <th></th> </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="(x, row) in product track by x">
                                <td>
                                    <?= Select2::widget([
                                        "name" => "agreement[][product]",
                                        "data" => Product::option(),
                                        "options" => [
                                            "placeholder" => "Choose Product",
                                            "ng-model" => "product[x].id",
                                        ]
                                    ]) ?>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{f.qtyCommitProduct(x)}}</span>
                                        <span class="input-group-btn" ng-if="f.isProductSelected(x)">
                                            <button type="button" class="btn btn-default btn-sm" modal-lg="#modal-variations" 
                                                ng-click="f.loadVariation(x, 'commit')">Detail</button>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{f.qtySafetyProduct(x)}}</span>
                                        <span class="input-group-btn" ng-if="f.isProductSelected(x)">
                                            <button type="button" class="btn btn-default btn-sm" modal-lg="#modal-variations"
                                                ng-click="f.loadVariation(x, 'ready')">Detail</button>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-danger" type="button" ng-if="product.length > 1" ng-click="f.delete(x)">
                                        <i class="fa fa-trash"></i>
                                    </button> 
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="text-right">
                        <a href="<?= Url::to(["product/create"]) ?>" class="btn btn-primary" modal-lg>
                            <i class="glyphicon glyphicon-plus"></i> Create Product
                        </a>

                        <button type="button" class="btn btn-info" ng-click="f.add()">
                            <i class="glyphicon glyphicon-plus"></i> Add Detail
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="text-right">
            <?php if (access('delete')) : ?>
                <?= Html::a("<i class='glyphicon glyphicon-remove'></i> Cancel", ["delete", "id" => $model->number_contract], [
                    "class" => "btn btn-danger",
                    "data" => [
                        "method" => "POST",
                        "confirm" => "This action will remove your draft. Are you sure ?",
                    ],
                ]); ?>
            <?php endif; ?>

            <?= Html::a("<i class='glyphicon glyphicon-ok'></i> Save", ["save", "id" => $model->number_contract], [
                'class' => 'btn btn-success',
                "ng-click" => 'f.formValidation($event)',
                "data" => [
                    // "method" => "POST",
                    "confirm" => "You cannot edit this draft afterward. Are you sure ?",
                ],
            ]) ?>
        </div>

        <!-- Variations Modal -->
        <?= $this->render("@common/views/modal", [
            "title" => "Modal",
            "attr" => ["id" => "modal-variations"],
            "body" => $this->render("@app/views/product/variations"),
        ]) ?>
    </div>
</div>