<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pick_item".
 *
 * @property integer $id_pick_item
 * @property integer $qty
 * @property string $number_batch
 * @property string $sku
 * @property string $number_box
 *
 * @property InventoryItem $inventoryItem
 * @property PickingBatch $pickingBatch
 * @property Box $box
 */
class PickItem extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pick_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            // [['id_pick_item'], 'required'],

            // unique
            // [['id_pick_item'], 'unique'],

            // safe
            [['batch_time'], 'safe'],

            // integer
            [['qty'], 'integer'],

            // string
            [['number_batch'], 'string', 'max' => 10],
            [['number_box'], 'string', 'max' => 12],

            // exist
            [['sku'], 'exist', 'skipOnError' => true, 'targetClass' => InventoryItem::className(), 'targetAttribute' => ['sku' => 'sku']],
            [['number_batch'], 'exist', 'skipOnError' => true, 'targetClass' => PickingBatch::className(), 'targetAttribute' => ['number_batch' => 'number_batch']],
            [['number_box'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['number_box' => 'number_box']],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pick_item' => 'Id Pick Item',
            'qty' => 'Qty',
            'pickingBatch.number_batch' => 'Picking Batch Number',
            'inventoryItem.product.name' => 'Product Name',
            'number_box' => 'number_box',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItem()
    {
        return $this->hasOne(InventoryItem::className(), ['sku' => 'sku']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPickingBatch()
    {
        return $this->hasOne(PickingBatch::className(), ['number_batch' => 'number_batch']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBox()
    {
        return $this->hasOne(Box::className(), ['number_box' => 'number_box']);
    }
}
