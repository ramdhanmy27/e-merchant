<?php

# debugging variable
// format view for printing variable
function prin_format() {
	echo "<style> pre {word-break: break-word} </style>";
	echo "<pre>";

	foreach (func_get_args() as $val) {
		echo "<hr>";
		var_dump($val);
	}

	echo "</pre>";
}

function prins() {
	global $backtrace_index;
	$backtrace = debug_backtrace();

	echo "<hr>";
	echo "<b>".trace_format($backtrace[$backtrace_index==null ? 0 : $backtrace_index])."</b>";
	call_user_func_array('prin_format', func_get_args());
}

function prin() {
	global $backtrace_index;
	$backtrace_index = 2;
	call_user_func_array('prins', func_get_args());
	exit;
}

# backtracing process
// format view for backtracing file
function trace_format($trace) {
	if (isset($trace['file']))
		return str_replace(realpath('sites/all/modules'), null, str_replace('\\', '/', $trace['file'])).' line: '.$trace['line'];
}

// get argument list as array
function get_arg($arg) {
	$res = array();

	if (count($arg)) {
		foreach ($arg as $val) {
			if (is_array($val)) 
				$res[] = '['.implode(', ', get_arg($val)).']';
			else if (is_object($val)) 
				$res[] = get_class($val);
			else if (is_string($val))
				$res[] = '"'.$val.'"';
			else if ($val == NULL)
				$res[] = 'NULL';
			else
				$res[] = $val;
		}
	}

	return $res;
}

// backtracing file
function traces() {
	$backtrace = debug_backtrace();

	foreach ($backtrace as $obj) {
		$arg = array();
		$str[] = trace_format($obj).' <b>'.@$obj['class'].'->'.$obj['function'].'('.implode(', ', get_arg($obj['args'])).')</b>';
	}

	echo "<div><li>".implode("</li><li>", $str)."</li></div>";
	call_user_func_array('prin_format', func_get_args());
}

// backtracing file
function trace() {
	$backtrace = debug_backtrace();

	foreach ($backtrace as $obj) {
		$arg = array();
		$str[] = trace_format($obj).' <b>'.@$obj['class'].'->'.$obj['function'].'('.implode(', ', get_arg($obj['args'])).')</b>';
	}

	echo "<div><li>".implode("</li><li>", $str)."</li></div>";
	call_user_func_array('prin_format', func_get_args());
	exit;
}