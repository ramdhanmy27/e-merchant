<?php namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DeliveryInstruction;

/**
 * DeliveryInstructionSearch represents the model behind the search form about `common\models\DeliveryInstruction`.
 */
class DeliveryInstructionSearch extends DeliveryInstruction
{
    public function rules()
    {
        return [
            [[
                'id_delivery_instruction',
                'instruction_time',
                'notes',
                'delivery_status',
                'number_order',
                'code_warehouse',
            ], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param mixed $status
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeliveryInstruction::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_delivery_instruction' => $this->id_delivery_instruction,
            'instruction_time' => $this->instruction_time,
            'notes' => $this->notes,
            'delivery_status' => $this->delivery_status,
            'number_order' => $this->number_order,
            'code_warehouse' => $this->code_warehouse,
        ]);

        $query->andFilterWhere(['ilike', 'id_delivery_instruction', $this->id_delivery_instruction])
            ->andFilterWhere(['ilike', 'instruction_time', $this->instruction_time])
            ->andFilterWhere(['ilike', 'notes', $this->notes])
            ->andFilterWhere(['ilike', 'number_order', $this->number_order])
            ->andFilterWhere(['ilike', 'code_warehouse', $this->code_warehouse]);

        return $dataProvider;
    }
}
