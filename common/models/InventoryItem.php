<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "inventory_item".
 *
 * @property string $sku
 * @property integer $qty
 * @property string $number_product
 * @property string $code_warehouse
 * @property string $number_product_color
 * @property string $code_size
 *
 * @property BoxContent[] $boxContents
 * @property Product $product
 * @property ProductColor $productColor
 * @property ProductSize $productSize
 * @property Warehouse $warehouse
 * @property PickItem[] $pickItems
 * @property StoringItem[] $storingItems
 * @property UnstoredItems[] $unstoredItems
 */
class InventoryItem extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['sku'], 'required'],

            // unique
            [['sku'], 'unique'],

            // safe
            [['instruction_time'], 'safe'],

            // integer
            [['qty'], 'integer'],

            // string
            [['code_size'], 'string', 'max' => 10],
            [['code_warehouse'], 'string', 'max' => 4],
            [['number_product'], 'string', 'max' => 10],
            [['sku'], 'string', 'max' => 255],
            [['number_product_color'], 'string', 'max' => 20],

            // exist
            [['number_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['number_product' => 'number_product']],
            [['number_product_color'], 'exist', 'skipOnError' => true, 'targetClass' => ProductColor::className(), 'targetAttribute' => ['number_product_color' => 'number_product_color']],
            [['code_size'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSize::className(), 'targetAttribute' => ['code_size' => 'code_size']],
            [['code_warehouse'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['code_warehouse' => 'code_warehouse']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sku' => 'Id Inventory Item',
            'sku' => 'Sku',
            'qty' => 'Qty',
            'number_product' => 'Id Product',
            'code_warehouse' => 'Id Warehouse',
            'number_product_color' => 'Id Product Color',
            'code_size' => 'Id Product Size',
            'product.name' => 'Product Name',
            'warehouse.name' => 'Warehouse Name',
            'productColor.color' => 'Product Color',
            'productSize.name' => 'Product Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoxContents()
    {
        return $this->hasMany(BoxContent::className(), ['sku' => 'sku']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductColor()
    {
        return $this->hasOne(ProductColor::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSize()
    {
        return $this->hasOne(ProductSize::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::className(), ['code_warehouse' => 'code_warehouse']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPickItems()
    {
        return $this->hasMany(PickItem::className(), ['sku' => 'sku']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoringItems()
    {
        return $this->hasMany(StoringItem::className(), ['sku' => 'sku']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnstoredItems()
    {
        return $this->hasMany(UnstoredItems::className(), ['sku' => 'sku']);
    }
}
