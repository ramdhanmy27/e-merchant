<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use common\models\Merchant;
use common\models\Product;
use common\models\PurchaseOrder;
use kartik\select2\Select2;

$this->title = 'Receive Product: '.$model->id_receiving;

register(["@js/receiving/form-create.js"], [
	"depends" => [
		"common\assets\AngularAsset",
		"common\assets\DatePickerAsset",
		"backend\assets\AceAsset",
	]
]);

$format = "d/m/Y";
$daterange = $model->receive_time!=null ? 
				date($format, strtotime($model->receive_time)): 
				null;
?>

<div class="receiving-draft" ng-controller="receivingController">
	<h1 class="page-header"><?= Html::encode($this->title) ?></h1>

	<div class="receiving-form" ng-init="f.init('<?= $model->id_receiving ?>')">

		<?php $form = ActiveForm::begin(["id" => "receiving", 'options' => ['name' => 'receiving_form', "onkeypress" => "return event.keyCode != 13;"]]); ?>
		
		<div class='row'>
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
				<!-- Product Received -->
				<div class='widget-box'>
					<div class="widget-header">
						<h5 class="widget-title">Product Received</h5>

						<div class="widget-toolbar">
							<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
						</div>
					</div>

					<!-- init data receiving, nantinya mau diubah pakai watch dan konek ke ng-model -->
					<div class="widget-body" ng-init="
						receiving.number_po='<?= $model->number_po ?>';
						receiving.number_merch_do='<?= $model->number_merch_do ?>';
						receiving.description='<?= $model->description ?>'; 
					">
						<div class="widget-main">
							<!-- modul terpisah, diakhir aja diurusin -->
							<?= $form->field($model, 'number_po', ['inputOptions'=>['ng-model'=>'receiving.number_po', 'required' => '']])->option(PurchaseOrder::option("number_po", "number_po")) ?>
							
							<?= $form->field($model, 'number_merch_do', ['inputOptions'=>['ng-model'=>'receiving.number_merch_do']])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'description', ['inputOptions'=>['ng-model'=>'receiving.description']])->textInput(['maxlength' => true]) ?>

							<br><hr><br>
							
							<table class="table table-responsive table-hover table-striped table-bordered" ng-if="product.length>0">
								<thead>
									<tr>
										<th>Product</th>
										<th>Quantity</th>
										<!-- <th>Action</th> -->
									</tr>
								</thead>
								<tbody class="product-details">
									<tr ng-repeat="(x, y) in product track by $index" class="receiving">
										<td>
											<?= Select2::widget([
												"name" => 'receiving[{{$index}}][number_product]',
												"data" => Product::option(),
												"options" => [
													"placeholder" => "Choose Product",
													"ng-model" => "product[x].number_product",
												]
											]) ?>

											<div ng-messages='receiving_form["receiving["+$index+"][number_product]"].$error' 
												 ng-if='receiving_form["receiving["+$index+"][number_product]"].$dirty'>
												<div ng-message="required">This field is required</div>
											</div>
										</td>
										<td>
											<div class="input-group">
												<span class="input-group-addon">{{f.qtyProduct(x)}}</span>
												<span class="input-group-btn" ng-if="f.isProductSelected(x)">
													<button type="button" class="btn btn-default btn-sm" modal="#modal-variations" 
														ng-click="f.loadVariation(x, 'total')">Detail</button>
												</span>
											</div>
										</td>
										<!--
										<td>
											<button class="btn btn-sm btn-danger" type="button" ng-if="product.length > 1" ng-click="f.delete(x)">
												<i class="fa fa-trash"></i>
											</button> 
										</td>
										-->
									</tr>
								</tbody>
							</table>
							<!--
							<div class="text-right">
								<button type="button" class="btn btn-info" ng-click="f.add()">
									<i class="glyphicon glyphicon-plus"></i> Add Detail
								</button>
							</div>
							-->
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php ActiveForm::end(); ?>

		<div class='row' ng-if="product.length>0">
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
				<hr>
				<?= Html::a("<i class='glyphicon glyphicon-remove'></i> Cancel", ["delete", "id_receiving" => $model->id_receiving], [
					"class" => "btn btn-danger",
					"data" => [
						"method" => "POST",
						"confirm" => "This action will remove your draft. Are you sure ?",
					],
				]); ?>
				<?= Html::a("<i class='glyphicon glyphicon-ok'></i> Save", ["save", "id_receiving" => $model->id_receiving], [
					'class' => 'btn btn-success',
					"ng-click" => "f.formValidation()",
					"data" => [
						"method" => "POST",
						"confirm" => "You cannot edit this draft afterward. Are you sure ?",
					],
				]) ?>
			</div>
		</div>
		<hr>

		<!-- Variations Modal -->
		<?= $this->render("@common/views/modal", [
			"title" => "Modal",
			"attr" => ["id" => "modal-variations"],
			"body" => $this->render("@app/views/product/variations"),
		]) ?>
	</div>
</div>