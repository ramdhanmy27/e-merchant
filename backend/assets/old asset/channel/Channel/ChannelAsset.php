<?php namespace backend\assets\channel\Channel;

class ChannelAsset extends \yii\web\AssetBundle
{
    public $baseUrl = '@web/js/channel/channel';
    
    public $js = [
        'channel.js',
    ];

    public $depends = [
        "backend\assets\AceAsset",
    ];
}
