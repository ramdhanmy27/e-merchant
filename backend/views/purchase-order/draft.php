<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\yii\widgets\ActiveForm;
use common\models\Merchant;
use common\models\Warehouse;
use common\models\Product;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\PurchaseOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- PO Information -->
<?= $this->render("@common/views/modal", [
    "title" => "Modal",
    "attr" => ["id" => "modal-variations"],
    "body" => $this->render("@app/views/product/variations"),
]) ?>

<?php ob_start(); ?>
<?= $form->field($model, 'code_merchant')->option(Merchant::option("name", "code_merchant"), null, []) ?>
<?= $form->field($model, 'code_warehouse')->option(Warehouse::option("name", "code_warehouse"), null, []) ?>
<?= $form->field($model, 'date', ["inputOptions" => [
    "class" => "input-date form-control", 
    "value" => $model->date ? date("d/m/Y", strtotime($model->date)) : null
]])->textInput() ?>
<?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

<?= $this->render("@common/views/widget", [
    "title" => "PO Information",
    "attr" => ["class" => "col-md-6"],
    "body" => ob_get_clean(),
]) ?>

<div class="clearfix"></div>

<!-- Product Details -->
<?php ob_start(); ?>
<table class="table table-bordered table-striped">
    <thead>
        <tr> <th>Name</th> <th>Quantity</th> <th class="nowrap">Item Price</th> <th class="nowrap">Total Price</th> <th></th> </tr>
    </thead>
    <tbody>
        <tr ng-repeat="(x, row) in product track by x">
            <td>
                <?= Select2::widget([
                    "name" => "po[][product]",
                    "data" => Product::option(),
                    "options" => [
                        "placeholder" => "Choose Product",
                        "ng-model" => "product[x].id",
                    ]
                ]) ?>
            </td>
            <td>
                <div class="input-group">
                    <span class="input-group-addon">{{f.getQtyTotal(x)}}</span>
                    <span class="input-group-btn" ng-if="f.isProductSelected(x)">
                        <button type="button" class="btn btn-default btn-sm" modal-lg="#modal-variations" 
                            ng-click="f.loadVariation(x)">Detail</button>
                    </span>
                </div>
            </td>
            <td><h5 class="nowrap">{{product[x].price | currency:"Rp ":0}}</h5></td>
            <td><h5 class="nowrap">{{f.priceTotal(x) | currency:"Rp ":0}}</h5></td>
            <td>
                <button class="btn btn-sm btn-danger" type="button" ng-if="product.length > 1" ng-click="f.delete(x)">
                    <i class="fa fa-trash"></i>
                </button> 
            </td>
        </tr>
    </tbody>
</table>

<div class="text-right">
    <a href="<?= Url::to(["product/create"]) ?>" class="btn btn-primary" modal-lg>
        <i class="glyphicon glyphicon-plus"></i> Create Product
    </a>

    <button type="button" class="btn btn-info" ng-click="f.add()">
        <i class="glyphicon glyphicon-plus"></i> Add Detail
    </button>
</div>

<?= $this->render("@common/views/widget", [
    "title" => "Product Details",
    "attr" => ["class" => "product-details"],
    "body" => ob_get_clean(),
]) ?>

<div class="clearfix"></div>

<!-- Variations Modal -->
<?= $this->render("@common/views/modal", [
    "title" => "Modal",
    "attr" => ["id" => "modal-variations"],
    "body" => $this->render("@app/views/product/variations"),
]) ?>