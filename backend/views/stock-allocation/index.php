<?php

use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Merchant */

$this->title = 'Product Allocation';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::csrfMetaTags() ?>
<div class="stock-allocation-index" ng-app="stock" ng-controller="app" ng-init="getsales='<?= Url::toRoute(["/stock-allocation/sales"], true) ?>'">
	<div class='row'>
		<div class='col-xs-12'>
			<!-- Monitoring Posisi Alokasi Barang -->
			<div class='widget-box'>
				<div class="widget-header">
					<h5 class="widget-title">Monitoring Posisi Alokasi Barang</h5>

					<div class="widget-toolbar">
						<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
					</div>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row" style="margin-bottom:10px">
							<div class="col-md-3">
								<?php
								echo Select2::widget([
								    'name' => 'Category',
								    'value' => $c,
								    'data' => $category,
								    'options' => ['placeholder' => 'Select Category ...']
								]);
								?>
								<?= Html::button("Filter", ['class' => 'btn btn-danger','onclick'=>'gogo("'.Url::toRoute(["/stock-allocation"], true).'")']) ?>
							</div>
						</div>
						<div class="forscrollhor">
							<table border="1" width="100%" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th class="resizing" rowspan="4">
											Kode Produk
										</th>
										<th class="resizing" rowspan="4">
											Nama Produk
										</th>
										<th class="resizing" rowspan="4">
											Total Qty
										</th>
										<th class="resizing" rowspan="4">
											Unallocated
										</th>
										<th class="resizing" colspan="<?= count($sales_self)+(count($sales_eks)*2) ?>">
											Allocated
										</th>
										<th rowspan="4">
											Action
										</th>
									</tr>
									<tr>
										<th class="resizing" colspan="<?= count($sales_self) ?>">
											Self Channel
										</th>
										<th class="resizing" colspan="<?= count($sales_eks)*2 ?>">
											E-Marketplace
										</th>
									</tr>
									<tr>
									<?php foreach ($sales_self as $ss) { ?>
										<th class="resizing" rowspan="2">
											<?=$ss["name"]?>
										</th>
									<?php } ?>
									<?php foreach ($sales_eks as $se) { ?>
										<th class="resizing" colspan="2">
											<?=$se["name"]?>
										</th>
									<?php } ?>
									</tr>
									<tr>
									<?php for($i=1;$i<=count($sales_eks);$i++){ ?>
										<th>
											Allocated
										</th>
										<th>
											Published
										</th>
									<?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach($product as $p) { ?>
										<tr>
											<td>
												<input readonly class="form-control" value="<?=$p["number_product"]?>">
											</td>
											<td>
												<input readonly class="form-control" value="<?=$p["brand_name"]?>">
											</td>
											<td>
												<input readonly class="form-control" value="<?=($total_qty[$p["number_product"]][0]["total"])?$total_qty[$p["number_product"]][0]["total"]:"0"?>">
											</td>
											<td>
												<input readonly class="form-control" value="<?=($total_unallocated[$p["number_product"]][0]["total"])?$total_unallocated[$p["number_product"]][0]["total"]:"0"?>">
											</td>
											<?php foreach ($sales_self as $ss) { $sudah=FALSE;?>
												<td>
													<?php for($i=0;$i<count($stock[$p["number_product"]]);$i++){ 
														if($ss["code_sales_channel"]==$stock[$p["number_product"]][$i]["code_sales_channel"]){
															 echo '<input readonly class="form-control" value="'.$stock[$p["number_product"]][$i]["allocated"].'">';
															 $sudah=TRUE; 
														}
													} 
													if($sudah==FALSE){
														echo '<input readonly class="form-control" value="0">';
													} 
													?>
												</td>
											<?php } 
											foreach ($sales_eks as $se) { 
													$sudah=FALSE;
												for($i=0;$i<count($stock[$p["number_product"]]);$i++){ 
													if($se["code_sales_channel"]==$stock[$p["number_product"]][$i]["code_sales_channel"]){
														 echo '<td><input readonly class="form-control" value="';
														 echo ($stock[$p["number_product"]][$i]["allocated"]!="")?$stock[$p["number_product"]][$i]["allocated"]:"0";
														 echo '"></td>';
														 echo '<td><input readonly class="form-control" value="';
														 echo ($stock[$p["number_product"]][$i]["published"]!="")?$stock[$p["number_product"]][$i]["published"]:"0";
														 echo '"></td>';
														 $sudah=TRUE;
													}
												}
												if($sudah==FALSE){
													echo '<td><input readonly class="form-control" value="0"></td><td><input readonly class="form-control" value="0"></td>';
												}
											} ?>
											<td>
												<button class="btn btn-success reallocate"  type="button" id="<?= $p['number_product'] ?>" ng-click="reallocate('<?= $p['number_product'] ?>','<?= $p['brand_name'] ?>','<?= Url::toRoute(['/stock-allocation/info','id'=>$p['number_product']], true) ?>')">Reallocate</button>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class='row'>
		<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-12" ?>'>
			<!-- Info -->
			<div class='widget-box'>
				<div class="widget-header">
					<h5 class="widget-title">Info</h5>

					<div class="widget-toolbar">
						<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
					</div>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						<div class="row">
							<div class="col-md-12">
								<table width="100%" class="table" border="0">
									<tr>
										<td style="max-width:100px">
											Kode Product
										</td>
										<td>
										<form id="forminfo" method="post" action="<?= Url::toRoute(["/stock-allocation/save"], true) ?>" enctype="multipart/form-data">
										<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
											<input readonly class="form-control" value="{{idproduct}}" name="number_product">
										</td>
									</tr>
									<tr>
										<td style="max-width:100px">
											Nama Product
										</td>
										<td>
											<input readonly class="form-control" value="{{productname}}">
										</td>
									</tr>
								</table>		
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-bordered" width="100%" border="1" style="text-align:center">
									<thead>
										<tr>
											<th rowspan="2">Color</th>
											<th rowspan="2">Size</th>
											<th rowspan="2">Total Qty</th>
											<th rowspan="2">Unallocated</th>
											<th colspan="<?= count($sales_self)+(count($sales_eks)*2) ?>">Allocated</th>
										</tr>
										<tr>			
											<?php foreach ($sales_self as $ss) { ?>
												<th class="resizing">
													<?=$ss["name"]?>
												</th>
											<?php } ?>
											<?php foreach ($sales_eks as $se) { ?>
												<th class="resizing">
													<?=$se["name"]?>
												</th>
											<?php } ?>
										</tr>
									</thead>
									<tbody>
										<?php
											$val="";
											foreach ($sales_self as $ss) { 
												$val.=$ss["code_sales_channel"]."[items.unique] +";
											}
											foreach ($sales_eks as $se) { 
												$val.=$se["code_sales_channel"]."[items.unique] +";
											}
											$val = rtrim($val,"+");
										?>
										<tr ng-if="item.length <= 0" >
											<input type="hidden" id="toggletotal" ng-model="togtal" value="">
											<td ng-if="togtal==''" id="noselects" colspan="<?= count($sales_eks)+count($sales_self)+4 ?>">
												 No Product Selected
											</td>
											<td ng-if="togtal=='TRUE'" id="nodatas" colspan="<?= count($sales_eks)+count($sales_self)+4 ?>">
												 No Data Available
											</td>
										</tr>
<!-- first fetch-->
										{{items.total_qty}}
										<tr ng-repeat="items in item track by $index" ng-class="((items.total_qty-(<?=$val?>)) < 0) ? 'danger' : ''" >
<!--- center -->
										<input type="hidden" id="ndatas" ng-if="togtal=='ADA'">
										<tr id="datakolom" ng-repeat="items in item track by $index" ng-class="((items.total_qty-(<?=$val?>)) < 0) ? 'danger thistrerror' : ''" >
<!--second fetch -->
											<input type="hidden" ng-model="buttonsave[items.unique]" value="{{((items.total_qty-(<?=$val?>)) < 0) ? 'TRUE' : 'FALSE'}}">
											<td>
												<input form="forminfo" type="text" readonly class="form-control" value="{{items.color}}">
												<input type="hidden" form="forminfo" name="number_product_color[]" value="{{items.code_color}}">
											</td>
											<td>
												<input form="forminfo" type="text" readonly class="form-control" value="{{items.size}}">
											</td>
											<td ng-if="items.total_qty!=null">
												<input type="text" readonly min="0" class="form-control totalqtycount" value="{{items.total_qty}}">
											</td>
											<td ng-if="items.total_qty==null">
												<input type="text" readonly min="0" class="form-control totalqtycount" value='0'>
											</td>
											<td>
												<input type="number" name="unallocated[{{items.code_color}}][{{items.code_size}}]" form="forminfo" min="0" readonly class="form-control totalunalcount" value="{{(<?=$val?>==0)&& items.unallocated || items.total_qty-(<?=$val?>)}}">
											</td>
											<?php foreach ($sales_self as $ss) { ?>
												<td >
													<input name="{{items.code_color}}[{{items.code_size}}][<?= $ss["code_sales_channel"]?>]" form="forminfo" min="0" ng-attr-max="{{(items.total_qty-(<?=$val?>) < 1) && <?= $ss["code_sales_channel"]?>[items.unique] || ''}}" class="form-control <?= $ss["code_sales_channel"]?> stockinput" alt="<?= $ss["code_sales_channel"]?>" onchange="totalsales('<?= $ss["code_sales_channel"]?>')" ng-model="<?= $ss["code_sales_channel"]?>[items.unique]" type="number" value="{{items.<?= $ss["code_sales_channel"]?> && items.<?= $ss["code_sales_channel"]?> || 0}}">
												</td>
											<?php } ?>
											<?php foreach ($sales_eks as $se) { ?>
												<td >
													<input name="{{items.code_color}}[{{items.code_size}}][<?= $se["code_sales_channel"]?>]" form="forminfo" min="0" ng-attr-max="{{(items.total_qty-(<?=$val?>) < 1) && <?= $se["code_sales_channel"]?>[items.unique] || ''}}" class="form-control <?= $se["code_sales_channel"]?> stockinput" alt="<?= $se["code_sales_channel"]?>" onchange="totalsales('<?= $se["code_sales_channel"]?>')" ng-model="<?= $se["code_sales_channel"]?>[items.unique]" type="number" value="{{items.<?= $se["code_sales_channel"]?> && items.<?= $se["code_sales_channel"]?> || 0}}">
												</td>
											<?php } ?>
										</tr>
										<tr>
											<td colspan="2">
												Total
											</td>
											<td>	
												<input readonly type="number" min="0" id="totalqty" value="0" class="form-control">
											</td>
											<td>	
												<input readonly type="number" min="0" id="totalunallo" value="0" class="form-control">
											</td>

											<?php foreach ($sales_self as $ss) { ?>
												<td >
													<input value="0" readonly min="0" class="form-control totalinput" id="total<?= $ss['code_sales_channel']?>" type="number">
													<input type="hidden" id="totalbef<?= $ss['code_sales_channel']?>">
												</td>
											<?php } ?>
											<?php foreach ($sales_eks as $se) { ?>
												<td >
													<input value="0" readonly min="0"  class="form-control totalinput" id="total<?= $se['code_sales_channel']?>" type="number">
													<input type="hidden" id="totalbef<?= $se['code_sales_channel']?>">
												</td>
											<?php } ?>
										</tr>
										<tr>
											<td colspan="2">
												Change (+/-)
											</td>
											<td>	
												
											</td>
											<td>	
												<input readonly type="number" min="0" value="0" id="changeunallo" class="form-control">
												<input type="hidden" id="unallobef">
											</td>
											<?php foreach ($sales_self as $ss) { ?>
												<td >
													<input readonly min="0" value="0" class="form-control focusthis" id="change<?= $ss['code_sales_channel']?>" type="number">
												</td>
											<?php } ?>
											<?php foreach ($sales_eks as $se) { ?>
												<td >
													<input readonly min="0" value="0" class="form-control focusthis" id="change<?= $se['code_sales_channel']?>" type="number">
												</td>
											<?php } ?>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="submit" id="submitbtn" ng-disabled="item.length==0" class="pull-right btn btn-info" value="Save" url="<?= Url::toRoute(["/stock-allocation/save"], true)?>" >
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
            
		</div>
	</div>

</div>
