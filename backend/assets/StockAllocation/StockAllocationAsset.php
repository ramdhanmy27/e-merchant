<?php namespace backend\assets\StockAllocation;

class StockAllocationAsset extends \yii\web\AssetBundle {
    public $baseUrl = '@web';
    
    public $js = [
        'js/stock-allocation/stock-allocation-jquery.js',
        'js/stock-allocation/stock-allocation.js'
    ];

    public $depends = [
        "common\assets\AngularAsset",
        "common\assets\AngularMessagesAsset",
        "backend\assets\AceAsset",
    ];
}