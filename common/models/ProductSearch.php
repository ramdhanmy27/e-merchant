<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number_product', 'code_category', 'code_merchant', 'brand_name', 'name', 'description', 'group_size'], 'safe'],
            [['basic_price'], 'number'],
            [['is_deprecated'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'basic_price' => $this->basic_price,
            'is_deprecated' => $this->is_deprecated,
        ]);

        $query->andFilterWhere(['ilike', 'number_product', $this->number_product])
            ->andFilterWhere(['ilike', 'brand_name', $this->brand_name])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'group_size', $this->group_size])
            ->andFilterWhere(['ilike', 'code_category', $this->code_category])
            ->andFilterWhere(['ilike', 'code_merchant', $this->code_merchant]);

        return $dataProvider;
    }
}
