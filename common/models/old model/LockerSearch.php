<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Locker;

/**
 * LockerSearch represents the model behind the search form about `common\models\Locker`.
 */
class LockerSearch extends Locker
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_warehouse'], 'integer'],
            [['locker','idWarehouse.name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($id_warehouse=null, $params)
    {

        $query = Locker::find();
        if ($id_warehouse != null){
            $query->where(["id_warehouse" => $id_warehouse]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->joinWith(["idWarehouse" => function($query) {$query->from(["warehouse" => "warehouse"]); }]);

        $dataProvider->sort->attributes["idWarehouse.name"] = [
            "asc" => ["warehouse.name" => SORT_ASC],
            "desc" => ["warehouse.name" => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['locker.id' => $this->id,])
        ->andFilterWhere(['ilike', 'warehouse.name', $this->getAttribute("idWarehouse.name")]);

        $query->andFilterWhere(['ilike', 'locker.locker', $this->locker]);

        return $dataProvider;
    }

    public function attributes(){
        return array_merge(parent::attributes(), ["idWarehouse.name"]);
    }
}
