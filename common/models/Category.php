<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property string $code_category
 * @property string $name
 * @property string $description
 *
 * @property Product[] $products
 */
class Category extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['code_category'], 'required'],

            // unique
            [['code_category'], 'unique'],

            // safe

            // integer

            // string
            [['code_category'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 16],
            [['description'], 'string', 'max' => 64],

            // exist
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_category' => 'Code',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['code_category' => 'code_category']);
    }
}
