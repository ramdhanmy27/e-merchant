<?php namespace common\assets;

use yii\web\AssetBundle;

class AngularMessagesAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/js';

    public $js = [
        "angular-messages.min.js",
    ];

    public $depends = [
    	"common\assets\AngularAsset"
    ];
}
