<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PurchaseOrder */

$this->title = $model->number_po;
?>

<div class="purchase-order-view">
    <p>
        <?php if ($model->isDraft() && $this->can("draft")): ?>
            <?= Html::a('Update', ['draft', 'id' => $model->number_po], ['class' => 'btn btn-primary']); ?>
        <?php endif; ?>

        <?php if (access("delete")): ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->number_po], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number_po',
            'merchant.name',
            'date:date',
            [
                'label'  => 'Status',
                'format'  => 'html',
                'value' => $model->labelStatus,
            ],
            'description',
        ],
    ]) ?>
</div>