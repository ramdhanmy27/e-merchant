<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unallocated".
 *
 * @property integer $id_unallocated
 * @property integer $qty
 * @property string $code_size
 * @property string $number_product
 * @property string $number_product_color
 *
 * @property Product $product
 * @property ProductColor $productColor
 * @property ProductSize $productSize
 */
class Unallocated extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unallocated';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            // [['id_unallocated'], 'required'],

            // unique
            // [['id_unallocated'], 'unique'],

            // safe

            // integer
            [['qty'], 'integer'],

            // string
            [['code_size'], 'string', 'max' => 10],
            [['number_product'], 'string', 'max' => 10],
            [['number_product_color'], 'string', 'max' => 20],

            // exist
            [['number_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['number_product' => 'number_product']],
            [['number_product_color'], 'exist', 'skipOnError' => true, 'targetClass' => ProductColor::className(), 'targetAttribute' => ['number_product_color' => 'number_product_color']],
            [['code_size'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSize::className(), 'targetAttribute' => ['code_size' => 'code_size']],
        
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_unallocated' => 'Id Unallocated',
            'qty' => 'Qty',
            'code_size' => 'Id Product Size',
            'number_product' => 'Id Product',
            'number_product_color' => 'Id Product Color',
            'product.name' => 'Product',
            'productSize.name' => 'Product Size',
            'productColor.color' => 'Product Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductColor()
    {
        return $this->hasOne(ProductColor::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSize()
    {
        return $this->hasOne(ProductSize::className(), ['code_size' => 'code_size']);
    }
}
