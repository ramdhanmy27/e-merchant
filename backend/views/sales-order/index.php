<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\SalesOrder;
use common\models\SalesChannel;

$this->title = 'Sales Order';

if ($this->session->hasFlash("report")) {
    $id = $this->session->getFlash("report");
    $this->registerJS("window.location.href=url.to('sales-order/render-pdf', {id: '$id', action: 'download'})");
}
?>

<div class="sales-order-index">
	<?php if (access("create")) : ?>
		<p> <?= Html::a('Create Sales Order', ['create'], ['class' => 'btn btn-success']) ?> </p>
	<?php endif; ?>

	<?= GridView::widget([
		"dataProvider" => $dataProvider,
		"filterModel" => $searchModel,
		"columns" => [
			['class' => 'yii\grid\SerialColumn'],

			'number_order',
			'number_transaction',
			[
				"attribute" => 'order_time',
				"format" => 'datetime',
				"filter" => Html::activeTextInput($searchModel, "order_time", ["class" => "input-date"]),
			],
			[
				"label" => "Buyer",
				"attribute" => 'code_buyer',
				"value" => 'buyer.name',
			],
			[
				"label" => "Channel",
				"attribute" => 'code_sales_channel',
                "filter" => SalesChannel::option("name", "code_sales_channel"),
				"value" => 'salesChannel.name'
			],
			[
				"attribute" => "total_bill",
				"value" => function($model) {
					return currency($model->total_bill);
				}
			],
			[
				"attribute" => "status",
                'format' => 'raw',
                "filter" => SalesOrder::$status,
				"value" => function($model) {
					return $model->labelStatus;
				}
			],

			[
				"class" => "common\yii\grid\ActionColumn",
				"template" => "{pdf} {view}",
				"contentOptions" => ["class" => "nowrap"],
				"buttons" => [
					"pdf" => function($url, $model, $id) {
						return Html::a(
							"<i class='fa fa-icon fa-file-pdf-o text-danger'></i>", 
							["render-pdf", "id" => $model->number_order], 
							["target" => "_blank", "title" => "View as PDF"]
						);
					}
				]
			],
		]
	]) ?>
</div>