<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SalesChannel */

$this->title = 'Create Sales Channel';
$this->params['breadcrumbs'][] = ['label' => 'Sales Channels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-channel-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
