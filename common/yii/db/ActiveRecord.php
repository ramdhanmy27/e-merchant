<?php namespace common\yii\db;

use Respect\Validation\Validator as v;

class ActiveRecord extends \yii\db\ActiveRecord {
    
    /**
     * return data indexed by id
     * @param  string|int   $id     "exception by id"
     * @param  string $field
     * @return array
     */
    public static function option($label=null, $key=null, $where=[]) {
        if ($key==null || $label==null) {
            $class = static::className();
            $model = new $class;

            if ($key==null) {
                $key = isset($model->tableSchema->primaryKey[0]) ? 
                        $model->tableSchema->primaryKey[0] : 
                        current($model->tableSchema->columns)->name;
            }

            $label = $label==null ? next($model->tableSchema->columns)->name : db::raw($label);
        }

        $query = static::find()->select([$label, $key]);

        if (count($where) >= 1)
            $query = call_user_func_array([$query, "where"], $where);

        return $query->indexBy($key)->column();
    }

    public function load($data, $formName = null) {
        $name = static::name();

        if (isset($data[$name])) 
            $data[$name] = array_filter($data[$name], function($val) {
                return $val!="";
            });
        
        return parent::load($data, $formName);
    }

    public static function assert(array $data, $scenario = null) {
        if (array_keys($data)!=range(0, count($data)-1))
            $data = [$data];

        $label = static::attributeLabels();

        foreach ($data as $value) {
            if (!isset($v)) {
                $v = v::alwaysValid();
                $validator = static::getValidator($scenario);
                
                if (!is_array($value))
                    throw new Exception("Assertion value must be an array associative");

                foreach ($value as $field => $val) {
                    if (isset($validator[$field]))
                        $v = $v->key($field, $validator[$field]);
                }
            }
            
            $v->assert($value);
        }
    }

    public static function getValidator($scenario = null) {
        $label = static::attributeLabels();
        $rules = static::getFieldRules($scenario);
        $validator = [];
        $func = [
            "required" => "notEmpty",
            "boolean" => "boolType",
            "date" => ["date", "format"],
            "email" => "email",
            "ip" => "ip",
            "in" => ["in", "range"],
            "integer" => "intVal",
            "number" => "numeric",
            "match" => ["regex", "pattern"],
            "string" => "stringType",
        ];

        foreach ($rules as $field => $rule) {
            $optional = !isset($rule["required"]);
            
            foreach ($func as $type => $val) {
                if (!isset($rule[$type]))
                    continue;

                $param_exists = is_array($val);
                $fn = $param_exists ? array_shift($val) : $val;
                $param = [];

                // fetching parameters
                if ($param_exists)
                    foreach ($val as $x) {
                        if (array_key_exists($x, $rule))
                            $param[] = $rule[$x];
                        else
                            break;
                    }

                $validator[$field] = call_user_func_array(
                    [isset($validator[$field]) ? $validator[$field] : "Respect\Validation\Validator", $fn], 
                    $param
                );

                switch ($type) {
                    case 'number': 
                    case 'integer': if ($type=="integer" || $type=="number") {
                        foreach (["min", "max"] as $param) {
                            if (isset($rule[$param]))
                                $validator[$field] = call_user_func_array([$validator[$field], $param], [$rule[$param]]);
                        }
                    }

                    case 'string': if ($type == "string") {
                        if (isset($rule["min"]) || isset($rule["max"]))
                            $validator[$field] = $validator[$field]->length(@$rule["min"], @$rule["max"]);
                    }
                }
            }

            if ($optional)
                $validator[$field] = v::optional($validator[$field]);

            if (isset($label[$field]))
                $validator[$field] = $validator[$field]->setName($label[$field]);
        }

        return $validator;
    }

    public function getFieldRules($scenario = null) {
        $rules = [];
        foreach (static::rules() as $rule) {
            $fields = array_shift($rule);

            if (isset($rule["on"]) && $rule["on"]!=$scenario)
                continue;

            foreach ($rule as $k => $v) {
                foreach ($fields as $field) {
                    $noval = is_numeric($k);
                    $rules[$field][$noval ? $v : $k] = $noval ? '' : $v;
                }
            }
        }

        return $rules;
    }

    /**
     * return model name id
     * @return string
     */
    public static function name() {
        $class = static::className();
        return substr($class, strrpos($class, "\\")+1);
    }

    /*public function getAttributes($names=null, $except=[]) {
        $data = parent::getAttributes($names, $except);

        if (!is_callable([$this, "rules"]))
            return $data;

        foreach ($this->rules() as $rule) {
            $type = null;
            $fields = [];

            foreach ($rule as $key => $val) {
                if (is_numeric($key)) {
                    // fields
                    if (is_array($val)) {
                        $fields = $val;
                    }
                    // type filter
                    else $type = $val;
                }
            }

            foreach ($fields as $field) {
                if (isset($data[$field]))
                    $data[$field] = static::filterValue($this->{$field}, $type);
            }
        }

        return $data;
    }

    public static function filterValue($value, $type) {
        if ($value !== null)
        switch ($type) {
            case 'integer':
                if (!is_numeric($value)) 
                    $value = intval($value);
                break;

            case 'string':
                if (!is_string($value)) 
                    $value = (string) $value;
                break;
        }

        return $value;
    }*/

    public static function query() {
        return db::query(static::tableName());
    }
}