$(document).ready(function() {
	//change status of user
	$("body").on("click", ".sw", function(){
		var status = $(this).attr("aktifkan");
		var iduser = $(this).attr("id_user");
		
		$.ajax({
			url : url.to("user/userstat"),
			type : "POST",
			data : {status : status, iduser : iduser},
			success: function(data){
				if (data === "success"){
					if(status == 0){
						$(".sw[id_user='"+iduser+"']").attr("aktifkan", "1");
						$("td span[iduser='"+iduser+"']").removeClass("label-success").addClass("label-warning");
						$("td span[iduser='"+iduser+"']").html("Inactive");
					}
					else if(status == 1){
						$(".sw[id_user='"+iduser+"']").attr("aktifkan", "0");
						$("td span[iduser='"+iduser+"']").removeClass("label-warning").addClass("label-success");
						$("td span[iduser='"+iduser+"']").html("Active");
					}
				}
			}
		});
	});
});