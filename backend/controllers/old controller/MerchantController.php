<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\Merchant;
use common\models\MerchantSearch;
use common\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MerchantController implements the CRUD actions for Merchant model.
 */
class MerchantController extends Controller
{
    public static $permissions = ["create", "update", "view"];

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['index', 'view', 'create'], "create"],
                [['index', 'view', 'update'], "update"],
            ]),
        ];
    }

    /**
     * Lists all Merchant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MerchantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Merchant model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Merchant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Merchant();
        $modelUser = new User(['scenario' => 'admin-create']);

        if($model->load(Yii::$app->request->post())
            && $modelUser->load(Yii::$app->request->post())
            && $model->validate()
            && $modelUser->validate()
            ){
            $modelUser->save(false);
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole('merchant'), $modelUser->id);
            $model->id_user = $modelUser->id;
            $model->status = 1;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelUser' => $modelUser,
            ]);
        }
    }

    /**
     * Updates an existing Merchant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUser = $model->user;

        if(Yii::$app->request->isPost){
            $model->load(Yii::$app->request->post());
            $modelUser->load(Yii::$app->request->post());
            $modelUser->save();
            $model->save();
            $this->session->setFlash("success", "Successfully update merchant");
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelUser' => $modelUser,
            ]);
        }
    }

    /**
     * Deletes an existing Merchant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Merchant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Merchant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Merchant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
