<?php namespace common\assets;

use yii\web\AssetBundle;

class FileInputAsset extends AssetBundle
{
    public $sourcePath = '@vendor/kartik-v/bootstrap-fileinput';

    public $js = [
        "js/fileinput.min.js",
    ];
 
    public $css = [
        "css/fileinput.min.css",
    ];

	public $depends = ["yii\web\JqueryAsset"];
}
