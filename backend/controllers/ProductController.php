<?php

namespace backend\controllers;

use Yii;
use common\models\Product;
use common\models\ProductSearch;
use common\models\ProductColor;
use common\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class ProductController extends Controller
{
    use \common\yii\web\CrudControllerTrait;

    public function behaviors() {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
                'delete-item' => ['post'],
                'delete-image' => ['post'],
                'upload-image' => ['post'],
                'set-color' => ['post'],
            ]),
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['create'], "create"],
                [['update'], "update"],
                [['delete'], "delete"],
            ]),
        ];
    }

    public function actionValidation() {
        return view();
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Product();
        $model->is_deprecated = false;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            /* Generate new model color */
            $newModelColor = Yii::$app->request->post('color', []);

            foreach($newModelColor as $key=>$value){
                // Insert new Model Color
                $modelColor = new ProductColor();
                $modelColor->setAttributes($value);
                $modelColor->number_product_color = $model->number_product . '-'. $modelColor->color;
                $modelColor->number_product = $model->number_product;
                $modelColor->save(false);
            }

            return redirect(['view', 'id' => $model->number_product]);
        } 

        return view(['model' => $model]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $post = $this->request->post();

        if ($model->load($post)) {
            $model->save();
            return redirect(['view', 'id' => $model->number_product]);
        }
        
        return view(['model' => $model]);
    }

    /**
     * create/update color
     * @param  string $id product ID
     */
    public function actionSetColor($id) {
        $color = $this->request->post("color");

        if ($color===null || !isset($color["new"]) || trim($color["new"])=="")
            return $this->abort();

        if (!isset($color["old"]) || trim($color["old"])=="") {
            $model = new ProductColor();
            $model->number_product = $id;
        }
        else if (!($model = $this->modelColor($id, $color["old"])))
            return;

        $model->color = $color["new"];
        $model->save();
    }

    /**
     * Delete product color
     * @param  string $id    product ID
     * @param  string $color color name
     * @return response
     */
    public function actionDeleteItem($id, $color) {
        if (!($model = $this->modelColor($id, $color)))
            return;

        $model->delete();
    }

    /**
     * upload image
     * @param  string $id product ID
     * @param  string $color color name
     * @return json filename[]
     */
    public function actionUploadImage($id, $color) {
        if (!($model = $this->modelColor($id, $color)))
            return;

        return json($model->upload($_FILES["img"]["tmp_name"]));
    }

    /**
     * delete image
     * @param  string $id    product ID
     * @param  string $color color name
     * @return response
     */
    public function actionDeleteImage($id, $color) {
        if (!($model = $this->modelColor($id, $color)))
            return;

        return $model->deleteImages($this->request->post("file"));
    }

    protected function modelColor($id, $color) {
        $model = ProductColor::findOne(["color"=>$color, "number_product"=>$id]);

        if ($model === null)
            $this->abort(400);

        return $model;
    }
}
