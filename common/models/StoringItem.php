<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "storing_item".
 *
 * @property integer $id_storing_item
 * @property integer $qty
 * @property string $number_instruction
 * @property string $sku
 * @property string $number_box
 *
 * @property Box $numberBox
 * @property InventoryItem $sku0
 * @property StoringInstruction $numberInstruction
 */
class StoringItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storing_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qty'], 'integer'],
            [['number_instruction'], 'string', 'max' => 20],
            [['sku'], 'string', 'max' => 255],
            [['number_box'], 'string', 'max' => 12],
            [['number_box'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['number_box' => 'number_box']],
            [['sku'], 'exist', 'skipOnError' => true, 'targetClass' => InventoryItem::className(), 'targetAttribute' => ['sku' => 'sku']],
            [['number_instruction'], 'exist', 'skipOnError' => true, 'targetClass' => StoringInstruction::className(), 'targetAttribute' => ['number_instruction' => 'number_instruction']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_storing_item' => 'Id Storing Item',
            'qty' => 'Qty',
            'number_instruction' => 'Number Instruction',
            'sku' => 'Sku',
            'number_box' => 'Number Box',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumberBox()
    {
        return $this->hasOne(Box::className(), ['number_box' => 'number_box']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSku0()
    {
        return $this->hasOne(InventoryItem::className(), ['sku' => 'sku']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumberInstruction()
    {
        return $this->hasOne(StoringInstruction::className(), ['number_instruction' => 'number_instruction']);
    }
}
