<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="category-form">

	<?php $form = ActiveForm::begin(); ?>
	
	<div class='row'>
		<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
			<!-- Category Information -->
			<div class='widget-box'>
				<div class="widget-header">
					<h5 class="widget-title">Category Information</h5>

					<div class="widget-toolbar">
						<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
					</div>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						<?= $form->field($model, 'code_category')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
					</div>
				</div>
			</div>
            
		</div>
	</div>
	
	<div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
	<hr>
</div>