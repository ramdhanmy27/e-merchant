<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = 'Create Box';
?>
<div class="category-create">
	<?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>