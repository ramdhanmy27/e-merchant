<?php namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StockAgreement;
use common\yii\db\db;

/**
 * StockAgreementSearch represents the model behind the search form about `common\models\StockAgreement`.
 */
class StockAgreementSearch extends StockAgreement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number_contract', 'contract_date', 'until_date', 'description', 'code_merchant', "status"], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param mixed $status
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StockAgreement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
            "to_char(contract_date, 'DD/MM/YYYY')" => $this->contract_date,
            "to_char(until_date, 'DD/MM/YYYY')" => $this->until_date,
        ]);

        $query->andFilterWhere(['ilike', 'number_contract', $this->number_contract])
            ->andFilterWhere(['ilike', 'code_merchant', $this->code_merchant])
            ->andFilterWhere(['ilike', 'description', $this->description]);

        return $dataProvider;
    }
}
