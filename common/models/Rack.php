<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rack".
 *
 * @property string $number_rack
 * @property string $row_number
 * @property string $column_number
 * @property integer $level_count
 * @property integer $box_capacity
 * @property integer $box_count
 * @property string $code_warehouse
 *
 * @property Box[] $boxes
 * @property Warehouse $warehouse
 */
class Rack extends \common\yii\db\ActiveRecord
{
    const BOX_LIMIT = 1000;

    public static function tableName()
    {
        return 'rack';
    }

    public function rules()
    {
        return [
            // required
            [['number_rack'], 'required'],

            // unique
            [['number_rack'], 'unique'],

            // integer
            [['level_count', 'box_capacity'], 'integer', "min" => 1],
            [['box_count'], 'integer', "min" => 1, "max" => self::BOX_LIMIT],

            // string
            [['row_number', 'column_number', 'code_warehouse'], 'string', 'max' => 4],
            [['number_rack'], 'string', 'max' => 12],

            // exist
            [['code_warehouse'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['code_warehouse' => 'code_warehouse']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'number_rack' => 'Number Rack',
            'row_number' => 'Row Number',
            'column_number' => 'Column Number',
            'level_count' => 'Level Count',
            'box_capacity' => 'Box Capacity',
            'box_count' => 'Box Count',
            'code_warehouse' => 'Warehouse Code',
            'warehouse.name' => 'Warehouse',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoxes()
    {
        return $this->hasMany(Box::className(), ['number_rack' => 'number_rack']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::className(), ['code_warehouse' => 'code_warehouse']);
    }
}
