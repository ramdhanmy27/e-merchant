<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BankAccount;
use common\yii\db\db;
use yii\data\SqlDataProvider;

/**
 * BankAccountSearch represents the model behind the search form about `common\models\BankAccount`.
 */
class BankAccountSearch extends BankAccount
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_bank', 'bank_name', 'account_number', 'account_name'], 'safe'],
            [['current_balance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /*$query = BankAccount::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'code_bank' => $this->code_bank,
            'current_balance' => $this->current_balance,
        ]);

        $query->andFilterWhere([
        	'ilike', 'bank_name', $this->bank_name,
            'current_balance' => $this->current_balance,
        ]);

        $query->andFilterWhere(['ilike', 'code_bank', $this->code_bank])
            ->andFilterWhere(['ilike', 'bank_name', $this->bank_name])
            ->andFilterWhere(['ilike', 'account_number', $this->account_number])
            ->andFilterWhere(['ilike', 'account_name', $this->account_name]);

        $query->leftJoin("bank_transaction bt","bank_account.code_bank=bt.code_bank");
        $query->select([
        	'*',
        	db::raw(
	        	"(select count(*) from bank_transaction bt 
	        	where bt.code_bank=bank_account.code_bank) as count"
	       	)
	    ]);*/
		$count = Yii::$app->db->createCommand('
		    SELECT COUNT(*) FROM bank_account
		')->queryScalar();
		$this->load($params);
        $query=db::raw("SELECT *,(select count(*) from bank_transaction bt where bt.code_bank=b.code_bank) as count FROM bank_account b WHERE code_bank ilike :code_bank AND bank_name ilike :bank_name AND account_number ilike :account_number AND account_name ilike :account_name");
		$dataProvider = new SqlDataProvider([
		    'sql' => $query,
		    'params'=>[
        		':code_bank'=>"%".$this->code_bank."%",
        		':bank_name'=>"%".$this->bank_name."%",
        		':account_number'=>"%".$this->account_number."%",
        		':account_name'=>"%".$this->account_name."%"
        	],
        	'key'=>'code_bank',
		    'totalCount' => $count,
		    'pagination' => [
		        'pageSize' => 10,
		    ],
		    'sort' => [
		        'attributes' => [
		            'title',
		            'view_count',
		            'created_at',
		        ],
		    ],
		]);
        return $dataProvider;
    }
}
