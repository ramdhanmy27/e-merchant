<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use common\models\Product;
use kartik\select2\Select2;

$this->title = 'Storing Instruction: '.$model->number_instruction;

register(["@js/storing-instruction/form-create.js"], [
	"depends" => [
		"common\assets\AngularAsset",
		"common\assets\DatePickerAsset",
		"backend\assets\AceAsset",
	]
]);

$format = "d/m/Y";
$daterange = $model->storing_time!=null ? 
				date($format, strtotime($model->storing_time)): 
				null;
?>

<div class="storing-instruction-draft" ng-controller="storingInstructionController">
	<div class="storing-instruction-form">
		<?php $form = ActiveForm::begin(["id" => "storing_instruction", 'options' => ['name' => 'storing_instruction_form', "onkeypress" => "return event.keyCode != 13;"]]); ?>
		
		<div class='row'>
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
				<!-- Storing Instruction -->
				<div class='widget-box'>
					<div class="widget-header">
						<h5 class="widget-title">Storing Instruction</h5>

						<div class="widget-toolbar">
							<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
						</div>
					</div>

					<!-- init data storing_instruction, nantinya mau diubah pakai watch dan konek ke ng-model -->
					<div class="widget-body">
						<div class="widget-main">

							<?= $form->field($model, 'number_instruction', ['inputOptions'=>['ng-model'=>'storing_instruction.number_instruction', 'required' => '']])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'storing_time', ['inputOptions'=>['ng-model'=>'storing_instruction.storing_time']])->textInput() ?>

						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php ActiveForm::end(); ?>

		<div class='row'>
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
				<?= Html::button('Load', ['class' => 'btn btn-primary', 'ng-click' => 'f.loadItem()']) ?>
				<hr>
			</div>
		</div>

		<div class='row' ng-if="product.length>0">
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
				<table class="table table-responsive table-hover table-striped table-bordered" ng-if="product.length>0">
					<thead>
						<tr>
							<th>Product</th>
							<th>Quantity</th>
							<!-- <th>Action</th> -->
						</tr>
					</thead>
					<tbody class="product-details">
						<tr ng-repeat="(x, y) in product track by $index" class="storing-instruction">
							<td>
								<?= Select2::widget([
									"name" => 'storing_instruction[{{$index}}][number_product]',
									"data" => Product::option(),
									"options" => [
										"placeholder" => "Choose Product",
										"ng-model" => "product[x].number_product",
									]
								]) ?>

								<div ng-messages='storing_instruction_form["storing_instruction["+$index+"][number_product]"].$error' 
									 ng-if='storing_instruction_form["storing_instruction["+$index+"][number_product]"].$dirty'>
									<div ng-message="required">This field is required</div>
								</div>
							</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">{{f.qtyProduct(x)}}</span>
									<span class="input-group-btn" ng-if="f.isProductSelected(x)">
										<button type="button" class="btn btn-default btn-sm" modal="#modal-variations" 
											ng-click="f.loadVariation(x, 'total')">Detail</button>
									</span>
								</div>
							</td>
							<!--
							<td>
								<button class="btn btn-sm btn-danger" type="button" ng-if="product.length > 1" ng-click="f.delete(x)">
									<i class="fa fa-trash"></i>
								</button> 
							</td>
							-->
						</tr>
					</tbody>
				</table>
				<!--
				<div class="text-right">
					<button type="button" class="btn btn-info" ng-click="f.add()">
						<i class="glyphicon glyphicon-plus"></i> Add Detail
					</button>
				</div>
				-->
			</div>
		</div>

		<div class='row' ng-if="product.length>0">
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
				<?= Html::button('Auto Generate', ['class' => 'btn btn-primary', 'ng-click' => 'f.generate()']) ?>
				<hr>
			</div>
		</div>

		<div class='row' ng-if="storing_item.length>0">
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-12" ?>'>
				<table class="table table-responsive table-hover table-striped table-bordered" ng-if="storing_item.length>0">
					<thead>
						<tr>
							<th>Rack</th>
							<th>Shelf Level</th>
							<th>Box</th>
							<th>Product</th>
							<th>Size</th>
							<th>Color</th>
							<th>Qty</th>
							<!-- <th>Action</th> -->
						</tr>
					</thead>
					<tbody class="product-details">
						<tr ng-repeat="(x, y) in storing_item track by $index" class="storing-item">
							
							<td>
								{{y.number_rack}}
							</td>
							<td>
								{{y.rack_level}}
							</td>
							<td>
								{{y.number_box}}
							</td>
							<td>
								{{y.number_product}}
							</td>
							<td>
								{{y.code_size}}
							</td>
							<td>
								{{y.number_product_color}}
							</td>
							<td>
								{{y.qty}}
							</td>

							<!--
							<td>
								<button class="btn btn-sm btn-danger" type="button" ng-if="product.length > 1" ng-click="f.delete(x)">
									<i class="fa fa-trash"></i>
								</button> 
							</td>
							-->
						</tr>
					</tbody>
				</table>
				<!--
				<div class="text-right">
					<button type="button" class="btn btn-info" ng-click="f.add()">
						<i class="glyphicon glyphicon-plus"></i> Add Detail
					</button>
				</div>
				-->
			</div>
		</div>

		<div class='row' ng-if="storing_item.length>0">
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-12" ?> text-right'>
				<?= Html::button('Print & Save', ['class' => 'btn btn-primary', 'ng-click' => 'f.saveStoringItem()']) ?>
				<hr>
			</div>
		</div>

		<div class='row' ng-if="0">
			<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
				<hr>
				<?= Html::button('Save', ['class' => 'btn btn-primary', 'ng-click' => 'f.saveItem()']) ?>
				
				<?= Html::a("<i class='glyphicon glyphicon-remove'></i> Cancel", ["delete", "number_instruction" => $model->number_instruction], [
					"class" => "btn btn-danger",
					"data" => [
						"method" => "POST",
						"confirm" => "This action will remove your draft. Are you sure ?",
					],
				]); ?>
				<?= Html::a("<i class='glyphicon glyphicon-ok'></i> Save", ["save", "number_instruction" => $model->number_instruction], [
					'class' => 'btn btn-success',
					"ng-click" => "f.formValidation()",
					"data" => [
						"method" => "POST",
						"confirm" => "You cannot edit this draft afterward. Are you sure ?",
					],
				]) ?>
			</div>
		</div>
		<hr>

		<!-- Variations Modal -->
		<?= $this->render("@common/views/modal", [
			"title" => "Modal",
			"attr" => ["id" => "modal-variations"],
			"body" => $this->render("@app/views/product/variations"),
		]) ?>
	</div>
</div>