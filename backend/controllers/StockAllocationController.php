<?php

namespace backend\controllers;

use Yii;
use common\models\StockAllocation;
use common\models\Product;

class StockAllocationController extends \yii\web\Controller
{
    public function actionIndex($c="")
    {
    	$query=(new \yii\db\Query())
    		->select(["brand_name","number_product"])
    		->from("product")
    		->orderBy("number_product");
    	if($c!=""){
    		$query->where(["code_category"=>$c]);
    	}
    	$product=$query->all();
    	$stock=array();
    	$total_qty=array();
    	$total_unallocated=array();
    	for($i=0;$i<count($product);$i++){
    		$stock[$product[$i]["number_product"]]=Yii::$app->db->createCommand("SELECT DISTINCT ON (s.code_sales_channel) s.id_stock_allocation,sum(s.allocation_qty) OVER(PARTITION by s.code_sales_channel) as allocated,sum(s.published_qty) OVER(PARTITION by s.code_sales_channel) as published,s.code_sales_channel FROM stock_allocation s INNER JOIN product p ON p.number_product=s.number_product WHERE s.number_product='".$product[$i]["number_product"]."'")->queryAll();
    		$total_qty[$product[$i]["number_product"]]= (new \yii\db\Query())
		    		->select(["sum(qty) as total"])
		    		->from("inventory_item")
		    		->where(["number_product"=>$product[$i]["number_product"]])
		    		->all();
    		$total_unallocated[$product[$i]["number_product"]]=Yii::$app->db->createCommand("SELECT sum(qty) as total FROM unallocated WHERE number_product = '".$product[$i]["number_product"]."'")->queryAll();
    	}
    	$sales_self= (new \yii\db\Query())
    		->select(["*"])
    		->from("sales_channel")
    		->where("is_internal='t'")
    		->all();
    	$sales_eks= (new \yii\db\Query())
    		->select(["name","code_sales_channel"])
    		->from("sales_channel")
    		->where("is_internal='f'")
    		->all();
    	$category = yii\helpers\ArrayHelper::map(\common\models\Category::find()->asArray()->all(), 'code_category', 'name');
        $category=array("allcategory"=>"All")+$category;
        return $this->render('index',[
        		"product"=>$product,
        		"stock"=>$stock,
        		"sales_self"=>$sales_self,
        		"sales_eks"=>$sales_eks,
        		"total_qty"=>$total_qty,
        		"category"=>$category,
        		"c"=>$c,
        		"category"=>$category,
        		"total_unallocated"=>$total_unallocated
        	]);
    }

    public function actionInfo($id){
    	$query=(new \yii\db\Query())
    			->select(["i.*","s.*","c.*","sa.code_sales_channel","sa.allocation_qty"])
    			->from(["inventory_item i"])
    			->join("LEFT JOIN","product_size s","s.code_size=i.code_size")
    			->join("LEFT JOIN","product_color c","c.number_product_color=i.number_product_color")
    			->join("LEFT JOIN","stock_allocation sa","i.number_product=sa.number_product AND i.number_product_color=sa.number_product_color AND i.code_size=sa.code_size")
    			->where(["i.number_product"=>$id])
    			->all();
    	$sales_self= (new \yii\db\Query())
    		->select(["*"])
    		->from("sales_channel")
    		->where("is_internal='t'")
    		->all();
    	$sales_eks= (new \yii\db\Query())
    		->select(["name","code_sales_channel"])
    		->from("sales_channel")
    		->where("is_internal='f'")
    		->all();
    	$info=array();		
    	for($i=0;$i<count($query);$i++){
    		$color=$query[$i]["number_product_color"];
    		$code_size=$query[$i]["code_size"];
    		if(!isset($total{$color."-".$code_size})){
    			$total{$color."-".$code_size}=0;
    		}
    		if(!isset($total{$color."-".$code_size})){
    			$total{$color."-".$code_size}=0;
    		}
    		/*if(!isset($unallo{$color."-".$code_size})){
    			$unallo{$color."-".$code_size}=0;
    		}*/
    		$total{$color."-".$code_size}=$total{$color."-".$code_size}+$query[$i]["qty"];
    		if($query[$i]["code_sales_channel"]!=""){
    			$info[$color."-".$code_size][$query[$i]["code_sales_channel"]]=$query[$i]["allocation_qty"];
    		}else{
    			//$unallo{$color."-".$code_size}=$total{$color."-".$code_size}-$total{$color."-".$code_size}+$query[$i]["allocation_qty"];
    		}    
    		$una=(new \yii\db\Query())
    		->select("sum(qty) as total_una")
    		->from("unallocated")
    		->where([
    			'code_size'=>$code_size,
    			'number_product'=>$id,
    			'number_product_color'=>$color
    			])
    		->all();
    		foreach ($una as $u) {
    			$info[$color."-".$code_size]["unallocated"]=($u["total_una"])?$u["total_una"]:0;
    		}
    		$info[$color."-".$code_size]["code_color"]=$color;
    		$info[$color."-".$code_size]["code_size"]=$code_size;
    		$info[$color."-".$code_size]["unique"]=preg_replace('/[0-9]+/', '',md5($code_size."-".$color));
    		$info[$color."-".$code_size]["color"]=$query[$i]["color"];
    		$info[$color."-".$code_size]["size"]=$code_size." (".$query[$i]["name"].")";
    		$info[$color."-".$code_size]["total_qty"]=$total{$color."-".$code_size};
    		//$info[$color."-".$code_size]["unallocated"]=$unallo{$color."-".$code_size};
    	}
    	return json_encode($info);
    }

    public function actionSales(){
    	$query=(new \yii\db\Query())
    			->select(["code_sales_channel","name"])
    			->from(["sales_channel"])
    			->all();
    	return json_encode($query);
    }

    public function actionSave(){
    	$request=Yii::$app->request;
    	$number_product=$request->post("number_product");
    	$number_product_color=$request->post("number_product_color");
    	$number_product_color=array_unique($number_product_color);
    	foreach ($number_product_color as $npc) {
    		$color=$request->post($npc);
    		foreach ($color as $kc => $kv) {
    			$color_code=$npc;
    			$code_size=$kc;
    			foreach ($kv as $kvk => $kvv) {
    				$code_sales_channel=$kvk;
    				$allocation_qty=$kvv;
    				if($allocation_qty!=0){
	    				Yii::$app->db->createCommand()->delete("stock_allocation",[
	    					'code_sales_channel'=>$code_sales_channel,
	    					'number_product'=>$number_product,
	    					'code_size'=>$code_size,
	    					'number_product_color'=>$color_code
	    					])->execute();
	    				Yii::$app->db->createCommand()->insert("stock_allocation",[
	    					'allocation_qty'=>$allocation_qty,
	    					'code_sales_channel'=>$code_sales_channel,
	    					'number_product'=>$number_product,
	    					'code_size'=>$code_size,
	    					'number_product_color'=>$color_code
	    					])->execute();
    				}
    			}
    		}
    	}
    	$unallocated=$request->post("unallocated");
    	foreach ($unallocated as $uk => $uv) {
    		$number_product_color_u=$uk;
    		foreach ($uv as $uvk => $uvv) {
    			$code_size_u=$uvk;
    			$qty_u=$uvv;
    			if($qty_u!=0){
	    			Yii::$app->db->createCommand()->delete("unallocated",[
		    			'number_product'=>$number_product,
		    			'code_size'=>$code_size_u,
		    			'number_product_color'=>$number_product_color_u
		    		])->execute();
		    		Yii::$app->db->createCommand()->insert("unallocated",[
		    			'qty'=>$qty_u,
		    			'number_product'=>$number_product,
		    			'code_size'=>$code_size_u,
		    			'number_product_color'=>$number_product_color_u
		    		])->execute();
	    		}else{
	    			Yii::$app->db->createCommand()->delete("unallocated",[
		    			'number_product'=>$number_product,
		    			'code_size'=>$code_size_u,
		    			'number_product_color'=>$number_product_color_u
		    		])->execute();
	    		}
    		}
    	}
    }

}
