<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use common\yii\db\db;
use common\models\BankAccount;

register("@web/js/delivery-instruction/deliver.js");

?>

<div class="bank-account-form">
    <!-- Order Information -->
    <?php ob_start(); ?>

    <?= $this->render("@common/views/form-group", [
        "label" => "Order No.",
        "content" => "<h5>$model->number_order</h5>"
    ]) ?>
    
    <?= $this->render("@common/views/form-group", [
        "label" => "Customer Name",
        "content" => "<h5>{$model->buyer->name}</h5>"
    ]) ?>
    
    <?= $this->render("@common/views/form-group", [
        "label" => "Address",
        "content" => "<h5>$model->delivery_address</h5>",
    ]) ?>
    
    <?= $this->render("@common/views/form-group", [
        "label" => "City",
        "content" => "<h5>$model->delivery_city</h5>",
    ]) ?>
    
    <?= $this->render("@common/views/form-group", [
        "label" => "Postal Code",
        "content" => "<h5>$model->post_code</h5>",
    ]) ?>
    
    <?= $this->render("@common/views/form-group", [
        "label" => "Sales Channel",
        "content" => "<h5>{$model->salesChannel->name}</h5>",
    ]) ?>

    <!-- box widget -->
    <?= $this->render("@common/views/widget", [
        "title" => "Order Information",
        "attr" => ["class" => $this->request->isAjax ? "col-xs-12" : "col-md-8"],
        "body" => ob_get_clean(),
    ]) ?>

    <div class="clearfix"></div>

    <!-- Delivery Warehouse -->
    <?php $form = ActiveForm::begin(); ?>
    <?php ob_start(); ?>
    <table class="table table-bordered table-responsive table-striped" id="order-item">
        <thead>
            <tr> 
                <th class="text-center" rowspan='2'>Product Name</th> 
                <th class="text-center" rowspan='2'>Size</th> 
                <th class="text-center" rowspan='2'>Color</th> 
                <th class="text-center" rowspan='2'>Qty</th> 
                <th class="text-center" colspan='<?= count($warehouse) ?>'>Available Qty</th> 
            </tr>
            <tr>
            <?php foreach($warehouse as $code => $name) : ?>
                <td>
                    <label for="<?= $code ?>">
                        <input type="radio" name="warehouse" value="<?= $code ?>" id="<?= $code ?>" />
                        <b><?= $name ?></b>
                    </label>
                </td>
            <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
        <?php foreach($order as $item) : ?>
            <tr>
                <td><?= $item["name"] ?></td>
                <td><?= $item["size"] ?></td>
                <td><?= $item["color"] ?></td>
                <td><?= $item["qty"] ?></td>

                <?php foreach($warehouse as $code => $name) : ?>
                    <td><?= isset($item["warehouse"][$code]) ? number_format($item["warehouse"][$code]) : 0 ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <!-- box widget -->
    <?= $this->render("@common/views/widget", [
        "title" => "Delivery Warehouse",
        "body" => ob_get_clean(),
    ]) ?>

    <hr>
    <div class="text-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>