<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="index">
    <?php if (!$this->request->isAjax): ?>
        <div class="page-header">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        
	    <?php if ($this->can("create")): ?>
        <p> <?= Html::a("Create $this->title", ['create'], ['class' => 'btn btn-success']) ?> </p>
        <?php endif; ?>
    <?php endif; ?>

    <?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => $columns,
        'filterModel' => isset($searchModel) ? $searchModel : null,
	]) ?>
</div>
