<?php namespace common\yii\db;

class Query extends \yii\db\Query {
	
	public function debug() {
		prin($this->sql());
	}
	
	public function sql() {
		return $this->createCommand()->rawsql;
	}
}