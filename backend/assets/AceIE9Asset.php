<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Configuration for Ace Admin Extra client script files
 */
class AceIE9Asset extends AssetBundle
{
    public $sourcePath = '@webroot/ace/dist';
    public $css = [
        'css/ace-part2.min.css',
		'css/ace-ie.min.css'
    ];
    public $cssOptions = ['condition' => 'lte IE 9'];
}
