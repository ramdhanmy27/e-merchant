<?= $number_instruction ?>

<table class="table table-responsive table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Rack</th>
            <th>Shelf Level</th>
            <th>Box</th>
            <th>Product</th>
            <th>Size</th>
            <th>Color</th>
            <th>Qty</th>
            <!-- <th>Action</th> -->
        </tr>
    </thead>
    <tbody class="product-details">
        <?php foreach($storing_item as $item): ?>
        <tr class="storing-item">
            <td>
                <?= $item['number_rack'] ?>
            </td>
            <td>
                <?= $item['rack_level'] ?>
            </td>
            <td>
                <?= $item['number_box'] ?>
            </td>
            <td>
                <?= $item['number_product'] ?>
            </td>
            <td>
                <?= $item['code_size'] ?>
            </td>
            <td>
                <?= $item['number_product_color'] ?>
            </td>
            <td>
                <?= $item['qty'] ?>
            </td>

            <!--
            <td>
                <button class="btn btn-sm btn-danger" type="button" ng-if="product.length > 1" ng-click="f.delete(x)">
                    <i class="fa fa-trash"></i>
                </button> 
            </td>
            -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

