<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SalesChannelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sales Channels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-channel-index">
    <?php if(!$this->request->isAjax) { ?>
        <?php if($this->can("create")){ ?>
            <p> <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?> </p>
        <?php } ?>
    <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'code_sales_channel',
            'name',
            'description',
            'is_internal:boolean',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} | {update}',
                "buttons" => [
                    "update" => function($url, $model, $id){
                        return "<a href='".$url."'' modal modal-title='Update Sales Channel'><i class='fa fa-pencil'></i></a>";
                    },
                ]
            ],
        ],
    ]); ?>
</div>
