<?php

namespace common\models;

use Yii;
use common\yii\db\db;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 *
 * @property string $number_product
 * @property string $brand_name
 * @property string $name
 * @property string $description
 * @property string $group_size
 * @property string $basic_price
 * @property boolean $is_deprecated
 * @property string $code_category
 * @property string $code_merchant
 *
 * @property AgreementItem[] $agreementItems
 * @property InventoryItem[] $inventoryItems
 * @property OrderItem[] $orderItems
 * @property Category $category
 * @property Merchant $merchant
 * @property ProductColor[] $productColors
 * @property PurchaseOrderItem[] $purchaseOrderItems
 * @property ReceiveItem[] $receiveItems
 * @property StockAllocation[] $stockAllocations
 * @property Unallocated[] $unallocateds
 */
class Product extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public static function getGroupSize(){
        $sizes = yii\helpers\ArrayHelper::map(ProductSize::find()->asArray()->all(), 'code_size', 'group_size');
        $groupSize = [];

        foreach($sizes as $code_size => $group_size){
            if(!isset($groupSize[$group_size]))
                $groupSize[$group_size] = [];
            $groupSize[$group_size][]= $code_size;
        }

        foreach($groupSize as $group_size => $code_size)
            $groupSize[$group_size] = implode(" | ", $code_size);

        return $groupSize;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['number_product'], 'required'],

            // unique
            [['number_product'], 'unique'],

            // integer
            [['basic_price'], 'number'],
            [['is_deprecated'], 'boolean'],

            // string
            [['group_size'], 'string', 'max' => 3],
            // [['number_batch'], 'string', 'max' => 10],
            [['number_product', 'code_merchant', 'code_category'], 'string', 'max' => 10],
            [['brand_name'], 'string', 'max' => 100],
            [['name'], 'string', 'max' => 100],
            [['description'], 'string'],

            // exist
            [['code_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['code_category' => 'code_category']],
            [['code_merchant'], 'exist', 'skipOnError' => true, 'targetClass' => Merchant::className(), 'targetAttribute' => ['code_merchant' => 'code_merchant']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number_product' => 'Product Number',
            'brand_name' => 'Brand Name',
            'name' => 'Name',
            'description' => 'Description',
            'group_size' => 'Size Set',
            'basic_price' => 'Basic Price',
            'is_deprecated' => 'Is Deprecated',
            'code_category' => 'Category',
            'code_merchant' => 'Merchant',
            'category.name' => 'Category',
            'merchant.name' => 'Merchant',
        ];
    }

    public static function option($label=null, $key="p.number_product", $where=[]) {
        $label = $label==null ? db::raw("p.name||' ('||p.number_product||')'") : $label;
        $query = db::query("product p", [$key, $label])->distinct()
                    ->innerJoin("product_size s", "s.group_size=p.group_size")
                    ->innerJoin("product_color c", "c.number_product=p.number_product");

        if (count($where) > 0)
            call_user_func_array([$query, "where"], $where);

        $query->orderBy($label);
        $data = [];

        foreach ($query->each() as $value) 
            $data[current($value)] = next($value);

        return $data;
    }

    public function getVariation($id = null) {
        return db::run(
            "SELECT c.number_product_color, c.color, s.code_size, s.name from product p
            left join product_size s on s.group_size=p.group_size
            left join product_color c on c.number_product=p.number_product
            where p.number_product=:id order by s.code_size, c.color",
            ["id" => $id===null ? $this->number_product : $id]
        )->query();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementItems()
    {
        return $this->hasMany(AgreementItem::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItems()
    {
        return $this->hasMany(InventoryItem::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['code_category' => 'code_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(Merchant::className(), ['code_merchant' => 'code_merchant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductColors()
    {
        return $this->hasMany(ProductColor::className(), ['number_product' => 'number_product'])
                    ->orderBy(['product_color.color'=>SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderItems()
    {
        return $this->hasMany(PurchaseOrderItem::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiveItems()
    {
        return $this->hasMany(ReceiveItem::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockAllocations()
    {
        return $this->hasMany(StockAllocation::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnallocateds()
    {
        return $this->hasMany(Unallocated::className(), ['number_product' => 'number_product']);
    }
}
