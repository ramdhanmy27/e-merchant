<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use yii\helpers\url;

/* @var $this yii\web\View */
/* @var $model common\models\Warehouse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="merchant-form">

	<?php $form = ActiveForm::begin(); ?>
	
	<div class='row'>
		<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
			<!-- Business Information -->
			<div class='widget-box'>
				<div class="widget-header">
					<h5 class="widget-title">Business Information</h5>

					<div class="widget-toolbar">
						<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
					</div>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						<?= $form->field($model, 'code_merchant')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
						
						<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
					</div>
				</div>
			</div>
            
		</div>
		<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
			<!-- Business Information -->
			<div class='widget-box'>
				<div class="widget-header">
					<h5 class="widget-title">Bank Information</h5>

					<div class="widget-toolbar">
						<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
					</div>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						<?= $form->field($model, 'bank')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'bank_acc_no')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'bank_acc_name')->textInput(['maxlength' => true]) ?>
					</div>
				</div>
			</div>

			<div class='widget-box'>
				<div class="widget-header">
					<h5 class="widget-title">Account Information</h5>

					<div class="widget-toolbar">
						<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
					</div>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
					</div>
				</div>
			</div>
            
		</div>
	</div>
	
	<div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-12" ?> text-right'>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
	<hr>
</div>