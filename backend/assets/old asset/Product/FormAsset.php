<?php namespace backend\assets\Product;

class FormAsset extends \yii\web\AssetBundle {
    public $baseUrl = '@web';
    
    public $js = [
    	"js/product/form.js",
    ];

    public $css = [
        'css/product/variations.css',
    ];

    public $depends = [
        "common\assets\AngularAsset",
        "common\assets\FileInputAsset",
        "common\assets\ColorboxAsset",
        "backend\assets\AceAsset",
    ];
}