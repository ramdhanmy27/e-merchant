<?php namespace common\yii\db;

use Yii;
use common\yii\db\Query;

class db extends \yii\base\Component {
	public static $connection = "db";

	/**
	 * switch database connection
	 * @param string $connection
	 * @return yii\db\Connection
	 */
	public static function connection($connection = "db") {
		return self::$connection = $connection;
	}

	public static function transaction($isolationLevel = null) {
		return Yii::$app->db->beginTransaction($isolationLevel);
	}

	/**
	 * return db raw expressions
	 * @param  string $expr
	 * @return \yii\db\Expression
	 */
	public static function raw($expr) {
		return new \yii\db\Expression($expr);
	}

	/**
	 * execute raw query
	 * @param  string $sql  
	 * @param  array  $bind
	 * @return yii\db\Command
	 *
	 * Fetch Method
	 * execute() 	 : Executes the SQL statement and returns affected rows data.
	 * query() 		 : Executes the SQL statement and returns query result.
	 * queryAll() 	 : Executes the SQL statement and returns ALL rows at once.
	 * queryColumn() : Executes the SQL statement and returns the first column of the result.
	 * queryOne() 	 : Executes the SQL statement and returns the first row of the result.
	 * queryScalar() : Executes the SQL statement and returns the value of the first column in the first row of data.
	 */
	public static function run($sql, $bind = []) {
		return Yii::$app->{self::$connection}->createCommand($sql, $bind);
	}

	/**
	 * query builder function
	 * @param  string 		$table  
	 * @param  string|array $select 
	 * @return yii\db\Query
	 *
	 * Query Method
	 * select()
	 * 	$query->select(['user.id AS user_id', 'email'])
	 * 	$query->select('user.id AS user_id, email')
	 * 	$query->select(['user_id' => 'user.id', 'email']);
	 * from()
	 * 	$query->from(['u' => 'public.user', 'p' => 'public.post']);
	 * 	$query->from(['public.user u', 'public.post p']);
	 * 	$query->from('public.user u, public.post p');
	 * where()
	 * 	$query->where('status=:status', [':status' => $status]);
	 * 	$query->where('YEAR(somedate) = 2015');
	 * 	$query->where('status=:status')->addParams([':status' => $status]);
	 * 	$query->where(['status' => 10, 'type' => null, 'id' => [4, 8, 15]]);
	 * 	$query->andWhere(['like', 'title', $search]);
	 * 
	 * Fetch Method
	 * all()	: returns an array of rows with each row being an associative array of name-value pairs.
	 * batch()	: To keep the memory requirement low, Yii provides the so-called batch query support. 
	 * 			  A batch query makes use of the data cursor and fetches data in batches.
	 * one()	: returns the first row of the result.
	 * each()	: if you want to iterate the row one by one
	 * column()	: returns the first column of the result.
	 * scalar()	: returns a scalar value located at the first row and first column of the result.
	 * exists()	: returns a value indicating whether the query contains any result.
	 * count()	: returns the result of a COUNT query.
	 */
	public static function query($table, $select = "*") {
		return (new Query())->from($table)->select($select);
	}

	/**
	 * execute yii\db\Command method
	 * @return
	 *
	 * Non-select query
	 * insert('user', ['name' => 'Sam', 'age' => 30])
	 * batchInsert('user', ['name', 'age'], [['Tom', 30], ['Jane', 20], ['Linda', 25]])
	 * update('user', ['status' => 1], 'age > :age', ['age'=>20])
	 * delete('user', 'status = :id', ['id'=>0])
	 *
	 * Fetch Method
	 * queryAll() : return a set of rows. each row is an associative array of column names and values
	 * queryOne() : return a single row (the first row)
	 * queryColumn() : return a single column (the first column)
	 * queryScalar() : return a scalar value
	 */
	public static function __callStatic($method, $args) {
		return call_user_func_array([Yii::$app->{self::$connection}->createCommand(), $method], $args);
	}

	public static function getLastInsertID() {
		return Yii::$app->{self::$connection}->getLastInsertID();
	}

	/**
	 * batch update
	 * db::batchUpdate(
     * 		"table p", ["field1=t.field1"]
     * 		[["field1", "int"], "field2"], [[1, 2]], 
     * 		"where (p.field1=:val or t.field2=p.field2)", 
     * 		["val" => $val]
     * )
	 * @param  string 			$table
	 * @param  string|array 	$set
	 * @param  array 			$field
	 * @param  array 			$data
	 * @param  string 			$condition
	 * @param  array 			$bind
	 */	
	public static function batchUpdate($table, $set, array $field, array $data, $condition=null, $bind=[]) {
		if (count($data) == 0)
			return false;
		
		$data = array_values($data);
		$field = array_values($field);
		
		if (!is_array($data[0]))
			$data = [$data];

		$column = [];
		$values = [];
		$type = [];

		// row
		foreach ($data as $x => $row) {
			$tmpVal = [];

			// column
			foreach (array_values($row) as $y => $val) {
				$is_cast = isset($field[$y]) && is_array($field[$y]);
				
				if ($x == 0) {
					if ($is_cast) {
						$column[$y] = current($field[$y]);
						$type[$y] = next($field[$y]);
					}
					else $column[$y] = $field[$y];
				}

				// casting data type
				if ($is_cast && $type[$y]!==false) {
					switch (strtolower($type[$y])) {
						case 'int':
							$val = is_numeric($val) ? intval($val) : null;
							break;

						case 'bool':
							$val = $val==null || trim($val)=="" ? null : (bool) $val;
							break;
					}
				}

				$tmpVal[] = ":t".$x.$y;
				$bind[":t".$x.$y] = $val;
			}

			$values[] = "(".implode(",", $tmpVal).")";
		}

		return db::run(
			"UPDATE {$table} set ".(is_array($set) ? implode(",", $set) : $set)
			." from (values ".implode(",", $values).") as t([[".implode(']],[[', $column)."]]) $condition", $bind
		);
	}
}