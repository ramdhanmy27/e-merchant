<?php namespace backend\assets\User;

class PermissionsAsset extends \yii\web\AssetBundle {
    public $baseUrl = '@web';
    
    public $js = [
        'js/user/permissions.js',
    ];

    public $depends = [
        "backend\assets\AceAsset",
    ];
}