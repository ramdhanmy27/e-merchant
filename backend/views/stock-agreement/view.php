<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->number_contract;

?>

<div class="stock-agreement-view">
    <p>
        <?php if ($model->isDraft() && access("draft")) : ?>
            <?= Html::a('Update', ['draft', 'id' => $model->number_contract], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        
        <?php if (access("delete")) : ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->number_contract], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number_contract',
            'merchant.name',
            'contract_date:date',
            'until_date:date',
            'description',
        ],
    ]) ?>
</div>