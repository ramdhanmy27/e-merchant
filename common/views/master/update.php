<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\modelName */

?>

<div class="update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render([
    		"@app/views/".$this->controller->id."/_form",
    		"_form",
    	]
    	, ['model' => $model]
    ) ?>
</div>