<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['id', 'parent', 'group', 'measurement', 'regular_price', 'sale_price', 'sale_disc', 'employee_price', 'employee_disc', 'wholesale_price', 'wholesale_disc', 'custom_price', 'custom_disc', 'cost', 'avg_cost', 'order_cost', 'msrp', 'margin', 'markup'], 'integer'],
            [['name', 'description', 'size', 'sku', 'attribute', 'comment', 'upc', 'alu', 'reorder_point', 'manufacturer', 'shipping_weight', 'shipping_height', 'shipping_length', 'shipping_width'], 'safe'],
            [['print_tags', 'unorderable', 'use_serial', 'earn_commission', 'earn_rewards'], 'boolean'],
            [["idMerchant.shop_name", "idCategory.name", "created_at", "updated_at"], "safe"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->joinWith(["idCategory" => function($query) {$query->from(["category" => "category"]); }]);

        $dataProvider->sort->attributes["idCategory.name"] = [
            "asc" => ["category.name" => SORT_ASC],
            "desc" => ["category.name" => SORT_DESC]
        ];
        
        $query->joinWith(["idMerchant" => function($query) {$query->from(["merchant" => "merchant"]); }]);        
        
        $dataProvider->sort->attributes["idMerchant.shop_name"] = [
            "asc" => ["merchant.shop_name" => SORT_ASC],
            "desc" => ["merchant.shop_name" => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'parent' => $this->parent,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
            'group' => $this->group,
            'sku' => $this->sku,
            'measurement' => $this->measurement,
            'print_tags' => $this->print_tags,
            'unorderable' => $this->unorderable,
            'use_serial' => $this->use_serial,
            'earn_commission' => $this->earn_commission,
            'earn_rewards' => $this->earn_rewards,
            'regular_price' => $this->regular_price,
            'sale_price' => $this->sale_price,
            'sale_disc' => $this->sale_disc,
            'employee_price' => $this->employee_price,
            'employee_disc' => $this->employee_disc,
            'wholesale_price' => $this->wholesale_price,
            'wholesale_disc' => $this->wholesale_disc,
            'custom_price' => $this->custom_price,
            'custom_disc' => $this->custom_disc,
            'cost' => $this->cost,
            'avg_cost' => $this->avg_cost,
            'order_cost' => $this->order_cost,
            'msrp' => $this->msrp,
            'margin' => $this->margin,
            'markup' => $this->markup,
        ]);
        
        if($this->created_at){
            $dateformatbaru = explode("-", $this->created_at);
            $y = $dateformatbaru[2];
            $m = $dateformatbaru[1];
            $d = $dateformatbaru[0];

            $originalDate = $y."-".$m."-".$d;
            $newDate = date("D, d M Y H:i:s O", strtotime($originalDate));
            $query->andFilterWhere(['>=', 'product.created_at', Yii::$app->formatter->asTimeStamp($newDate)]);
        }

        
        if($this->updated_at){
            $dateformatbaru = explode("-", $this->updated_at);
            $y = $dateformatbaru[2];
            $m = $dateformatbaru[1];
            $d = $dateformatbaru[0];

            $originalDate = $y."-".$m."-".$d;
            $newDate = date("D, d M Y H:i:s O", strtotime($originalDate));
            $query->andFilterWhere(['>=', 'product.updated_at', Yii::$app->formatter->asTimeStamp($newDate)]);
        }

        $query->andFilterWhere(['ilike', 'product.name', $this->name])
            ->andFilterWhere(['ilike', 'product.description', $this->description])
            ->andFilterWhere(['ilike', 'product.size', $this->size])
            ->andFilterWhere(['ilike', 'product.attribute', $this->attribute])
            ->andFilterWhere(['ilike', 'product.comment', $this->comment])
            ->andFilterWhere(['ilike', 'product.upc', $this->upc])
            ->andFilterWhere(['ilike', 'product.alu', $this->alu])
            ->andFilterWhere(['ilike', 'product.reorder_point', $this->reorder_point])
            ->andFilterWhere(['ilike', 'product.manufacturer', $this->manufacturer])
            ->andFilterWhere(['ilike', 'product.shipping_weight', $this->shipping_weight])
            ->andFilterWhere(['ilike', 'product.shipping_height', $this->shipping_height])
            ->andFilterWhere(['ilike', 'product.shipping_length', $this->shipping_length])
            ->andFilterWhere(['ilike', 'category.name', $this->getAttribute("idCategory.name")])
            ->andFilterWhere(['ilike', 'merchant.shop_name', $this->getAttribute("idMerchant.shop_name")])
            ->andFilterWhere(['ilike', 'product.shipping_width', $this->shipping_width]);

        return $dataProvider;
    }

    public function attributes(){
        return array_merge(parent::attributes(), ["idCategory.name", "idMerchant.shop_name"]);
    }
}
