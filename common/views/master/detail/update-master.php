<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\modelName */

?>

<?= $this->render([
		"@app/views/".$this->controller->id."/update",
		'../update',
	],
    ['model' => $model]
) ?>

<hr>

<div class="row">
	<div class="col-xs-6"> 
		<h2><?= ucwords($detail_model::name()) ?></h2> 
	</div>

	<div class='col-xs-6'>
		<div class="pull-right">
			<?= Html::a("Tambah", ["create-detail", "parent"=>$parent], [
				"class" => "btn btn-warning",
				"modal" => "",
			]); ?>
		</div>
	</div>
</div>

<div class="index">
	<?= $child_list ?>
</div>