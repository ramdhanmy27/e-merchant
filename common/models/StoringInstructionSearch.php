<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoringInstruction;

/**
 * StoringInstructionSearch represents the model behind the search form about `common\models\StoringInstruction`.
 */
class StoringInstructionSearch extends StoringInstruction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number_instruction', 'storing_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoringInstruction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'storing_time' => $this->storing_time,
        ]);

        $query->andFilterWhere(['ilike', 'number_instruction', $this->number_instruction]);

        return $dataProvider;
    }
}
