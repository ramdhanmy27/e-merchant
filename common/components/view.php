<?php

/*Initialize Alias*/
Yii::setAlias("js", "@web/js");
Yii::setAlias("css", "@web/css");


/**
 * Menampilkan gridview beserta menu exportnya
 * @param  array $columns      
 * @param  yii\data\ActiveDataProvider $dataProvider 
 * @param  array  $grid       config untuk grid	
 * @param  array  $export     config untuk export menu
 * @return
 */
function gridview($columns, $dataProvider, $grid=[], $export=[]) {
	return (
		$export !== null 
		? kartik\export\ExportMenu::widget(array_merge([
				'dataProvider' => $dataProvider,
				'columns' => $columns,
				'fontAwesome' => true,
				'dropdownOptions' => [
					'label' => 'Export All',
					'class' => 'btn btn-default'
				],
				'target' => kartik\export\ExportMenu::TARGET_SELF,
			], $export))."<br><br>"
		: null
	)
	.kartik\grid\GridView::widget(array_merge([
		'dataProvider' => $dataProvider,
		'columns' => $columns,
	], $grid));
}

/**
 * register assets file
 * @param string $url the JS file to be registered.
 * @param array $options the HTML attributes for the script tag. The following options are specially handled
 * and are not treated as HTML attributes:
 *
 * - `depends`: array, specifies the names of the asset bundles that this JS file depends on.
 * - `position`: specifies where the JS script tag should be inserted in a page. The possible values are:
 *     * [[POS_HEAD]]: in the head section
 *     * [[POS_BEGIN]]: at the beginning of the body section
 *     * [[POS_END]]: at the end of the body section. This is the default value.
 *
 * Please refer to [[Html::jsFile()]] for other supported options.
 * 
 * @param string $key the key that identifies the JS script file. If null, it will use
 * $url as the key. If two JS files are registered with the same key, the latter
 * will overwrite the former.
 */
function register($files, $options=["depends" => ["backend\assets\AceAsset"]], $key=null) {
	if (!is_array($files))
		$files = [$files];

	foreach ($files as $file) {
		if (!file_exists($_SERVER["DOCUMENT_ROOT"].Yii::getAlias($file)))
			continue;

		switch (substr($file, strrpos($file, ".")+1)) {
			case 'js':
				Yii::$app->view->registerJsFile($file, $options, $key);
				break;
			
			case "css":
				Yii::$app->view->registerCssFile($file, $options, $key);
				break;
		}
	}
}

/**
 * convert value into currency format
 * @param  numeric $value
 * @return string       
 */
function currency($value, $decimals=0, $currency="Rp") {
	return "$currency ".number_format($value, $decimals);
}

function url($url = '', $scheme = false) {
	return \yii\helpers\Url::to($url, $scheme);
}

function access($permission) {
	return Yii::$app->controller->can($permission);
}