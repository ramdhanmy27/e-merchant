<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AceAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use common\widgets\Alert;
use common\models\Menu;

AceAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app='main-app'>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->name ?> <?= isset($this->title) ? '|' : null ?> <?= Html::encode($this->title) ?></title>
    <link rel='shortcut icon' href='<?= Url::to("images/favicon.ico") ?>' type="image/x-icon" />
	
	<!-- page specific plugin styles -->
	<!-- inline styles related to this page -->
    <?php $this->head() ?>
</head>
<body class="no-skin">
<?php $this->beginBody() ?>

<div id="navbar" class="navbar navbar-default">
	<script type="text/javascript">
		try{ace.settings.check('navbar' , 'fixed')}catch(e){}
	</script>

	<div class="navbar-container" id="navbar-container">
		<!-- Toggle Sidebar -->
		<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
			<span class="sr-only">Toggle sidebar</span>

			<span class="icon-bar"></span>

			<span class="icon-bar"></span>

			<span class="icon-bar"></span>
		</button>

		<!-- Logo -->
		<div class="navbar-header pull-left">
			<a href="#" class="navbar-brand">
				<small>
					<i class="fa fa-leaf"></i>
					<?= Yii::$app->name; ?>
				</small>
			</a>
		</div>

		<!-- Top Navbar -->
		<div class="navbar-buttons navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav">

			<?php
				if (!Yii::$app->user->isGuest) { ?>

				<li class="light-blue">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<!--<img class="nav-user-photo" src="<?=Yii::getAlias('@web')?>/ace/dist/avatars/user.jpg" alt="Jason's Photo" />-->
						<span class="user-info">
							
						</span>
						Welcome <?= Yii::$app->user->identity->username?>

						<i class="ace-icon fa fa-caret-down"></i>
					</a>

					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li>
							<a href='<?= Yii::$app->urlManager->createUrl("user/view-profile") ?>'>
								<i class="ace-icon fa fa-user"></i>
								Profile
							</a>
						</li>
						<li>
							<a href='<?= Yii::$app->urlManager->createUrl("user/edit-profile") ?>' modal>
								<i class="ace-icon fa fa-cog"></i>
								Edit Profile
							</a>
						</li>

						<li>
							<a href='<?= Yii::$app->urlManager->createUrl("user/change-password") ?>' modal>
								<i class="ace-icon fa fa-edit"></i>
								Change Password
							</a>
						</li>

						<li class="divider"></li>

						<li>
							<a href='<?= Yii::$app->urlManager->createUrl("site/logout") ?>' data-method="post">
								<i class="ace-icon fa fa-power-off"></i>
								Logout
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div><!-- /.navbar-container -->
</div>

<div class="main-container" id="main-container">
	<script type="text/javascript">
		try{ace.settings.check('main-container' , 'fixed')}catch(e){}
	</script>
<?php
	if (!Yii::$app->user->isGuest) { ?>
	<!-- Sidebar -->
	<div id="sidebar" class="sidebar responsive">
		<script type="text/javascript">
			try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
		</script>

		<!-- Sidebar Icon -->
		<div class="sidebar-shortcuts" id="sidebar-shortcuts">
			<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
				<button class="btn btn-success">
					<i class="ace-icon fa fa-signal"></i>
				</button>

				<button class="btn btn-info">
					<i class="ace-icon fa fa-pencil"></i>
				</button>

				<button class="btn btn-warning">
					<i class="ace-icon fa fa-users"></i>
				</button>

				<button class="btn btn-danger">
					<i class="ace-icon fa fa-cogs"></i>
				</button>
			</div>

			<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
				<span class="btn btn-success"></span>

				<span class="btn btn-info"></span>

				<span class="btn btn-warning"></span>

				<span class="btn btn-danger"></span>
			</div>
		</div><!-- /.sidebar-shortcuts -->
		
		<!-- Sidebar Menu -->
		
		<?=\backend\widgets\Menu::widget([
			'options' => ['class' => 'nav nav-list'],
			"items" => Menu::nav(),
		]); ?>
		
		<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
			<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
		</div>

		<script type="text/javascript">
			try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
		</script>
	</div>
<?php } ?>
	
	<!-- Main Content -->
	<div class="main-content">
		<!-- Main Content -->
		<div class="main-content-inner">
			<!-- Breadcrumbs -->
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
				</script>

				<?= Breadcrumbs::widget(
					[
						'homeLink'=> [
							'label' => Yii::t('app', 'Home'),
							'url' => ['/'],
							'template' => '<li><i class="ace-icon fa fa-home home-icon"></i> {link}</li>'
						],
						'encodeLabels' => false,
						'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					]
				) . "\n" ?>
				
			</div>
			
			<!-- Page Content -->
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<?php if (!$this->request->isAjax) : ?>
						    <div class="page-header">
						        <h1><?= Html::encode($this->title) ?></h1>
						    </div>
						<?php endif; ?>

						<?= Alert::widget() ?>
						<?= $content ?>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.main-content -->
	
	<!-- Footer -->
	<div class="footer">
		<div class="footer-inner">
			<div class="footer-content">
				<span class="description text-muted">
					Saqina &copy; <?= date('Y') ?>
				</span>

				&nbsp; &nbsp;
				<span class="action-buttons">
					<a href="#">
						<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
					</a>

					<a href="#">
						<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
					</a>

					<a href="#">
						<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
					</a>
				</span>
			</div>
		</div>
	</div>
	
	<!-- Go Up Button -->
	<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
		<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
	</a>
</div><!-- /.main-container -->

<!-- modal frame -->
<!-- action confirm -->
<?= $this->render("@common/views/modal", [
    "attr" => ["id" => "winconfirm"],
    "title" => "Warning!",
    "footer" => 
    	'<button type="button" data-dismiss="modal" class="btn btn-default modal-close">No</button>
        <button type="button" data-dismiss="modal" class="btn btn-primary modal-accept">Yes</button>',
]) ?>

<!-- general -->
<?= $this->render("@common/views/modal", [
    "attr" => ["id" => "modal-basic"],
]) ?>

<!-- error -->
<?= $this->render("@common/views/modal", [
    "attr" => "id='error-alert'",
    "title" => "Ups.. Something Wrong!",
    "body" => "Do you want to reload page ?",
    "footer" => "<button type='button' data-dismiss='modal' class='btn btn-default'>No</button>
                <a href='$_SERVER[REQUEST_URI]' class='btn btn-danger'>Yes</a>"
]) ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
