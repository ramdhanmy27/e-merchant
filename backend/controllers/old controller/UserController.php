<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use common\models\Merchant;
use common\yii\web\Controller;
use common\yii\db\db;
use backend\controllers\MenuController;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public static $permissions = [
        "create", "update", "view", ["stat","Change Status User"], "permissions", "view-profile", "edit-profile", "change-password"
    ];

    public function behaviors()
    {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
            ]),
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['index', 'view', 'create'], "create"],
                [['index', 'view', 'update'], "update"],
                [['index', 'stat'], "stat"],
                [['permissions'], "permissions"],
                [['view-profile'], "view-profile"],
                [['edit-profile'], "edit-profile"],
                [['change-password'], "change-password"],
            ]),
        ];
    }

    /*Actions*/

    /**
     * role & permission configuration page
     * @param  string|array $role
     * @return
     */
    public function actionPermissions($role = null) {
        $post = $this->request->post();

        // init all permissions
        \common\models\Permission::initAllController([
            "backend\controllers" => "@backend/controllers",
        ]);

        /*Form submission process*/
        if (count($post) > 0) {
            // recreate role-permissions
            db::delete("auth_item_child")->query();
            $data = [];

            foreach ($post['perm'] as $perm => $roles) {
                foreach ($roles as $role)
                    $data[] = [$role, $perm];
            }

            db::batchInsert("auth_item_child", ["parent", "child"], $data)->query();
            return $this->redirect(["permissions"]);
        }

        /*Render view*/
        $roles = db::query("auth_item")->select("name, description")->orderBy("name");

        if ($role!=null)
            $roles->where(["in", "name", is_array($role) ? [$role] : $role]);

        # permissions
        $perm = clone $roles;
        $perm->where("type=2");
        $permissions = [];
        $other = [];

        foreach ($perm->each() as $val) {
            // getting controller name
            preg_match_all("/^([\w\-\\\\]+)\:\w+/", $val['name'], $match);

            if (isset($match[1][0]))
                $permissions[$match[1][0]][] = $val;
            else
                $other[] = $val;
        }

        // put "other" permission category to the last
        if (count($other) > 0)
            $permissions['other'] = $other;

        # role permission data
        $query = db::query("auth_item_child");
        $data = [];

        foreach ($query->each() as $val)
            $data[$val["child"]][] = $val['parent'];

        return $this->render("permissions", [
            "roles" => $roles->andwhere("type=1"),
            "permissions" => $permissions,
            "data" => $data,
        ]);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //prin($dataProvider, Yii::$app->request->queryParams);
        $arrayStatus = User::getArrayStatus();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'arrayStatus' => $arrayStatus,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User(['scenario' => 'admin-create']);
        
        $roles = Yii::$app->authManager->getRoles();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->save();

            //dapetin array permissions
            $assignments = Yii::$app->request->post('assignments',[]);
            foreach($assignments as $assignment){
                Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model->id);
            }

            //$model->sendEmailConfirmation();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            return $this->render('create', [
                'model' => $model,
                'roles' => $roles,
            ]);
        }
    }

    public function actionEmail(){
        $email = \Yii::$app->mail->compose()
            ->setTo("pradana.fandy@gmail.com")
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
            ->setSubject('Signup Confirmation')
            ->setHtmlBody('<a href="/yii-application/backend/web/index.php?r=user%2Fconfirm&amp;token=DY05F_D0sPL8c-YPr_9o76fEOBdeTnt86XR6q2qm">confirm</a>')
            ->send();
            
        if($email){
            Yii::$app->getSession()->setFlash('success','Check Your email!');
            return $this->redirect(['index']);
        }
        else{
            Yii::$app->getSession()->setFlash('warning','Failed, contact Admin!');
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('admin-update');
        $roles = Yii::$app->authManager->getRoles();
        $assignments = Yii::$app->authManager->getRolesByUser($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->save();
            Yii::$app->authManager->revokeAll($id);

            $assignments = Yii::$app->request->post('assignments',[]);
            foreach($assignments as $assignment){
                Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model->id);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'roles' => $roles,
                'assignments' => $assignments
            ]);
        }
    }

    public function actionViewProfile()
    {
        $auth = Yii::$app->authManager;
        if(isset($auth->getRolesByUser(Yii::$app->user->identity->id)['merchant'])){
            $modelMerchant = Merchant::findOne([
                'id_user' => Yii::$app->user->identity->id
            ]);
        } else{
            $modelMerchant = null;
        }
        return $this->render('view-profile', [
            'model' => $this->findModel(Yii::$app->user->identity->id),
            'modelMerchant' => $modelMerchant,
        ]);
    }

    public function actionEditProfile()
    {

        $model = $this->findModel(Yii::$app->user->identity->id);
        $model->setScenario('admin-update');

        $auth = Yii::$app->authManager;
        if(isset($auth->getRolesByUser(Yii::$app->user->identity->id)['merchant'])){

            $modelMerchant = Merchant::findOne([
                'id_user' => Yii::$app->user->identity->id
            ]);
        } else{
            $modelMerchant = null;
        }
        
        if ($model->load(Yii::$app->request->post()) &&
            $model->save()) {

            if(isset($auth->getRolesByUser(Yii::$app->user->identity->id)['merchant'])){
                $modelMerchant->load(Yii::$app->request->post());
                $modelMerchant->save();
            }

            return $this->redirect(['view-profile']);
        } else {
            return $this->render('edit-profile', [
                'model' => $model,
                'modelMerchant' => $modelMerchant,
            ]);
        }
    }

    public function actionChangePassword()
    {
        $model = $this->findModel(Yii::$app->user->identity->id);
        $model->setScenario('change-password');
        
        if ($model->load(Yii::$app->request->post()) &&
            $model->save()) {
            
            return $this->redirect(['view-profile']);
        } else {
            return $this->render('change-password', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
		$model->status = $model::STATUS_INACTIVE;

        if ($model->save()) {
            return $this->redirect(['index']);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUserstat(){
        $post       = $this->request->post();

        $status     = $post['status'];
        $iduser     = $post['iduser'];

        db::update("user",["status"=>$status], "id='$iduser'")->execute();

        return "success";
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //dapetin available list roles
    protected function getRoles() {
        $models = Auth::find()->where(['type' => Auth::TYPE_ROLE])->all();
        $roles = [];
        foreach($models as $model) {
            $roles[$model->name] = $model->description;
        }
        return $roles;
    }

    //dapetin list roles dari form user
    protected function prepareRoles($post) {
        return (isset($post['User']['_roles']) &&
            is_array($post['User']['_roles'])) ? $post['User']['_roles'] : [];
    }

    public function actionConfirm($token)
    {
        $user = User::findIdentityByAccessToken($token);

        if($user){
            if($user->status == 0){
                $user->status=1;
                $user->save();
                Yii::$app->getSession()->setFlash('success','Success!');
            }
            else if ($user->status == 1){
                Yii::$app->getSession()->setFlash('success','Your account is already activated');
            }
        }
        else{
            Yii::$app->getSession()->setFlash('warning','Failed!');
        }
        return $this->goHome();
    }

}
