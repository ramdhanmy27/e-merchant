<?php

use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use common\models\Product;
use common\models\SalesOrder;
use common\models\BankAccount;
use common\models\LogisticService;
use common\yii\db\db;
use kartik\select2\Select2;

$this->title = 'Sales Order';

register(["@js/sales-order/form.js"], [
    "depends" => [
        "common\assets\AngularAsset",
        "backend\assets\AceAsset",
    ]
]);
?>

<style>
    .min-width { min-width: 100px }
</style>

<div class="sales-order-draft" ng-controller="salesOrderController">
    <div class="sales-order-form">
        <?php $form = ActiveForm::begin(["options" => ["name" => "form"]]); ?>

        <!-- Contract Information -->
        <?php ob_start(); ?>
        <?= $form->field($model, "number_order")->textInput(['maxlength' => true]) ?>

        <?php $img = Html::img("@web/images/loading-2.gif") ?>
        <?= $this->render("@common/views/form-group", [
            "label" => "Customer Phone",
            "content" => <<<HTML
                <div class="input-icon input-icon-right">
                    <input type="text" name="phone" class="form-control" ng-model="phone" 
                        ng-model-options="{debounce:1000}" ng-change="f.findBuyer(phone)" required />
                    <span class="ace-icon" ng-if="loading"> {$img} </span>
                </div>
HTML
        ]) ?>

        <?= $this->render("@common/views/form-group", [
            "label" => "Customer Name",
            "content" => Html::input("text", "name", null, [
                "class" => "form-control",
                "ng-model" => "name",
                "ng-disabled" => "f.isBuyerExists()",
                "required" => "",
            ]),
        ]) ?>

        <?= $form->field($model, "delivery_city", ["inputOptions" => [
            "ng-model" => "delivery_city",
            "class" => "form-control",
        ]])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, "delivery_address", ["inputOptions" => [
            "ng-model" => "delivery_address",
            "class" => "form-control",
        ]])->textarea(['maxlength' => true]) ?>

        <?= $form->field($model, "post_code", ["inputOptions" => [
            "ng-model" => "post_code",
            "class" => "form-control",
        ]])->textInput(['maxlength' => true]) ?>
        
        <?= $this->render("@common/views/widget", [
            "attr" => ["class" => "col-md-8"],
            "title" => "Order Information",
            "body" => ob_get_clean(),
        ]) ?>

        <div class="clearfix"></div>

        <!-- Product Details -->
        <?php ob_start(); ?>
        <table id="order" class="table table-bordered table-striped table-responsive table-hover">
            <thead>
                <tr> 
                    <th class="text-center">Product</th> 
                    <th class="text-center">Variations</th> 
                    <th class="text-center">Available Qty</th> 
                    <th class="text-center">Qty</th> 
                    <th class="text-center">Harga</th> 
                    <th class="text-center">Total</th> 
                    <th></th> 
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="item in order track by $index">
                    <td> <?= Select2::widget([
                        "name" => 'order[{{$index}}][product]',
                        "data" => Product::option(),
                        "options" => ["placeholder" => "Choose Product"]
                    ]) ?> </td>
                    <td class="min-width"> 
                        <select name="order[{{$index}}][variation]" ng-disabled="!f.isProductSelected($index)" ng-model="order[$index].variation" 
                            ng-change="f.getAvailableQty($index)" class="form-control">
                            <option ng-repeat="(key, val) in item.variations track by key" value="{{key}}">{{val}}</option>
                        </select>
                    </td>
                    <td> <h5 align='center'>{{item.availableQty}}</h5> </td>
                    <td class="min-width"> 
                        <?= Html::input("number", 'order[{{$index}}][qty]', null, [
                            "class" => "form-control",
                            "min" => 1, 
                            "max" => "{{item.availableQty}}", 
                            "ng-model" => 'order[$index].qty',
                            "ng-disabled" => '!(order[$index].availableQty > 0)',
                        ]) ?> 
                        <div class="help-block"></div>
                    </td>
                    <td class="nowrap"> 
                        <h5>{{item.price | currency:"Rp ":0}}</h5> 
                        <input type="hidden" ng-value="item.price" name="order[{{$index}}][price]" />
                    </td>
                    <td class="nowrap"> <h5>{{(item.price * item.qty) | currency:"Rp ":0}}</h5> </td>
                    <td> 
                        <button type="button" class="btn btn-danger btn-sm" ng-click="f.delete($index)" ng-if="order.length > 1">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button> 
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <button class="btn btn-primary" type="button" ng-click="f.add()">
                            <i class="glyphicon glyphicon-plus"></i> Add Product
                        </button>

                        <h5 class="pull-right"><b>Total : </b></h5>
                    </td>
                    <td> <h5 align="center">{{f.totalQty()}}</h5> </td>
                    <td></td>
                    <td colspan="2"> <h5>{{f.totalPrice() | currency:"Rp ":0}}</h5> </td>
                </tr>
            </tbody>
        </table>

        <?= $this->render("@common/views/widget", [
            "title" => "Ordered Product",
            "body" => ob_get_clean()
        ]) ?>

        <div class="clearfix"></div>

        <!-- Bill -->
        <?php ob_start() ?>

        <?= $form->field($model, "code_service")->option(LogisticService::option("code_service", "name")) ?>

        <?= $form->field($model, "shipping_cost", ["inputOptions" => [
            "ng-model" => "shipping_cost",
            "class" => "form-control",
        ]])->textInput() ?>

        <?= $this->render("@common/views/form-group", [
            "label" => "Total Bill",
            "content" => "<h5>{{(f.totalPrice() + (shipping_cost-0)) | currency:'Rp ':0}}</h5>"
        ]) ?>

        <?= $form->field($model, "code_bank")->option(BankAccount::option("account_name", "code_bank")) ?>
        <?= $form->field($model, "payment_deadline")->option(SalesOrder::$deadline_opt) ?>

        <?= $this->render("@common/views/widget", [
            "attr" => ["class" => "col-md-8"],
            "title" => "Bill",
            "body" => ob_get_clean()
        ]) ?>

        <div class="clearfix"></div> <hr>

        <div class="text-right">
            <?= Html::submitButton(
                "<i class='glyphicon glyphicon-ok'></i> Save & Create Bill", ['class' => 'btn btn-success']
            ) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>