<?php
/**
 * modal frame bootstrap
 * @param  string|array $attr attribute modal
 * @param  string $title
 * @param  string $body
 * @param  string $footer
 */

$attribute = null;

if (isset($attr)) {
    if (is_array($attr)) {
        foreach ($attr as $key => $val)
            $attribute .= " $key='$val'";
    }
    else $attribute = $attr;
}

?>

<div class="modal fade" <?= $attribute ?>>
    <div class="modal-dialog <?= isset($class) ? $class : null ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type='button' aria-label="Close" data-dismiss="modal" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><?= isset($title) ? $title : null ?></h4>
            </div>
            
            <div class="modal-body">
                <?= isset($body) ? $body : null ?>
            </div>

            <div class="modal-footer">
                <?= isset($footer) ? $footer : null ?>
            </div>
        </div>
    </div>
</div>