<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logistic_service".
 *
 * @property string $code_service
 * @property string $name
 * @property string $description
 * @property string $code_tpl
 *
 * @property DeliveryOrder[] $deliveryOrders
 * @property SalesOrder[] $salesOrders
 * @property LogisticPartner $logisticPartner
 */
class LogisticService extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistic_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['code_service'], 'required'],

            // unique
            [['code_service'], 'unique'],

            // safe

            // integer

            // string
            [['code_service', 'code_tpl'], 'string', 'max' => 4],
            [['name'], 'string', 'max' => 16],
            [['description'], 'string', 'max' => 32],

            // exist
            [['code_tpl'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticPartner::className(), 'targetAttribute' => ['code_tpl' => 'code_tpl']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_service' => 'Service Code',
            'name' => 'Service Name',
            'description' => 'Description',
            'code_tpl' => 'Id Logistic Partner',
            'logisticPartner.name' => 'Logistic Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryOrders()
    {
        return $this->hasMany(DeliveryOrder::className(), ['code_service' => 'code_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrders()
    {
        return $this->hasMany(SalesOrder::className(), ['code_service' => 'code_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticPartner()
    {
        return $this->hasOne(LogisticPartner::className(), ['code_tpl' => 'code_tpl']);
    }
}
