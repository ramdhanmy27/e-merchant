<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Stock Agreements';
$model = $draft["filterModel"];

$list["columns"] = $draft["columns"] = [
	['class' => 'yii\grid\SerialColumn'],

	'number_contract',
	[
		"attribute" => 'contract_date',
		"format" => 'datetime',
		// "filter" => Html::activeTextInput($model, "contract_date"),
		"filter" => Html::activeTextInput($model, "contract_date", ["class" => "input-date"]),
	],
	[
		"attribute" => 'until_date',
		"format" => 'datetime',
		"filter" => Html::activeTextInput($model, "until_date", ["class" => "input-date"]),
	],
	'description',

	[
		"class" => "common\yii\grid\ActionColumn",
		"template" => "{view}",
	],
];

if (access("draft"))
	$draft["columns"][count($list["columns"])-1] = [
		"class" => "common\yii\grid\ActionColumn",
		"template" => "{view} {draft} {delete}",
		"buttons" => [
			"draft" => function($url, $model, $id) {
				return Html::a(
					"<i class='glyphicon glyphicon-pencil' title='Update'></i>", ["draft", "id" => $id]
				);
			}
		]
	];
?>

<div class="stock-agreement-index">
	<?php if (access(["create", "draft"])) : ?>
		<p> <?= Html::a('Create Contract Commitment', ['create'], ['class' => 'btn btn-success']) ?> </p>
	<?php endif; ?>

	<div class="widget-box transparent">
		<div class="widget-header" style="border-bottom: none">
			<ul class="nav nav-tabs">
				<li class="active"> <a data-toggle="tab" href="#list">Contract Commitment</a> </li>

				<?php if (access("draft")): ?>
					<li> <a data-toggle="tab" href="#draft">
						Draft <?= ($count=$draft["dataProvider"]->getTotalCount()) > 0 ? "($count)" : "" ?>
					</a> </li>
				<?php endif; ?>
			</ul>
		</div>

		<div class="no-border widget-main tab-content">
			<div id="list" class="tab-pane active forscrollhor">
				<?= GridView::widget($list); ?>
			</div>

			<?php if (access("draft")): ?>
				<div id="draft" class="tab-pane forscrollhor">
					<?= GridView::widget($draft); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>