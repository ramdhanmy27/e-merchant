<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Configuration for Ace Admin client script files
 */
class AceBaseAsset extends AssetBundle
{
    public $sourcePath = '@webroot/ace/dist';
    public $css = [
        'fonts/fonts.googleapis.com.css',
        'css/ace.min.css',
    ];
    public $js = [
        'js/ace-elements.min.js',
        'js/ace.min.js',
    ];
}
