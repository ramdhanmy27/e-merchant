<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\modelName */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <?php $form = ActiveForm::begin(); ?>

    <?php
    	$fields = $model->attributeLabels();

    	foreach ($fields as $field_name => $label) 
    		echo $form->field($model, $field_name)->textInput();
    ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
