<?php namespace backend\controllers;

use Yii;
use common\models\SalesOrder;
use common\models\Buyer;
use common\yii\db\db;
use yii\helpers\ArrayHelper;

class SalesOrderController extends \common\yii\web\Controller {
    use \common\yii\web\CrudControllerTrait {
        actionUpdate as private;
        actionDelete as private;
    }

    public static $permissions =  ["create", "view"];

    /**
     * create product page
     * @return view|response
     */
    public function actionCreate() {
        $post = $this->request->post();
        $model = new SalesOrder(["scenario" => SalesOrder::SC_CREATE]);

        if ($model->load($post)) {
            try {
                $transaction = db::transaction();

                // create buyer if not exists
                if (!($buyer = Buyer::findOne(["phone" => $post["phone"]]))) {
                    $buyer = new Buyer();
                    $buyer->name = $post["name"];
                    $buyer->phone = $post["phone"];
                    $buyer->delivery_city = $model->delivery_city;
                    $buyer->delivery_address = $model->delivery_address;
                    $buyer->post_code = $model->post_code;

                    if (!$buyer->save())
                        throw new \Exception(current($buyer->getFirstErrors()));
                }

                // collecting order item data
                $data = [];
                $model->total_price = 0;

                foreach ($post["order"] as $val) {
                    list($val["color"], $val["size"]) = explode(':', $val["variation"]);

                    $data[] = [
                        $model->number_order,
                        $val["product"],
                        $val["color"],
                        $val["size"],
                        $val["qty"],
                        $val["price"],
                    ];
                    
                    $model->total_price += $val["price"] * $val["qty"];
                }

                // set buyer and create sales order
                $model->code_buyer = $buyer->code_buyer;
                $model->code_sales_channel = SalesOrder::CHANNEL_MOBILE;
                $model->status = SalesOrder::STATUS_OPEN;
                $model->total_bill = $model->total_price + $model->shipping_cost;
                $model->order_time = db::raw("now()");
                $model->payment_deadline = date("Y-m-d H:i:s", strtotime("+$model->payment_deadline"));

                if (!$model->save())
                    throw new \Exception(current($model->getFirstErrors()));

                // create order items
                $field = [
                    "number_order", 
                    "number_product", 
                    "number_product_color", 
                    "code_size", 
                    "qty", 
                    "unit_price",
                ];

                db::batchInsert("order_item", $field, $data)->execute();

                // decrease allocation qty
                db::batchUpdate(
                    "stock_allocation sa", ["allocation_qty=allocation_qty-t.qty::int"], $field, $data,
                    "where sa.number_product_color=t.number_product_color and sa.code_size=t.code_size 
                        and sa.number_product=t.number_product and sa.code_sales_channel=:channel",
                    ["channel" => SalesOrder::CHANNEL_MOBILE]
                )->execute();

                $transaction->commit();
                $this->session->setFlash("report", $model->number_order);

                return redirect(["index"]);
            } 
            catch (\Exception $e) {
                $transaction->rollBack();
                handle($e, 0);
            }
        }

        return view(["model" => $model]);
    }

    /**
     * reporting
     * @param  string $id sales order ID
     * @return  response PDF
     */
    public function actionRenderPdf($id, $action=null) {
        $order = db::query("sales_order o")
                    ->innerjoin("buyer b", "b.code_buyer=o.code_buyer")
                    ->where(["number_order" => $id])
                    ->one();

        $item = db::query("order_item i", ["i.*", "p.name", "c.color", "s.name as size"])
                    ->innerjoin("product p", "p.number_product=i.number_product")
                    ->innerjoin("product_color c", "i.number_product_color=c.number_product_color")
                    ->innerjoin("product_size s", "s.code_size=i.code_size")
                    ->where(["number_order" => $id])
                    ->all();

        $account = db::query("bank_account")->all();
        $this->pdf->content = $this->renderPartial("report", [
            "id" => $id, 
            "order" => $order, 
            "item" => $item,
            "account" => $account,
        ]);

        if ($action == "download") {
            $this->pdf->destination = \kartik\mpdf\Pdf::DEST_DOWNLOAD;
            $this->pdf->filename = "SalesOrder-$id.pdf";
        }

        return $this->pdf->render();
    }

    /*Data Provider*/

    /**
     * POST - find buyer by phone number
     * @return json
     */
    public function actionFindBuyer() {
        if (!($phone = $this->request->post("phone")))
            return $this->abort(400);

        $model = Buyer::findOne(["phone" => $phone]);

        return json($model===null ? [] : $model->attributes);
    }

    /**
     * return product information
     * @param  string $id number_product
     * @return json
     */
    public function actionGetProductDetail($id) {
        $bind = ["id" => $id];
        $data = db::run(
            "SELECT p.name, p.basic_price as price from product p
            inner join product_color c on c.number_product=p.number_product
            inner join product_size s on s.group_size=p.group_size
            where p.number_product=:id limit 1", $bind
        )->queryOne();

        // data either invalid or not found
        if (count($data)===0)
            return json([]);

        return json($data + [
            "variations" => ArrayHelper::map(db::run(
                    "SELECT 
                        c.number_product_color||':'||s.code_size as code, 
                        c.color||' - '||s.name||' ('||sa.allocation_qty||')' as variations 
                    FROM product p
                    inner join product_size s on s.group_size=p.group_size
                    inner join product_color c on c.number_product=p.number_product
                    inner join stock_allocation sa on sa.number_product_color=c.number_product_color 
                        and s.code_size=sa.code_size
                    where allocation_qty>0 and p.number_product=:id
                    order by variations", $bind
                )->queryAll(), "code", "variations"),
        ]);
    }

    /**
     * get available quantity
     * @param  string $id    product ID
     * @param  string $size  code size
     * @param  string $color number product color
     * @return json
     */
    public function actionGetAvailableQty($id, $size, $color) {
        $qty = db::query("stock_allocation", "allocation_qty")
                ->where([
                    "code_size"=>$size, 
                    "number_product_color"=>$color, 
                    "number_product"=>$id, 
                    "code_sales_channel" => SalesOrder::CHANNEL_MOBILE,
                ])
                ->scalar();

        return json(["qty" => $qty ? $qty : 0]);
    }
}