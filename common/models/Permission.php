<?php namespace common\models;

use Yii;
use common\yii\db\db;
use yii\base\Model;
use yii\helpers\FileHelper;

class Permission extends Model {
	/**
	 * register permission (or s) to database
	 * @param  array $permission 
	 * e.g [["name"=>"perm-name1", "description"=>"description1"], ["perm-name2", "description2"]]
	 * @return int "affected rows"
	 */
	public static function register(array $permissions) {
		$data = [];
		$time = time();

		foreach ($permissions as $key => $perm)
			$data[] = [
				isset($perm['name']) ? $perm['name'] : current($perm),
				isset($perm['description']) ? $perm['description'] : next($perm),
				2,
				$time,
				$time,
			];

		return db::batchInsert("auth_item", ["name", "description", "type", "created_at", "updated_at"], $data)->execute();
	}

	/**
	 * update permissions descripton
	 * @param  array $permission e.g [["permName", "description"]]
	 * @return int "affected rows"
	 */
	public static function update($permission) {
		return db::batchUpdate(
			"auth_item i", ["description=t.description"], 
			["name", "description"], $permission, "where t.name::varchar=i.name and i.type=2"
		)->execute();
	}

	/**
	 * remove permissions from database
	 * @param  string|array $permission e.g ["permName1", "permName2"]
	 * @return int "affected rows"
	 */
	public static function remove($permission) {
		if (!is_array($permission))
			$permission = [$permission];

		return db::delete("auth_item", "name in ('".implode("','", $permission)."')")->execute();
	}

	/*Controllers*/

	/**
	 * clear permissions cache and re-register all controller permissions
	 * @param  array  $dir ["namespace\path" => "@folder/path"]
	 */
	public static function initAllController(array $dir) {
		foreach ($dir as $namespace => $path) {
			$path = Yii::getAlias($path);
			$controllers = FileHelper::findFiles($path, ["only" => ["*Controller.php"]]);

			foreach ($controllers as $file) {
				$strstart = strpos($file, $path)+strlen($path)+1;
				$classname = str_replace("/", "\\", substr($file, $strstart, strrpos($file, ".php")-$strstart));
				$class = $namespace."\\".$classname;

				if (!class_exists($class))
					continue;

				if (isset($class::$permissions)) {
					$permission = $class::$permissions;
				}
				else if (method_exists($class, "permissions")) 
					$permission = $class::permissions();
				else
					$permission = null;

				if (isset($permission) && method_exists($class, "id")) {
					$id = $class::id();
					
					Yii::$app->cache->delete("$id-controller-permissions");
					self::initController($id, $permission);
				}
			}
		}
	}

	/**
	 * checking for permissions update and registering permissions
	 * @param  array  $permissions
	 */
	public static function initController($id, array $permissions) {
		if (!is_array($permissions) || count($permissions)==0)
			return;

		$cache = Yii::$app->cache;
		$update_cache = false;
		$key = "$id-controller-permissions";
		$cache_exists = $cache->exists($key);

		// get current permissions from cache | database
		$current = $cache_exists ? $cache->get($key) : self::getByController($id);
		$new = self::parse($permissions, $id);


		// insert new permissions
		global $update_diff;
		$update_diff = [];

		$insert_diff = array_diff_ukey($new, $current, function($x, $y) use ($current, $new) {
			if ($x == $y) {
				// update description
				if ($current[$x]['description'] != $new[$x]['description']) {
					global $update_diff;
					$update_diff[] = $new[$x];
				}

				return 0;
			}
			else return $x > $y ? 1 : -1;
		});

		// update description
		if (count($update_diff) > 0) {
			self::update($update_diff);
			$update_cache = true;
		}

		// create new permissions
		if (count($insert_diff) > 0) {
			self::register($insert_diff);
			$update_cache = true;
		}

		// remove old permissions
		$delete_diff = count($current)>0 ? array_diff_key($current, $new) : [];

		if (count($delete_diff) > 0) {
			self::remove(array_keys($delete_diff));
			$update_cache = true;
		}

		// update cache
		if ($update_cache === true)
			$cache->set($key, self::getByController($id));
	}

	/**
	 * parsing data permissions from controller
	 * @param  array  $permissions 
	 * @param  string $controller  "controller name id"
	 * @return array
	 *
	 * $permissions Format
	 * [["perm-name", "description"]]
     * [["perm-name"]]
     * ["perm-name"]
	 */
	public static function parse($permissions, $controller) {
		$data = [];

		foreach ($permissions as $perm) {
			$desc = null;

			if (is_array($perm)) {
				// skip empty array
				if (count($perm)==0)
					continue;

				@list($perm, $desc) = $perm;
			}
			else if (empty($perm) || trim($perm)=='')
				continue;

			// auto-defined description
			if (!isset($desc))
				$desc = ucwords($perm." ".str_replace("-", " ", $controller));

			$perm = self::getPermissionName($perm, $controller);
			$data[$perm] = ["name" => $perm, "description" => $desc];
		}

		return $data;
	}

	/**
	 * get formatted permission name i.e. controller-id:permission
	 * @param  string $permission
	 * @param  string $controller 	"controller name id"
	 * @return string
	 */
	public static function getPermissionName($permission, $controller) {
		return $controller.':'.$permission;
	}

	/**
	 * get all permissions on specific controller
	 * @param  string $controller "controller name id, will use current controller if its empty"
	 * @return array
	 */
	public static function getByController($controller) {
		return db::query("auth_item", "name, description")
				->where("type=2")
				->andwhere("name ~* E'^".str_replace("\\", "\\\\\\\\", $controller).":.+'")
				->indexBy("name")->all();
	}
}