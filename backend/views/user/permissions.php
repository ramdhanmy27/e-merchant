<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$column = 1;
?>

<style>
    .fixed-button {
        position: fixed;
        top: 20%;
        right: 0;
    }
    .fixed-button .btn {
        border-radius: 5px 0px 0px 5px;
        box-shadow: -1px 1px 3px #999 !important;
    }
</style>

<?php $form = ActiveForm::begin(); ?>

<div class="user-form">
    <table class="table table-responsive table-hover table-striped table-bordered">
        <thead>
            <tr> 
                <th>Permissions \ Role</th> 
                <?php foreach ($roles->each() as $role) : ?>
                    <th>
                        <label class="control-label" style="cursor:pointer">
                            <input type='checkbox' class='check-all' title='Uncheck / Check All'> 
                            <?= $role["name"] ?>
                        </label>
                    </th>
                <?php $column++; endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <!-- permission category group -->
            <?php foreach ($permissions as $group => $permission) : ?>
                <tr><th colspan="<?= $column ?>" class="bg-primary"><?= ucwords(str_replace("-", " ", $group)) ?></th></tr>

                <!-- permission list -->
                <?php foreach ($permission as $perm) : ?>
                    <tr>
                        <td><?= $perm["description"] ?></td>
                        <!-- role checkboxes -->
                        <?php foreach ($roles->each() as $role) : ?>
                            <td>
                                <?= Html::checkbox(
                                    "perm[$perm[name]][]", 
                                    isset($data[$perm['name']]) && in_array($role['name'], $data[$perm['name']]),
                                    ["value" => $role['name']]
                                ) ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class='fixed-button'>
        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i>', [
            'class' => 'btn btn-warning',
            'title' => 'Simpan',
            'data-placement' => 'left',
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>