<?php namespace backend\assets\Product;

class UpdateAsset extends \yii\web\AssetBundle {
    public $baseUrl = '@web';
    
    public $js = [
        "js/product/form-update.js",
    ];

    public $css = [
    	"css/product/form-update.css",
    ];

    public $depends = [
        // "common\assets\AngularMessagesAsset",
        "common\assets\AngularAsset",
        "common\assets\DropzoneAsset",
        "backend\assets\AceAsset",
    ];
}