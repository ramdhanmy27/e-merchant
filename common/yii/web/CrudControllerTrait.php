<?php namespace common\yii\web;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\BaseInflector;

/**
 * CRUD trait
 * @property array $param
 *   @param string model
 *   @param string id (optional)
 */
trait CrudControllerTrait {
	public $breadcrumb, $param;

    public function behaviors() {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
            ]),
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['create'], "create"],
                [['update'], "update"],
                [['delete'], "delete"],
            ]),
        ];
    }

	public function param($key) {
		// model instance
		if (!isset($this->param['model-instance']) && isset($this->param['model'])) 
			$this->param['model-instance'] = new $this->param['model'];

		switch ($key) {
			case 'id':
				if (isset($this->param['model-instance']))
					return $this->param['model-instance']->tableSchema->primaryKey[0];
				else 
					return "id";

			default: 
				return isset($this->param[$key]) ? $this->param[$key] : null;
		}
	}

	public static function permissions() {
		return ["create", "update", "delete", "view"];
	}

	public function beforeAction($action) {
    	Yii::setAlias("crud", "@common/views/master");

    	if (!isset($this->param)) {
			$this->param = [
				'model' => "common\\models\\".BaseInflector::camelize($this->id),
				'model-search' => "common\\models\\".BaseInflector::camelize($this->id)."Search",
			];
    	}

    	if (!isset($this->breadcrumb))
			$this->breadcrumb = [
				"index" => [
					"label" => $this->name(),
					"child" => [
						"view" => [
							"label" => "[[get:id]]",
							"url" => ["view", "id"=>"[[get:id]]"],
							"child" => [
								"update" => ["label" => "Update"],
							]
						],
						"create" => ["label" => "Create"],
					]
				]
			];

		return parent::beforeAction($action);
	}

	/* Action Pages */

	/**
	 * Lists all data.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$model = $this->param("model");
		$modelSearch = $this->param("model-search");
		$msExists = class_exists($modelSearch);

		if ($msExists) {
			$searchModel = new $modelSearch;
	        $dataProvider = $searchModel->search($this->request->queryParams);
		}
		else {
			$dataProvider = new ActiveDataProvider([
				'query' => $model::find()
			]);
		}

		return $this->render([
				"@app/views/$this->id/index",
				"@common/views/master/index",
			],
			[
				'dataProvider' => $dataProvider,
				'columns' =>  array_merge(
					[['class' => 'yii\grid\SerialColumn']],
					array_keys((new $model)->attributeLabels()),
					[['class' => 'yii\grid\ActionColumn']]
				),
				'searchModel' => $msExists ? $searchModel : null,
			]
		);
	}

	/**
	 * Displays a single data.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render([
				"@app/views/$this->id/view",
				"@common/views/master/view",
			],
			['model' => $this->findModel($id)]
		);
	}

	/**
	 * Creates a new data.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param function $callback
	 * @return mixed
	 */
	public function actionCreate($callback = null)
	{
		$model = $this->param("model-instance");

		if (!is_callable($callback)) {
	        if ($model->load($this->request->post())) {
				try {
		            if (!$model->save())
		            	throw new \Exception(current($model->getFirstErrors()));

	                return $this->redirect(['view', 'id' => $model->{$this->param("id")}]);
				}
				catch(\Exception $e) {
					handle($e);
				}
	        }
		}
		else $callback($model);

        return $this->render([
				"@app/views/$this->id/create",
				"@common/views/master/create",
			], 
			['model' => $model]
		);
	}

	/**
	 * Updates an existing data.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer|function $id
	 * @return mixed
	 */
	public function actionUpdate($id, $callback = null)
	{
		$model = $this->findModel($id);

		if (!is_callable($callback)) {
			if ($model->load($this->request->post())) {
				try {
		            if (!$model->save())
		            	throw new \Exception(current($model->getFirstErrors()));

	                return $this->redirect(['view', 'id' => $model->{$this->param("id")}]);
				}
				catch(\Exception $e) {
					handle($e);
				}
			} 
		}
		else $callback($model, $id);

		return $this->render([
				"@app/views/$this->id/update",
				"@common/views/master/update",
			],
			['model' => $model]
		);
	}

	/**
	 * Deletes an existing data.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id, $callback = null)
	{
		$model = $this->findModel($id);

		if (!is_callable($callback)) {
			$model->delete();
		}
		else $callback($model, $id);

		return $this->redirect(['index']);
	}

	/**
	 * Finds the data based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Menu the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		$model = $this->param("model");

		if (($model = $model::findOne($id)) !== null)
			return $model;

		$this->abort(404);
	}
}