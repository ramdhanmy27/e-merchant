app.controller("receivingController", function($scope, $http, $func) {
	// receiving ID
	$scope.id_receiving = "";
	
	/*receiving Information*/
	$scope.receiving = {
		"number_po": "",
		"number_merch_do": "",
		"description": "",
	};

	var timeout;
	var setInformation = function() {
		if (timeout !== undefined)
			clearTimeout(timeout);

		timeout = setTimeout(function() {
			// update receiving information
			$func.post(url.to("receiving/update-receiving", {id_receiving: $scope.id_receiving}), $scope.receiving)
				.error(fn.alertError);
		}, 1000);
	};

	$scope.$watch(function() {
		$("#receiving-number_po").off("select2:select").on("select2:select", function() {
			$scope.f.changeItem($(this).val());
		});
		$("#receiving-number_po").off("select2:unselect").on("select2:unselect", function() {
			$scope.f.changeItem(null);
		});
		$("#receiving-number_merch_do").keyup(function() {
			// $scope.receiving["number_merch_do"] = $(this).val();
			setInformation();
		});
		$("#receiving-description").keyup(function() {
			// $scope.receiving["description"] = $(this).val();
			setInformation();
		});
	});

	// product items
	$scope.product = [];

	// product selected index
	$scope.index = "";
	// menentukan qty type: (total) yang dipilih
	$scope.type = "";

	/*Variations*/

	// object product yang ditambah
	var init = {
		product: {
			number_product: "",
			size: [""],
			attribute: [""],
			qty: {
				total: {},
			},
		},
	};

	/*initialize data*/

	// POST - dipanggil ketika close modal qty product, save quantity product yang diisi
	$("#modal-variations").on("hidden.bs.modal", function() {
		$func.post(
			url.to("receiving/save-qty", {type: $scope.type, id_receiving: $scope.id_receiving}),
			{
				number_product: $scope.product[$scope.index].number_product,
				qty: $scope.product[$scope.index].qty[$scope.type]
			}
		)
		.error(fn.alertError);
	});

	// dipanggil ketika milih product di select2, ambil data keterangan product & init null untuk quantitynya
	var initRow = function(index) {
		$(".product-details select[data-krajee-select2]:eq("+ index +")").off("select2:select").on("select2:select", function(e) {
			var $this = $(this);
			var value = $this.val();

			var unsetProduct = function() {
				$this.val($scope.product[index].number_product).trigger("change");
			};
			var setProduct = function() {
				// product already exists
				if ($.grep($scope.product, function(val, i) {return val.number_product==value}).length > 0) {
					fn.alert("Product is already exists");
					unsetProduct();
					return;
				}

				$scope.$apply(function() {
					var getProduct = function(){
						$http.get(url.to("receiving/get-product-data", {id: value}))
							.success(function(data) {
								$scope.product[index] = data;
							})
							.error(function() {
								$scope.product[index].id = value;
								fn.alertError();
							})
					}

					if (!fn.isEmpty($scope.product[index].number_product)) {
						$func.post(url.to("receiving/delete-item", {id: $scope.id_receiving}), {product: $scope.product[index].number_product})
							.success(getProduct)	
							.error(fn.alertError);
					}
					else getProduct();
				});
			};

			// on update product
			if (!fn.isEmpty($scope.product[index].number_product)) {
				fn.confirm({
					body: "It will erase your data quantity. Are you sure ?",
					yes: setProduct,
					no: unsetProduct
				});
			}
			// first try
			else setProduct();

			return false;
		});
	}

	$scope.f = {
		// dipanggil pertama kali saat halaman draft di load, ambil data banyak product & keterangan product & quantitynya
		init: function(id_receiving) {
			$scope.id_receiving = id_receiving;

			$http
				.get(url.to("receiving/get-draft-data", {id: id_receiving}))
				.success(function(data) {
					if (fn.isEmpty(data)){
						// $scope.f.add();
					}
					else {
						$scope.product = $.map(data, function(value, index) {
							return [value];
						});

						$scope.$watch(function() {
							for (var index in $scope.product) {
								// add event to each row
								initRow(index);
							};
						});
					}
				})
				.error(fn.alertError);
		},
		// dipanggil ketika ganti PO di select2, ambil data banyak product & keterangan product & quantitynya
		changeItem : function(new_number_po){
			var delete_item = function() {
				$scope.product.splice(0);
			};

			var get_and_save_item = function() {
				$func
					.post(
						url.to("receiving/get-purchase-order-item", {'number_po' : $scope.receiving.number_po})
					)
					.success(function(response) {
						$scope.product = $.map(response, function(value, index) {
							return [value];
						});

						$scope.$watch(function() {
							for (var index in $scope.product) {
								// add event to each row
								initRow(index);
							};
						});

						for (var index in $scope.product) {
							$func
								.post(
									url.to("receiving/save-qty", {type: 'total', id_receiving: $scope.id_receiving}),
									{
										number_product: $scope.product[index].number_product,
										qty: $scope.product[index].qty['total']
									}
								)
								.error(fn.alertError);
						}
					})
					.error(fn.alertError);
			}

			if (!fn.isEmpty($scope.product)) {
				fn.confirm({
					body: "It will erase your existing products. Are you sure ?",
					yes: function() {
						$scope.receiving["number_po"] = new_number_po; // ganti dulu variabel lamanya
						setInformation(); // variabel baru di update ke database
						$func
							.post(url.to("receiving/delete-item", {id: $scope.id_receiving}))
							.success(function(){
								delete_item();
								get_and_save_item(); //get and save new item
							})
							.error(fn.alertError); // delete old item di database dan di angular
					},
					no: function() {
						$("#receiving-number_po").val($scope.receiving.number_po).trigger("change");
					}
				});
			}
			else {
				$scope.receiving["number_po"] = new_number_po; // ganti dulu variabel lamanya
				setInformation(); // variabel baru di update ke database
				delete_item(); // delete old item di angular
				get_and_save_item(); //get and save new item
			}
		},
		add: function() {
			$scope.product.push($.extend({}, init.product));
			$scope.$watch(function() {
				initRow($scope.product.length-1);
			});
		},
		delete: function(index) {
			var delete_item = function() {
				$scope.product.splice(index, 1);
			};

			if (!fn.isEmpty($scope.product[index].number_product)) {
				fn.confirm({
					body: "It will erase your data quantity. Are you sure ?",
					yes: function() {
						$scope.$apply(function() {
							$func.post(url.to("receiving/delete-item", {id: $scope.id_receiving}), {product: $scope.product[index].number_product})
								.success(delete_item)
								.error(fn.alertError);
						})
					}
				})
			}
			else delete_item();
		},
		isProductSelected: function(index) {
			return !fn.isEmpty($scope.product[index].number_product);
		},
		qtyProduct: function(index) {
			return $scope.f.getQtyTotal($scope.product[index].qty.total);
		},
		getQtyTotal: function(data) {
			var total = 0;
			
			for (var row in data) {
				for (var value in data[row])
					total += fn.int(data[row][value]);
			}
					
			return total;
		},
		loadVariation: function(index, type) {
			$scope.$$childHead.variations.size = $scope.product[index].size;
			$scope.$$childHead.variations.attribute = $scope.product[index].attribute;
			$scope.$$childHead.variations.quantity = $scope.product[index].qty[type];

			$scope.index = index;
			$scope.type = type;
		},
		formValidation: function() {
			
		},
	}
});