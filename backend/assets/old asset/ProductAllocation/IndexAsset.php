<?php namespace backend\assets\ProductAllocation;

class IndexAsset extends \yii\web\AssetBundle
{
    public $baseUrl = '@web';
    
    public $css = [
        'css/product-allocation/allocation.css',
    ];
    
    public $js = [
        'js/product-allocation/allocation.js',
    ];

    public $depends = [
        "common\assets\AngularAsset",
        "backend\assets\AceAsset",
        "common\assets\DatePickerAsset",
    ];
}
