<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Configuration for Ace Admin client script files
 */
class AceAsset extends AssetBundle
{
    public $sourcePath = '@webroot/ace/dist';

    public $depends = [
		'yii\web\YiiAsset',

        'common\assets\JqueryUIAsset',
        'common\assets\FontAwesomeAsset',
        
        'yii\bootstrap\BootstrapPluginAsset',
		'yii\bootstrap\BootstrapAsset',
		
        'backend\assets\AceBaseAsset',
        'backend\assets\AceIE8Asset',
		'backend\assets\AceIE9Asset',
        'backend\assets\CustomAsset',
        
        'common\assets\DatePickerAsset',
    ];
}
