<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BankAccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bank Accounts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bank-account-index">
    <?php if (access("create")) : ?>
        <p> <?= Html::a('Create Bank Account', ['create'], ['class' => 'btn btn-success']) ?> </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'code_bank',
            'bank_name',
            'account_number',
            'account_name',

            [
            	'class' => 'yii\grid\ActionColumn',
            	'template' => '{update} {view} {delete}',
            	'buttons'=> [
            		"delete"=> function($url,$model,$index){
            			return $model["count"]<1 ?  Html::a("<span class='glyphicon glyphicon-trash'></span>",$url,['data'=>[
            					'confirm'=>'Are you sure you want to delete this item ?',
            					'method'=>'post'
            				]]):'';
            		}
            	]
            ],
        ],
    ]); ?>
</div>
