<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stock_allocation".
 *
 * @property integer $id_stock_allocation
 * @property integer $allocation_qty
 * @property integer $published_qty
 * @property string $code_sales_channel
 * @property string $number_product
 * @property string $code_size
 * @property string $number_product_color
 *
 * @property Product $product
 * @property ProductColor $productColor
 * @property ProductSize $productSize
 * @property SalesChannel $salesChannel
 */
class StockAllocation extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_allocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            //[['id_stock_allocation'], 'required'],

            // unique
            //[['id_stock_allocation'], 'unique'],

            // safe

            // integer
            [['allocation_qty', 'published_qty'], 'integer'],

            // string
            [['code_size'], 'string', 'max' => 10],
            [['code_sales_channel'], 'string', 'max' => 4],
            [['number_product'], 'string', 'max' => 10],
            [['number_product_color'], 'string', 'max' => 20],

            // exist
            [['number_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['number_product' => 'number_product']],
            [['number_product_color'], 'exist', 'skipOnError' => true, 'targetClass' => ProductColor::className(), 'targetAttribute' => ['number_product_color' => 'number_product_color']],
            [['code_size'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSize::className(), 'targetAttribute' => ['code_size' => 'code_size']],
            [['code_sales_channel'], 'exist', 'skipOnError' => true, 'targetClass' => SalesChannel::className(), 'targetAttribute' => ['code_sales_channel' => 'code_sales_channel']],
        
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_stock_allocation' => 'Id Stock Allocation',
            'allocation_qty' => 'Allocation Qty',
            'published_qty' => 'Published Qty',
            'code_sales_channel' => 'Id Sales Channel',
            'number_product' => 'Id Product',
            'code_size' => 'Id Product Size',
            'number_product_color' => 'Id Product Color',
            'salesChannel.name' => 'Sales Channel',
            'product.name' => 'Product',
            'productSize.name' => 'Product Size',
            'productColor.color' => 'Product Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['number_product' => 'number_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductColor()
    {
        return $this->hasOne(ProductColor::className(), ['number_product_color' => 'number_product_color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSize()
    {
        return $this->hasOne(ProductSize::className(), ['code_size' => 'code_size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesChannel()
    {
        return $this->hasOne(SalesChannel::className(), ['code_sales_channel' => 'code_sales_channel']);
    }
}
