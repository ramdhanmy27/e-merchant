<?php namespace backend\api;

use common\models\Channel as Model;
use yii\httpclient\Client;
use DateTime;

/**
 * Adapter class for Zalora API
 * @param url
 * data-urlencode Action
 * data-urlencode Timestamp
 * data-urlencode UserID
 * data-urlencode Version
 * data-urlencode Signature
 */
class Zalora extends Channel implements ApiInterface {
	private 
		$version = "1.0", 
		$format = "JSON";

	protected 
		$url = "https://sellercenter-api.zalora.co.id", 
		$userid,
		$apikey,
		$param;

	public function __construct($user_id, $api_key) {
		$this->userid = $user_id;
		$this->apikey = $api_key;

		$this->setParam("UserID", $this->userid);
		$this->setParam("Version", $this->version);
		$this->setParam("Format", $this->format);
		$this->setParam("Timestamp", $this->getTimestamp());
	}

	public function action($get, $post = []) {
		parent::action($get);
		$this->setParam("Signature", rawurlencode($this->getSignature()));

		// invoke method if exists
		$method = lcfirst($get['Action']);

		if (isset($get['Action']) && method_exists($this, $method))
			return call_user_method_array($method, $this, [$post]);

		// access api url
		$response = $this->client($this->url($this->param))->send();

		if ($response->isOk)
			return $response->data;

		return null;
	}
 
 	/**
	 * Products
	 * --------
	 * Available | Integer | The overall available stock
	 * FulfillmentByNonSellable | Integer | The non-sellable stock in Venture warehouse
	 * FulfillmentBySellable | Integer | The available stock in Venture warehouse
	 * MainImage | String | Link to main image of product
	 * Name | String | Product's name
	 * Price | Decimal | The product's regular price
	 * ProductId | String | EAN/UPC/ISBN of the product, if it exists
	 * Quantity | Integer | The available seller stock
	 * RealTimeStock | Integer | The stock reserved in Shop
	 * ReservedStock | Integer | The stock reserved in Seller Center
	 * SaleEndDate | DateTime | Ending date for the special sale
	 * SalePrice | Decimal | The product's special sale price
	 * SaleStartDate | DateTime | Starting date for the special sale
	 * SellerSku | String | Seller's unique identifier
	 * ShopSku | String | Shop's unique identifier
	 * Status | Integer | Product's status: 'active', 'inactive' or 'deleted'
	 * Url | String | Link to product on venture website
	 * Variation | String | Product's variation
 	 */

	/**
	 * Get data Products
	 * @return yii\httpclient\Response
	 */
	public function apiGetProducts() {
		// Action GetProducts
		// $response = $this->client(["Action" => "GetProducts"])->send();
		// $this->handleResponse($response);
		// $this->handleResponse();

		return json_decode('{"ErrorResponse":{"Head":{"RequestAction":"","ErrorType":"Sender","ErrorCode":"1","ErrorMessage":"E001: Parameter Action is mandatory"},"Body":""}}');
	}
 	
	public function apiCreateProducts($data) {
		return $this->client($this->url())
						->setMethod('post')
						->setOptions([
					        CURLOPT_POSTFIELDS => "xmlRequest=".xml_encode(["Request" => $data]),
					    ])
					    ->send();
	}

	public function apiUpdateProducts() {}
	public function apiDeleteProducts() {}
	public function createProducts() {}
	public function updateProducts() {}
	public function deleteProducts() {}
	public function apiGetOrders() {}
	public function apiSetOrderStatus() {}

	/*Request & Response*/

	public function url($param = []) {
		$this->setParam($param);
		$this->setParam("Signature", rawurlencode($this->getSignature()));

		return parent::url($this->param);
	}

	/**
	 * handle API responses
	 * @param yii\httpclient\Response $response
	 *
	 * result format:
	 * ErrorResponse
	 * 	Head
	 * 		RequestAction
	 *		ErrorType
	 *		ErrorCode
	 *		ErrorMessage
	 *	Body
	 *
	 * SuccessResponse
	 * 	Head
	 * 		RequestId
	 *		RequestAction
	 *		ResponseType
	 *		Timestamp
	 *	Body
	 *
	 * Errors
	 * code | message
	 * -----------------------------------
	 * 17 	| E017: "%s" Invalid Date Format
 	 * 19 	| E019: "%s" Invalid Limit
 	 * 14 	| E014: "%s" Invalid Offset
 	 * 36 	| E036: Invalid status filter
 	 * 70 	| E070: You have corrupt data in your sku seller list.
	 */
	// public function handleResponse(\yii\httpclient\Response $response) {
	public function handleResponse() {}

	/*Getter - Setter*/

	public function getSignature() {
		ksort($this->param);
		$encoded = [];

		foreach ($this->param as $key => $value)
			$encoded[] = rawurlencode($key)."=".rawurlencode($value);

		return hash_hmac("sha256", implode("&", $encoded), $this->apikey, false);
	}

	public function getTimestamp($format = null) {
		return (new DateTime())->format($format==null ? DateTime::ISO8601 : $format);
	}
}

/**
 *
 * Sample Data
 * -----------
 * "Product" => [
 * 	"Brand" => "ASM",
 * 	"Categories" => "2,3,5",
 * 	"Condition" => "new",
 * 	"Description" => "<![CDATA[This is a <b>bold</b> product.]]>",
 * 	"Name" => "Magic Product",
 * 	"Price" => "1.00",
 * 	"PrimaryCategory" => "4",
 * 	"ProductData" => [
 * 		"Megapixels" => "490",
 * 		"OpticalZoom" => "7",
 * 		"SystemMemory" => "4",
 * 		"NumberCpus" => "32",
 * 		"Network" => "This is network",
 * 	],
 * 	"ProductId" => "xyzabc",
 * 	"Quantity" => 10,
 * 	"SaleEndDate" => "2013-10-03T11:31:23+00:00",
 * 	"SalePrice" => "32.5",
 * 	"SaleStartDate" => "2013-09-03T11:31:23+00:00",
 * 	"SellerSku" => "4105382173aaee4",
 * 	"ShipmentType" => "dropshipping",
 * 	"Status" => "active",
 * 	"TaxClass" => "default",
 * 	"Variation" => "XXL",
 * ]
 */