<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Merchant */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Merchants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="merchant-view">
    <p>
        <?php if (access("update")) : ?>
            <?= Html::a('Update', ['update', 'id' => $model->code_merchant], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        
        <?php if (access("update")) : ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->code_merchant], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>

        <?php if (access("invite")) : ?>
            <?= Html::a("Invite",Url::toRoute(["/merchant/mail", "id"=>$model->attributes["code_merchant"]], true), [
                'class'=>'btn btn-success',
                'data'=> [
                    'confirm'=>'Are you sure you want to sent invitation to this merchant ?',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code_merchant',
            'name',
            'address',
            'contact_name',
            'phone',
            'email:email',
            'website',
            'description',
            'bank',
            'bank_acc_no',
            'bank_acc_name',
        ],
    ]) ?>

</div>
