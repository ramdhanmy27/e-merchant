<?php

namespace common\models;

use Yii;
use common\yii\db\db;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "warehouse".
 *
 * @property string $code_warehouse
 * @property string $name
 * @property string $address
 * @property string $city
 * @property string $description
 * @property integer $rack_count
 * @property integer $room_width
 * @property integer $room_length
 * @property integer $room_height
 *
 * @property DeliveryInstruction[] $deliveryInstructions
 * @property InventoryItem[] $inventoryItems
 * @property PurchaseOrderItem[] $purchaseOrders
 * @property Rack[] $racks
 */
class Warehouse extends \common\yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'warehouse';
	}

	public function rules()
	{
		return [
			// required
			[['code_warehouse', 'name'], 'required'],

			// unique
			[['code_warehouse'], 'unique'],

			// integer
			[['rack_count', 'room_width', 'room_length', 'room_height'], 'integer', "min" => 1],

			// string
			[['code_warehouse'], 'string', 'max' => 4],
			[['name', 'city'], 'string', 'max' => 16],
			[['address'], 'string', 'max' => 255],
			[['description'], 'string', 'max' => 255],
		];
	}

	public function attributeLabels()
	{
		return [
			'code_warehouse' => 'Warehouse ID',
			'name' => 'Warehouse Name',
			'address' => 'Address',
			'city' => 'City',
			'description' => 'Description',
			'rack_count' => 'Rack Count',
			'room_width' => 'Room Width',
			'room_length' => 'Room Length',
			'room_height' => 'Room Height',
		];
	}

	/**
	 * konfigurasi posisi layout dan penamaan rack
	 * Delete / Create:
	 *     - tidak ada kode dan posisi yang sama antara data sebelumnya dengan data yang baru
	 * Update:
	 *     - pindah posisi (kode harus sama seperti sebelumnya)
	 *     - ganti kode (posisi sama seperti sebelumnya)
	 *
	 * logic:
	 * lokasi v | kode v - skip
	 * lokasi v | kode x
	 *     curr v | data v - update loc curr (2x)
	 *     curr v | data x - create data | update loc curr
	 *     curr x | data v - delete curr
	 *     curr x | data x - update code
	 * lokasi x | kode v
	 *     update loc curr
	 * lokasi x | kode x
	 *     delete curr / create data
	 * @param array $data
	 */
	public function setRackLayout(array $data) {
		if ($this->isNewRecord)
			return false;

		// persiapan data baru dan data lama
		$current = db::query("rack", ["number_rack", db::raw("row_number||'-'||column_number as pos")])
					->where(["code_warehouse" => $this->code_warehouse])
					->indexBy("pos")
					->column();

		$data = array_filter($data, function($val) {
			return trim($val)!="";
		});

		$insert = $delete = [];
		$update = ["code" => [], "loc" => []];

		$diff = array_diff_ukey($current, $data, function($x, $y) use ($current, $data, &$delete, &$update, &$insert) {
			// lokasi v
			if ($x == $y) {
				// kode x
				if ($current[$x]!=$data[$x]) {
					$iscurr = in_array($current[$x], $data);
					$isdata = in_array($data[$x], $current);

					// curr v | data v
					if ($iscurr && $isdata) {
						$update["loc"][] = [
							"code" => $current[$x],
							"value" => array_search($current[$x], $data),
						];
					}
					// curr v | data x
					else if ($iscurr && !$isdata) {
						$insert[] = array_merge(explode("-", $x), [$data[$x], $this->code_warehouse]);
						$update["loc"][] = [
							"code" => $current[$x],
							"value" => array_search($current[$x], $data),
						];
					}
					// curr x | data v
					else if (!$iscurr && $isdata) {
						$delete[] = [$x, $current[$x]];
					}
					// curr x | data x
					else if (!$iscurr && !$isdata) {
						$update["code"][] = [
							"loc" => $x, 
							"value" => $data[$x],
						];
					}
				}

				return 0;
			}
			// lokasi x
			else return $x > $y ? 1 : -1;
		});

		// lokasi x
		// current diff
		foreach ($diff as $loc => $code) {
			// kode v
			if (in_array($code, $data))
				$update["loc"][] = [
					"code" => $code,
					"value" => array_search($code, $data),
				];
			// kode x
			else
				$delete[] = [$loc, $code];
		}

		// lokasi x
		// data diff - cari rak yang ada di lokasi baru (belum ada rak disitu)
		foreach (array_diff_key($data, $current) as $loc => $code) {
			// kode v
			if (in_array($code, $current))
				$update["loc"][] = [
					"code" => $code,
					"value" => $loc,
				];
			// kode x
			else
				$insert[] = array_merge(explode("-", $loc), [$code, $this->code_warehouse]);
		}

		/*Execute Query*/
		// prin($insert, $update, $delete);

		/*Delete*/
		if (count($delete) > 0) {
			$params = ["code" => $this->code_warehouse];
			$condition = [];

			foreach ($delete as $i => $data) {
				$params["l$i"] = $data[0];
				$params["c$i"] = $data[1];
				$condition[] = "(row_number||'-'||column_number=:l$i and number_rack=:c$i)";
			}

			db::delete("rack", "(".implode(" or ", $condition).") and code_warehouse=:code", $params)->execute();
		}

		/*Update Location*/
		if (count($update["loc"]) > 0) {
			$params = ["code" => $this->code_warehouse];
			$condition = [];

			foreach ($update["loc"] as $i => $data) {
				$condition[] = "(:col$i, :row$i, :c$i)";

				$tmp = explode('-', $data['value']);
				$params["row$i"] = $tmp[0];
				$params["col$i"] = $tmp[1];
				$params["c$i"] = $data["code"];
			}

			db::run(
			    "UPDATE rack r set column_number=t.col, row_number=t.row from (values ".implode(",", $condition).") as t(col, row, code)
			    where number_rack=t.code and r.code_warehouse=:code", $params
			)->execute();
		}

		/*Update Code*/
		if (count($update["code"]) > 0) {
			$params = ["code" => $this->code_warehouse];
			$condition = [];

			foreach ($update["code"] as $i => $data) {
				$condition[] = "(:c$i, :l$i)";
				$params["l$i"] = $data["loc"];
				$params["c$i"] = $data["value"];
			}

			db::run(
			    "UPDATE rack r set number_rack=t.code from (values ".implode(",", $condition).") as t(code, loc)
			    where row_number||'-'||column_number=t.loc and r.code_warehouse=:code", $params
			)->execute();
		}

		/*Insert*/
		if (count($insert) > 0)
		    db::batchInsert("rack", ["row_number", "column_number", "number_rack", "code_warehouse"], $insert)->execute();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDeliveryInstructions()
	{
		return $this->hasMany(DeliveryInstruction::className(), ['code_warehouse' => 'code_warehouse']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getInventoryItems()
	{
		return $this->hasMany(InventoryItem::className(), ['code_warehouse' => 'code_warehouse']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPurchaseOrderItems()
	{
		return $this->hasMany(PurchaseOrderItem::className(), ['code_warehouse' => 'code_warehouse']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRacks()
	{
		return $this->hasMany(Rack::className(), ['code_warehouse' => 'code_warehouse']);
	}
}
