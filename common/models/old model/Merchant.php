<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "merchant".
 *
 * @property integer $id
 * @property integer $phone
 * @property string $shop_name
 * @property string $company_name
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $country
 * @property string $postal_code
 * @property string $PIC
 * @property string $bank
 * @property string $bank_code
 * @property string $bank_swift
 * @property string $bank_account_name
 * @property string $bank_account_number
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Product[] $products
 */
class Merchant extends \common\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = -1;
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    private $_statusLabel;

    public static function tableName()
    {
        return 'merchant';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'shop_name', 'address_1', 'city', 'country', 'postal_code', 'PIC', 'bank', 'bank_code'], 'required'],
            [['id_user'], 'safe'],
            [['phone', 'status', 'created_at', 'updated_at'], 'integer'],
            [['shop_name', 'company_name', 'city', 'country', 'PIC', 'bank', 'bank_code', 'bank_swift', 'bank_account_name', 'bank_account_number'], 'string', 'max' => 100],
            [['address_1', 'address_2'], 'string', 'max' => 255],
            [['postal_code'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'shop_name' => 'Shop Name',
            'company_name' => 'Company Name',
            'address_1' => 'Address 1',
            'address_2' => 'Address 2',
            'city' => 'City',
            'country' => 'Country',
            'postal_code' => 'Postal Code',
            'PIC' => 'Pic',
            'bank' => 'Bank',
            'bank_code' => 'Bank Code',
            'bank_swift' => 'Bank Swift',
            'bank_account_name' => 'Bank Account Name',
            'bank_account_number' => 'Bank Account Number',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getStatusLabel()
    {
        if ($this->_statusLabel === null) {
            $statuses = self::getArrayStatus();
            $this->_statusLabel = $statuses[$this->status];
        }
        return $this->_statusLabel;
    }

    public static function getArrayStatus()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
            //self::STATUS_DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id_merchant' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
