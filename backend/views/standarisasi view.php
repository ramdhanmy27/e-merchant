1.

//kecuali _form
$this->title = $model->[ID];
$this->params['breadcrumbs'][] = $this->title;

2.

//kecuali _form
<div class="[controller-action]">

	//kecuali _form
	<?php if(!$this->request->isAjax) { ?>
		<div class="page-header">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
		<p>
		//index
		<?php if($this->can("create")){ ?>
			<?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>
		
		//view
		<?php if($this->can("update")){ ?>
			<?= Html::a('Update', ['update', 'id' => $model->[ID]], ['class' => 'btn btn-primary', "modal"=>""]) ?> 
		<?php } ?>
		<?php if($this->can("delete")){ ?>
			<?= Html::a('Delete', ['delete', 'id' => $model->[ID]], [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => 'Are you sure you want to delete this item?',
							'method' => 'post',
						],
					]) ?> 
		<?php } ?>
		</p>
	<?php } ?>
	
	//index
	<div class="forscrollhor">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				
				//
				['class' => 'yii\grid\ActionColumn'],
			],
		]); ?>
	</div>
	
	//create update
	<?= $this->render('_form', [
        'model' => $model,
    ]) ?>
	
	//view
	<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
        ],
    ]) ?>

</div>

3.

//form
<div class="[controller]-form">

	<?php $form = ActiveForm::begin(); ?>
	
	<div class='row'>
		<div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
			<!-- [FORM SECTION TITLE] -->
			<div class='widget-box'>
				<div class="widget-header">
					<h5 class="widget-title">[FORM SECTION TITLE]</h5>

					<div class="widget-toolbar">
						<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
					</div>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
					</div>
				</div>
			</div>
            
		</div>
	</div>
	
	<div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
	<hr>
</div>