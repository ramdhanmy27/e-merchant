<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Edit Profile ') . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Edit Profile'), 'url' => ['edit-profile']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                // 'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]
    ); ?>

    <h4>Account Information</h4>
    <br/>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?php if($modelMerchant) { ?>

    <?= $form->field($modelMerchant, 'phone')->textInput() ?>

    <h4>Business Information</h4>

    <?= $form->field($modelMerchant, 'shop_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'address_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'address_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'postal_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'PIC')->textInput(['maxlength' => true]) ?>

    <h4>Bank Information</h4>

    <?= $form->field($modelMerchant, 'bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'bank_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'bank_swift')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'bank_account_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelMerchant, 'bank_account_number')->textInput(['maxlength' => true]) ?>

    </div>

    <?php } ?>

    <?php //echo $form->field($modelMerchant, 'status')->dropDownList(User::getArrayStatus()) ?>
    <div class='row'>
        <div class='col-sm-offset-2 col-sm-10'>
            <div class="pull-right">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
