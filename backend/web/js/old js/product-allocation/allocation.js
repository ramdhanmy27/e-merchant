app.controller("allocationController", function($scope, $http, $func) {
	// Definitions
	$scope.pid = {};
	$scope.alloc = {
		f: {
			/**
			 * update quantity
			 * @param  int pid 	product item id
			 * @param  int cid	channel id
			 */
			update: function(pid, cid) {
				var value = parseInt($scope.alloc[pid][cid].quantity || 0);
				var diff = value-parseInt($scope.alloc[pid][cid].tmp || 0);

				// invalid input | no update
				if (($scope.alloc[pid][cid].error = (value<0 || ($scope.alloc[pid].quantity-diff)<0)) || diff==0)
					return;

				if (!$scope.alloc[pid].enableAction)
					$scope.alloc[pid].enableAction = true;

				// update temporary data
				$scope.alloc[pid][cid].tmp = value;

				// tambah alokasi - kurangi gudang
				// kurangi alokasi - tambah gudang
				$scope.alloc[pid].quantity -= diff;
			},
			// return formatted data for ajax post purposes
			get: function(pid) {
				var pid = pid || $scope.pid;
				var data = {};

				// array PAKSA *kyaa~~~
				if (!(pid instanceof Object))
					pid = [pid];

				for (var i in pid) {
					if (!$scope.alloc.hasOwnProperty(pid[i]))
						continue;

					data[pid[i]] = {};

					for (var cid in $scope.alloc[pid[i]]) {
						if ($scope.alloc[pid[i]][cid].hasOwnProperty("tmp"))
							data[pid[i]][cid] = {
								quantity: $scope.alloc[pid[i]][cid].tmp,
								price: $scope.alloc[pid[i]][cid].price,
								sale_price: $scope.alloc[pid[i]][cid].sale_price,
								sale_start_date: $scope.alloc[pid[i]][cid].sale_start_date,
								sale_end_date: $scope.alloc[pid[i]][cid].sale_end_date,
							};
					}
				}

				return data;
			},
			save: function(pid) {
				$func.post(url.to("product-allocation/alloc", pid!==undefined ? {"pid": pid} : undefined), {
					data: $scope.alloc.f.get(pid),
				})
				.success(function(data) {
					console.log(data);
				})
				.error(function() {
					$("#error-alert").modal("show");
				})
			},
			reset: function(pid) {
				var pids = pid===undefined ? Object.keys($scope.alloc) : [pid];

				for (var i=pids.length-1; i>=0; i--) {
					pid = pids[i];
					
					if ($scope.alloc.hasOwnProperty("pid") && $scope.alloc[pid].hasOwnProperty('enableAction'))
						continue;

					for (var cid in $scope.alloc[pid]) {
						// reset alloc channels
						if (typeof $scope.alloc[pid][cid].origin !== 'undefined') {
							for (var key in $scope.alloc[pid][cid].origin) 
								$scope.alloc[pid][cid][key] = $scope.alloc[pid][cid].origin[key];

							$scope.alloc.f.update(pid, cid);
						}
					}

					// reset gudang
					$scope.alloc[pid].quantity = $scope.alloc[pid].origin;
					$scope.alloc[pid].enableAction = false;
				}
			},

			/*Form*/
			initForm: function(pid, channel) {
				$scope.alloc.form.pid = pid;
				$scope.alloc.form.cid = channel.id;

				$scope.alloc.form.title = channel.name;
				$scope.alloc.form.price = $scope.alloc[pid][channel.id].price;
				$scope.alloc.form.sale_price = $scope.alloc[pid][channel.id].sale_price;
				$scope.alloc.form.sale_start_date = $scope.alloc[pid][channel.id].sale_start_date;
			},
			saveForm: function() {
				var pid = $scope.alloc.form.pid;
				var cid = $scope.alloc.form.cid;

				$scope.alloc[pid][cid].price = $scope.alloc.form.price;
				$scope.alloc[pid][cid].sale_price = $scope.alloc.form.sale_price;
				$scope.alloc[pid][cid].sale_start_date = $scope.alloc.form.sale_start_date;
				$scope.alloc[pid][cid].sale_end_date = $scope.alloc.form.sale_end_date;

				if (!$scope.alloc[pid].enableAction)
					$scope.alloc[pid].enableAction = true;
			},
		},
		form: {
			title: "",
			price: 0,
			sale_price: 0,
			sale_start_date: 0,
			sale_end_date: 0,
			pid: 0,
			cid: 0,
		},
	};

	$(document).ready(function() {
		/*Initialize*/
		// fetch product allocation data
		$func.post(url.to("product-allocation/get-allocation"), {
			"pid": $scope.pid,
		})
		.success(function(data) {
			$scope.channel = data.channel;

			for (var pid in data.allocation) {
				$scope.alloc[pid].enableAction = false;
				$scope.alloc[pid].origin = $scope.alloc[pid].quantity;

				for (var cid in data.allocation[pid]) {
					$scope.alloc[pid][cid] = data.allocation[pid][cid];
					$scope.alloc[pid][cid]["error"] = false;
					$scope.alloc[pid][cid]["tmp"] = angular.copy(data.allocation[pid][cid].quantity);

					// setup origin for reseting value
					$scope.alloc[pid][cid]["origin"] = angular.copy(data.allocation[pid][cid]);
				}
			}
		});
	})
});