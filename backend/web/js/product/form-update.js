app.controller("productController", function($scope, $http, $func) {
	/*initialize data*/
	Dropzone.autoDiscover = false;

	/*Update Product Color*/
	$scope.f = {
		data: {},
		color: {}, // temporary
		newData: {
			color: "",
			photo: [],
		},
		init: function(data) {
			$scope.f.data = $.extend({}, data);

			for (var i in $scope.f.data) {
				$scope.f.color[i] = $scope.f.data[i].color;
				$scope.f.initEvent(i);
			}
		},
		initEvent: function(index) {
			$scope.$watch(function() {
				var sel = "#dropzone-"+index;
				var $dz = $(sel);
				var param = {id: $scope.id};

				// skip attached element | invalid element
				if ($dz.length==0 || $dz[0].dropzone!==undefined)
					return;

				var dz = new Dropzone(sel, {
					paramName: "img",
					maxFiles: 5,
					parallelUploads: 1,
					maxFilesize: 10,
					url: url.to("product/upload-image", param),
				    headers: {'X-CSRF-Token': csrfToken},

				    uploadMultiple: false,
					autoProcessQueue: true,
					addRemoveLinks: true,
					dictResponseError: 'Error while uploading file!',
					dictRemoveFileConfirmation: "Are you sure ?",

					dictDefaultMessage:
						'<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \
						<span class="smaller-80 grey">(or click)</span> <br /> \
						<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',

					maxfilesexceeded: function(file) {
						dz.removeFile(file)
					}
				});

				dz.on("removedfile", function(file) {
					if (!$scope.f.isColorExists(index))
						return;

					// remove file on server
					$.ajax({
						url: url.to("product/delete-image", Object.assign(param, {color: $scope.f.data[index].color})),
						method: 'post',
						data: {file: $('[data-dz-name]', file.previewElement).html() || file.name},
						error: function() {
							fn.alertError();
						}
					})
				})

				dz.on("addedfile", function(file) {
					if (!$scope.f.isColorExists(index)) {
						dz.removeAllFiles(true);
						fn.alert("You have to set color name first.");
					}
					else if ($(".dz-preview", this.element).length > this.options.maxFiles) {
						this.options.maxfilesexceeded(file);
						fn.alert("You can only upload 5 images");
					}
				});

				dz.on("success", function(file, data) {
					// set filename (for delete image purpose)
					if (data.length > 0)
						$('[data-dz-name]', file.previewElement).html(data[0]);
				});

				dz.on("processing", function() {
					this.options.url = url.to("product/upload-image", Object.assign(param, {color: $scope.f.data[index].color}));
				});

				// view stored images
				for (var i in $scope.f.data[index].photo) {
					var file = {
						name: $scope.f.data[index].photo[i].name, 
						size: $scope.f.data[index].photo[i].size,
					};

					dz.emit('addedfile', file);
					dz.emit('thumbnail', file, $scope.f.data[index].photo[i].url);
				}
			})
		},
		setColor: function(index) {
			// found duplicate
			if ($.grep(Object.keys($scope.f.data), function(val) {
					return $scope.f.data[val].color==$scope.f.color[index]
				}) > 0)
				return fn.alert("There is already Product Color named '"+ $scope.f.color[index] +"'");

			if (!fn.isset($scope.loading))
				$scope.loading = {};

			$scope.loading[index] = true;

			$func.post(
				url.to("product/set-color", {id: $scope.id}), 
				{color: {"new": $scope.f.color[index], "old": new String($scope.f.data[index].color)}}
			)
			.success(function() {
				delete $scope.loading[index];
				$scope.f.data[index].color = $scope.f.color[index];
			})
			.error(function() {
				delete $scope.loading[index];
				fn.alertError();
			})
		},
		addRow: function(){
			var indexes = Object.keys($scope.f.data);
			var index = indexes[indexes.length-1]+1;

			console.log($scope.f.data)
			$scope.f.data[index] = angular.copy($scope.f.newData);
			$scope.f.initEvent(index);
		},
		isColorExists: function(index) {
			return !fn.isEmpty($scope.f.data[index].color);
		},
		deleteRow: function(index) {
			var removeItem = function() {
				delete $scope.f.color[index];
				delete $scope.f.data[index];
			};

			if (!$scope.f.isColorExists(index))
				return removeItem();

			fn.confirm({
				yes: function() {
					$func.post(url.to("product/delete-item", {id: $scope.id, color: $scope.f.data[index].color}))
						.success(removeItem);
				}
			})
		}
	}
});