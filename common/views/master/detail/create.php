<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\modelName */

$this->title = $model::name();
?>

<div class="create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render([
    		"@app/views/".$this->context->id."/_form",
    		"../_form",
    	],
        ['model' => $model]
    ) ?>
</div>
