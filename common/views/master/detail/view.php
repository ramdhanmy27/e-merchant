<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\modelName */

$this->title = $model::name();
?>

<div class="view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update-detail', 'id' => $id, 'parent' => $parent], ['class' => 'btn btn-primary', "modal"=>""]) ?>
        <?= Html::a('Delete', ['delete-detail', 'id' => $id, 'parent' => $parent], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Apakah Anda Yakin ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => array_keys($model->attributeLabels()),
    ]) ?>
</div>
