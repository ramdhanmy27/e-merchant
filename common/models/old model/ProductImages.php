<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use common\yii\db\db;

/**
 * This is the model class for table "product_images".
 *
 * @property integer $id
 * @property string $file
 * @property integer $id_product
 *
 * @property Product $idProduct
 */
class ProductImages extends \common\yii\db\ActiveRecord
{
	public 
		// product images path
		$path = "@webroot/images/product", 
		// product images path
		$url = "@web/images/product", 
		
		// allowed extensions
		$ext = [".gif", ".png", ".jpg", ".jpeg"],

		// custom size
		$size = [
			"small" => [100, 100], 
			"thumb" => [600, 600],
		];

	public function __construct($id_product = null) {
		parent::__construct();

		if ($id_product != null)
			$this->id_product = $id_product;

		$this->path = Yii::getAlias($this->path);
		$this->url = Yii::getAlias($this->url);
	}

	public static function tableName() {
		return 'product_images';
	}

	public function rules() {
		return [
			[['id_product'], 'integer'],
			[['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id']],
			[["type"], "string"],
		];
	}

	public function attributeLabels() {
		return [
			'id' => 'ID',
			'id_product' => 'Id Product',
			'type' => 'Type'
		];
	}

	/**
	 * get image url path
	 * @param  string  $size  @see property $size
	 * @return string
	 */
	public function getUrl($size = null) {
		$file = $this->id_product."/".$this->id.($size==null ? null : "-".$size).$this->type;

		if ($this->isNewRecord && file_exists($this->path."/".$file))
			return false;

		return $this->url."/".$file;
	}

	/**
	 * delete data (also it's file)
	 * @return boolean
	 */
	public function delete() {
		if ($this->isNewRecord)
			return false;

		$path = $this->path."/".$this->id_product;
		
		foreach (array_keys($this->size) as $size) {
			$file = $path."/".$this->id."-".$size.$this->type;

			if (file_exists($file))
				@unlink($file);
		}
		
		return parent::delete();
	}

	/**
	 * upload image files (group by product id)
	 * @param  string|array $files
	 * @return boolean
	 */
	public function upload($files) {
		if ($this->id_product == null)
			return false;

		$path = $this->path."/".$this->id_product;

        // path is readable and writeable
        if (!FileHelper::createDirectory($path))
            return false;

        // checking image existence
        $data = [];
    	$files = is_array($files) ? array_values($files) : [$files];

        foreach ($files as $file) {
        	if (!file_exists($file)) 
        		continue;

        	$ext = image_type_to_extension(exif_imagetype($file));

        	if (in_array($ext, $this->ext))
	            $data[] = [$this->id_product, $ext];
        }

        if (count($data) == 0)
        	return false;
        
        // make allocation in database
        $images = db::run(
	            	db::batchInsert("product_images", ["id_product", "type"], $data)->sql." returning id, type"
	            )->queryAll();

        // upload images
        foreach ($images as $i => $val) {
            // original
            $image = Yii::$app->image->load($files[$i]);
            $image->save($path."/".$val["id"].$val["type"]);
            
            // custom size
            foreach ($this->size as $label => $size) {
                $image->resize($size[0], $size[1], \yii\image\drivers\Image::HEIGHT);
                $image->save($path."/".$val["id"]."-".$label.$val["type"]);
            }
        }

        return true;
	}

	/*Relations*/

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getIdProduct() {
		return $this->hasOne(Product::className(), ['id' => 'id_product']);
	}
}
