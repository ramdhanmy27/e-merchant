<?php namespace backend\controllers\channel\api;

use yii\helpers\Inflector;

abstract class BaseChannelController extends \common\yii\web\Controller {
    private $api, $channel;

    public function __construct($id, $app, $config, $model) {
        parent::__construct($id, $app, $config);

		$apiClass = isset($this->apiClass) ? $this->apiClass : "\backend\api\\".Inflector::camelize($model->name);
		$this->api = new $apiClass($model->user_id, $model->api_key);
		$this->channel = $model;
    }

    public function render($view=null, $params=[]) {
        $actions = preg_split("/\/\\\\/", $this->request->get("action"));
		return parent::render($view===null ? $actions[0] : $view, $params);
	}
}