<?php

use yii\grid\GridView;
use yii\helpers\Html;
use common\yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

register(["@js/barcode/barcode2.js"], [
    "depends" => [
        "common\assets\AngularMessagesAsset",
        "common\assets\AngularAsset",
        "backend\assets\AceAsset",
    ]
]);


$this->title = 'Barcode & Repackaging';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= $this->controller->id ?>-index">
    <?php $form = ActiveForm::begin([
        "id" => "barcode", 
        'options' => [
            'name' => 'barcode',
        ]
    ]); ?>
    <div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?>'>
            <!-- Category Information -->
            <div class='widget-box'>
                <div class="widget-header">
                    <h5 class="widget-title">Product to be Printed</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" ng-controller="barcodeController">
                    <div class="widget-main">


                        <div class='form-group'>
                            <label class='control-label col-md-2'>Product</label>
                            <div class="col-md-6">
                                <?= Select2::widget([
                                    'name' => 'product',
                                    'value' => '',
                                    'data' => ArrayHelper::map($model->queryAll(), "number_product", "name"),
                                    'options' => [
                                        'placeholder' => 'Select Product', 
                                        "id" => "select2",
                                    ],
                                ]); ?>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='control-label col-md-2'>Brand</label>
                            <div class="col-md-6">
                                <?= Html::input('text', 'brand', "", ['class' => "form-control", "ng-model" => "product.brand_name"]) ?>
                            </div>
                        </div>
                        <br>
                        <div class='form-group' ng-if="product.number_product!=''">
                            <label class='control-label col-md-2'>Qty</label>
                            <div class="col-xs-10">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th ng-repeat="(x, y) in product.group_size track by $index">{{product.group_size[$index].name}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="(x, y) in product.product_color track by $index">
                                            <td>{{product.product_color[$index].color}}</td>
                                            <td ng-repeat="(i, j) in product.group_size track by $index">
                                                <span >
                                                    {{product['inventory'][j.code_size][y.number_product_color]}}
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class='row'>
        <div class='<?= $this->request->isAjax ? "col-xs-12" : "col-xs-6" ?> text-right'>
            <?= Html::submitButton('Print', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>