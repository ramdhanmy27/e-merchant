<?php namespace backend\controllers;

use common\models\Rack;
use common\models\Box;
use common\yii\db\db;
use Respect\Validation\Validator as v;

class WarehouseController extends \common\yii\web\Controller {
    use \common\yii\web\CrudControllerTrait;

    static $permissions = [
        "create", "update", "view", "delete", ["rack-config", "Konfigurasi Rak"], ["rack-layout", "Konfigurasi Layout Rak"]
    ];

    function behaviors() {
        $this->breadcrumb['index']['child']['view']['child'] = array_merge(
            $this->breadcrumb['index']['child']['view']['child'],
            [
                "rack-layout" => ["label" => "Rack Layout"],
                "rack-config" => ["label" => "Rack Configuration"],
            ]
        );

        return [
            "verbs" => $this->verbs([
                "delete" => ["post"],
            ]),
            "access" => $this->access([
                [["index", "view"], "view"],
                [["create"], "create"],
                [["update"], "update"],
                [["delete"], "delete"],
                [["rack-layout"], "rack-layout"],
                [["rack-config"], "rack-config"],
            ]),
        ];
    }

    public function actionRackLayout($id = null) {
        $model = $this->findModel($id);

        // set rack layout
        if (count($this->request->post('rack')) > 0) {
            $model->setRackLayout($this->request->post('rack'));
            return redirect(["rack-layout", "id"=>$id]);
        }

        return view(["model" => $model]);
    }

    public function actionRackConfig($id) {
        $model = $this->findModel($id);
        $data = $this->request->post();

        if (count($data) > 0) {
            if (count($data["Rack"])) {
                $boxes = [];

                try {
                    // transaction
                    $transaction = db::transaction();

                    foreach ($data["Rack"] as $x => $val) {
                        if (!isset($val["box_count"])) {
                            $data["Rack"][$x]["box_count"] = null;
                            continue;
                        }

                        Rack::assert($val);

                        for ($i=1; $i <= $val["box_count"]; $i++) {
                            $key = $i-1;
                            $boxes[$key] = [
                                "number_rack" => $val["number_rack"],
                                "number_box" => $val["number_rack"]."-".str_pad($i, 2, 0, STR_PAD_LEFT), 
                                "capacity" => $data["default_capacity"],
                            ];

                            Box::assert($boxes[$key]);
                        }
                    }

                    // create boxes
                    if (count($boxes))
                        db::batchInsert("box", ["number_rack", "number_box", "capacity"], $boxes)->execute();

                    // update rack information
                    db::batchUpdate("rack r", [
                            "number_rack=t.number_rack", "level_count=t.level_count::int", "box_capacity=t.box_capacity::int",
                            "box_count=case when t.box_count is null then r.box_count else t.box_count::int end",
                        ],
                        ["number_rack", ["level_count", "int"], ["box_capacity", "int"], ["box_count", "int"]], $data["Rack"],
                        "where code_warehouse=:warehouse and t.number_rack=r.number_rack", ["warehouse" => $id]
                    )->execute();

                    $transaction->commit();
                } 
                catch (\Exception $e) {
                    $transaction->rollBack();
                    handle($e);
                }
            }

            return redirect(["rack-config", "id"=>$id]);
        }

        return view(["model" => $model]);
    }
}
