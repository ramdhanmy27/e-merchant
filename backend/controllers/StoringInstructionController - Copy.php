<?php

namespace backend\controllers;

use Yii;
use common\models\StoringInstruction;
use common\models\StoringInstructionSearch;
use common\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\yii\db\db;

/**
 * StoringInstructionController implements the CRUD actions for StoringInstruction model.
 */
class StoringInstructionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Storing Instruction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new StoringInstructionSearch();

        return view([
            "list" => [
                "dataProvider" => $model->search(
                    $this->request->queryParams + ["StoringInstructionSearch" => ["status" => 1]]
                ),
                "filterModel" => $model,
            ],
            "draft" => [
                "dataProvider" => $model->search(
                    $this->request->queryParams + ["StoringInstructionSearch" => ["status" => 0]]
                ),
                "filterModel" => $model,
            ],
        ]);
    }

    /**
     * Displays a single Storing Instruction model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Storing Instruction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $raw = db::insert("storing_instruction", ["status" => 0])->rawSql ;
        $lastInsertId = db::run($raw." returning number_storing")->queryScalar();

        return $this->redirect(["draft", "id" => $lastInsertId]);
    }

    /**
     * halaman draft
     * @param  string $id storing instruction ID
     * @return view
     */
    public function actionDraft($id = null) {
        $model = $this->findModel($id);

        if (!$model->isDraft()) {
            $this->abort();
            return;
        }

        return $this->render([
            "model" => $model
        ]);
    }

    /**
     * dipanggil ketika klik tombol load, ambil data banyak product & keterangan product & quantitynya
     * @return mixed
     */
    public function actionGetUnstoredItem()
    {
        // 2 sintaks dibawah sama saja, muahahahahaha
        // $bind = ["number_po" => $number_po];
        // $bind["number_po"] = $number_po;

        if ($this->user->identity->code_warehouse)
            $bind["code_warehouse"] = $this->user->identity->code_warehouse;

        $query = db::run(
            "SELECT p.number_product, i.sku, c.number_product_color, c.color, s.code_size, s.name, u.qty
            FROM
            (
                    SELECT DISTINCT p.number_product, p.group_size
                    FROM product p
                    INNER JOIN inventory_item i ON p.number_product=i.number_product
                    WHERE i.code_warehouse=:code_warehouse
                    ORDER BY p.number_product
            ) AS p
            RIGHT JOIN product_color c ON c.number_product=p.number_product
            RIGHT JOIN product_size s ON s.group_size=p.group_size
            LEFT JOIN inventory_item i ON i.number_product_color = c.number_product_color
                    AND i.code_size = s.code_size
            LEFT JOIN unstored_items u ON u.sku = i.sku
            ORDER BY p.number_product", $bind
        )->query();
        $data = [];

        foreach ($query as $val) {
            $data[$val["number_product"]]["number_product"] = $val["number_product"];
            $data[$val["number_product"]]["size"][$val["code_size"]] = $val["name"];
            $data[$val["number_product"]]["attribute"][$val["number_product_color"]] = $val["color"];
            $data[$val["number_product"]]["qty"]["total"][$val["code_size"]][$val["number_product_color"]] = $val["qty"];
        }

        return $this->request->isAjax || $this->controller->action->id=="get-unstored-item" ? json($data) : $data;
    }

    /**
     * POST - update storing instruction information
     * @param  string $number_storing number_storing
     */
    public function actionUpdateStoringInstruction($number_storing) {
        $post = $this->request->post();
        $storing_time = date('Y-m-d H:i:s');
        $format = "m/d/Y H:i:s";

        db::update("storing_item", [
            "number_storing" => trim($post["number_storing"])=="" ? null : $post["number_storing"],
        ], "number_storing=:number_storing", ["number_storing" => $number_storing])->execute();

        db::update("storing_instruction", [
            "number_storing" => trim($post["number_storing"])=="" ? null : $post["number_storing"],
            "storing_time" => $storing_time,
        ], "number_storing=:number_storing", ["number_storing" => $number_storing])->execute();
    }

    /**
     * save storing instruction
     * @param  string $number_storing storing instruction ID
     * @return response
     */
    public function actionSave($number_storing) {
        db::update("storing_instruction", ["status" => "1"], "number_storing=:number_storing", ["number_storing" => $number_storing])->execute();
        return redirect(["index"]);
    }

    /**
     * delete storing instruction
     * @param  string $number_storing storing instruction ID
     * @return response
     */
    public function actionDelete($number_storing) {
        db::delete("storing_item", "number_storing=:number_storing", [
            "number_storing" => $number_storing
        ])->execute();
        db::delete("storing_instruction", "number_storing=:number_storing", ["number_storing" => $number_storing])->execute();
        return redirect(["index"]);
    }

    /**
     * POST - delete product item 
     * @param  string $number_storing storing instruction ID
     */
    public function actionDeleteItem($number_storing) {
        if(null!==$this->request->post("product"))
            db::delete("storing_item", "number_storing=:number_storing and number_product=:product", [
                "number_storing" => $number_storing, "product" => $this->request->post("product")
            ])->execute();
        else
            db::delete("storing_item", "number_storing=:number_storing", [
                "number_storing" => $number_storing
            ])->execute();
    }

    /**
     * POST - dipanggil ketika close modal qty product, save quantity product yang diisi
     * @param  string $id   stock agreement ID 
     * @param  string $type commit|ready
     */
    public function actionSaveQty($number_storing, $type) {
        $post = $this->request->post();
        $field_type = [
            "total" => "qty",
        ];

        // invalid type
        if (!isset($field_type[$type])) {
            $this->abort(400);
            return;
        }

        // ambil data variasi qty untuk dibandingkan nanti
        $query = db::query("storing_item", [
                    "code_size", 
                    "number_product_color", 
                    "qty as qty", 
                ])
                ->where(["number_storing" => $number_storing, "number_product" => $post['number_product']]);
        $data = [];

        foreach ($query->each() as $val) 
            $data[$val["code_size"]][$val["number_product_color"]] = [
                "total" => $val["qty"],
            ];

        // prin($data, $post);

        // membandingkan data yang dikirim dan yang ada pada database
        // dan menentukan aksi yang akan dilakukan pada data lama
        $insert = $update = $delete = [];

        foreach ($post["qty"] as $size => $item) {
            foreach ($item as $color => $newval) {
                $oldval_exists = isset($data[$size][$color]);
                $oldval = $oldval_exists ? $data[$size][$color][$type] : null;
                $newval_exists = trim($newval)!="";

                // data yang dimasukkan bukan angka
                // atau tidak ada perbedaan data dengan yang ada pada database
                if (($newval_exists && !is_numeric($newval)) || ($oldval_exists && $newval==$oldval))
                    continue;

                if ($oldval_exists) {
                    // delete data quantity
                    if (!$newval_exists) {
                        $will_delete = true;

                        foreach ($data[$size][$color] as $itype => $val) {
                            if ($type == $itype)
                                continue;

                            // hanya update null jika masih ada qty di salah satu kolom (total|)
                            if ($val != null) {
                                $update[] = [$number_storing, $post["number_product"], $size, $color, null];
                                $will_delete = false;
                                break;
                            }
                        }

                        // delete jika qty di semua kolom null
                        if ($will_delete)
                            $delete[] = [$number_storing, $post["number_product"], $size, $color];
                    }
                    // update quantity
                    else if ($newval!=$data[$size][$color])
                        $update[] = [$number_storing, $post["number_product"], $size, $color, $newval];
                }
                else if ($newval_exists) {
                    // buat data baru
                    $insert[] = [$number_storing, $post["number_product"], $size, $color, $newval];
                }
            }
        }

        $field = [
            "number_storing", 
            "number_product", 
            "code_size", 
            "number_product_color", 
            $field_type[$type],
        ];

        if (count($insert) > 0)
            db::batchInsert("receive_item", $field, $insert)->execute();
        
        if (count($update) > 0)
            db::batchUpdate(
                "receive_item i", [$field_type[$type]."=t.".$field_type[$type]."::int"], $field, $update, 
                "where t.number_storing::int=i.number_storing
                    and t.number_product=i.number_product
                    and t.code_size::char=i.code_size::char
                    and t.number_product_color=i.number_product_color"
            )->execute();

        if (count($delete) > 0) {
            $bind = $condition = [];

            foreach ($delete as $i => $val) {
                $bind["n$i"] = $val[0];
                $bind["p$i"] = $val[1];
                $bind["s$i"] = $val[2];
                $bind["c$i"] = $val[3];
                $condition[] = "(number_storing=:n$i and number_product=:p$i and code_size=:s$i and number_product_color=:c$i)";
            }

            db::delete("receive_item", implode(" or ", $condition), $bind)->execute();
        }
    }

    /**
     * dipanggil pertama kali saat halaman draft di load, ambil data banyak product & keterangan product & quantitynya
     * @param  string $id storing instruction ID
     * @param  string $product product ID
     * @return json
     */
    public function actionGetDraftData($id, $product = null) {
        $bind = ["id" => $id];

        if ($product!=null)
            $bind["product"] = $product;

        $query = db::run(
            "SELECT p.number_product, c.number_product_color, c.color, s.code_size, s.name, 
                i.qty from receiving r
            left join (
                select distinct i.number_storing, p.number_product, p.group_size from product p
                inner join receive_item i on p.number_product=i.number_product
            ) as p on r.number_storing=p.number_storing
            right join product_color c on c.number_product=p.number_product
            right join product_size s on s.group_size=p.group_size
            left join receive_item i on i.number_storing=r.number_storing
                and c.number_product_color=i.number_product_color
                and s.code_size=i.code_size
            where r.number_storing=:id ".($product==null ? "" : "and p.number_product=:product")
            ." order by p.number_product", $bind
        )->query();
        $data = [];

        foreach ($query as $val) {
            $data[$val["number_product"]]["number_product"] = $val["number_product"];
            $data[$val["number_product"]]["size"][$val["code_size"]] = $val["name"];
            $data[$val["number_product"]]["attribute"][$val["number_product_color"]] = $val["color"];
            $data[$val["number_product"]]["qty"]["total"][$val["code_size"]][$val["number_product_color"]] = $val["qty"];
        }

        return $this->request->isAjax || $this->controller->action->id=="get-draft-data" ? json($data) : $data;
    }

    /**
     * dipanggil ketika milih product di select2, ambil data keterangan product & init null untuk quantitynya
     * @param  string $id product ID
     * @return json
     */
    public function actionGetProductData($id) {
        $query = db::run(
            "SELECT c.number_product_color, c.color, s.code_size, s.name from product p
            left join product_size s on s.group_size=p.group_size
            left join product_color c on c.number_product=p.number_product
            where p.number_product=:id order by s.code_size, c.color",
            ["id" => $id]
        )->query();
        $data = [];

        foreach ($query as $val) {
            $data["number_product"] = $id;
            $data["size"][$val["code_size"]] = $val["name"];
            $data["attribute"][$val["number_product_color"]] = $val["color"];
            $data["qty"]["total"][$val["code_size"]][$val["number_product_color"]] = null;
        }

        return $this->request->isAjax || $this->controller->action->id=="get-product-data" ? json($data) : $data;
    }

    /**
     * Finds the StoringInstruction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StoringInstruction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StoringInstruction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
