app.controller("stockAgreementController", function($scope, $http, $func, Variations) {
	// stock agreement ID
	$scope.id = "";
	
	/*Contract Information*/
	$scope.contract = {
		"merchant": "",
		"date": "",
		"description": "",
	};

	var timeout;
	var setInformation = function() {
		if (timeout !== undefined)
			clearTimeout(timeout);

		timeout = setTimeout(function() {
			// update contract information
			$func.post(url.to("stock-agreement/update-contract", {id: $scope.id}), $scope.contract)
				.error(function() { fn.alertError() });
		}, 1000);
	};

	$scope.$watch(function() {
		$("#stockagreement-code_merchant").off("select2:select").on("select2:select", function() {
			$scope.contract["merchant"] = $(this).val();
			setInformation();
		});
		$("#date-range").off("apply.daterangepicker").on("apply.daterangepicker", function() {
			$scope.contract["date"] = $(this).val();
			setInformation();
		});
		$("#stockagreement-description").keyup(function() {
			$scope.contract["description"] = $(this).val();
			setInformation();
		});
	});

	/*Variations*/
	$scope.product = [];

	// product index
	$scope.index = "";
	// menentukan qty commit/safety
	$scope.type = "";

	var init = {
		product: $.extend(true, {}, Variations.init, {
			id: "",
			error: {
				commit: {a:'asd'},
				ready: {},
			},
			qty: {
				commit: {},
				ready: {},
			},
		}),
	};

	var initRow = function(index) {
		$(".product-details select[data-krajee-select2]:eq("+ index +")").off("select2:select").on("select2:select", function(e) {
			var $this = $(this);
			var value = $this.val();

			var resetProduct = function() {
				$this.val($scope.product[index].id).trigger("change");
			};

			// product already exists
			if ($.grep($scope.product, function(val, i) {return val.id==value}).length > 0) {
				fn.alert("Product is already exists");
				resetProduct();
				return;
			}
			else {
				delete_item(index, function() {
					$http.get(url.to("stock-agreement/get-product-data", {id: value}))
						.success(function(data) {
							$scope.product[index] = $.extend(true, {}, init.product, data);
						})
						.error(function() {
							$scope.product[index].id = value;
							fn.alertError();
						})
				}, resetProduct);
			}
		});
	}

	var delete_item = function(index, yes, no) {
		if ($scope.f.isProductSelected(index)) {
			fn.confirm({
				body: "It will erase your data quantity. Are you sure ?",
				yes: function() {
					$func.post(url.to("stock-agreement/delete-item", {id: $scope.id}), {product: $scope.product[index].id})
						.success(yes)
						.error(function() { fn.alertError() });
				},
				no: no
			});
		}
		else yes();
	}

	$scope.f = {
		init: function(id) {
			$scope.id = id;

			$http.get(url.to("stock-agreement/get-draft-data", {id: id})).success(function(data) {
				if (fn.isEmpty(data))
					$scope.f.add();
				else {
					$scope.product = $.map(data, function(value, index) {
						return [$.extend(true, {}, init.product, value)];
					});

					$scope.$watch(function() {
						for (var index in $scope.product)
							// add event to each row
							initRow(index);
					});
				}
			})
		},
		add: function() {
			$scope.product.push($.extend(true, {}, init.product));
			$scope.$watch(function() {
				initRow($scope.product.length-1);
			});
		},
		delete: function(index) {
			delete_item(index, function() {
				$scope.product.splice(index, 1);
			});
		},
		isProductSelected: function(index) {
			return !fn.isEmpty($scope.product[index].id);
		},
		qtyCommitProduct: function(index) {
			return $scope.f.getQtyTotal($scope.product[index].qty.commit);
		},
		qtySafetyProduct: function(index) {
			return $scope.f.getQtyTotal($scope.product[index].qty.ready);
		},
		getQtyTotal: function(data) {
			var total = 0;
			
			for (var row in data) {
				for (var value in data[row])
					total += fn.int(data[row][value]);
			}
					
			return total;
		},
		loadVariation: function(index, type) {
			$scope.$$childHead.variations.error = $scope.product[index].error;
			$scope.$$childHead.variations.size = $scope.product[index].size;
			$scope.$$childHead.variations.attribute = $scope.product[index].attribute;
			$scope.$$childHead.variations.quantity = $scope.product[index].qty[type];

			$scope.index = index;
			$scope.type = type;
		},
		formValidation: function(e) {
			var errType = ["commit","ready"];

			for (var i in $scope.product) {
				for (var j in errType) {
					// prevent submit & show variations modal
					if (!fn.isEmpty($scope.product[i].error[errType[j]])) {
						e.stopPropagation();
						e.preventDefault();
						$scope.f.loadVariation(i, errType[j]);
						$("#modal-variations").modal("show");

						return false;
					}
				}
			}

			return true;
		},
	}

	$("#modal-variations").on("hidden.bs.modal", function() {
		$func.post(
			url.to("stock-agreement/save-qty", {type: $scope.type, id: $scope.id}),
			{
				id: $scope.product[$scope.index].id,
				qty: $scope.product[$scope.index].qty[$scope.type]
			}
		)
		.error(function() { fn.alertError() });
	})
});