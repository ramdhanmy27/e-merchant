<?php

namespace common\models;

use Yii;
use common\yii\db\db;

/**
 * This is the model class for table "purchase_order".
 *
 * @property string $number_po
 * @property string $date
 * @property integer $status
 * @property string $description
 * @property string $code_merchant
 * @property string $code_warehouse
 *
 * @property Merchant $merchant
 * @property Warehouse $warehouse
 * @property PurchaseOrderItem[] $purchaseOrderItems
 * @property Receiving[] $receivings
 */
class PurchaseOrder extends \common\yii\db\ActiveRecord implements \common\yii\db\DraftModelInterface
{
    const STATUS_CLOSE = '2';

    public static $status = [
        self::STATUS_DRAFT => "draft",
        self::STATUS_SAVE => "open",
        self::STATUS_CLOSE => "close",
    ];

    public function getLabelStatus($textOnly=false) {
        if (!isset(self::$status[$this->status]))
            return null;

        $label = ucwords(self::$status[$this->status]);

        if ($textOnly)
            return $label;

        $class = [
            self::STATUS_DRAFT => "label label-default",
            self::STATUS_SAVE => "label label-warning",
            self::STATUS_CLOSE => "label label-success",
        ];

        return "<span class='{$class[$this->status]}'>$label</span>";
    }

    public static function tableName()
    {
        return 'purchase_order';
    }

    public function setCloseStatus() {
        $this->status = self::STATUS_CLOSE;
        return $this;
    }

    public function rules()
    {
        return [
            // required
            [['number_po'], 'required'],

            // unique
            [['number_po'], 'unique'],

            // safe
            [['date'], 'safe'],

            // integer
            [['status'], 'integer'],

            // string
            [['code_warehouse'], 'string', 'max' => 4],
            [['code_merchant'], 'string', 'max' => 10],
            [['number_po'], 'string', 'max' => 32],
            [['description'], 'string', 'max' => 32],

            // exist
            [['code_merchant'], 'exist', 'skipOnError' => true, 'targetClass' => Merchant::className(), 'targetAttribute' => ['code_merchant' => 'code_merchant']],
            [['code_warehouse'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['code_warehouse' => 'code_warehouse']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'number_po' => 'PO Number',
            'date' => 'Date',
            'status' => 'Status',
            'description' => 'Description',
            'code_merchant' => 'Merchant',
            'code_warehouse' => 'Warehouse',
            'merchant.name' => 'Merchant',
            'warehouse.name' => 'Warehouse Name',
        ];
    }

    public function isDraft() {
        return $this->status == self::STATUS_DRAFT;
    }

    public static function getCode() {
        return unic();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(Merchant::className(), ['code_merchant' => 'code_merchant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderItems()
    {
        return $this->hasMany(PurchaseOrderItem::className(), ['number_po' => 'number_po']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceivings()
    {
        return $this->hasMany(Receiving::className(), ['number_po' => 'number_po']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::className(), ['code_warehouse' => 'code_warehouse']);
    }
}
