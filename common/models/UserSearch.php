<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `backend\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // jika param di get ngga ada, maka data provider langsung direturn dengan query User::find();
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            //'created_at' => $this->created_at,
            //'to_date(created_at, d-m-Y)' => Yii::$app->formatter->asDate(strtotime($this->created_at), 'php:d-m-Y'),
        ]);

        // prin(Yii::$app->formatter->asTimeStamp($newDate), $this->created_at);
        $query->andFilterWhere(['ilike', 'username', $this->username])
            //->andFilterWhere(['ilike', 'auth_key', $this->auth_key])
            //->andFilterWhere(['ilike', 'password_hash', $this->password_hash])
            //->andFilterWhere(['ilike', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['ilike', 'email', $this->email])
            // ->andFilterWhere(['DATE(updated_at)' => $this->updated_at])
            ;

        if($this->created_at){
            $dateformatbaru = explode("-", $this->created_at);
            //prin($dateformatbaru, $this->created_at);
            $y = $dateformatbaru[2];
            $m = $dateformatbaru[1];
            $d = $dateformatbaru[0];

            $originalDate = $y."-".$m."-".$d;
            $newDate = date("D, d M Y H:i:s O", strtotime($originalDate));
            //prin(strtotime($originalDate), Yii::$app->formatter->asTimeStamp(strtotime($originalDate)), Yii::$app->formatter->asTimeStamp($originalDate));
            $query->andFilterWhere(['>=', 'created_at', Yii::$app->formatter->asTimeStamp($newDate)]);
        }

        if($this->updated_at){
            $dateformatbaru = explode("-", $this->updated_at);
            //prin($dateformatbaru, $this->created_at);
            $y = $dateformatbaru[2];
            $m = $dateformatbaru[1];
            $d = $dateformatbaru[0];

            $originalDate = $y."-".$m."-".$d;
            $newDate = date("D, d M Y H:i:s O", strtotime($originalDate));
            //prin(strtotime($originalDate), Yii::$app->formatter->asTimeStamp(strtotime($originalDate)), Yii::$app->formatter->asTimeStamp($originalDate));
            $query->andFilterWhere(['>=', 'updated_at', Yii::$app->formatter->asTimeStamp($newDate)]);
        }

        return $dataProvider;
    }
}
