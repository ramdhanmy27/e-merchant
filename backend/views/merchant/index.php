<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MerchantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Merchants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="merchant-index">
    <?php if(!$this->request->isAjax) { ?>
        <?php if($this->can("create")){ ?>
            <p> <?= Html::a('Registrasi Merchant', ['create'], ['class' => 'btn btn-success']) ?> </p>
        <?php } ?>
    <?php } ?>
    
    <div class="forscrollhor">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'code_merchant',
                'name',
                // 'address',
                'contact_name',
                'phone',
                'email:email',
                'website',
                // 'description',
                // 'bank',
                // 'bank_acc_no',
                // 'bank_acc_name',
                
                [
                	'class' => 'yii\grid\ActionColumn',
                	'template' => '{view} {update} {delete} {send}',
                	'buttons' => [
                		"send"=>function($url,$model,$index){
                			$url=Url::toRoute(["/merchant/mail", "id"=>$model->attributes["code_merchant"]], true);
                			return Html::a("<span class='glyphicon glyphicon-envelope'></span>",$url,['data'=>[
            					'confirm'=>'Are you sure you want to sent invitation to this merchant?'
            				]]);
                		}
                	]
                ],
            ],
        ]); ?>
    </div>
</div>
