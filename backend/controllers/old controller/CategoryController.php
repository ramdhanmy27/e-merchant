<?php namespace backend\controllers;

use Yii;
use common\models\Category;
use common\models\CategorySearch;
use common\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \common\yii\db\db;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller {
    use \common\yii\web\CrudControllerTrait;

    public static $permissions = ["create", "update", "view"];

    public function behaviors()
    {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
            ]),
            "access" => $this->access([
                [['index'], "view"],
                [['index', 'create'], "create"],
                [['index', 'update'], "update"],
            ]),
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex() {
        $model = new Category();
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                $this->session->setFlash("success","Successfully add category !");
                return $this->redirect(["index"]);
        }

        return $this->render('index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Category();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render("create", [
            "model" => $model
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->session->setFlash("success", "Successfully edit category !");
            return $this->redirect(["index"]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeletebaru($id){
        Category::findOne($id)->delete();
        $this->session->setFlash("success", "Successfully delete category !");
        return $this->redirect(["index"]);
    }

}
