<?php namespace backend\api;

interface ApiInterface {
	public function action($get, $post = []);

	/*Products*/
	public function apiGetProducts();
	public function apiCreateProducts($data);
	public function apiUpdateProducts();
	public function apiDeleteProducts();

	public function createProducts();
	public function updateProducts();
	public function deleteProducts();

	/*Orders*/
	public function apiGetOrders();
	public function apiSetOrderStatus();
}