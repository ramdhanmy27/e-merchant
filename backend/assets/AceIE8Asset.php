<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Configuration for Ace Admin Extra client script files
 */
class AceIE8Asset extends AssetBundle
{
    public $sourcePath = '@webroot/ace/dist';
    public $js = [
        'js/html5shiv.min.js',
        'js/respond.min.js',
    ];
    public $jsOptions = ['condition' => 'lte IE 8'];
}
