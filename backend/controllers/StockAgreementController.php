<?php namespace backend\controllers;

use common\yii\db\db;
use common\models\StockAgreement;
use common\models\StockAgreementSearch;

class StockAgreementController extends \common\yii\web\Controller
{
	use \common\yii\web\CrudControllerTrait {
		actionUpdate as private;
	}

	public static $permissions = ["create", "draft", "delete", "view"];

	public function behaviors() {
		$this->breadcrumb = [
			"index" => [
				"label" => $this->name(),
				"child" => [
					"view" => [
						"label" => "[[get:id]]",
						"url" => ["view", "id"=>"[[get:id]]"],
						"child" => [
							"draft" => ["label" => "Draft"],
						]
					],
					"create" => ["label" => "Create"],
				]
			]
		];

        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
                'save' => ['post'],
            ]),
            "access" => $this->access([
                [['index', 'view'], "view"],
                [['create'], "create"],
                [['draft'], "draft"],
                [['delete'], "delete"],
            ]),
        ];
    }

    public function actionIndex() {
        $model = new StockAgreementSearch();

		$param_list = $param_draft = $this->request->queryParams;
		$param_list["StockAgreementSearch"]["status"] = StockAgreement::STATUS_SAVE;
		$param_draft["StockAgreementSearch"]["status"] = StockAgreement::STATUS_DRAFT;

		// prin($param_draft);
    	return view([
    		"list" => [
    			"dataProvider" => $model->search($param_list),
    			"filterModel" => $model,
    		],
    		"draft" => [
    			"dataProvider" => $model->search($param_draft),
    			"filterModel" => $model,
    		],
    	]);
    }

	/**
	 * create data master
	 * redirect ke halaman draft dengan data master yang baru dibuat
	 * @return response
	 */
	public function actionCreate() {
		$contract = StockAgreement::getCode();
		
		db::insert("stock_agreement", [
			"number_contract" => $contract, 
			"status" => StockAgreement::STATUS_DRAFT,
		])->execute();

		return $this->redirect(["draft", "id" => $contract]);
	}

	/**
	 * halaman draft
	 * @param  string $id master ID
	 * @return view
	 */
	public function actionDraft($id) {
		$model = $this->findModel($id);

		if (!$model->isDraft()) {
			$this->abort();
			return;
		}

		return $this->render([
			"model" => $model
		]);
	}

	/**
	 * delete master
	 * @param  string $id master ID
	 * @return response
	 */
	public function actionSave($id) {
		db::update("stock_agreement", ["status" => "1"], "number_contract=:id", ["id" => $id])->execute();
		return redirect(["index"]);
	}

	/**
	 * delete master
	 * @param  string $id master ID
	 * @return response
	 */
	public function actionDelete($id) {
		db::delete("agreement_item", "number_contract=:id", ["id" => $id])->execute();
		db::delete("stock_agreement", "number_contract=:id", ["id" => $id])->execute();

		return redirect(["index"]);
	}

	/*Service Provider*/

	/**
	 * POST - update contract information
	 * @param  string $id master ID
	 */
	public function actionUpdateContract($id) {
		$post = $this->request->post();
		$date = preg_split("/\s*-\s*/", $post["date"]);
		$format = "m/d/Y";

		db::update("stock_agreement", [
			"code_merchant" => trim($post["merchant"])=="" ? null : $post["merchant"],
			"description" => trim($post["description"])=="" ? null : $post["description"],
			"contract_date" => isset($date[0]) && ($datetime = \DateTime::createFromFormat($format, $date[0])) ? $datetime->format("Y-m-d") : null,
			"until_date" => isset($date[1]) && ($datetime = \DateTime::createFromFormat($format, $date[1])) ? $datetime->format("Y-m-d") : null,
		], "number_contract=:id", ["id" => $id])->execute();
	}

	/**
	 * POST - delete product item 
	 * @param  string $id master ID
	 */
	public function actionDeleteItem($id) {
		db::delete("agreement_item", "number_contract=:id and number_product=:product", [
			"id" => $id, "product" => $this->request->post("product")
		])->execute();
	}

	/**
	 * POST - save quantity di setiap product
	 * @param  string $id   master ID 
	 * @param  string $type commit|ready
	 */
	public function actionSaveQty($id, $type) {
		$post = $this->request->post();
		$field_type = [
			"commit" => "commited_qty",
			"ready" => "ready_qty",
		];

		// invalid type
		if (!isset($field_type[$type])) {
			$this->abort(400);
			return;
		}

		// ambil data variasi qty untuk dibandingkan nanti
		$query = db::query("agreement_item", [
					"code_size", 
					"number_product_color", 
					"commited_qty as commit", 
					"ready_qty as ready", 
				])
				->where(["number_contract" => $id, "number_product" => $post['id']]);
		$data = [];

		foreach ($query->each() as $val) 
			$data[$val["code_size"]][$val["number_product_color"]] = [
				"commit" => $val["commit"],
				"ready" => $val["ready"],
			];

		// membandingkan data yang dikirim dan yang ada pada database
		// dan menentukan aksi yang akan dilakukan pada data lama
		$insert = $update = $delete = [];

		foreach ($post["qty"] as $size => $item) {
			foreach ($item as $color => $newval) {
				$oldval_exists = isset($data[$size][$color]);
				$oldval = $oldval_exists ? $data[$size][$color][$type] : null;
				$newval_exists = trim($newval)!="";

				// data yang dimasukkan bukan angka
				// atau tidak ada perbedaan data dengan yang ada pada database
				if (($newval_exists && !is_numeric($newval)) || ($oldval_exists && $newval==$oldval))
					continue;

				if ($oldval_exists) {
					// delete data quantity
					if (!$newval_exists) {
						$will_delete = true;

						foreach ($data[$size][$color] as $itype => $val) {
							if ($type == $itype)
								continue;

							// set null
							// masih ada data di salah satu kolom (commit|ready)
							if ($val != null) {
								$update[] = [$id, $post["id"], $size, $color, null];
								$will_delete = false;
								break;
							}
						}

						if ($will_delete)
							$delete[] = [$id, $post["id"], $size, $color];
					}
					// update quantity
					else if ($newval!=$data[$size][$color])
						$update[] = [$id, $post["id"], $size, $color, $newval];
				}
				else if ($newval_exists) {
					// buat data baru
					$insert[] = [$id, $post["id"], $size, $color, $newval];
				}
			}
		}

		$field = [
			"number_contract", 
			"number_product", 
			"code_size", 
			"number_product_color", 
			$field_type[$type],
		];

		if (count($insert) > 0)
			db::batchInsert("agreement_item", $field, $insert)->execute();
		
		if (count($update) > 0)
			db::batchUpdate(
				"agreement_item i", [$field_type[$type]."=t.".$field_type[$type]."::int"], $field, $update, 
				"where t.number_contract=i.number_contract 
					and t.number_product=i.number_product
					and t.code_size::char=i.code_size::char
					and t.number_product_color=i.number_product_color"
			)->execute();

		if (count($delete) > 0) {
			$bind = $condition = [];

			foreach ($delete as $i => $val) {
				$bind["n$i"] = $val[0];
				$bind["p$i"] = $val[1];
				$bind["s$i"] = $val[2];
				$bind["c$i"] = $val[3];
				$condition[] = "(number_contract=:n$i and number_product=:p$i and code_size=:s$i and number_product_color=:c$i)";
			}

			db::delete("agreement_item", implode(" or ", $condition), $bind)->execute();
		}
	}

	/*Data Provider*/

	/**
	 * ambil data pertama kali saat halaman draft di load
	 * @param  string $id master ID
	 * @param  string $product product ID
	 * @return json
	 */
	public function actionGetDraftData($id, $product = null) {
		$bind = ["id" => $id];

		if ($product!=null)
			$bind["product"] = $product;

		$query = db::run(
			"SELECT p.number_product, c.number_product_color, c.color, s.code_size, s.name, 
				i.commited_qty, i.ready_qty from stock_agreement sa 
			left join (
				select distinct i.number_contract, p.number_product, p.group_size from product p
				inner join agreement_item i on p.number_product=i.number_product
			) as p on sa.number_contract=p.number_contract
			right join product_color c on c.number_product=p.number_product
			right join product_size s on s.group_size=p.group_size
			left join agreement_item i on i.number_contract=sa.number_contract
				and c.number_product_color=i.number_product_color
				and s.code_size=i.code_size
			where sa.number_contract=:id ".($product==null ? "" : "and p.number_product=:product")
			." order by p.number_product", $bind
		)->query();
		$data = [];

		foreach ($query as $val) {
			$data[$val["number_product"]]["id"] = $val["number_product"];
			$data[$val["number_product"]]["size"][$val["code_size"]] = $val["name"];
			$data[$val["number_product"]]["attribute"][$val["number_product_color"]] = $val["color"];
			$data[$val["number_product"]]["qty"]["commit"][$val["code_size"]][$val["number_product_color"]] = $val["commited_qty"];
			$data[$val["number_product"]]["qty"]["ready"][$val["code_size"]][$val["number_product_color"]] = $val["ready_qty"];
		}

		return $this->request->isAjax || $this->controller->action->id=="get-draft-data" ? json($data) : $data;
	}

	/**
	 * load variasi product
	 * @param  string $id product ID
	 * @return json
	 */
	public function actionGetProductData($id) {
		$query = db::run(
			"SELECT c.number_product_color, c.color, s.code_size, s.name from product p
			left join product_size s on s.group_size=p.group_size
			left join product_color c on c.number_product=p.number_product
			where p.number_product=:id order by s.code_size, c.color",
			["id" => $id]
		)->query();
		$data = [];

		foreach ($query as $val) {
			$data["id"] = $id;
			$data["size"][$val["code_size"]] = $val["name"];
			$data["attribute"][$val["number_product_color"]] = $val["color"];
			$data["qty"]["commit"][$val["code_size"]][$val["number_product_color"]] = null;
			$data["qty"]["ready"][$val["code_size"]][$val["number_product_color"]] = null;
		}

		return $this->request->isAjax || $this->controller->action->id=="get-product-data" ? json($data) : $data;
	}
}
