<?php namespace common\assets;

use yii\web\AssetBundle;

class FontAwesomeAsset extends AssetBundle
{
	public $sourcePath = '@bower'; 
    public $css = [
        "fontawesome/css/font-awesome.min.css",
    ];
}